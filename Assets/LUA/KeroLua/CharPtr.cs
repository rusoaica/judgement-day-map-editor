﻿using System;
using System.Text;

namespace KeraLua
{
    public struct CharPtr
    {
        private IntPtr _str;

        public CharPtr(IntPtr _ptr_string) : this()
        {
            _str = _ptr_string;
        }

        static public implicit operator CharPtr(IntPtr _ptr)
        {
            return new CharPtr(_ptr);
        }

        public static string StringFromNativeUtf8(IntPtr _native_utf8, int _len = 0)
        {
            if (_len == 0)
                while (System.Runtime.InteropServices.Marshal.ReadByte(_native_utf8, _len) != 0)
                    ++_len;
            if (_len == 0)
                return string.Empty;
            byte[] buffer = new byte[_len];
            System.Runtime.InteropServices.Marshal.Copy(_native_utf8, buffer, 0, buffer.Length);
            return Encoding.UTF8.GetString(buffer, 0, _len);
        }

        static private string PointerToString(IntPtr _ptr)
        {
#if WSTRING
			return StringFromNativeUtf8 (_ptr);
#else
            return System.Runtime.InteropServices.Marshal.PtrToStringAnsi(_ptr);
#endif
        }

        static private string PointerToString(IntPtr _ptr, int _length)
        {
#if WSTRING
			return StringFromNativeUtf8 (_ptr, _length);
#else
            return System.Runtime.InteropServices.Marshal.PtrToStringAnsi(_ptr, _length);
#endif
        }

        static private byte[] PointerToBuffer(IntPtr _ptr, int _length)
        {
            byte[] _buffer = new byte[_length];
            System.Runtime.InteropServices.Marshal.Copy(_ptr, _buffer, 0, _length);
            return _buffer;
        }

        public override string ToString()
        {
            if (_str == IntPtr.Zero)
                return "";
            return PointerToString(_str);
        }

        public string ToString(int _length)
        {
            if (_str == IntPtr.Zero)
                return "";
            byte[] _buffer = PointerToBuffer(_str, _length);
            if (_length > 3 && _buffer[0] == 0x1B && _buffer[1] == 0x4C && _buffer[2] == 0x75 && _buffer[3] == 0x61)
            {
                StringBuilder _string = new StringBuilder(_length);
                foreach (byte _byte in _buffer)
                    _string.Append((char)_byte);
                return _string.ToString();
            }
            else
#if WSTRING
				return Encoding.UTF8.GetString(_buffer);
#else
                return PointerToString(_str, _length);
#endif
        }
    }
}
