﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace KeraLua
{
    public static class DynamicLibraryPath
    {
        [DllImport("kernel32", SetLastError = true)]
        static extern bool SetDllDirectory(string lpPathName);
        const string LD_LIBRARY_PATH = "LD_LIBRARY_PATH";

        static string GetAssemblyPath()
        {
            string _code_base = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder _uri = new UriBuilder(_code_base);
            string _path = Uri.UnescapeDataString(_uri.Path);
            return Path.GetDirectoryName(_path);
        }

        static public void RegisterPathForDll(string _name)
        {
            string _extension = GetDllExtension();
            if (string.IsNullOrEmpty(_extension))
                return;
            string _dll_mame = _name + _extension;
            string _assembly_path = GetAssemblyPath();
            string _path = Path.Combine(_assembly_path, _dll_mame);
            if (File.Exists(_path))
                return;
            if (IntPtr.Size == 8)
                Register64bitPath(_assembly_path, _dll_mame);
            else
                Register32bitPath(_assembly_path, _dll_mame);
        }

        static void Register64bitPath(string _assembly_path, string _dll_name)
        {
            string _x64_path = Path.Combine(_assembly_path, "x64");
            if (Directory.Exists(_x64_path) && File.Exists(Path.Combine(_x64_path, _dll_name)))
            {
                RegisterLibrarySearchPath(_x64_path);
                return;
            }
            _x64_path = Path.Combine(_assembly_path, "amd64");
            if (Directory.Exists(_x64_path) && File.Exists(Path.Combine(_x64_path, _dll_name)))
                RegisterLibrarySearchPath(_x64_path);
        }

        static void Register32bitPath(string _assembly_path, string _dll_name)
        {
            string _x86_path = Path.Combine(_assembly_path, "x86");
            if (Directory.Exists(_x86_path) && File.Exists(Path.Combine(_x86_path, _dll_name)))
            {
                RegisterLibrarySearchPath(_x86_path);
                return;
            }
            _x86_path = Path.Combine(_assembly_path, "i386");
            if (Directory.Exists(_x86_path) && File.Exists(Path.Combine(_x86_path, _dll_name)))
                RegisterLibrarySearchPath(_x86_path);
        }

        static string GetDllExtension()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                    return ".dll";
                case PlatformID.Unix:
                    return ".so";
                case PlatformID.MacOSX:
                    return ".dylib";
            }
            return null;
        }

        static void RegisterLibrarySearchPath(string _path)
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                    SetDllDirectory(_path);
                    break;
                case PlatformID.Unix:
                case PlatformID.MacOSX:
                    string _current_ld_library_path = Environment.GetEnvironmentVariable(LD_LIBRARY_PATH) ?? string.Empty;
                    string _new_ld_library_path = string.IsNullOrEmpty(_current_ld_library_path) ? _path : _current_ld_library_path + Path.PathSeparator + _path;
                    Environment.SetEnvironmentVariable(LD_LIBRARY_PATH, _new_ld_library_path);
                    break;
            }
        }
    }
}
