﻿using System;
using System.Runtime.InteropServices;


namespace KeraLua
{
    public delegate int LuaNativeFunction(LuaState _lua_state);

    public static partial class Lua
    {
        public static int LuaGC(IntPtr _lua_state, int _what, int _data)
        {
            return NativeMethods.LuaGC(_lua_state, _what, _data);
        }

        public static CharPtr LuaTypeName(IntPtr _lua_state, int _type)
        {
            return NativeMethods.LuaTypeName(_lua_state, _type);
        }

        public static void LuaLError(IntPtr _lua_state, string _message)
        {
            NativeMethods.LuaLError(_lua_state, _message);
        }

        public static void LuaLWhere(IntPtr _lua_state, int _level)
        {
            NativeMethods.LuaLWhere(_lua_state, _level);
        }

        public static IntPtr LuaLNewState()
        {
            return NativeMethods.LuaLNewState();
        }

        public static void LuaClose(IntPtr _lua_state)
        {
            NativeMethods.LuaClose(_lua_state);
        }

        public static void LuaLOpenLibs(IntPtr _lua_state)
        {
            NativeMethods.LuaLOpenLibs(_lua_state);
        }

        public static int LuaLLoadString(IntPtr _lua_state, string _chunk)
        {
            return NativeMethods.LuaLLoadString(_lua_state, _chunk);
        }

        public static int LuaLLoadString(IntPtr _lua_state, byte[] _chunk)
        {
            return NativeMethods.LuaLLoadString(_lua_state, _chunk);
        }

        public static void LuaCreateTable(IntPtr _lua_state, int _narr, int _nrec)
        {
            NativeMethods.LuaCreateTable(_lua_state, _narr, _nrec);
        }

        public static void LuaGetTable(IntPtr _lua_state, int _index)
        {
            NativeMethods.LuaGetTable(_lua_state, _index);
        }

        public static void LuaSetTop(IntPtr _lua_state, int _new_top)
        {
            NativeMethods.LuaSetTop(_lua_state, _new_top);
        }

        public static void LuaInsert(IntPtr _lua_state, int _new_top)
        {
            NativeMethods.LuaInsert(_lua_state, _new_top);
        }

        public static void LuaRemove(IntPtr _lua_state, int _index)
        {
            NativeMethods.LuaRemove(_lua_state, _index);
        }

        public static void LuaRawGet(IntPtr _lua_state, int _index)
        {
            NativeMethods.LuaRawGet(_lua_state, _index);
        }

        public static void LuaSetTable(IntPtr _lua_state, int _index)
        {
            NativeMethods.LuaSetTable(_lua_state, _index);
        }

        public static void LuaRawSet(IntPtr _lua_state, int _index)
        {
            NativeMethods.LuaRawSet(_lua_state, _index);
        }

        public static void LuaSetMetatable(IntPtr _lua_state, int _obj_index)
        {
            NativeMethods.LuaSetMetatable(_lua_state, _obj_index);
        }

        public static int LuaGetMetatable(IntPtr _lua_state, int _obj_index)
        {
            return NativeMethods.LuaGetMetatable(_lua_state, _obj_index);
        }

        public static int LuaNetEqual(IntPtr _lua_state, int _index_1, int _index_2)
        {
            return NativeMethods.LuaNetEqual(_lua_state, _index_1, _index_2);
        }

        public static void LuaPushValue(IntPtr _lua_state, int _index)
        {
            NativeMethods.LuaPushValue(_lua_state, _index);
        }

        public static void LuaReplace(IntPtr _lua_state, int _index)
        {
            NativeMethods.LuaReplace(_lua_state, _index);
        }

        public static int LuaGetTop(IntPtr _lua_state)
        {
            return NativeMethods.LuaGetTop(_lua_state);
        }

        public static int LuaType(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaType(_lua_state, _index);
        }

        public static int LuaLRef(IntPtr _lua_state, int _registry_index)
        {
            return NativeMethods.LuaLRef(_lua_state, _registry_index);
        }

        public static void LuaRawGetI(IntPtr _lua_state, int _table_index, int _index)
        {
            NativeMethods.LuaRawGetI(_lua_state, _table_index, _index);
        }


        public static void LuaRawSetI(IntPtr _lua_state, int _table_index, int _index)
        {
            NativeMethods.LuaRawSetI(_lua_state, _table_index, _index);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static IntPtr LuaNewUserData(IntPtr _lua_state, uint _size)
        {
            return NativeMethods.LuaNewUserData(_lua_state, _size);
        }

        public static IntPtr LuaToUserData(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaToUserData(_lua_state, _index);
        }

        public static void LuaLUnref(IntPtr _lua_state, int _registry_index, int _reference)
        {
            NativeMethods.LuaLUnref(_lua_state, _registry_index, _reference);
        }

        public static int LuaIsString(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaIsString(_lua_state, _index);
        }

        public static int LuaNetIsStringStrict(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaNetIsStringStrict(_lua_state, _index);
        }

        public static bool LuaIsCFunction(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaIsCFunction(_lua_state, _index) != 0;
        }

        public static void LuaPushNil(IntPtr _lua_state)
        {
            NativeMethods.LuaPushNil(_lua_state);
        }

        public static int LuaNetPCall(IntPtr _lua_state, int _n_args, int _n_results, int _err_func)
        {
            return NativeMethods.LuaNetPCall(_lua_state, _n_args, _n_results, _err_func);
        }

        public static LuaNativeFunction LuaToCFunction(IntPtr _lua_state, int _index)
        {
            IntPtr _ptr = NativeMethods.LuaToCFunction(_lua_state, _index);
            if (_ptr == IntPtr.Zero)
                return null;
            LuaNativeFunction function = Marshal.GetDelegateForFunctionPointer(_ptr, typeof(LuaNativeFunction)) as LuaNativeFunction;
            return function;
        }

        public static double LuaNetToNumber(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaNetToNumber(_lua_state, _index);
        }

        public static int LuaToBoolean(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaToBoolean(_lua_state, _index);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr LuaToLString(IntPtr _lua_state, int _index, out uint _str_len)
        {
            return NativeMethods.LuaToLString(_lua_state, _index, out _str_len);
        }

        public static void LuaAtPanic(IntPtr luaState, LuaNativeFunction _panic_f)
        {
            IntPtr _fn_panic = Marshal.GetFunctionPointerForDelegate(_panic_f);
            NativeMethods.LuaAtPanic(luaState, _fn_panic);
        }

        public static void LuaPushStdCallCFunction(IntPtr _lua_state, LuaNativeFunction _fn)
        {
            IntPtr _p_func = Marshal.GetFunctionPointerForDelegate(_fn);
            NativeMethods.LuaPushStdCallCFunction(_lua_state, _p_func);
        }

        public static void LuaPushNumber(IntPtr _lua_state, double _number)
        {
            NativeMethods.LuaPushNumber(_lua_state, _number);
        }

        public static void LuaPushBoolean(IntPtr _lua_state, int _value)
        {
            NativeMethods.LuaPushBoolean(_lua_state, _value);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static void LuaNetPushLString(IntPtr _lua_state, string _str, uint _size)
        {
            NativeMethods.LuaNetPushLString(_lua_state, _str, _size);
        }

        public static void LuaPushString(IntPtr _lua_state, string _str)
        {
            NativeMethods.LuaPushString(_lua_state, _str);
        }

        public static int LuaLNewMetatable(IntPtr _lua_state, string _meta)
        {
            return NativeMethods.LuaLNewMetatable(_lua_state, _meta);
        }

        public static void LuaGetField(IntPtr _lua_state, int _stack_pos, string _meta)
        {
            NativeMethods.LuaGetField(_lua_state, _stack_pos, _meta);
        }

        public static IntPtr LuaLCheckUData(IntPtr _lua_state, int _stack_pos, string _meta)
        {
            return NativeMethods.LuaLCheckUData(_lua_state, _stack_pos, _meta);
        }

        public static int LuaLGetMetafield(IntPtr _lua_state, int _stack_pos, string _field)
        {
            return NativeMethods.LuaLGetMetafield(_lua_state, _stack_pos, _field);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaNetLoadBuffer(IntPtr _lua_state, string _buff, uint _size, string _name)
        {
            return NativeMethods.LuaNetLoadBuffer(_lua_state, _buff, _size, _name);

        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaNetLoadBuffer(IntPtr _lua_state, byte[] _buff, uint _size, string _name)
        {
            return NativeMethods.LuaNetLoadBuffer(_lua_state, _buff, _size, _name);
        }

        public static int LuaNetLoadFile(IntPtr _lua_state, string _filename)
        {
            return NativeMethods.LuaNetLoadFile(_lua_state, _filename);
        }

        public static void LuaError(IntPtr _lua_state)
        {
            NativeMethods.LuaError(_lua_state);
        }

        public static int LuaCheckStack(IntPtr _lua_state, int _extra)
        {
            return NativeMethods.LuaCheckStack(_lua_state, _extra);
        }

        public static int LuaNext(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaNext(_lua_state, _index);
        }

        public static void LuaPushLightUserData(IntPtr _lua_state, IntPtr _udata)
        {
            NativeMethods.LuaPushLightUserData(_lua_state, _udata);
        }

        public static bool LuaLCheckMetatable(IntPtr _lua_state, int _obj)
        {
            return NativeMethods.LuaLCheckMetatable(_lua_state, _obj) != 0;
        }

        public static int LuaGetHookMask(IntPtr _lua_state)
        {
            return NativeMethods.LuaGetHookMask(_lua_state);
        }

        public static int LuaSetHook(IntPtr _lua_state, LuaHook _func, int _mask, int _count)
        {
            IntPtr _func_hook = Marshal.GetFunctionPointerForDelegate(_func);
            return NativeMethods.LuaSetHook(_lua_state, _func_hook, _mask, _count);
        }

        public static int LuaGetHookCount(IntPtr _lua_state)
        {
            return NativeMethods.LuaGetHookCount(_lua_state);
        }

        public static CharPtr LuaGetLocal(IntPtr _lua_state, LuaDebug _ar, int _n)
        {
            IntPtr _p_debug = Marshal.AllocHGlobal(Marshal.SizeOf(_ar));
            CharPtr _local = IntPtr.Zero;
            try
            {
                Marshal.StructureToPtr(_ar, _p_debug, false);
                _local = NativeMethods.LuaGetLocal(_lua_state, _p_debug, _n);
            }
            finally
            {
                Marshal.FreeHGlobal(_p_debug);
            }
            return _local;
        }

        public static CharPtr LuaSetLocal(IntPtr _lua_state, LuaDebug _ar, int _n)
        {
            IntPtr _p_debug = Marshal.AllocHGlobal(Marshal.SizeOf(_ar));
            CharPtr _local = IntPtr.Zero;
            try
            {
                Marshal.StructureToPtr(_ar, _p_debug, false);
                _local = NativeMethods.LuaSetLocal(_lua_state, _p_debug, _n);
            }
            finally
            {
                Marshal.FreeHGlobal(_p_debug);
            }
            return _local;
        }

        public static int LuaGetInfo(IntPtr _lua_state, string _what, ref LuaDebug _ar)
        {
            IntPtr _p_debug = Marshal.AllocHGlobal(Marshal.SizeOf(_ar));
            int _ret = 0;

            try
            {
                Marshal.StructureToPtr(_ar, _p_debug, false);
                _ret = NativeMethods.LuaGetInfo(_lua_state, _what, _p_debug);
                _ar = (LuaDebug)Marshal.PtrToStructure(_p_debug, typeof(LuaDebug));
            }
            finally
            {
                Marshal.FreeHGlobal(_p_debug);
            }
            return _ret;
        }

        public static int LuaGetStack(IntPtr _lua_state, int _level, ref LuaDebug _ar)
        {
            IntPtr _p_debug = Marshal.AllocHGlobal(Marshal.SizeOf(_ar));
            int _ret = 0;
            try
            {
                Marshal.StructureToPtr(_ar, _p_debug, false);
                _ret = NativeMethods.LuaGetStack(_lua_state, _level, _p_debug);
                _ar = (LuaDebug)Marshal.PtrToStructure(_p_debug, typeof(LuaDebug));
            }
            finally
            {
                Marshal.FreeHGlobal(_p_debug);
            }
            return _ret;
        }

        public static CharPtr LuaGetUpValue(IntPtr _lua_state, int _func_index, int _n)
        {
            return NativeMethods.LuaGetUpValue(_lua_state, _func_index, _n);
        }

        public static CharPtr LuaSetUpValue(IntPtr _lua_state, int _func_index, int _n)
        {
            return NativeMethods.LuaSetUpValue(_lua_state, _func_index, _n);
        }

        public static int LuaNetToNetObject(IntPtr _lua_state, int _index)
        {
            return NativeMethods.LuaNetToNetObject(_lua_state, _index);
        }

        public static int LuaNetRegistryIndex()
        {
            return NativeMethods.LuaNetRegistryIndex();
        }

        public static void LuaNetNewUData(IntPtr _lua_state, int _val)
        {
            NativeMethods.LuaNetNewUData(_lua_state, _val);
        }

        public static int LuaNetRawNetObj(IntPtr _lua_state, int _obj)
        {
            return NativeMethods.LuaNetRawNetObj(_lua_state, _obj);
        }

        public static int LuaNetCheckUData(IntPtr _lua_state, int _ud, string _t_name)
        {
            return NativeMethods.LuaNetCheckUData(_lua_state, _ud, _t_name);
        }

        public static IntPtr LuaNetGetTag()
        {
            return NativeMethods.LuaNetGetTag();
        }

        public static void LuaNetPushGlobalTable(IntPtr _lua_state)
        {
            NativeMethods.LuaNetPushGlobalTable(_lua_state);
        }

        public static void LuaNetPopGlobalTable(IntPtr _lua_state)
        {
            NativeMethods.LuaNetPopGlobalTable(_lua_state);
        }

        public static void LuaNetSetGlobal(IntPtr _lua_state, string _name)
        {
            NativeMethods.LuaNetSetGlobal(_lua_state, _name);
        }

        public static void LuaNetGetGlobal(IntPtr _lua_state, string _name)
        {
            NativeMethods.LuaNetGetGlobal(_lua_state, _name);
        }

        public static IntPtr LuaNetGetMainState(IntPtr _lua_state)
        {
            return NativeMethods.LuaNetGetMainState(_lua_state);
        }
    }
}