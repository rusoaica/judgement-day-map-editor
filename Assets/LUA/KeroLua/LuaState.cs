﻿using System;

namespace KeraLua
{
    public struct LuaState
    {
        IntPtr state;

        public LuaState(IntPtr _ptr_state) : this()
        {
            state = _ptr_state;
        }

        static public implicit operator LuaState(IntPtr _ptr)
        {
            return new LuaState(_ptr);
        }

        static public implicit operator IntPtr(LuaState _lua_state)
        {
            return _lua_state.state;
        }
    }
}