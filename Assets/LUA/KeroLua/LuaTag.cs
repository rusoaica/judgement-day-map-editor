﻿using System;

namespace KeraLua
{
    public struct LuaTag
    {
        public IntPtr Tag { get; set; }

        public LuaTag(IntPtr _tag) : this()
        {
            Tag = _tag;
        }

        static public implicit operator LuaTag(IntPtr _ptr)
        {
            return new LuaTag(_ptr);
        }
    }
}
