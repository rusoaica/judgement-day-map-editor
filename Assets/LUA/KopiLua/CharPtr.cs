﻿using System;
using System.Diagnostics;

namespace KopiLua
{
    public class CharPtr
    {
        public char[] chars;
        public int index;
        public static bool operator ==(CharPtr ptr, char ch) { return ptr[0] == ch; }
        public static bool operator ==(char ch, CharPtr ptr) { return ptr[0] == ch; }
        public static bool operator !=(CharPtr ptr, char ch) { return ptr[0] != ch; }
        public static bool operator !=(char ch, CharPtr ptr) { return ptr[0] != ch; }
        public static implicit operator CharPtr(string str) { return new CharPtr(str); }
        public static implicit operator CharPtr(char[] chars) { return new CharPtr(chars); }
        public static implicit operator CharPtr(byte[] bytes) { return new CharPtr(bytes); }
        public static CharPtr operator +(CharPtr ptr, int offset) { return new CharPtr(ptr.chars, ptr.index + offset); }
        public static CharPtr operator -(CharPtr ptr, int offset) { return new CharPtr(ptr.chars, ptr.index - offset); }
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr operator +(CharPtr ptr, uint offset) { return new CharPtr(ptr.chars, ptr.index + (int)offset); }
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr operator -(CharPtr ptr, uint offset) { return new CharPtr(ptr.chars, ptr.index - (int)offset); }

        public char this[int _offset]
        {
            get { return chars[index + _offset]; }
            set { chars[index + _offset] = value; }
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public char this[uint _offset]
        {
            get { return chars[index + _offset]; }
            set { chars[index + _offset] = value; }
        }

        public char this[long _offset]
        {
            get { return chars[index + (int)_offset]; }
            set { chars[index + (int)_offset] = value; }
        }

        public CharPtr()
        {
            this.chars = null;
            this.index = 0;
        }

        public CharPtr(string _str)
        {
            this.chars = (_str + '\0').ToCharArray();
            this.index = 0;
        }

        public CharPtr(CharPtr _ptr)
        {
            this.chars = _ptr.chars;
            this.index = _ptr.index;
        }

        public CharPtr(CharPtr _ptr, int _index)
        {
            this.chars = _ptr.chars;
            this.index = _index;
        }

        public CharPtr(char[] _chars)
        {
            this.chars = _chars;
            this.index = 0;
        }

        public CharPtr(char[] _chars, int _index)
        {
            this.chars = _chars;
            this.index = _index;
        }

        public CharPtr(byte[] _bytes)
        {
            this.chars = new char[_bytes.Length];
            for (int i = 0; i < _bytes.Length; i++)
                this.chars[i] = (char)_bytes[i];
            this.index = 0;
        }

        public CharPtr(IntPtr _ptr)
        {
            this.chars = new char[0];
            this.index = 0;
        }

        public void inc() { this.index++; }
        public void dec() { this.index--; }
        public CharPtr next() { return new CharPtr(this.chars, this.index + 1); }
        public CharPtr prev() { return new CharPtr(this.chars, this.index - 1); }
        public CharPtr add(int _ofs) { return new CharPtr(this.chars, this.index + _ofs); }
        public CharPtr sub(int _ofs) { return new CharPtr(this.chars, this.index - _ofs); }

        public static CharPtr operator +(CharPtr _ptr_1, CharPtr _ptr_2)
        {
            string result = "";
            for (int i = 0; _ptr_1[i] != '\0'; i++)
                result += _ptr_1[i];
            for (int i = 0; _ptr_2[i] != '\0'; i++)
                result += _ptr_2[i];
            return new CharPtr(result);
        }

        public static int operator -(CharPtr _ptr1, CharPtr _ptr2)
        {
            Debug.Assert(_ptr1.chars == _ptr2.chars); return _ptr1.index - _ptr2.index;
        }

        public static bool operator <(CharPtr _ptr1, CharPtr _ptr2)
        {
            Debug.Assert(_ptr1.chars == _ptr2.chars); return _ptr1.index < _ptr2.index;
        }

        public static bool operator <=(CharPtr _ptr1, CharPtr _ptr2)
        {
            Debug.Assert(_ptr1.chars == _ptr2.chars); return _ptr1.index <= _ptr2.index;
        }

        public static bool operator >(CharPtr _ptr1, CharPtr _ptr2)
        {
            Debug.Assert(_ptr1.chars == _ptr2.chars); return _ptr1.index > _ptr2.index;
        }

        public static bool operator >=(CharPtr _ptr1, CharPtr _ptr2)
        {
            Debug.Assert(_ptr1.chars == _ptr2.chars); return _ptr1.index >= _ptr2.index;
        }

        public static bool operator ==(CharPtr _ptr1, CharPtr _ptr2)
        {
            object _o1 = _ptr1 as CharPtr;
            object _o2 = _ptr2 as CharPtr;
            if ((_o1 == null) && (_o2 == null)) return true;
            if (_o1 == null) return false;
            if (_o2 == null) return false;
            return (_ptr1.chars == _ptr2.chars) && (_ptr1.index == _ptr2.index);
        }

        public static bool operator !=(CharPtr _ptr1, CharPtr _ptr2) { return !(_ptr1 == _ptr2); }

        public override bool Equals(object _o)
        {
            return this == (_o as CharPtr);
        }

        public override int GetHashCode()
        {
            return 0;
        }
        public override string ToString()
        {
            System.Text.StringBuilder _result = new System.Text.StringBuilder();
            for (int i = index; (i < chars.Length) && (chars[i] != '\0'); i++)
                _result.Append(chars[i]);
            return _result.ToString();
        }

        public string ToString(int _length)
        {
            System.Text.StringBuilder _result = new System.Text.StringBuilder();
            for (int i = index; (i < chars.Length) && i < _length; i++)
                _result.Append(chars[i]);
            return _result.ToString();
        }
    }
}