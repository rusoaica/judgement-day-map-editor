﻿using System;

namespace KopiLua
{
    public class LuaDebug
    {
        public int event_;
        public CharPtr name;
        public CharPtr nameWhat;
        public CharPtr what;
        public CharPtr source;
        public int currentLine;
        public int nups;
        public int lineDefined;
        public int lastLineDefined;
        public CharPtr shortSrc = new char[Lua.LUA_IDSIZE];
        public int iCi;
        public string shortsrc
        {
            get { return shortSrc.ToString(); }
        }
    };
}