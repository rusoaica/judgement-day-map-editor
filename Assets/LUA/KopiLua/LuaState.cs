﻿using System;

namespace KopiLua
{
    using lu_byte = System.Byte;
    using lu_int32 = System.Int32;
    using lu_mem = System.UInt32;
    using TValue = Lua.LuaTypeValue;
    using StkId = Lua.LuaTypeValue;
    using ptrdiff_t = System.Int32;
    using Instruction = System.UInt32;

    public class LuaState : Lua.GCObject
    {
        public lu_byte status;
        public StkId top;
        public StkId base_;
        public Lua.GlobalState lG;
        public Lua.CallInfo ci;
        public InstructionPtr saveDpc = new InstructionPtr();
        public StkId stackLast;
        public StkId[] stack;
        public Lua.CallInfo endCi;
        public Lua.CallInfo[] baseCi;
        public int stackSize;
        public int sizeCi;
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public ushort nCcalls;
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public ushort baseCCalls;
        public lu_byte hookMask;
        public lu_byte allowHook;
        public int baseHookCount;
        public int hookCount;
        public LuaHook hook;
        public TValue lGt = new Lua.LuaTypeValue();
        public TValue env = new Lua.LuaTypeValue();
        public Lua.GCObject openUpVal;
        public Lua.GCObject gcList;
        public Lua.LuaLongJmp errorJmp;
        public ptrdiff_t errFunc;
    }
}