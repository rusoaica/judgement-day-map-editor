﻿using System;

namespace KopiLua
{
    public struct LuaTag
    {
        public LuaTag(object _tag) : this()
        {
            this.Tag = _tag;
        }

        public object Tag { get; set; }
    }
}