﻿using System;

namespace KopiLua
{
    using lu_mem = System.UInt32;
    using lua_Integer = System.Int32;
    using lua_Number = System.Double;
    using ptrdiff_t = System.Int32;
    using StkId = Lua.LuaTypeValue;
    using TValue = Lua.LuaTypeValue;
    using ZIO = Lua.Zio;

    public partial class Lua
    {
        public const string LuaIdent =
          "$Lua: " + LUA_RELEASE + " " + LUA_COPYRIGHT + " $\n" +
          "$Authors: " + LUA_AUTHORS + " $\n" +
          "$URL: www.lua.org $\n";

        public static void CheckNElements(LuaState _l, int _n)
        {
            ApiCheck(_l, _n <= _l.top - _l.base_);
        }

        public static void CheckValidIndex(LuaState _l, StkId _i)
        {
            ApiCheck(_l, _i != LuaONilObject);
        }

        public static void IncrementTop(LuaState _l)
        {
            ApiCheck(_l, _l.top < _l.ci.top);
            StkId.Inc(ref _l.top);
        }

        static TValue Index2Address(LuaState _l, int _idx)
        {
            if (_idx > 0)
            {
                TValue _o = _l.base_ + (_idx - 1);
                ApiCheck(_l, _idx <= _l.ci.top - _l.base_);
                if (_o >= _l.top) return LuaONilObject;
                else return _o;
            }
            else if (_idx > LUA_REGISTRYINDEX)
            {
                ApiCheck(_l, _idx != 0 && -_idx <= _l.top - _l.base_);
                return _l.top + _idx;
            }
            else switch (_idx)
                {
                    case LUA_REGISTRYINDEX: return Registry(_l);
                    case LUA_ENVIRONINDEX:
                        {
                            Closure _func = CurrFunc(_l);
                            SetHValue(_l, _l.env, _func.c.env);
                            return _l.env;
                        }
                    case LUA_GLOBALSINDEX: return Gt(_l);
                    default:
                        {
                            Closure _func = CurrFunc(_l);
                            _idx = LUA_GLOBALSINDEX - _idx;
                            return (_idx <= _func.c.nupvalues) ? _func.c.upvalue[_idx - 1] : (TValue)LuaONilObject;
                        }
                }
        }

        private static Table GetCurrentEnv(LuaState _l)
        {
            if (_l.ci == _l.baseCi[0]) 
                return HValue(Gt(_l));  
            else
            {
                Closure _func = CurrFunc(_l);
                return _func.c.env;
            }
        }

        public static void LuaAPushObject(LuaState _l, TValue _o)
        {
            SetObj2S(_l, _l.top, _o);
            IncrementTop(_l);
        }

        public static int LuaCheckStack(LuaState _l, int _size)
        {
            int _res = 1;
            LuaLock(_l);
            if (_size > LUAI_MAXCSTACK || (_l.top - _l.base_ + _size) > LUAI_MAXCSTACK)
                _res = 0;  /* stack overflow */
            else if (_size > 0)
            {
                LuaDCheckStack(_l, _size);
                if (_l.ci.top < _l.top + _size)
                    _l.ci.top = _l.top + _size;
            }
            LuaUnlock(_l);
            return _res;
        }

        public static void LuaXMove(LuaState _from, LuaState _to, int _n)
        {
            int _i;
            if (_from == _to) return;
            LuaLock(_to);
            CheckNElements(_from, _n);
            ApiCheck(_from, G(_from) == G(_to));
            ApiCheck(_from, _to.ci.top - _to.top >= _n);
            _from.top -= _n;
            for (_i = 0; _i < _n; _i++)
                SetObj2S(_to, StkId.Inc(ref _to.top), _from.top + _i);
            LuaUnlock(_to);
        }

        public static void LuaSetLevel(LuaState _from, LuaState _to)
        {
            _to.nCcalls = _from.nCcalls;
        }

        public static LuaNativeFunction LuaAtPanic(LuaState _l, LuaNativeFunction _panic_f)
        {
            LuaNativeFunction _old;
            LuaLock(_l);
            _old = G(_l).panic;
            G(_l).panic = _panic_f;
            LuaUnlock(_l);
            return _old;
        }

        public static LuaState LuaNewThread(LuaState _l)
        {
            LuaState L1;
            LuaLock(_l);
            LuaCCheckGC(_l);
            L1 = luaE_newthread(_l);
            SetTTHValue(_l, _l.top, L1);
            IncrementTop(_l);
            LuaUnlock(_l);
            LuaIUserStateThread(_l, L1);
            return L1;
        }

        public static int LuaGetTop(LuaState _l)
        {
            return CastInt(_l.top - _l.base_);
        }

        public static void LuaSetTop(LuaState _l, int _idx)
        {
            LuaLock(_l);
            if (_idx >= 0)
            {
                ApiCheck(_l, _idx <= _l.stackLast - _l.base_);
                while (_l.top < _l.base_ + _idx)
                    SetNilValue(StkId.Inc(ref _l.top));
                _l.top = _l.base_ + _idx;
            }
            else
            {
                ApiCheck(_l, -(_idx + 1) <= (_l.top - _l.base_));
                _l.top += _idx + 1;  
            }
            LuaUnlock(_l);
        }

        public static void LuaRemove(LuaState _l, int _idx)
        {
            StkId _p;
            LuaLock(_l);
            _p = Index2Address(_l, _idx);
            CheckValidIndex(_l, _p);
            while ((_p = _p[1]) < _l.top) SetObj2S(_l, _p - 1, _p);
            StkId.Dec(ref _l.top);
            LuaUnlock(_l);
        }

        public static void LuaInsert(LuaState _l, int _idx)
        {
            StkId _p;
            StkId _q;
            LuaLock(_l);
            _p = Index2Address(_l, _idx);
            CheckValidIndex(_l, _p);
            for (_q = _l.top; _q > _p; StkId.Dec(ref _q)) SetObj2S(_l, _q, _q - 1);
            SetObj2S(_l, _p, _l.top);
            LuaUnlock(_l);
        }

        public static void LuaReplace(LuaState _l, int _idx)
        {
            StkId _o;
            LuaLock(_l);
            if (_idx == LUA_ENVIRONINDEX && _l.ci == _l.baseCi[0])
                LuaGRunError(_l, "no calling environment");
            CheckNElements(_l, 1);
            _o = Index2Address(_l, _idx);
            CheckValidIndex(_l, _o);
            if (_idx == LUA_ENVIRONINDEX)
            {
                Closure _func = CurrFunc(_l);
                ApiCheck(_l, TTIsTable(_l.top - 1));
                _func.c.env = HValue(_l.top - 1);
                LuaCBarrier(_l, _func, _l.top - 1);
            }
            else
            {
                SetObj(_l, _o, _l.top - 1);
                if (_idx < LUA_GLOBALSINDEX) 
                    LuaCBarrier(_l, CurrFunc(_l), _l.top - 1);
            }
            StkId.Dec(ref _l.top);
            LuaUnlock(_l);
        }

        public static void LuaPushValue(LuaState _l, int _idx)
        {
            LuaLock(_l);
            SetObj2S(_l, _l.top, Index2Address(_l, _idx));
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static int LuaType(LuaState _l, int _idx)
        {
            StkId _o = Index2Address(_l, _idx);
            return (_o == LuaONilObject) ? LUA_TNONE : TType(_o);
        }

        public static CharPtr LuaTypeName(LuaState _l, int _t)
        {
            return (_t == LUA_TNONE) ? "no value" : LuaTTypenames[_t];
        }

        public static bool LuaIsCFunction(LuaState _l, int _idx)
        {
            StkId o = Index2Address(_l, _idx);
            return IsCFunction(o);
        }

        public static int LuaIsNumber(LuaState _l, int _idx)
        {
            TValue _n = new LuaTypeValue();
            TValue _o = Index2Address(_l, _idx);
            return ToNumber(ref _o, _n);
        }

        public static int LuaIsString(LuaState _l, int _idx)
        {
            int _t = LuaType(_l, _idx);
            return (_t == LUA_TSTRING || _t == LUA_TNUMBER) ? 1 : 0;
        }

        public static int LuaIsUserData(LuaState _l, int _idx)
        {
            TValue o = Index2Address(_l, _idx);
            return (TTIsUserData(o) || TTIsLightUserData(o)) ? 1 : 0;
        }
        
        public static int LuaRawEqual(LuaState _l, int _index_1, int _index_2)
        {
            StkId _o_1 = Index2Address(_l, _index_1);
            StkId _o_2 = Index2Address(_l, _index_2);
            return (_o_1 == LuaONilObject || _o_2 == LuaONilObject) ? 0 : LuaORawEqualObj(_o_1, _o_2);
        }

        public static int LuaEqual(LuaState _l, int _index_1, int index_2)
        {
            StkId _o_1, _o_2;
            int _i;
            LuaLock(_l); 
            _o_1 = Index2Address(_l, _index_1);
            _o_2 = Index2Address(_l, index_2);
            _i = (_o_1 == LuaONilObject || _o_2 == LuaONilObject) ? 0 : EqualObj(_l, _o_1, _o_2);
            LuaUnlock(_l);
            return _i;
        }

        public static int LuaLessThan(LuaState _l, int _index_1, int _index_2)
        {
            StkId _o_1, _o_2;
            int _i;
            LuaLock(_l);
            _o_1 = Index2Address(_l, _index_1);
            _o_2 = Index2Address(_l, _index_2);
            _i = (_o_1 == LuaONilObject || _o_2 == LuaONilObject) ? 0
                 : LuaVLessThan(_l, _o_1, _o_2);
            LuaUnlock(_l);
            return _i;
        }

        public static lua_Number LuaToNumber(LuaState _l, int _idx)
        {
            TValue _n = new LuaTypeValue();
            TValue _o = Index2Address(_l, _idx);
            if (ToNumber(ref _o, _n) != 0)
                return NValue(_o);
            else
                return 0;
        }
        
        public static lua_Integer LuaToInteger(LuaState _l, int _idx)
        {
            TValue _n = new LuaTypeValue();
            TValue _o = Index2Address(_l, _idx);
            if (ToNumber(ref _o, _n) != 0)
            {
                lua_Integer _res;
                lua_Number _num = NValue(_o);
                LuaNumber2Integer(out _res, _num);
                return _res;
            }
            else
                return 0;
        }

        public static int LuaToBoolean(LuaState _l, int _idx)
        {
            TValue _o = Index2Address(_l, _idx);
            return (LIsFalse(_o) == 0) ? 1 : 0;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr LuaToLString(LuaState _l, int _idx, out uint _len)
        {
            StkId _o = Index2Address(_l, _idx);
            if (!TTIsString(_o))
            {
                LuaLock(_l); 
                if (LuaVToString(_l, _o) == 0)
                {  
                    _len = 0;
                    LuaUnlock(_l);
                    return null;
                }
                LuaCCheckGC(_l);
                _o = Index2Address(_l, _idx);  
                LuaUnlock(_l);
            }
            _len = TSValue(_o).len;
            return SValue(_o);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static uint LuaObjectLen(LuaState _l, int _idx)
        {
            StkId _o = Index2Address(_l, _idx);
            switch (TType(_o))
            {
                case LUA_TSTRING: return TSValue(_o).len;
                case LUA_TUSERDATA: return UValue(_o).len;
                case LUA_TTABLE: return (uint)LuaHGetN(HValue(_o));
                case LUA_TNUMBER:
                    {
                        uint __l;
                        LuaLock(_l);  
                        __l = (LuaVToString(_l, _o) != 0 ? TSValue(_o).len : 0);
                        LuaUnlock(_l);
                        return __l;
                    }
                default: return 0;
            }
        }

        public static LuaNativeFunction LuaToCFunction(LuaState _l, int _idx)
        {
            StkId _o = Index2Address(_l, _idx);
            return (!IsCFunction(_o)) ? null : CLValue(_o).c.f;
        }

        public static object LuaToUserData(LuaState _l, int _idx)
        {
            StkId _o = Index2Address(_l, _idx);
            switch (TType(_o))
            {
                case LUA_TUSERDATA: return (RawUValue(_o).user_data);
                case LUA_TLIGHTUSERDATA: return PValue(_o);
                default: return null;
            }
        }

        public static LuaState LuaToThread(LuaState _l, int _idx)
        {
            StkId _o = Index2Address(_l, _idx);
            return (!TTIsThread(_o)) ? null : THValue(_o);
        }

        public static object LuaToPointer(LuaState _l, int _idx)
        {
            StkId _o = Index2Address(_l, _idx);
            switch (TType(_o))
            {
                case LUA_TTABLE: return HValue(_o);
                case LUA_TFUNCTION: return CLValue(_o);
                case LUA_TTHREAD: return THValue(_o);
                case LUA_TUSERDATA:
                case LUA_TLIGHTUSERDATA:
                    return LuaToUserData(_l, _idx);
                default: return null;
            }
        }

        public static void LuaPushNil(LuaState _l)
        {
            LuaLock(_l);
            SetNilValue(_l.top);
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static void LuaPushNumber(LuaState _l, lua_Number _n)
        {
            LuaLock(_l);
            SetNValue(_l.top, _n);
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static void LuaPushInteger(LuaState _l, lua_Integer _n)
        {
            LuaLock(_l);
            SetNValue(_l.top, CastNum(_n));
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        private static void LuaPushLString(LuaState _l, CharPtr _s, uint _len)
        {
            LuaLock(_l);
            LuaCCheckGC(_l);
            SetSValue2S(_l, _l.top, LuaSNewLStr(_l, _s, _len));
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static void LuaPushString(LuaState _l, CharPtr _s)
        {
            if (_s == null)
                LuaPushNil(_l);
            else
                LuaPushLString(_l, _s, (uint)StrLen(_s));
        }


        public static CharPtr LuaPushVFString(LuaState _l, CharPtr _fmt, object[] _argp)
        {
            CharPtr _ret;
            LuaLock(_l);
            LuaCCheckGC(_l);
            _ret = LuaOPushVFString(_l, _fmt, _argp);
            LuaUnlock(_l);
            return _ret;
        }

        public static CharPtr LuaPushFString(LuaState _l, CharPtr _fmt)
        {
            CharPtr _ret;
            LuaLock(_l);
            LuaCCheckGC(_l);
            _ret = LuaOPushVFString(_l, _fmt, null);
            LuaUnlock(_l);
            return _ret;
        }

        public static CharPtr LuaPushFString(LuaState _l, CharPtr _fmt, params object[] _p)
        {
            CharPtr _ret;
            LuaLock(_l);
            LuaCCheckGC(_l);
            _ret = LuaOPushVFString(_l, _fmt, _p);
            LuaUnlock(_l);
            return _ret;
        }

        public static void LuaPushCClosure(LuaState _l, LuaNativeFunction _fn, int _n)
        {
            Closure _cl;
            LuaLock(_l);
            LuaCCheckGC(_l);
            CheckNElements(_l, _n);
            _cl = LuaFNewCclosure(_l, _n, GetCurrentEnv(_l));
            _cl.c.f = _fn;
            _l.top -= _n;
            while (_n-- != 0)
                SetObj2N(_l, _cl.c.upvalue[_n], _l.top + _n);
            SetCLValue(_l, _l.top, _cl);
            LuaAssert(IsWhite(obj2gco(_cl)));
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static void LuaPushBoolean(LuaState _l, int _b)
        {
            LuaLock(_l);
            SetBValue(_l.top, (_b != 0) ? 1 : 0); 
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static void LuaPushLightUserData(LuaState _l, object _p)
        {
            LuaLock(_l);
            SetPValue(_l.top, _p);
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static int LuaPushThread(LuaState _l)
        {
            LuaLock(_l);
            SetTTHValue(_l, _l.top, _l);
            IncrementTop(_l);
            LuaUnlock(_l);
            return (G(_l).mainThread == _l) ? 1 : 0;
        }

        public static void LuaGetTable(LuaState _l, int _idx)
        {
            StkId _t;
            LuaLock(_l);
            _t = Index2Address(_l, _idx);
            CheckValidIndex(_l, _t);
            LuaVGetTable(_l, _t, _l.top - 1, _l.top - 1);
            LuaUnlock(_l);
        }

        public static void LuaGetField(LuaState _l, int _idx, CharPtr _k)
        {
            StkId _t;
            TValue _key = new LuaTypeValue();
            LuaLock(_l);
            _t = Index2Address(_l, _idx);
            CheckValidIndex(_l, _t);
            SetSValue(_l, _key, LuaSNew(_l, _k));
            LuaVGetTable(_l, _t, _key, _l.top);
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static void LuaRawGet(LuaState _l, int _idx)
        {
            StkId _t;
            LuaLock(_l);
            _t = Index2Address(_l, _idx);
            ApiCheck(_l, TTIsTable(_t));
            SetObj2S(_l, _l.top - 1, LuaHGet(HValue(_t), _l.top - 1));
            LuaUnlock(_l);
        }

        public static void LuaRawGetI(LuaState _l, int _idx, int _n)
        {
            StkId _o;
            LuaLock(_l);
            _o = Index2Address(_l, _idx);
            ApiCheck(_l, TTIsTable(_o));
            SetObj2S(_l, _l.top, LuaHGetnum(HValue(_o), _n));
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static void LuaCreateTable(LuaState _l, int _n_array, int _n_rec)
        {
            LuaLock(_l);
            LuaCCheckGC(_l);
            SetHValue(_l, _l.top, LuaHNew(_l, _n_array, _n_rec));
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static int LuaGetMetatable(LuaState _l, int _obj_index)
        {
            TValue _obj;
            Table _mt = null;
            int _res;
            LuaLock(_l);
            _obj = Index2Address(_l, _obj_index);
            switch (TType(_obj))
            {
                case LUA_TTABLE:
                    _mt = HValue(_obj).metatable;
                    break;
                case LUA_TUSERDATA:
                    _mt = UValue(_obj).metatable;
                    break;
                default:
                    _mt = G(_l).mt[TType(_obj)];
                    break;
            }
            if (_mt == null)
                _res = 0;
            else
            {
                SetHValue(_l, _l.top, _mt);
                IncrementTop(_l);
                _res = 1;
            }
            LuaUnlock(_l);
            return _res;
        }

        public static void LuaGetFEnv(LuaState _l, int _idx)
        {
            StkId _o;
            LuaLock(_l);
            _o = Index2Address(_l, _idx);
            CheckValidIndex(_l, _o);
            switch (TType(_o))
            {
                case LUA_TFUNCTION:
                    SetHValue(_l, _l.top, CLValue(_o).c.env);
                    break;
                case LUA_TUSERDATA:
                    SetHValue(_l, _l.top, UValue(_o).env);
                    break;
                case LUA_TTHREAD:
                    SetObj2S(_l, _l.top, Gt(THValue(_o)));
                    break;
                default:
                    SetNilValue(_l.top);
                    break;
            }
            IncrementTop(_l);
            LuaUnlock(_l);
        }

        public static void LuaSetTable(LuaState _l, int _idx)
        {
            StkId _t;
            LuaLock(_l);
            CheckNElements(_l, 2);
            _t = Index2Address(_l, _idx);
            CheckValidIndex(_l, _t);
            LuaVSetTable(_l, _t, _l.top - 2, _l.top - 1);
            _l.top -= 2;  
            LuaUnlock(_l);
        }

        public static void LuaSetField(LuaState _l, int _idx, CharPtr _k)
        {
            StkId _t;
            TValue _key = new LuaTypeValue();
            LuaLock(_l);
            CheckNElements(_l, 1);
            _t = Index2Address(_l, _idx);
            CheckValidIndex(_l, _t);
            SetSValue(_l, _key, LuaSNew(_l, _k));
            LuaVSetTable(_l, _t, _key, _l.top - 1);
            StkId.Dec(ref _l.top);  /* pop value */
            LuaUnlock(_l);
        }

        public static void LuaRawSet(LuaState _l, int _idx)
        {
            StkId _t;
            LuaLock(_l);
            CheckNElements(_l, 2);
            _t = Index2Address(_l, _idx);
            ApiCheck(_l, TTIsTable(_t));
            SetObj2T(_l, LuaHSet(_l, HValue(_t), _l.top - 2), _l.top - 1);
            LuaCBarrierT(_l, HValue(_t), _l.top - 1);
            _l.top -= 2;
            LuaUnlock(_l);
        }

        public static void LuaRawSetI(LuaState _l, int _idx, int _n)
        {
            StkId _o;
            LuaLock(_l);
            CheckNElements(_l, 1);
            _o = Index2Address(_l, _idx);
            ApiCheck(_l, TTIsTable(_o));
            SetObj2T(_l, LuaHSetNum(_l, HValue(_o), _n), _l.top - 1);
            LuaCBarrierT(_l, HValue(_o), _l.top - 1);
            StkId.Dec(ref _l.top);
            LuaUnlock(_l);
        }

        public static int LuaSetMetatable(LuaState _l, int _obj_index)
        {
            TValue _obj;
            Table _mt;
            LuaLock(_l);
            CheckNElements(_l, 1);
            _obj = Index2Address(_l, _obj_index);
            CheckValidIndex(_l, _obj);
            if (TTIsNil(_l.top - 1))
                _mt = null;
            else
            {
                ApiCheck(_l, TTIsTable(_l.top - 1));
                _mt = HValue(_l.top - 1);
            }
            switch (TType(_obj))
            {
                case LUA_TTABLE:
                    {
                        HValue(_obj).metatable = _mt;
                        if (_mt != null)
                            LuaCObjBarrierT(_l, HValue(_obj), _mt);
                        break;
                    }
                case LUA_TUSERDATA:
                    {
                        UValue(_obj).metatable = _mt;
                        if (_mt != null)
                            LuaCObjBarrier(_l, RawUValue(_obj), _mt);
                        break;
                    }
                default:
                    {
                        G(_l).mt[TType(_obj)] = _mt;
                        break;
                    }
            }
            StkId.Dec(ref _l.top);
            LuaUnlock(_l);
            return 1;
        }

        public static int LuaSetFEnv(LuaState _l, int _idx)
        {
            StkId _o;
            int _res = 1;
            LuaLock(_l);
            CheckNElements(_l, 1);
            _o = Index2Address(_l, _idx);
            CheckValidIndex(_l, _o);
            ApiCheck(_l, TTIsTable(_l.top - 1));
            switch (TType(_o))
            {
                case LUA_TFUNCTION:
                    CLValue(_o).c.env = HValue(_l.top - 1);
                    break;
                case LUA_TUSERDATA:
                    UValue(_o).env = HValue(_l.top - 1);
                    break;
                case LUA_TTHREAD:
                    SetHValue(_l, Gt(THValue(_o)), HValue(_l.top - 1));
                    break;
                default:
                    _res = 0;
                    break;
            }
            if (_res != 0) LuaCObjBarrier(_l, GCValue(_o), HValue(_l.top - 1));
            StkId.Dec(ref _l.top);
            LuaUnlock(_l);
            return _res;
        }

        public static void AdjustResults(LuaState _l, int _n_res)
        {
            if (_n_res == LUA_MULTRET && _l.top >= _l.ci.top)
                _l.ci.top = _l.top;
        }

        public static void CheckResults(LuaState _l, int _na, int _nr)
        {
            ApiCheck(_l, (_nr) == LUA_MULTRET || (_l.ci.top - _l.top >= (_nr) - (_na)));
        }

        public static void LuaCall(LuaState _l, int _n_args, int _n_results)
        {
            StkId _func;
            LuaLock(_l);
            CheckNElements(_l, _n_args + 1);
            CheckResults(_l, _n_args, _n_results);
            _func = _l.top - (_n_args + 1);
            LuaDCall(_l, _func, _n_results);
            AdjustResults(_l, _n_results);
            LuaUnlock(_l);
        }

        public class CallS
        {  
            public StkId func;
            public int nresults;
        };

        static void FunctionCall(LuaState _l, object _ud)
        {
            CallS _c = _ud as CallS;
            LuaDCall(_l, _c.func, _c.nresults);
        }

        public static int LuaPCall(LuaState _l, int _n_args, int _n_results, int _err_func)
        {
            CallS _c = new CallS();
            int _status;
            ptrdiff_t _func;
            LuaLock(_l);
            CheckNElements(_l, _n_args + 1);
            CheckResults(_l, _n_args, _n_results);
            if (_err_func == 0)
                _func = 0;
            else
            {
                StkId o = Index2Address(_l, _err_func);
                CheckValidIndex(_l, o);
                _func = SaveStack(_l, o);
            }
            _c.func = _l.top - (_n_args + 1);  
            _c.nresults = _n_results;
            _status = LuaDPCall(_l, FunctionCall, _c, SaveStack(_l, _c.func), _func);
            AdjustResults(_l, _n_results);
            LuaUnlock(_l);
            return _status;
        }

        public class CCallS
        { 
            public LuaNativeFunction func;
            public object ud;
        };

        static void FunctionCCall(LuaState _l, object _ud)
        {
            CCallS _c = _ud as CCallS;
            Closure _cl;
            _cl = LuaFNewCclosure(_l, 0, GetCurrentEnv(_l));
            _cl.c.f = _c.func;
            SetCLValue(_l, _l.top, _cl); 
            IncrementTop(_l);
            SetPValue(_l.top, _c.ud); 
            IncrementTop(_l);
            LuaDCall(_l, _l.top - 2, 0);
        }

        public static int LuaCPCall(LuaState _l, LuaNativeFunction _func, object _ud)
        {
            CCallS _c = new CCallS();
            int _status;
            LuaLock(_l);
            _c.func = _func;
            _c.ud = _ud;
            _status = LuaDPCall(_l, FunctionCCall, _c, SaveStack(_l, _l.top), 0);
            LuaUnlock(_l);
            return _status;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaLoad(LuaState _l, LuaReader _reader, object _data, CharPtr _chunk_name)
        {
            ZIO _z = new ZIO();
            int _status;
            LuaLock(_l);
            if (_chunk_name == null) _chunk_name = "?";
            LuaZInit(_l, _z, _reader, _data);
            _status = LuaDProtectedParser(_l, _z, _chunk_name);
            LuaUnlock(_l);
            return _status;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaDump(LuaState _l, LuaWriter _writer, object _data)
        {
            int _status;
            TValue _o;
            LuaLock(_l);
            CheckNElements(_l, 1);
            _o = _l.top - 1;
            if (IsLfunction(_o))
                _status = LuaUDump(_l, CLValue(_o).l.p, _writer, _data, 0);
            else
                _status = 1;
            LuaUnlock(_l);
            return _status;
        }

        public static int LuaStatus(LuaState _l)
        {
            return _l.status;
        }

        public static int LuaGC(LuaState _l, int _what, int _data)
        {
            int _res = 0;
            GlobalState _g;
            LuaLock(_l);
            _g = G(_l);
            switch (_what)
            {
                case LUA_GCSTOP:
                    {
                        _g.gcThreshold = MAXLUMEM;
                        break;
                    }
                case LUA_GCRESTART:
                    {
                        _g.gcThreshold = _g.totalBytes;
                        break;
                    }
                case LUA_GCCOLLECT:
                    {
                        LuaCFullGC(_l);
                        break;
                    }
                case LUA_GCCOUNT:
                    {
                        _res = CastInt(_g.totalBytes >> 10);
                        break;
                    }
                case LUA_GCCOUNTB:
                    {
                        _res = CastInt(_g.totalBytes & 0x3ff);
                        break;
                    }
                case LUA_GCSTEP:
                    {
                        lu_mem _a = ((lu_mem)_data << 10);
                        if (_a <= _g.totalBytes)
                            _g.gcThreshold = (uint)(_g.totalBytes - _a);
                        else
                            _g.gcThreshold = 0;
                        while (_g.gcThreshold <= _g.totalBytes)
                        {
                            LuaCStep(_l);
                            if (_g.gcState == GCSpause)
                            {  
                                _res = 1;  
                                break;
                            }
                        }
                        break;
                    }
                case LUA_GCSETPAUSE:
                    {
                        _res = _g.gcPause;
                        _g.gcPause = _data;
                        break;
                    }
                case LUA_GCSETSTEPMUL:
                    {
                        _res = _g.gcStepMul;
                        _g.gcStepMul = _data;
                        break;
                    }
                default:
                    _res = -1;  
                    break;
            }
            LuaUnlock(_l);
            return _res;
        }

        public static int LuaError(LuaState _l)
        {
            LuaLock(_l);
            CheckNElements(_l, 1);
            LuaGErrorMsg(_l);
            LuaUnlock(_l);
            return 0;  
        }

        public static int LuaNext(LuaState _l, int _idx)
        {
            StkId _t;
            int _more;
            LuaLock(_l);
            _t = Index2Address(_l, _idx);
            ApiCheck(_l, TTIsTable(_t));
            _more = LuaHNext(_l, HValue(_t), _l.top - 1);
            if (_more != 0)
                IncrementTop(_l);
            else  
                StkId.Dec(ref _l.top);  
            LuaUnlock(_l);
            return _more;
        }

        public static void LuaConcat(LuaState _l, int _n)
        {
            LuaLock(_l);
            CheckNElements(_l, _n);
            if (_n >= 2)
            {
                LuaCCheckGC(_l);
                LuaVConcat(_l, _n, CastInt(_l.top - _l.base_) - 1);
                _l.top -= (_n - 1);
            }
            else if (_n == 0)
            { 
                SetSValue2S(_l, _l.top, LuaSNewLStr(_l, "", 0));
                IncrementTop(_l);
            }
            LuaUnlock(_l);
        }

        public static luaAlloc LuaGetAllocF(LuaState _l, ref object _ud)
        {
            luaAlloc _f;
            LuaLock(_l);
            if (_ud != null) _ud = G(_l).ud;
            _f = G(_l).freAlloc;
            LuaUnlock(_l);
            return _f;
        }

        public static void LuaSetAllocF(LuaState _l, luaAlloc _f, object _ud)
        {
            LuaLock(_l);
            G(_l).ud = _ud;
            G(_l).freAlloc = _f;
            LuaUnlock(_l);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static object LuaNewUserData(LuaState _l, uint _size)
        {
            Udata _u;
            LuaLock(_l);
            LuaCCheckGC(_l);
            _u = LuaSNewUData(_l, _size, GetCurrentEnv(_l));
            SetUValue(_l, _l.top, _u);
            IncrementTop(_l);
            LuaUnlock(_l);
            return _u.user_data;
        }

        internal static object LuaNewUserData(LuaState _l, Type _t)
        {
            Udata _u;
            LuaLock(_l);
            LuaCCheckGC(_l);
            _u = LuaSNewUData(_l, _t, GetCurrentEnv(_l));
            SetUValue(_l, _l.top, _u);
            IncrementTop(_l);
            LuaUnlock(_l);
            return _u.user_data;
        }

        static CharPtr AuxUpValue(StkId _fi, int _n, ref TValue _val)
        {
            Closure _f;
            if (!TTIsFunction(_fi)) return null;
            _f = CLValue(_fi);
            if (_f.c.isC != 0)
            {
                if (!(1 <= _n && _n <= _f.c.nupvalues)) return null;
                _val = _f.c.upvalue[_n - 1];
                return "";
            }
            else
            {
                Proto _p = _f.l.p;
                if (!(1 <= _n && _n <= _p.sizeUpValues)) return null;
                _val = _f.l.upvals[_n - 1].v;
                return GetStr(_p.upValues[_n - 1]);
            }
        }

        public static CharPtr LuaGetUpValue(LuaState _l, int _func_index, int _n)
        {
            CharPtr _name;
            TValue _val = new LuaTypeValue();
            LuaLock(_l);
            _name = AuxUpValue(Index2Address(_l, _func_index), _n, ref _val);
            if (_name != null)
            {
                SetObj2S(_l, _l.top, _val);
                IncrementTop(_l);
            }
            LuaUnlock(_l);
            return _name;
        }

        public static CharPtr LuaSetUpValue(LuaState _l, int _func_index, int _n)
        {
            CharPtr _name;
            TValue _val = new LuaTypeValue();
            StkId _fi;
            LuaLock(_l);
            _fi = Index2Address(_l, _func_index);
            CheckNElements(_l, 1);
            _name = AuxUpValue(_fi, _n, ref _val);
            if (_name != null)
            {
                StkId.Dec(ref _l.top);
                SetObj(_l, _val, _l.top);
                LuaCBarrier(_l, CLValue(_fi), _l.top);
            }
            LuaUnlock(_l);
            return _name;
        }
    }
}