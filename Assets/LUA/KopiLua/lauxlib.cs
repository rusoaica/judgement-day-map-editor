﻿#define lauxlib_c
#define LUA_LIB

using System;
using System.IO;

namespace KopiLua
{
    using LuaIntegerType = System.Int32;
    using LuaNumberType = System.Double;

    public partial class Lua
    {
#if LUA_COMPAT_GETN
		public static int LuaLGetN(LuaState L, int t);
		public static void LuaLSetN(LuaState L, int t, int n);
#else
        public static int LuaLGetN(LuaState _l, int _i) { return (int)LuaObjectLen(_l, _i); }
        public static void LuaLSetN(LuaState _l, int _i, int _j) { } 
#endif

#if LUA_COMPAT_OPENLIB
		//#define luaI_openlib	luaL_openlib
#endif

        public const int LUA_ERRFILE = (LUA_ERRERR + 1);
        public const int LUA_NOREF = (-2);
        public const int LUA_REFNIL = (-1);
        public const int FREELIST_REF = 0;  
        public const int LIMIT = LUA_MINSTACK / 2;

        public class LuaLReg
        {
            public LuaLReg(CharPtr _name, LuaNativeFunction _func)
            {
                this.name = _name;
                this.func = _func;
            }
            public CharPtr name;
            public LuaNativeFunction func;
        };

        public static void LuaLArgCheck(LuaState _l, bool _cond, int _num_arg, string _extra_msg)
        {
            if (!_cond)
                LuaLArgError(_l, _num_arg, _extra_msg);
        }

        public static CharPtr LuaLCheckString(LuaState _l, int _n) { return LuaLCheckLString(_l, _n); }
        public static CharPtr LuaLOptString(LuaState _l, int _n, CharPtr _d) { uint len; return LuaLOptLString(_l, _n, _d, out len); }
        public static CharPtr LuaLTypeName(LuaState _l, int _i) { return LuaTypeName(_l, LuaType(_l, _i)); }
        public static int LuaLCheckInt(LuaState _l, int _n) { return (int)LuaLCheckInteger(_l, _n); }
        public static int LuaLOptInt(LuaState _l, int _n, LuaIntegerType _d) { return (int)LuaLOptInteger(_l, _n, _d); }
        public static long LuaLCheckLong(LuaState _l, int _n) { return LuaLCheckInteger(_l, _n); }
        public static long LuaLOptLong(LuaState _l, int _n, LuaIntegerType _d) { return LuaLOptInteger(_l, _n, _d); }
        public static void LuaLGetMetatable(LuaState _l, CharPtr _n) { LuaGetField(_l, LUA_REGISTRYINDEX, _n); }
        public delegate LuaNumberType LuaLOptDelegate(LuaState _l, int _n_arg);
        public delegate LuaIntegerType LuaLOptDelegateInteger(LuaState _l, int _n_arg);

        public static LuaNumberType LuaLOpt(LuaState _l, LuaLOptDelegate _f, int _n, LuaNumberType _d)
        {
            return LuaIsNoneOrNil(_l, (_n != 0) ? _d : _f(_l, _n)) ? 1 : 0;
        }

        public static LuaIntegerType LuaLOptInteger(LuaState _l, LuaLOptDelegateInteger _f, int _n, LuaNumberType _d)
        {
            return (LuaIntegerType)(LuaIsNoneOrNil(_l, _n) ? _d : _f(_l, (_n)));
        }

        public class LuaLBuffer
        {
            public int p;           
            public int lvl;  
            public LuaState l;
            public CharPtr buffer = new char[LUAL_BUFFERSIZE];
        };

        public static void LuaLAddChar(LuaLBuffer _b, char _c)
        {
            if (_b.p >= LUAL_BUFFERSIZE)
                LuaLPrepBuffer(_b);
            _b.buffer[_b.p++] = _c;
        }

        public static void LuaLPutChar(LuaLBuffer _b, char _c) { LuaLAddChar(_b, _c); }

        public static void LuaLAddSize(LuaLBuffer _b, int _n) { _b.p += _n; }

        public static int AbsIndex(LuaState _l, int _i)
        {
            return ((_i) > 0 || (_i) <= LUA_REGISTRYINDEX ? (_i) : LuaGetTop(_l) + (_i) + 1);
        }

        public static int LuaLArgError(LuaState _l, int _n_arg, CharPtr _extra_msg)
        {
            LuaDebug _ar = new LuaDebug();
            if (LuaGetStack(_l, 0, ref _ar) == 0)
                return LuaLError(_l, "bad argument #%d (%s)", _n_arg, _extra_msg);
            LuaGetInfo(_l, "n", ref _ar);
            if (StrCmp(_ar.nameWhat, "method") == 0)
            {
                _n_arg--;
                if (_n_arg == 0)
                    return LuaLError(_l, "calling " + LUA_QS + " on bad self ({1})", _ar.name, _extra_msg);
            }
            if (_ar.name == null)
                _ar.name = "?";
            return LuaLError(_l, "bad argument #%d to " + LUA_QS + " (%s)", _n_arg, _ar.name, _extra_msg);
        }

        public static int LuaLTypeError(LuaState _l, int _n_arg, CharPtr _t_name)
        {
            CharPtr _msg = LuaPushFString(_l, "%s expected, got %s", _t_name, LuaLTypeName(_l, _n_arg));
            return LuaLArgError(_l, _n_arg, _msg);
        }

        private static void TagError(LuaState _l, int _n_arg, int _tag)
        {
            LuaLTypeError(_l, _n_arg, LuaTypeName(_l, _tag));
        }

        public static void LuaLWhere(LuaState _l, int _level)
        {
            LuaDebug _ar = new LuaDebug();
            if (LuaGetStack(_l, _level, ref _ar) != 0)
            {
                LuaGetInfo(_l, "Sl", ref _ar);
                if (_ar.currentLine > 0)
                {
                    LuaPushFString(_l, "%s:%d: ", _ar.shortSrc, _ar.currentLine);
                    return;
                }
            }
            LuaPushLiteral(_l, "");
        }

        public static int LuaLError(LuaState _l, CharPtr _fmt, params object[] _p)
        {
            LuaLWhere(_l, 1);
            LuaPushVFString(_l, _fmt, _p);
            LuaConcat(_l, 2);
            return LuaError(_l);
        }

        public static int LuaLCheckOption(LuaState _l, int _n_arg, CharPtr _def, CharPtr[] _lst)
        {
            CharPtr _name = (_def != null) ? LuaLOptString(_l, _n_arg, _def) : LuaLCheckString(_l, _n_arg);
            int _i;
            for (_i = 0; _i < _lst.Length; _i++)
                if (StrCmp(_lst[_i], _name) == 0)
                    return _i;
            return LuaLArgError(_l, _n_arg, LuaPushFString(_l, "invalid option " + LUA_QS, _name));
        }

        public static int LuaLNewMetatable(LuaState _l, CharPtr _t_name)
        {
            LuaGetField(_l, LUA_REGISTRYINDEX, _t_name);
            if (!LuaIsNil(_l, -1))
                return 0;
            LuaPop(_l, 1);
            LuaNewTable(_l);
            LuaPushValue(_l, -1);
            LuaSetField(_l, LUA_REGISTRYINDEX, _t_name);
            return 1;
        }

        public static object LuaLCheckUData(LuaState _l, int _ud, CharPtr _t_name)
        {
            object _p = LuaToUserData(_l, _ud);
            if (_p != null)
            {
                if (LuaGetMetatable(_l, _ud) != 0)
                {
                    LuaGetField(_l, LUA_REGISTRYINDEX, _t_name);
                    if (LuaRawEqual(_l, -1, -2) != 0)
                    {
                        LuaPop(_l, 2);
                        return _p;
                    }
                }
            }
            LuaLTypeError(_l, _ud, _t_name); 
            return null;  
        }

        public static void LuaLCheckStack(LuaState _l, int _space, CharPtr _mes)
        {
            if (LuaCheckStack(_l, _space) == 0)
                LuaLError(_l, "stack overflow (%s)", _mes);
        }

        public static void LuaLCheckType(LuaState _l, int _n_arg, int _t)
        {
            if (LuaType(_l, _n_arg) != _t)
                TagError(_l, _n_arg, _t);
        }


        public static void LuaLCheckAny(LuaState _l, int _n_arg)
        {
            if (LuaType(_l, _n_arg) == LUA_TNONE)
                LuaLArgError(_l, _n_arg, "value expected");
        }

        public static CharPtr LuaLCheckLString(LuaState _l, int _n_arg) { uint _len; return LuaLCheckLString(_l, _n_arg, out _len); }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr LuaLCheckLString(LuaState _l, int _n_arg, out uint _len)
        {
            CharPtr _s = LuaToLString(_l, _n_arg, out _len);
            if (_s == null) TagError(_l, _n_arg, LUA_TSTRING);
            return _s;
        }

        public static CharPtr LuaLOptLString(LuaState _l, int _n_arg, CharPtr _def)
        {
            uint _len; return LuaLOptLString(_l, _n_arg, _def, out _len);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr LuaLOptLString(LuaState _l, int _n_arg, CharPtr _def, out uint _len)
        {
            if (LuaIsNoneOrNil(_l, _n_arg))
            {
                _len = (uint)((_def != null) ? StrLen(_def) : 0);
                return _def;
            }
            else return LuaLCheckLString(_l, _n_arg, out _len);
        }

        public static LuaNumberType LuaLCheckNumber(LuaState _l, int _n_arg)
        {
            LuaNumberType _d = LuaToNumber(_l, _n_arg);
            if ((_d == 0) && (LuaIsNumber(_l, _n_arg) == 0))
                TagError(_l, _n_arg, LUA_TNUMBER);
            return _d;
        }

        public static LuaNumberType LuaLOptNumber(LuaState _l, int _n_arg, LuaNumberType _def)
        {
            return LuaLOpt(_l, LuaLCheckNumber, _n_arg, _def);
        }

        public static LuaIntegerType LuaLCheckInteger(LuaState _l, int _n_arg)
        {
            LuaIntegerType _d = LuaToInteger(_l, _n_arg);
            if (_d == 0 && LuaIsNumber(_l, _n_arg) == 0)
                TagError(_l, _n_arg, LUA_TNUMBER);
            return _d;
        }

        public static LuaIntegerType LuaLOptInteger(LuaState _l, int _n_arg, LuaIntegerType _def)
        {
            return LuaLOptInteger(_l, LuaLCheckInteger, _n_arg, _def);
        }

        public static int LuaLGetMetafield(LuaState _l, int _obj, CharPtr _event)
        {
            if (LuaGetMetatable(_l, _obj) == 0)
                return 0;
            LuaPushString(_l, _event);
            LuaRawGet(_l, -2);
            if (LuaIsNil(_l, -1))
            {
                LuaPop(_l, 2);
                return 0;
            }
            else
            {
                LuaRemove(_l, -2);
                return 1;
            }
        }

        public static int LuaLCallMeta(LuaState _l, int _obj, CharPtr _event)
        {
            _obj = AbsIndex(_l, _obj);
            if (LuaLGetMetafield(_l, _obj, _event) == 0)
                return 0;
            LuaPushValue(_l, _obj);
            LuaCall(_l, 1, 1);
            return 1;
        }

        public static void LuaLRegister(LuaState _l, CharPtr _lib_name, LuaLReg[] __l)
        {
            LuaIOpenLib(_l, _lib_name, __l, 0);
        }

        private static int LibSize(LuaLReg[] _l)
        {
            int _size = 0;
            for (; _l[_size].name != null; _size++) ;
            return _size;
        }

        public static void LuaIOpenLib(LuaState _l, CharPtr _lib_name, LuaLReg[] __l, int _nup)
        {
            if (_lib_name != null)
            {
                int _size = LibSize(__l);
                LuaLFindTable(_l, LUA_REGISTRYINDEX, "_LOADED", 1);
                LuaGetField(_l, -1, _lib_name);
                if (!LuaIsTable(_l, -1))
                {
                    LuaPop(_l, 1);
                    if (LuaLFindTable(_l, LUA_GLOBALSINDEX, _lib_name, _size) != null)
                        LuaLError(_l, "name conflict for module " + LUA_QS, _lib_name);
                    LuaPushValue(_l, -1);
                    LuaSetField(_l, -3, _lib_name);
                }
                LuaRemove(_l, -2);
                LuaInsert(_l, -(_nup + 1));
            }
            int _reg_num = 0;
            for (; __l[_reg_num].name != null; _reg_num++)
            {
                int _i;
                for (_i = 0; _i < _nup; _i++)
                    LuaPushValue(_l, -_nup);
                LuaPushCClosure(_l, __l[_reg_num].func, _nup);
                LuaSetField(_l, -(_nup + 2), __l[_reg_num].name);
            }
            LuaPop(_l, _nup);
        }

#if LUA_COMPAT_GETN

        static int checkint(LuaState _l, int _topop)
        {
            int _n = (lua_type(_l, -1) == LUA_TNUMBER) ? lua_tointeger(_l, -1) : -1;
            lua_pop(_l, _topop);
            return _n;
        }

        static void getsizes(LuaState _l)
        {
            lua_getfield(_l, LUA_REGISTRYINDEX, "LUA_SIZES");
            if (lua_isnil(_l, -1))
            {
                lua_pop(_l, 1);
                lua_newtable(_l);
                lua_pushvalue(_l, -1);
                lua_setmetatable(_l, -2);
                lua_pushliteral(_l, "kv");
                lua_setfield(_l, -2, "__mode");
                lua_pushvalue(_l, -1);
                lua_setfield(_l, LUA_REGISTRYINDEX, "LUA_SIZES");
            }
        }

        public static void luaL_setn(LuaState _l, int _t, int _n)
        {
            _t = abs_index(_l, _t);
            lua_pushliteral(_l, "n");
            lua_rawget(_l, _t);
            if (checkint(_l, 1) >= 0)
            {  
                lua_pushliteral(_l, "n"); 
                lua_pushinteger(_l, _n);
                lua_rawset(_l, _t);
            }
            else
            { 
                getsizes(_l);
                lua_pushvalue(_l, _t);
                lua_pushinteger(_l, _n);
                lua_rawset(_l, -3); 
                lua_pop(_l, 1); 
            }
        }

        public static int luaL_getn(LuaState _l, int _t)
        {
            int _n;
            _t = abs_index(_l, _t);
            lua_pushliteral(_l, "n"); 
            lua_rawget(_l, _t);
            if ((_n = checkint(_l, 1)) >= 0) return _n;
            getsizes(_l); 
            lua_pushvalue(_l, _t);
            lua_rawget(_l, -2);
            if ((_n = checkint(_l, 2)) >= 0) return _n;
            return (int)lua_objlen(_l, _t);
        }

#endif

        public static CharPtr LuaLGSub(LuaState _l, CharPtr _s, CharPtr _p, CharPtr _r)
        {
            CharPtr _wild;
            uint __l = (uint)StrLen(_p);
            LuaLBuffer _b = new LuaLBuffer();
            LuaLBuffInit(_l, _b);
            while ((_wild = StrStr(_s, _p)) != null)
            {
                LuaLAddLString(_b, _s, (uint)(_wild - _s)); 
                LuaLAddString(_b, _r); 
                _s = _wild + __l; 
            }
            LuaLAddString(_b, _s);
            LuaLPushResult(_b);
            return LuaToString(_l, -1);
        }

        public static CharPtr LuaLFindTable(LuaState _l, int _idx, CharPtr _f_name, int _szh_int)
        {
            CharPtr _e;
            LuaPushValue(_l, _idx);
            do
            {
                _e = StrChr(_f_name, '.');
                if (_e == null) _e = _f_name + StrLen(_f_name);
                LuaPushLString(_l, _f_name, (uint)(_e - _f_name));
                LuaRawGet(_l, -2);
                if (LuaIsNil(_l, -1))
                {  
                    LuaPop(_l, 1); 
                    LuaCreateTable(_l, 0, (_e == '.' ? 1 : _szh_int)); 
                    LuaPushLString(_l, _f_name, (uint)(_e - _f_name));
                    LuaPushValue(_l, -2);
                    LuaSetTable(_l, -4);  
                }
                else if (!LuaIsTable(_l, -1))
                {  
                    LuaPop(_l, 2);  
                    return _f_name; 
                }
                LuaRemove(_l, -2);  
                _f_name = _e + 1;
            } while (_e == '.');
            return null;
        }

        private static int BufferLen(LuaLBuffer _b) { return _b.p; }
        private static int BufferFree(LuaLBuffer _b) { return LUAL_BUFFERSIZE - BufferLen(_b); }

        private static int EmptyBuffer(LuaLBuffer _b)
        {
            uint _l = (uint)BufferLen(_b);
            if (_l == 0) return 0;  
            else
            {
                LuaPushLString(_b.l, _b.buffer, _l);
                _b.p = 0;
                _b.lvl++;
                return 1;
            }
        }

        private static void AdjustStack(LuaLBuffer _b)
        {
            if (_b.lvl > 1)
            {
                LuaState L = _b.l;
                int _toget = 1;  
                uint _toplen = LuaStrLen(L, -1);
                do
                {
                    uint _l = LuaStrLen(L, -(_toget + 1));
                    if (_b.lvl - _toget + 1 >= LIMIT || _toplen > _l)
                    {
                        _toplen += _l;
                        _toget++;
                    }
                    else break;
                } while (_toget < _b.lvl);
                LuaConcat(L, _toget);
                _b.lvl = _b.lvl - _toget + 1;
            }
        }

        public static CharPtr LuaLPrepBuffer(LuaLBuffer _b)
        {
            if (EmptyBuffer(_b) != 0)
                AdjustStack(_b);
            return new CharPtr(_b.buffer, _b.p);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static void LuaLAddLString(LuaLBuffer _b, CharPtr _s, uint _l)
        {
            while (_l-- != 0)
            {
                char _c = _s[0];
                _s = _s.next();
                LuaLAddChar(_b, _c);
            }
        }

        public static void LuaLAddString(LuaLBuffer _b, CharPtr _s)
        {
            LuaLAddLString(_b, _s, (uint)StrLen(_s));
        }


        public static void LuaLPushResult(LuaLBuffer _b)
        {
            EmptyBuffer(_b);
            LuaConcat(_b.l, _b.lvl);
            _b.lvl = 1;
        }

        public static void LuaLAddValue(LuaLBuffer _b)
        {
            LuaState L = _b.l;
            uint _vl;
            CharPtr _s = LuaToLString(L, -1, out _vl);
            if (_vl <= BufferFree(_b))
            {  
                CharPtr _dst = new CharPtr(_b.buffer.chars, _b.buffer.index + _b.p);
                CharPtr _src = new CharPtr(_s.chars, _s.index);
                for (uint i = 0; i < _vl; i++)
                    _dst[i] = _src[i];
                _b.p += (int)_vl;
                LuaPop(L, 1);  
            }
            else
            {
                if (EmptyBuffer(_b) != 0)
                    LuaInsert(L, -2);  
                _b.lvl++;  
                AdjustStack(_b);
            }
        }

        public static void LuaLBuffInit(LuaState _l, LuaLBuffer _b)
        {
            _b.l = _l;
            _b.p = 0;
            _b.lvl = 0;
        }

        public static int LuaLRef(LuaState _l, int _t)
        {
            int _ref_;
            _t = AbsIndex(_l, _t);
            if (LuaIsNil(_l, -1))
            {
                LuaPop(_l, 1);  
                return LUA_REFNIL;  
            }
            LuaRawGetI(_l, _t, FREELIST_REF); 
            _ref_ = (int)LuaToInteger(_l, -1);  
            LuaPop(_l, 1);  
            if (_ref_ != 0)
            { 
                LuaRawGetI(_l, _t, _ref_);  
                LuaRawSetI(_l, _t, FREELIST_REF); 
            }
            else
            {  
                _ref_ = (int)LuaObjectLen(_l, _t);
                _ref_++;  
            }
            LuaRawSetI(_l, _t, _ref_);
            return _ref_;
        }


        public static void LuaLUnref(LuaState _l, int _t, int _ref)
        {
            if (_ref >= 0)
            {
                _t = AbsIndex(_l, _t);
                LuaRawGetI(_l, _t, FREELIST_REF);
                LuaRawSetI(_l, _t, _ref);  
                LuaPushInteger(_l, _ref);
                LuaRawSetI(_l, _t, FREELIST_REF); 
            }
        }

        public class LoadF
        {
            public int _extra_line;
            public Stream _f;
            public CharPtr buff = new char[LUAL_BUFFERSIZE];
        };

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr GetF(LuaState _l, object _ud, out uint _size)
        {
            _size = 0;
            LoadF _lf = (LoadF)_ud;
            if (_lf._extra_line != 0)
            {
                _lf._extra_line = 0;
                _size = 1;
                return "\n";
            }
            if (FEof(_lf._f) != 0) return null;
            _size = (uint)FRead(_lf.buff, 1, _lf.buff.chars.Length, _lf._f);
            return (_size > 0) ? new CharPtr(_lf.buff) : null;
        }

        private static int ErrFile(LuaState _l, CharPtr _what, int _f_name_index)
        {
            CharPtr _serr = StrError(ErrNo());
            CharPtr _filename = LuaToString(_l, _f_name_index) + 1;
            LuaPushFString(_l, "cannot %s %s: %s", _what, _filename, _serr);
            LuaRemove(_l, _f_name_index);
            return LUA_ERRFILE;
        }

        public static int LuaLLoadFile(LuaState _l, CharPtr _filename)
        {
            LoadF _lf = new LoadF();
            int _status, _read_status;
            int _c;
            int _f_name_index = LuaGetTop(_l) + 1; 
            _lf._extra_line = 0;
            if (_filename == null)
            {
                LuaPushLiteral(_l, "=stdin");
                _lf._f = stdIn;
            }
            else
            {
                LuaPushFString(_l, "@%s", _filename);
                _lf._f = FOpen(_filename, "r");
                if (_lf._f == null) return ErrFile(_l, "open", _f_name_index);
            }
            _c = GetC(_lf._f);
            if (_c == '#')
            {  
                _lf._extra_line = 1;
                while ((_c = GetC(_lf._f)) != EOF && _c != '\n') ; 
                if (_c == '\n') _c = GetC(_lf._f);
            }
            if (_c == LUA_SIGNATURE[0] && (_filename != null))
            { 
                _lf._f = FReopen(_filename, "rb", _lf._f);  
                if (_lf._f == null) return ErrFile(_l, "reopen", _f_name_index);
                while ((_c = GetC(_lf._f)) != EOF && _c != LUA_SIGNATURE[0]) ;
                _lf._extra_line = 0;
            }
            UngetC(_c, _lf._f);
            _status = LuaLoad(_l, GetF, _lf, LuaToString(_l, -1));
            _read_status = FError(_lf._f);
            if (_filename != null) FClose(_lf._f);  
            if (_read_status != 0)
            {
                LuaSetTop(_l, _f_name_index); 
                return ErrFile(_l, "read", _f_name_index);
            }
            LuaRemove(_l, _f_name_index);
            return _status;
        }

        public class LoadS
        {
            public CharPtr s;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public uint size;
        };


        static CharPtr GetS(LuaState _l, object _ud, out uint _size)
        {
            LoadS _ls = (LoadS)_ud;
            _size = _ls.size;
            _ls.size = 0;
            return _ls.s;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaLLoadBuffer(LuaState _l, CharPtr _buff, uint _size, CharPtr _name)
        {
            LoadS _ls = new LoadS();
            _ls.s = new CharPtr(_buff);
            _ls.size = _size;
            return LuaLoad(_l, GetS, _ls, _name);
        }

        public static int LuaLLoadString(LuaState _l, CharPtr _s)
        {
            return LuaLLoadBuffer(_l, _s, (uint)StrLen(_s), _s);
        }

        private static object LuaAlloc(Type _t)
        {
            return System.Activator.CreateInstance(_t);
        }

        private static int Panic(LuaState _l)
        {
            FPrintF(stdErr, "PANIC: unprotected error in call to Lua API (%s)\n", LuaToString(_l, -1));
            return 0;
        }

        public static LuaState LuaLNewState()
        {
            LuaState _l = LuaNewState(LuaAlloc, null);
            if (_l != null) LuaAtPanic(_l, Panic);
            return _l;
        }
    }
}