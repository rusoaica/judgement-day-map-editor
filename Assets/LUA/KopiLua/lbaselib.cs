﻿using System;

namespace KopiLua
{
    using LuaNumberType = System.Double;

    public partial class Lua
    {
        public const int CO_RUN = 0;
        public const int CO_SUS = 1;
        public const int CO_NOR = 2;
        public const int CO_DEAD = 3;
        private static readonly string[] _stat_names = { "running", "suspended", "normal", "dead" };

        private static int LuaBPrint(LuaState _l)
        {
            int _n = LuaGetTop(_l);  
            int _i;
            LuaGetGlobal(_l, "tostring");
            for (_i = 1; _i <= _n; _i++)
            {
                CharPtr _s;
                LuaPushValue(_l, -1); 
                LuaPushValue(_l, _i); 
                LuaCall(_l, 1, 1);
                _s = LuaToString(_l, -1); 
                if (_s == null)
                    return LuaLError(_l, LUA_QL("tostring") + " must return a string to " + LUA_QL("print"));
                if (_i > 1) FPuts("\t", stdOut);
                FPuts(_s, stdOut);
                LuaPop(_l, 1); 
            }
            Console.Write("\n", stdOut);
            return 0;
        }

        private static int LuaBToNumber(LuaState _l)
        {
            int _base_ = LuaLOptInt(_l, 2, 10);
            if (_base_ == 10)
            { 
                LuaLCheckAny(_l, 1);
                if (LuaIsNumber(_l, 1) != 0)
                {
                    LuaPushNumber(_l, LuaToNumber(_l, 1));
                    return 1;
                }
            }
            else
            {
                CharPtr _s_1 = LuaLCheckString(_l, 1);
                CharPtr _s_2;
                ulong _n;
                LuaLArgCheck(_l, 2 <= _base_ && _base_ <= 36, 2, "base out of range");
                _n = StrToUl(_s_1, out _s_2, _base_);
                if (_s_1 != _s_2)
                {  
                    while (IsSpace((byte)(_s_2[0]))) _s_2 = _s_2.next();  
                    if (_s_2[0] == '\0')
                    {  
                        LuaPushNumber(_l, (LuaNumberType)_n);
                        return 1;
                    }
                }
            }
            LuaPushNil(_l);  
            return 1;
        }

        private static int LuaBError(LuaState _l)
        {
            int _level = LuaLOptInt(_l, 2, 1);
            LuaSetTop(_l, 1);
            if ((LuaIsString(_l, 1) != 0) && (_level > 0))
            {  
                LuaLWhere(_l, _level);
                LuaPushValue(_l, 1);
                LuaConcat(_l, 2);
            }
            return LuaError(_l);
        }
        
        private static int LuaBGetMetatable(LuaState _l)
        {
            LuaLCheckAny(_l, 1);
            if (LuaGetMetatable(_l, 1) == 0)
            {
                LuaPushNil(_l);
                return 1;  
            }
            LuaLGetMetafield(_l, 1, "__metatable");
            return 1;  
        }

        private static int LuaBSetMetatable(LuaState _l)
        {
            int _t = LuaType(_l, 2);
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaLArgCheck(_l, _t == LUA_TNIL || _t == LUA_TTABLE, 2, "nil or table expected");
            if (LuaLGetMetafield(_l, 1, "__metatable") != 0)
                LuaLError(_l, "cannot change a protected metatable");
            LuaSetTop(_l, 2);
            LuaSetMetatable(_l, 1);
            return 1;
        }

        private static void GetFunc(LuaState _l, int _opt)
        {
            if (LuaIsFunction(_l, 1))
                LuaPushValue(_l, 1);
            else
            {
                LuaDebug _ar = new LuaDebug();
                int _level = (_opt != 0) ? LuaLOptInt(_l, 1, 1) : LuaLCheckInt(_l, 1);
                LuaLArgCheck(_l, _level >= 0, 1, "level must be non-negative");
                if (LuaGetStack(_l, _level, ref _ar) == 0)
                    LuaLArgError(_l, 1, "invalid level");
                LuaGetInfo(_l, "f", ref _ar);
                if (LuaIsNil(_l, -1))
                    LuaLError(_l, "no function environment for tail call at level %d", _level);
            }
        }

        private static int LuaBGetFEnv(LuaState _l)
        {
            GetFunc(_l, 1);
            if (LuaIsCFunction(_l, -1))  
                LuaPushValue(_l, LUA_GLOBALSINDEX);  
            else
                LuaGetFEnv(_l, -1);
            return 1;
        }

        private static int LuaBSetFEnv(LuaState _l)
        {
            LuaLCheckType(_l, 2, LUA_TTABLE);
            GetFunc(_l, 0);
            LuaPushValue(_l, 2);
            if ((LuaIsNumber(_l, 1) != 0) && (LuaToNumber(_l, 1) == 0))
            {
                LuaPushThread(_l);
                LuaInsert(_l, -2);
                LuaSetFEnv(_l, -2);
                return 0;
            }
            else if (LuaIsCFunction(_l, -2) || LuaSetFEnv(_l, -2) == 0)
                LuaLError(_l, LUA_QL("setfenv") + " cannot change environment of given object");
            return 1;
        }

        private static int LuaBRawEqual(LuaState _l)
        {
            LuaLCheckAny(_l, 1);
            LuaLCheckAny(_l, 2);
            LuaPushBoolean(_l, LuaRawEqual(_l, 1, 2));
            return 1;
        }

        private static int LuaBRawGet(LuaState _l)
        {
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaLCheckAny(_l, 2);
            LuaSetTop(_l, 2);
            LuaRawGet(_l, 1);
            return 1;
        }

        private static int LuaBRawSet(LuaState _l)
        {
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaLCheckAny(_l, 2);
            LuaLCheckAny(_l, 3);
            LuaSetTop(_l, 3);
            LuaRawSet(_l, 1);
            return 1;
        }

        private static int LuaBGGInfo(LuaState _l)
        {
            LuaPushInteger(_l, LuaGetGCCount(_l));
            return 1;
        }

        public static readonly CharPtr[] opts = {"stop", "restart", "collect", "count", "step", "setpause", "setstepmul", null};
        public readonly static int[] optsnum = {LUA_GCSTOP, LUA_GCRESTART, LUA_GCCOLLECT, LUA_GCCOUNT, LUA_GCSTEP, LUA_GCSETPAUSE, LUA_GCSETSTEPMUL};

        private static int LuaBCollectGarbage(LuaState _l)
        {
            int _o = LuaLCheckOption(_l, 1, "collect", opts);
            int _ex = LuaLOptInt(_l, 2, 0);
            int _res = LuaGC(_l, optsnum[_o], _ex);
            switch (optsnum[_o])
            {
                case LUA_GCCOUNT:
                    {
                        int _b = LuaGC(_l, LUA_GCCOUNTB, 0);
                        LuaPushNumber(_l, _res + ((LuaNumberType)_b / 1024));
                        return 1;
                    }
                case LUA_GCSTEP:
                    {
                        LuaPushBoolean(_l, _res);
                        return 1;
                    }
                default:
                    {
                        LuaPushNumber(_l, _res);
                        return 1;
                    }
            }
        }

        private static int LuaBType(LuaState _l)
        {
            LuaLCheckAny(_l, 1);
            LuaPushString(_l, LuaLTypeName(_l, 1));
            return 1;
        }

        private static int LuaBNext(LuaState _l)
        {
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaSetTop(_l, 2);  
            if (LuaNext(_l, 1) != 0)
                return 2;
            else
            {
                LuaPushNil(_l);
                return 1;
            }
        }

        private static int LuaBPairs(LuaState _l)
        {
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaPushValue(_l, LuaUpValueIndex(1));
            LuaPushValue(_l, 1); 
            LuaPushNil(_l); 
            return 3;
        }

        private static int CheckPairsAux(LuaState _l)
        {
            int _i = LuaLCheckInt(_l, 2);
            LuaLCheckType(_l, 1, LUA_TTABLE);
            _i++; 
            LuaPushInteger(_l, _i);
            LuaRawGetI(_l, 1, _i);
            return (LuaIsNil(_l, -1)) ? 0 : 2;
        }

        private static int LuaBIPairs(LuaState _l)
        {
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaPushValue(_l, LuaUpValueIndex(1)); 
            LuaPushValue(_l, 1); 
            LuaPushInteger(_l, 0); 
            return 3;
        }

        private static int LoadAux(LuaState _l, int _status)
        {
            if (_status == 0)  
                return 1;
            else
            {
                LuaPushNil(_l);
                LuaInsert(_l, -2);  
                return 2;  
            }
        }

        private static int LuaBLoadString(LuaState _l)
        {
            uint __l;
            CharPtr _s = LuaLCheckLString(_l, 1, out __l);
            CharPtr _chunk_name = LuaLOptString(_l, 2, _s);
            return LoadAux(_l, LuaLLoadBuffer(_l, _s, __l, _chunk_name));
        }

        private static int LuaBLoadFile(LuaState L)
        {
            CharPtr _f_name = LuaLOptString(L, 1, null);
            return LoadAux(L, LuaLLoadFile(L, _f_name));
        }

        private static CharPtr GenericReader(LuaState _l, object _ud, out uint _size)
        {
            LuaLCheckStack(_l, 2, "too many nested functions");
            LuaPushValue(_l, 1);  
            LuaCall(_l, 0, 1); 
            if (LuaIsNil(_l, -1))
            {
                _size = 0;
                return null;
            }
            else if (LuaIsString(_l, -1) != 0)
            {
                LuaReplace(_l, 3);  
                return LuaToLString(_l, 3, out _size);
            }
            else
            {
                _size = 0;
                LuaLError(_l, "reader function must return a string");
            }
            return null;  
        }

        private static int LuaBLoad(LuaState _l)
        {
            int _status;
            CharPtr _c_name = LuaLOptString(_l, 2, "=(load)");
            LuaLCheckType(_l, 1, LUA_TFUNCTION);
            LuaSetTop(_l, 3);  
            _status = LuaLoad(_l, GenericReader, null, _c_name);
            return LoadAux(_l, _status);
        }

        private static int LuaBDoFile(LuaState _l)
        {
            CharPtr _f_name = LuaLOptString(_l, 1, null);
            int _n = LuaGetTop(_l);
            if (LuaLLoadFile(_l, _f_name) != 0) LuaError(_l);
            LuaCall(_l, 0, LUA_MULTRET);
            return LuaGetTop(_l) - _n;
        }

        private static int LuaBAssert(LuaState _l)
        {
            LuaLCheckAny(_l, 1);
            if (LuaToBoolean(_l, 1) == 0)
                return LuaLError(_l, "%s", LuaLOptString(_l, 2, "assertion failed!"));
            return LuaGetTop(_l);
        }

        private static int LuaBUnpack(LuaState _l)
        {
            int _i, _e, _n;
            LuaLCheckType(_l, 1, LUA_TTABLE);
            _i = LuaLOptInt(_l, 2, 1);
            _e = LuaLOptInteger(_l, LuaLCheckInt, 3, LuaLGetN(_l, 1));
            if (_i > _e) return 0;  
            _n = _e - _i + 1;  
            if (_n <= 0 || (LuaCheckStack(_l, _n) == 0)) 
                return LuaLError(_l, "too many results to unpack");
            LuaRawGetI(_l, 1, _i); 
            while (_i++ < _e)  
                LuaRawGetI(_l, 1, _i);
            return _n;
        }

        private static int LuaBSelect(LuaState _l)
        {
            int _n = LuaGetTop(_l);
            if (LuaType(_l, 1) == LUA_TSTRING && LuaToString(_l, 1)[0] == '#')
            {
                LuaPushInteger(_l, _n - 1);
                return 1;
            }
            else
            {
                int _i = LuaLCheckInt(_l, 1);
                if (_i < 0) _i = _n + _i;
                else if (_i > _n) _i = _n;
                LuaLArgCheck(_l, 1 <= _i, 1, "index out of range");
                return _n - _i;
            }
        }

        private static int LuaBPCall(LuaState _l)
        {
            int _status;
            LuaLCheckAny(_l, 1);
            _status = LuaPCall(_l, LuaGetTop(_l) - 1, LUA_MULTRET, 0);
            LuaPushBoolean(_l, (_status == 0) ? 1 : 0);
            LuaInsert(_l, 1);
            return LuaGetTop(_l);  
        }

        private static int LuaBXPCall(LuaState _l)
        {
            int _status;
            LuaLCheckAny(_l, 2);
            LuaSetTop(_l, 2);
            LuaInsert(_l, 1);  
            _status = LuaPCall(_l, 0, LUA_MULTRET, 1);
            LuaPushBoolean(_l, (_status == 0) ? 1 : 0);
            LuaReplace(_l, 1);
            return LuaGetTop(_l);  
        }

        private static int LuaBToString(LuaState _l)
        {
            LuaLCheckAny(_l, 1);
            if (LuaLCallMeta(_l, 1, "__tostring") != 0) 
                return 1;  
            switch (LuaType(_l, 1))
            {
                case LUA_TNUMBER:
                    LuaPushString(_l, LuaToString(_l, 1));
                    break;
                case LUA_TSTRING:
                    LuaPushValue(_l, 1);
                    break;
                case LUA_TBOOLEAN:
                    LuaPushString(_l, (LuaToBoolean(_l, 1) != 0 ? "true" : "false"));
                    break;
                case LUA_TNIL:
                    LuaPushLiteral(_l, "nil");
                    break;
                default:
                    LuaPushFString(_l, "%s: %p", LuaLTypeName(_l, 1), LuaToPointer(_l, 1));
                    break;
            }
            return 1;
        }

        private static int LuaBNewProxy(LuaState _l)
        {
            LuaSetTop(_l, 1);
            LuaNewUserData(_l, 0);  
            if (LuaToBoolean(_l, 1) == 0)
                return 1;  
            else if (LuaIsBoolean(_l, 1))
            {
                LuaNewTable(_l);  
                LuaPushValue(_l, -1);  
                LuaPushBoolean(_l, 1);
                LuaRawSet(_l, LuaUpValueIndex(1)); 
            }
            else
            {
                int _valid_proxy = 0;  
                if (LuaGetMetatable(_l, 1) != 0)
                {
                    LuaRawGet(_l, LuaUpValueIndex(1));
                    _valid_proxy = LuaToBoolean(_l, -1);
                    LuaPop(_l, 1);  
                }
                LuaLArgCheck(_l, _valid_proxy != 0, 1, "boolean or proxy expected");
                LuaGetMetatable(_l, 1);  
            }
            LuaSetMetatable(_l, 2);
            return 1;
        }

        private readonly static LuaLReg[] _baseFuncs = {
          new LuaLReg("assert", LuaBAssert),
          new LuaLReg("collectgarbage", LuaBCollectGarbage),
          new LuaLReg("dofile", LuaBDoFile),
          new LuaLReg("error", LuaBError),
          new LuaLReg("gcinfo", LuaBGGInfo),
          new LuaLReg("getfenv", LuaBGetFEnv),
          new LuaLReg("getmetatable", LuaBGetMetatable),
          new LuaLReg("loadfile", LuaBLoadFile),
          new LuaLReg("load", LuaBLoad),
          new LuaLReg("loadstring", LuaBLoadString),
          new LuaLReg("next", LuaBNext),
          new LuaLReg("pcall", LuaBPCall),
          new LuaLReg("print", LuaBPrint),
          new LuaLReg("rawequal", LuaBRawEqual),
          new LuaLReg("rawget", LuaBRawGet),
          new LuaLReg("rawset", LuaBRawSet),
          new LuaLReg("select", LuaBSelect),
          new LuaLReg("setfenv", LuaBSetFEnv),
          new LuaLReg("setmetatable", LuaBSetMetatable),
          new LuaLReg("tonumber", LuaBToNumber),
          new LuaLReg("tostring", LuaBToString),
          new LuaLReg("type", LuaBType),
          new LuaLReg("unpack", LuaBUnpack),
          new LuaLReg("xpcall", LuaBXPCall),
          new LuaLReg(null, null)
        };

        private static int costatus(LuaState _l, LuaState _co)
        {
            if (_l == _co) return CO_RUN;
            switch (LuaStatus(_co))
            {
                case LUA_YIELD:
                    return CO_SUS;
                case 0:
                    {
                        LuaDebug ar = new LuaDebug();
                        if (LuaGetStack(_co, 0, ref ar) > 0) 
                            return CO_NOR;  
                        else if (LuaGetTop(_co) == 0)
                            return CO_DEAD;
                        else
                            return CO_SUS; 
                    }
                default:  
                    return CO_DEAD;
            }
        }

        private static int LuaBCosStatus(LuaState _l)
        {
            LuaState _co = LuaToThread(_l, 1);
            LuaLArgCheck(_l, _co != null, 1, "coroutine expected");
            LuaPushString(_l, _stat_names[costatus(_l, _co)]);
            return 1;
        }

        private static int AuxResume(LuaState _l, LuaState _co, int _n_arg)
        {
            int _status = costatus(_l, _co);
            if (LuaCheckStack(_co, _n_arg) == 0)
                LuaLError(_l, "too many arguments to resume");
            if (_status != CO_SUS)
            {
                LuaPushFString(_l, "cannot resume %s coroutine", _stat_names[_status]);
                return -1;  
            }
            LuaXMove(_l, _co, _n_arg);
            LuaSetLevel(_l, _co);
            _status = LuaResume(_co, _n_arg);
            if (_status == 0 || _status == LUA_YIELD)
            {
                int nres = LuaGetTop(_co);
                if (LuaCheckStack(_l, nres + 1) == 0)
                    LuaLError(_l, "too many results to resume");
                LuaXMove(_co, _l, nres); 
                return nres;
            }
            else
            {
                LuaXMove(_co, _l, 1); 
                return -1;  
            }
        }

        private static int LuaBCorResume(LuaState _l)
        {
            LuaState _co = LuaToThread(_l, 1);
            int _r;
            LuaLArgCheck(_l, _co != null, 1, "coroutine expected");
            _r = AuxResume(_l, _co, LuaGetTop(_l) - 1);
            if (_r < 0)
            {
                LuaPushBoolean(_l, 0);
                LuaInsert(_l, -2);
                return 2;  
            }
            else
            {
                LuaPushBoolean(_l, 1);
                LuaInsert(_l, -(_r + 1));
                return _r + 1;  
            }
        }

        private static int LuaBAuxWrap(LuaState _l)
        {
            LuaState co = LuaToThread(_l, LuaUpValueIndex(1));
            int _r = AuxResume(_l, co, LuaGetTop(_l));
            if (_r < 0)
            {
                if (LuaIsString(_l, -1) != 0)
                { 
                    LuaLWhere(_l, 1); 
                    LuaInsert(_l, -2);
                    LuaConcat(_l, 2);
                }
                LuaError(_l);  
            }
            return _r;
        }

        private static int LuaBCoCreate(LuaState _l)
        {
            LuaState _nl = LuaNewThread(_l);
            LuaLArgCheck(_l, LuaIsFunction(_l, 1) && !LuaIsCFunction(_l, 1), 1, "Lua function expected");
            LuaPushValue(_l, 1);  
            LuaXMove(_l, _nl, 1);
            return 1;
        }

        private static int LuaBCoWrap(LuaState _l)
        {
            LuaBCoCreate(_l);
            LuaPushCClosure(_l, LuaBAuxWrap, 1);
            return 1;
        }

        private static int LuaBYield(LuaState _l)
        {
            return LuaYield(_l, LuaGetTop(_l));
        }

        private static int LuaBCoRunning(LuaState _l)
        {
            if (LuaPushThread(_l) != 0)
                LuaPushNil(_l);  
            return 1;
        }

        private readonly static LuaLReg[] _co_funcs = {
          new LuaLReg("create", LuaBCoCreate),
          new LuaLReg("resume", LuaBCorResume),
          new LuaLReg("running", LuaBCoRunning),
          new LuaLReg("status", LuaBCosStatus),
          new LuaLReg("wrap", LuaBCoWrap),
          new LuaLReg("yield", LuaBYield),
          new LuaLReg(null, null)
        };

        private static void AuxOpen(LuaState _l, CharPtr _name, LuaNativeFunction _f, LuaNativeFunction _u)
        {
            LuaPushCFunction(_l, _u);
            LuaPushCClosure(_l, _f, 1);
            LuaSetField(_l, -2, _name);
        }

        private static void BaseOpen(LuaState L)
        {
            LuaPushValue(L, LUA_GLOBALSINDEX);
            LuaSetGlobal(L, "_G");
            LuaLRegister(L, "_G", _baseFuncs);
            LuaPushLiteral(L, LUA_VERSION);
            LuaSetGlobal(L, "_VERSION"); 
            AuxOpen(L, "ipairs", LuaBIPairs, CheckPairsAux);
            AuxOpen(L, "pairs", LuaBPairs, LuaBNext);
            LuaCreateTable(L, 0, 1); 
            LuaPushValue(L, -1); 
            LuaSetMetatable(L, -2);
            LuaPushLiteral(L, "kv");
            LuaSetField(L, -2, "__mode"); 
            LuaPushCClosure(L, LuaBNewProxy, 1);
            LuaSetGlobal(L, "newproxy");  
            LuaPushThread(L);
            LuaSetField(L, LUA_REGISTRYINDEX, "main_state");
        }

        public static int LuaOpenBase(LuaState _l)
        {
            BaseOpen(_l);
            LuaLRegister(_l, LUA_COLIBNAME, _co_funcs);
            return 2;
        }
    }
}