﻿using System;
using System.Diagnostics;

namespace KopiLua
{
    using Instruction = System.UInt32;
    using LuaNumberType = System.Double;
    using TValue = Lua.LuaTypeValue;

    public class InstructionPtr
    {
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public Instruction[] codes;
        public int pc;

        public InstructionPtr() { this.codes = null; ; this.pc = -1; }
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public InstructionPtr(Instruction[] _codes, int _pc)
        {
            this.codes = _codes; this.pc = _pc;
        }

        public static InstructionPtr Assign(InstructionPtr _ptr)
        {
            if (_ptr == null) return null;
            return new InstructionPtr(_ptr.codes, _ptr.pc);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public Instruction this[int _index]
        {
            get { return this.codes[pc + _index]; }
            set { this.codes[pc + _index] = value; }
        }

        public static InstructionPtr inc(ref InstructionPtr _ptr)
        {
            InstructionPtr _result = new InstructionPtr(_ptr.codes, _ptr.pc);
            _ptr.pc++;
            return _result;
        }

        public static InstructionPtr dec(ref InstructionPtr _ptr)
        {
            InstructionPtr _result = new InstructionPtr(_ptr.codes, _ptr.pc);
            _ptr.pc--;
            return _result;
        }

        public static bool operator <(InstructionPtr _p_1, InstructionPtr _p_2)
        {
            Debug.Assert(_p_1.codes == _p_2.codes);
            return _p_1.pc < _p_2.pc;
        }

        public static bool operator >(InstructionPtr _p_1, InstructionPtr _p_2)
        {
            Debug.Assert(_p_1.codes == _p_2.codes);
            return _p_1.pc > _p_2.pc;
        }

        public static bool operator <=(InstructionPtr _p_1, InstructionPtr _p_2)
        {
            Debug.Assert(_p_1.codes == _p_2.codes);
            return _p_1.pc < _p_2.pc;
        }

        public static bool operator >=(InstructionPtr _p_1, InstructionPtr _p_2)
        {
            Debug.Assert(_p_1.codes == _p_2.codes);
            return _p_1.pc > _p_2.pc;
        }
    };

    public partial class Lua
    {
        public const int NO_JUMP = (-1);

        public enum BinOpr
        {
            OPR_ADD, OPR_SUB, OPR_MUL, OPR_DIV, OPR_MOD, OPR_POW,
            OPR_CONCAT,
            OPR_NE, OPR_EQ,
            OPR_LT, OPR_LE, OPR_GT, OPR_GE,
            OPR_AND, OPR_OR,
            OPR_NOBINOPR
        };

        public enum UnOpr { OPR_MINUS, OPR_NOT, OPR_LEN, OPR_NOUNOPR };

        public static InstructionPtr GetCode(FuncState _fs, expdesc _e) { return new InstructionPtr(_fs.f.code, _e.u.__s.info); }

        public static int LuaKCodeAsBx(FuncState _fs, OpCode _o, int _a, int _sbx) { return LuaKCodeABx(_fs, _o, _a, _sbx + MAXARG_sBx); }

        public static void LuaKSetMultRet(FuncState _fs, expdesc _e) { LuaKSetReturns(_fs, _e, LUA_MULTRET); }

        public static bool HasJumps(expdesc _e) { return _e.t != _e.f; }

        private static int IsNumeral(expdesc _e)
        {
            return (_e.k == expkind.VKNUM && _e.t == NO_JUMP && _e.f == NO_JUMP) ? 1 : 0;
        }

        public static void LuaKNil(FuncState _fs, int _from, int _n)
        {
            InstructionPtr _previous;
            if (_fs.pc > _fs.lastTarget)
            {
                if (_fs.pc == 0)
                {
                    if (_from >= _fs.nActVar)
                        return;
                }
                else
                {
                    _previous = new InstructionPtr(_fs.f.code, _fs.pc - 1);
                    if (GET_OPCODE(_previous) == OpCode.OP_LOADNIL)
                    {
                        int _p_from = GETARG_A(_previous);
                        int _p_to = GETARG_B(_previous);
                        if (_p_from <= _from && _from <= _p_to + 1)
                        {
                            if (_from + _n - 1 > _p_to)
                                SETARG_B(_previous, _from + _n - 1);
                            return;
                        }
                    }
                }
            }
            LuaKCodeABC(_fs, OpCode.OP_LOADNIL, _from, _from + _n - 1, 0);
        }

        public static int LuaKJump(FuncState _fs)
        {
            int _jpc = _fs.jpc;
            int _j;
            _fs.jpc = NO_JUMP;
            _j = LuaKCodeAsBx(_fs, OpCode.OP_JMP, 0, NO_JUMP);
            LuaKConcat(_fs, ref _j, _jpc);
            return _j;
        }

        public static void LuaKRet(FuncState _fs, int _first, int _nret)
        {
            LuaKCodeABC(_fs, OpCode.OP_RETURN, _first, _nret + 1, 0);
        }

        private static int CondJump(FuncState _fs, OpCode _op, int _a, int _b, int _c)
        {
            LuaKCodeABC(_fs, _op, _a, _b, _c);
            return LuaKJump(_fs);
        }

        private static void FixJump(FuncState _fs, int _pc, int _dest)
        {
            InstructionPtr _jmp = new InstructionPtr(_fs.f.code, _pc);
            int _offset = _dest - (_pc + 1);
            LuaAssert(_dest != NO_JUMP);
            if (Math.Abs(_offset) > MAXARG_sBx)
                LuaXSyntaxError(_fs.ls, "control structure too long");
            SETARG_sBx(_jmp, _offset);
        }

        public static int LuaKGetLabel(FuncState _fs)
        {
            _fs.lastTarget = _fs.pc;
            return _fs.pc;
        }

        private static int GetJump(FuncState _fs, int _pc)
        {
            int offset = GETARG_sBx(_fs.f.code[_pc]);
            if (offset == NO_JUMP)
                return NO_JUMP;
            else
                return (_pc + 1) + offset;
        }

        private static InstructionPtr GetJumpControl(FuncState _fs, int _pc)
        {
            InstructionPtr _pi = new InstructionPtr(_fs.f.code, _pc);
            if (_pc >= 1 && (testTMode(GET_OPCODE(_pi[-1])) != 0))
                return new InstructionPtr(_pi.codes, _pi.pc - 1);
            else
                return new InstructionPtr(_pi.codes, _pi.pc);
        }

        private static int NeedValue(FuncState _fs, int _list)
        {
            for (; _list != NO_JUMP; _list = GetJump(_fs, _list))
            {
                InstructionPtr _i = GetJumpControl(_fs, _list);
                if (GET_OPCODE(_i[0]) != OpCode.OP_TESTSET) return 1;
            }
            return 0;
        }

        private static int PatchTestReg(FuncState _fs, int _node, int _reg)
        {
            InstructionPtr _i = GetJumpControl(_fs, _node);
            if (GET_OPCODE(_i[0]) != OpCode.OP_TESTSET)
                return 0;
            if (_reg != NO_REG && _reg != GETARG_B(_i[0]))
                SETARG_A(_i, _reg);
            else
                _i[0] = (uint)CREATE_ABC(OpCode.OP_TEST, GETARG_B(_i[0]), 0, GETARG_C(_i[0]));
            return 1;
        }

        private static void RemoveValues(FuncState _fs, int _list)
        {
            for (; _list != NO_JUMP; _list = GetJump(_fs, _list))
                PatchTestReg(_fs, _list, NO_REG);
        }

        private static void PatchListAux(FuncState _fs, int _list, int _v_target, int _reg, int _d_target)
        {
            while (_list != NO_JUMP)
            {
                int _next = GetJump(_fs, _list);
                if (PatchTestReg(_fs, _list, _reg) != 0)
                    FixJump(_fs, _list, _v_target);
                else
                    FixJump(_fs, _list, _d_target);
                _list = _next;
            }
        }

        private static void DischargeJPC(FuncState _fs)
        {
            PatchListAux(_fs, _fs.jpc, _fs.pc, NO_REG, _fs.pc);
            _fs.jpc = NO_JUMP;
        }

        public static void LuaKPatchList(FuncState _fs, int _list, int _target)
        {
            if (_target == _fs.pc)
                LuaKPatchToHere(_fs, _list);
            else
            {
                LuaAssert(_target < _fs.pc);
                PatchListAux(_fs, _list, _target, NO_REG, _target);
            }
        }

        public static void LuaKPatchToHere(FuncState _fs, int _list)
        {
            LuaKGetLabel(_fs);
            LuaKConcat(_fs, ref _fs.jpc, _list);
        }

        public static void LuaKConcat(FuncState _fs, ref int _l_1, int _l_2)
        {
            if (_l_2 == NO_JUMP) return;
            else if (_l_1 == NO_JUMP)
                _l_1 = _l_2;
            else
            {
                int _list = _l_1;
                int _next;
                while ((_next = GetJump(_fs, _list)) != NO_JUMP)
                    _list = _next;
                FixJump(_fs, _list, _l_2);
            }
        }

        public static void LuaKCheckStack(FuncState _fs, int _n)
        {
            int _newstack = _fs.freeReg + _n;
            if (_newstack > _fs.f.maxStackSize)
            {
                if (_newstack >= MAXSTACK)
                    LuaXSyntaxError(_fs.ls, "function or expression too complex");
                _fs.f.maxStackSize = CastByte(_newstack);
            }
        }

        public static void LuaKReserveRegs(FuncState _fs, int _n)
        {
            LuaKCheckStack(_fs, _n);
            _fs.freeReg += _n;
        }

        private static void FreeReg(FuncState _fs, int _reg)
        {
            if ((ISK(_reg) == 0) && _reg >= _fs.nActVar)
            {
                _fs.freeReg--;
                LuaAssert(_reg == _fs.freeReg);
            }
        }

        private static void FreeExp(FuncState _fs, expdesc _e)
        {
            if (_e.k == expkind.VNONRELOC)
                FreeReg(_fs, _e.u.__s.info);
        }

        private static int AddK(FuncState _fs, TValue _k, TValue _v)
        {
            LuaState _lL = _fs.L;
            TValue _idx = LuaHSet(_lL, _fs.h, _k);
            Proto _f = _fs.f;
            int _old_size = _f.sizeK;
            if (TTIsNumber(_idx))
            {
                LuaAssert(LuaORawEqualObj(_fs.f.k[CastInt(NValue(_idx))], _v));
                return CastInt(NValue(_idx));
            }
            else
            {
                SetNValue(_idx, CastNum(_fs.nk));
                LuaMGrowVector(_lL, ref _f.k, _fs.nk, ref _f.sizeK, MAXARG_Bx, "constant table overflow");
                while (_old_size < _f.sizeK) SetNilValue(_f.k[_old_size++]);
                SetObj(_lL, _f.k[_fs.nk], _v);
                LuaCBarrier(_lL, _f, _v);
                return _fs.nk++;
            }
        }

        public static int LuaKStringK(FuncState _fs, TString _s)
        {
            TValue _o = new LuaTypeValue();
            SetSValue(_fs.L, _o, _s);
            return AddK(_fs, _o, _o);
        }

        public static int LuaKNumberK(FuncState _fs, LuaNumberType _r)
        {
            TValue _o = new LuaTypeValue();
            SetNValue(_o, _r);
            return AddK(_fs, _o, _o);
        }

        private static int BoolValueK(FuncState _fs, int _b)
        {
            TValue _o = new LuaTypeValue();
            SetBValue(_o, _b);
            return AddK(_fs, _o, _o);
        }

        private static int NilK(FuncState _fs)
        {
            TValue k = new LuaTypeValue(), v = new LuaTypeValue();
            SetNilValue(v);
            SetHValue(_fs.L, k, _fs.h);
            return AddK(_fs, k, v);
        }

        public static void LuaKSetReturns(FuncState _fs, expdesc _e, int _n_results)
        {
            if (_e.k == expkind.VCALL)
                SETARG_C(GetCode(_fs, _e), _n_results + 1);
            else if (_e.k == expkind.VVARARG)
            {
                SETARG_B(GetCode(_fs, _e), _n_results + 1);
                SETARG_A(GetCode(_fs, _e), _fs.freeReg);
                LuaKReserveRegs(_fs, 1);
            }
        }

        public static void LuaKSetOneRet(FuncState _fs, expdesc _e)
        {
            if (_e.k == expkind.VCALL)
            {
                _e.k = expkind.VNONRELOC;
                _e.u.__s.info = GETARG_A(GetCode(_fs, _e));
            }
            else if (_e.k == expkind.VVARARG)
            {
                SETARG_B(GetCode(_fs, _e), 2);
                _e.k = expkind.VRELOCABLE;
            }
        }

        public static void LuaKDischargeVars(FuncState _fs, expdesc _e)
        {
            switch (_e.k)
            {
                case expkind.VLOCAL:
                    {
                        _e.k = expkind.VNONRELOC;
                        break;
                    }
                case expkind.VUPVAL:
                    {
                        _e.u.__s.info = LuaKCodeABC(_fs, OpCode.OP_GETUPVAL, 0, _e.u.__s.info, 0);
                        _e.k = expkind.VRELOCABLE;
                        break;
                    }
                case expkind.VGLOBAL:
                    {
                        _e.u.__s.info = LuaKCodeABx(_fs, OpCode.OP_GETGLOBAL, 0, _e.u.__s.info);
                        _e.k = expkind.VRELOCABLE;
                        break;
                    }
                case expkind.VINDEXED:
                    {
                        FreeReg(_fs, _e.u.__s.aux);
                        FreeReg(_fs, _e.u.__s.info);
                        _e.u.__s.info = LuaKCodeABC(_fs, OpCode.OP_GETTABLE, 0, _e.u.__s.info, _e.u.__s.aux);
                        _e.k = expkind.VRELOCABLE;
                        break;
                    }
                case expkind.VVARARG:
                case expkind.VCALL:
                    {
                        LuaKSetOneRet(_fs, _e);
                        break;
                    }
                default: break;
            }
        }

        private static int CodeLabel(FuncState _fs, int _a, int _b, int _jump)
        {
            LuaKGetLabel(_fs);
            return LuaKCodeABC(_fs, OpCode.OP_LOADBOOL, _a, _b, _jump);
        }

        private static void Discharge2Reg(FuncState _fs, expdesc _e, int _reg)
        {
            LuaKDischargeVars(_fs, _e);
            switch (_e.k)
            {
                case expkind.VNIL:
                    {
                        LuaKNil(_fs, _reg, 1);
                        break;
                    }
                case expkind.VFALSE:
                case expkind.VTRUE:
                    {
                        LuaKCodeABC(_fs, OpCode.OP_LOADBOOL, _reg, (_e.k == expkind.VTRUE) ? 1 : 0, 0);
                        break;
                    }
                case expkind.VK:
                    {
                        LuaKCodeABx(_fs, OpCode.OP_LOADK, _reg, _e.u.__s.info);
                        break;
                    }
                case expkind.VKNUM:
                    {
                        LuaKCodeABx(_fs, OpCode.OP_LOADK, _reg, LuaKNumberK(_fs, _e.u.nval));
                        break;
                    }
                case expkind.VRELOCABLE:
                    {
                        InstructionPtr pc = GetCode(_fs, _e);
                        SETARG_A(pc, _reg);
                        break;
                    }
                case expkind.VNONRELOC:
                    {
                        if (_reg != _e.u.__s.info)
                            LuaKCodeABC(_fs, OpCode.OP_MOVE, _reg, _e.u.__s.info, 0);
                        break;
                    }
                default:
                    {
                        LuaAssert(_e.k == expkind.VVOID || _e.k == expkind.VJMP);
                        return;  /* nothing to do... */
                    }
            }
            _e.u.__s.info = _reg;
            _e.k = expkind.VNONRELOC;
        }

        private static void Discharge2AnyReg(FuncState _fs, expdesc _e)
        {
            if (_e.k != expkind.VNONRELOC)
            {
                LuaKReserveRegs(_fs, 1);
                Discharge2Reg(_fs, _e, _fs.freeReg - 1);
            }
        }

        private static void Exp2Reg(FuncState _fs, expdesc _e, int _reg)
        {
            Discharge2Reg(_fs, _e, _reg);
            if (_e.k == expkind.VJMP)
                LuaKConcat(_fs, ref _e.t, _e.u.__s.info);
            if (HasJumps(_e))
            {
                int _final;
                int _p_f = NO_JUMP;
                int _p_t = NO_JUMP;
                if (NeedValue(_fs, _e.t) != 0 || NeedValue(_fs, _e.f) != 0)
                {
                    int _fj = (_e.k == expkind.VJMP) ? NO_JUMP : LuaKJump(_fs);
                    _p_f = CodeLabel(_fs, _reg, 0, 1);
                    _p_t = CodeLabel(_fs, _reg, 1, 0);
                    LuaKPatchToHere(_fs, _fj);
                }
                _final = LuaKGetLabel(_fs);
                PatchListAux(_fs, _e.f, _final, _reg, _p_f);
                PatchListAux(_fs, _e.t, _final, _reg, _p_t);
            }
            _e.f = _e.t = NO_JUMP;
            _e.u.__s.info = _reg;
            _e.k = expkind.VNONRELOC;
        }

        public static void LuaKExp2NextReg(FuncState _fs, expdesc _e)
        {
            LuaKDischargeVars(_fs, _e);
            FreeExp(_fs, _e);
            LuaKReserveRegs(_fs, 1);
            Exp2Reg(_fs, _e, _fs.freeReg - 1);
        }

        public static int LuaKExp2AnyReg(FuncState _fs, expdesc _e)
        {
            LuaKDischargeVars(_fs, _e);
            if (_e.k == expkind.VNONRELOC)
            {
                if (!HasJumps(_e)) return _e.u.__s.info;
                if (_e.u.__s.info >= _fs.nActVar)
                {
                    Exp2Reg(_fs, _e, _e.u.__s.info);
                    return _e.u.__s.info;
                }
            }
            LuaKExp2NextReg(_fs, _e);
            return _e.u.__s.info;
        }

        public static void LuaKExp2Val(FuncState _fs, expdesc _e)
        {
            if (HasJumps(_e))
                LuaKExp2AnyReg(_fs, _e);
            else
                LuaKDischargeVars(_fs, _e);
        }

        public static int LuaKExp2RK(FuncState _fs, expdesc _e)
        {
            LuaKExp2Val(_fs, _e);
            switch (_e.k)
            {
                case expkind.VKNUM:
                case expkind.VTRUE:
                case expkind.VFALSE:
                case expkind.VNIL:
                    {
                        if (_fs.nk <= MAXINDEXRK)
                        {
                            _e.u.__s.info = (_e.k == expkind.VNIL) ? NilK(_fs) : (_e.k == expkind.VKNUM) ? LuaKNumberK(_fs, _e.u.nval) : BoolValueK(_fs, (_e.k == expkind.VTRUE) ? 1 : 0);
                            _e.k = expkind.VK;
                            return RKASK(_e.u.__s.info);
                        }
                        else break;
                    }
                case expkind.VK:
                    {
                        if (_e.u.__s.info <= MAXINDEXRK)
                            return RKASK(_e.u.__s.info);
                        else break;
                    }
                default: break;
            }
            return LuaKExp2AnyReg(_fs, _e);
        }


        public static void LuaKStoreVar(FuncState _fs, expdesc _var, expdesc _ex)
        {
            switch (_var.k)
            {
                case expkind.VLOCAL:
                    {
                        FreeExp(_fs, _ex);
                        Exp2Reg(_fs, _ex, _var.u.__s.info);
                        return;
                    }
                case expkind.VUPVAL:
                    {
                        int e = LuaKExp2AnyReg(_fs, _ex);
                        LuaKCodeABC(_fs, OpCode.OP_SETUPVAL, e, _var.u.__s.info, 0);
                        break;
                    }
                case expkind.VGLOBAL:
                    {
                        int e = LuaKExp2AnyReg(_fs, _ex);
                        LuaKCodeABx(_fs, OpCode.OP_SETGLOBAL, e, _var.u.__s.info);
                        break;
                    }
                case expkind.VINDEXED:
                    {
                        int e = LuaKExp2RK(_fs, _ex);
                        LuaKCodeABC(_fs, OpCode.OP_SETTABLE, _var.u.__s.info, _var.u.__s.aux, e);
                        break;
                    }
                default:
                    {
                        LuaAssert(0);
                        break;
                    }
            }
            FreeExp(_fs, _ex);
        }

        public static void LuaKSelf(FuncState _fs, expdesc _e, expdesc _key)
        {
            int _func;
            LuaKExp2AnyReg(_fs, _e);
            FreeExp(_fs, _e);
            _func = _fs.freeReg;
            LuaKReserveRegs(_fs, 2);
            LuaKCodeABC(_fs, OpCode.OP_SELF, _func, _e.u.__s.info, LuaKExp2RK(_fs, _key));
            FreeExp(_fs, _key);
            _e.u.__s.info = _func;
            _e.k = expkind.VNONRELOC;
        }

        private static void InvertJump(FuncState _fs, expdesc _e)
        {
            InstructionPtr _pc = GetJumpControl(_fs, _e.u.__s.info);
            LuaAssert(testTMode(GET_OPCODE(_pc[0])) != 0 && GET_OPCODE(_pc[0]) != OpCode.OP_TESTSET && GET_OPCODE(_pc[0]) != OpCode.OP_TEST);
            SETARG_A(_pc, (GETARG_A(_pc[0]) == 0) ? 1 : 0);
        }

        private static int JumpOnCond(FuncState _fs, expdesc _e, int _cond)
        {
            if (_e.k == expkind.VRELOCABLE)
            {
                InstructionPtr _ie = GetCode(_fs, _e);
                if (GET_OPCODE(_ie) == OpCode.OP_NOT)
                {
                    _fs.pc--;
                    return CondJump(_fs, OpCode.OP_TEST, GETARG_B(_ie), 0, (_cond == 0) ? 1 : 0);
                }
            }
            Discharge2AnyReg(_fs, _e);
            FreeExp(_fs, _e);
            return CondJump(_fs, OpCode.OP_TESTSET, NO_REG, _e.u.__s.info, _cond);
        }

        public static void LuaKGoIfTrue(FuncState _fs, expdesc _e)
        {
            int _pc;
            LuaKDischargeVars(_fs, _e);
            switch (_e.k)
            {
                case expkind.VK:
                case expkind.VKNUM:
                case expkind.VTRUE:
                    {
                        _pc = NO_JUMP;
                        break;
                    }
                case expkind.VJMP:
                    {
                        InvertJump(_fs, _e);
                        _pc = _e.u.__s.info;
                        break;
                    }
                default:
                    {
                        _pc = JumpOnCond(_fs, _e, 0);
                        break;
                    }
            }
            LuaKConcat(_fs, ref _e.f, _pc);
            LuaKPatchToHere(_fs, _e.t);
            _e.t = NO_JUMP;
        }

        private static void LuaKGoIFalse(FuncState _fs, expdesc _e)
        {
            int _pc;
            LuaKDischargeVars(_fs, _e);
            switch (_e.k)
            {
                case expkind.VNIL:
                case expkind.VFALSE:
                    {
                        _pc = NO_JUMP;
                        break;
                    }
                case expkind.VJMP:
                    {
                        _pc = _e.u.__s.info;
                        break;
                    }
                default:
                    {
                        _pc = JumpOnCond(_fs, _e, 1);
                        break;
                    }
            }
            LuaKConcat(_fs, ref _e.t, _pc);
            LuaKPatchToHere(_fs, _e.f);
            _e.f = NO_JUMP;
        }

        private static void CodeNot(FuncState _fs, expdesc _e)
        {
            LuaKDischargeVars(_fs, _e);
            switch (_e.k)
            {
                case expkind.VNIL:
                case expkind.VFALSE:
                    {
                        _e.k = expkind.VTRUE;
                        break;
                    }
                case expkind.VK:
                case expkind.VKNUM:
                case expkind.VTRUE:
                    {
                        _e.k = expkind.VFALSE;
                        break;
                    }
                case expkind.VJMP:
                    {
                        InvertJump(_fs, _e);
                        break;
                    }
                case expkind.VRELOCABLE:
                case expkind.VNONRELOC:
                    {
                        Discharge2AnyReg(_fs, _e);
                        FreeExp(_fs, _e);
                        _e.u.__s.info = LuaKCodeABC(_fs, OpCode.OP_NOT, 0, _e.u.__s.info, 0);
                        _e.k = expkind.VRELOCABLE;
                        break;
                    }
                default:
                    {
                        LuaAssert(0);
                        break;
                    }
            }
            { int _temp = _e.f; _e.f = _e.t; _e.t = _temp; }
            RemoveValues(_fs, _e.f);
            RemoveValues(_fs, _e.t);
        }

        public static void LuaKIndexed(FuncState _fs, expdesc _t, expdesc _k)
        {
            _t.u.__s.aux = LuaKExp2RK(_fs, _k);
            _t.k = expkind.VINDEXED;
        }

        private static int ConstFolding(OpCode _op, expdesc _e_1, expdesc _e_2)
        {
            LuaNumberType _v_1, _v_2, _r;
            if ((IsNumeral(_e_1) == 0) || (IsNumeral(_e_2) == 0)) return 0;
            _v_1 = _e_1.u.nval;
            _v_2 = _e_2.u.nval;
            switch (_op)
            {
                case OpCode.OP_ADD: _r = LuaINumAdd(_v_1, _v_2); break;
                case OpCode.OP_SUB: _r = LuaINumSub(_v_1, _v_2); break;
                case OpCode.OP_MUL: _r = LuaINumMul(_v_1, _v_2); break;
                case OpCode.OP_DIV:
                    if (_v_2 == 0) return 0;
                    _r = LuaINumDiv(_v_1, _v_2); break;
                case OpCode.OP_MOD:
                    if (_v_2 == 0) return 0;
                    _r = LuaINumMod(_v_1, _v_2); break;
                case OpCode.OP_POW: _r = LuaINumPow(_v_1, _v_2); break;
                case OpCode.OP_UNM: _r = LuaINumUnm(_v_1); break;
                case OpCode.OP_LEN: return 0;
                default: LuaAssert(0); _r = 0; break;
            }
            if (LuaINumIsNaN(_r)) return 0;
            _e_1.u.nval = _r;
            return 1;
        }

        private static void CodeArith(FuncState _fs, OpCode _op, expdesc _e_1, expdesc _e_2)
        {
            if (ConstFolding(_op, _e_1, _e_2) != 0)
                return;
            else
            {
                int o2 = (_op != OpCode.OP_UNM && _op != OpCode.OP_LEN) ? LuaKExp2RK(_fs, _e_2) : 0;
                int o1 = LuaKExp2RK(_fs, _e_1);
                if (o1 > o2)
                {
                    FreeExp(_fs, _e_1);
                    FreeExp(_fs, _e_2);
                }
                else
                {
                    FreeExp(_fs, _e_2);
                    FreeExp(_fs, _e_1);
                }
                _e_1.u.__s.info = LuaKCodeABC(_fs, _op, 0, o1, o2);
                _e_1.k = expkind.VRELOCABLE;
            }
        }

        private static void CodeComp(FuncState _fs, OpCode _op, int _cond, expdesc _e_1, expdesc _e_2)
        {
            int _o_1 = LuaKExp2RK(_fs, _e_1);
            int _o_2 = LuaKExp2RK(_fs, _e_2);
            FreeExp(_fs, _e_2);
            FreeExp(_fs, _e_1);
            if (_cond == 0 && _op != OpCode.OP_EQ)
            {
                int _temp;
                _temp = _o_1; _o_1 = _o_2; _o_2 = _temp;
                _cond = 1;
            }
            _e_1.u.__s.info = CondJump(_fs, _op, _cond, _o_1, _o_2);
            _e_1.k = expkind.VJMP;
        }

        public static void LuaKPrefix(FuncState _fs, UnOpr _op, expdesc _e)
        {
            expdesc _e_2 = new expdesc();
            _e_2.t = _e_2.f = NO_JUMP; _e_2.k = expkind.VKNUM; _e_2.u.nval = 0;
            switch (_op)
            {
                case UnOpr.OPR_MINUS:
                    {
                        if (IsNumeral(_e) == 0)
                            LuaKExp2AnyReg(_fs, _e);
                        CodeArith(_fs, OpCode.OP_UNM, _e, _e_2);
                        break;
                    }
                case UnOpr.OPR_NOT: CodeNot(_fs, _e); break;
                case UnOpr.OPR_LEN:
                    {
                        LuaKExp2AnyReg(_fs, _e);
                        CodeArith(_fs, OpCode.OP_LEN, _e, _e_2);
                        break;
                    }
                default: LuaAssert(0); break;
            }
        }

        public static void LuaKInfix(FuncState _fs, BinOpr _op, expdesc _v)
        {
            switch (_op)
            {
                case BinOpr.OPR_AND:
                    {
                        LuaKGoIfTrue(_fs, _v);
                        break;
                    }
                case BinOpr.OPR_OR:
                    {
                        LuaKGoIFalse(_fs, _v);
                        break;
                    }
                case BinOpr.OPR_CONCAT:
                    {
                        LuaKExp2NextReg(_fs, _v);
                        break;
                    }
                case BinOpr.OPR_ADD:
                case BinOpr.OPR_SUB:
                case BinOpr.OPR_MUL:
                case BinOpr.OPR_DIV:
                case BinOpr.OPR_MOD:
                case BinOpr.OPR_POW:
                    {
                        if ((IsNumeral(_v) == 0)) LuaKExp2RK(_fs, _v);
                        break;
                    }
                default:
                    {
                        LuaKExp2RK(_fs, _v);
                        break;
                    }
            }
        }

        public static void LuaKPosFix(FuncState _fs, BinOpr _op, expdesc _e_1, expdesc _e_2)
        {
            switch (_op)
            {
                case BinOpr.OPR_AND:
                    {
                        LuaAssert(_e_1.t == NO_JUMP);
                        LuaKDischargeVars(_fs, _e_2);
                        LuaKConcat(_fs, ref _e_2.f, _e_1.f);
                        _e_1.Copy(_e_2);
                        break;
                    }
                case BinOpr.OPR_OR:
                    {
                        LuaAssert(_e_1.f == NO_JUMP);
                        LuaKDischargeVars(_fs, _e_2);
                        LuaKConcat(_fs, ref _e_2.t, _e_1.t);
                        _e_1.Copy(_e_2);
                        break;
                    }
                case BinOpr.OPR_CONCAT:
                    {
                        LuaKExp2Val(_fs, _e_2);
                        if (_e_2.k == expkind.VRELOCABLE && GET_OPCODE(GetCode(_fs, _e_2)) == OpCode.OP_CONCAT)
                        {
                            LuaAssert(_e_1.u.__s.info == GETARG_B(GetCode(_fs, _e_2)) - 1);
                            FreeExp(_fs, _e_1);
                            SETARG_B(GetCode(_fs, _e_2), _e_1.u.__s.info);
                            _e_1.k = expkind.VRELOCABLE; _e_1.u.__s.info = _e_2.u.__s.info;
                        }
                        else
                        {
                            LuaKExp2NextReg(_fs, _e_2);
                            CodeArith(_fs, OpCode.OP_CONCAT, _e_1, _e_2);
                        }
                        break;
                    }
                case BinOpr.OPR_ADD: CodeArith(_fs, OpCode.OP_ADD, _e_1, _e_2); break;
                case BinOpr.OPR_SUB: CodeArith(_fs, OpCode.OP_SUB, _e_1, _e_2); break;
                case BinOpr.OPR_MUL: CodeArith(_fs, OpCode.OP_MUL, _e_1, _e_2); break;
                case BinOpr.OPR_DIV: CodeArith(_fs, OpCode.OP_DIV, _e_1, _e_2); break;
                case BinOpr.OPR_MOD: CodeArith(_fs, OpCode.OP_MOD, _e_1, _e_2); break;
                case BinOpr.OPR_POW: CodeArith(_fs, OpCode.OP_POW, _e_1, _e_2); break;
                case BinOpr.OPR_EQ: CodeComp(_fs, OpCode.OP_EQ, 1, _e_1, _e_2); break;
                case BinOpr.OPR_NE: CodeComp(_fs, OpCode.OP_EQ, 0, _e_1, _e_2); break;
                case BinOpr.OPR_LT: CodeComp(_fs, OpCode.OP_LT, 1, _e_1, _e_2); break;
                case BinOpr.OPR_LE: CodeComp(_fs, OpCode.OP_LE, 1, _e_1, _e_2); break;
                case BinOpr.OPR_GT: CodeComp(_fs, OpCode.OP_LT, 0, _e_1, _e_2); break;
                case BinOpr.OPR_GE: CodeComp(_fs, OpCode.OP_LE, 0, _e_1, _e_2); break;
                default: LuaAssert(0); break;
            }
        }

        public static void LuaKFixLine(FuncState _fs, int _line)
        {
            _fs.f.lineInfo[_fs.pc - 1] = _line;
        }

        private static int LuaKCode(FuncState _fs, int _i, int _line)
        {
            Proto _f = _fs.f;
            DischargeJPC(_fs);
            LuaMGrowVector(_fs.L, ref _f.code, _fs.pc, ref _f.sizeCode, MAXINT, "code size overflow");
            _f.code[_fs.pc] = (uint)_i;
            LuaMGrowVector(_fs.L, ref _f.lineInfo, _fs.pc, ref _f.sizeLineInfo, MAXINT, "code size overflow");
            _f.lineInfo[_fs.pc] = _line;
            return _fs.pc++;
        }

        public static int LuaKCodeABC(FuncState _fs, OpCode _o, int _a, int _b, int _c)
        {
            LuaAssert(getOpMode(_o) == OpMode.iABC);
            LuaAssert(getBMode(_o) != OpArgMask.OpArgN || _b == 0);
            LuaAssert(getCMode(_o) != OpArgMask.OpArgN || _c == 0);
            return LuaKCode(_fs, CREATE_ABC(_o, _a, _b, _c), _fs.ls.lastLine);
        }

        public static int LuaKCodeABx(FuncState _fs, OpCode _o, int _a, int _bc)
        {
            LuaAssert(getOpMode(_o) == OpMode.iABx || getOpMode(_o) == OpMode.iAsBx);
            LuaAssert(getCMode(_o) == OpArgMask.OpArgN);
            return LuaKCode(_fs, CREATE_ABx(_o, _a, _bc), _fs.ls.lastLine);
        }

        public static void LuaKSetList(FuncState _fs, int _base, int _n_elems, int _to_store)
        {
            int _c = (_n_elems - 1) / LFIELDS_PER_FLUSH + 1;
            int _b = (_to_store == LUA_MULTRET) ? 0 : _to_store;
            LuaAssert(_to_store != 0);
            if (_c <= MAXARG_C)
                LuaKCodeABC(_fs, OpCode.OP_SETLIST, _base, _b, _c);
            else
            {
                LuaKCodeABC(_fs, OpCode.OP_SETLIST, _base, _b, 0);
                LuaKCode(_fs, _c, _fs.ls.lastLine);
            }
            _fs.freeReg = _base + 1;
        }
    }
}