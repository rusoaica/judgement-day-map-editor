﻿namespace KopiLua
{
    public partial class Lua
    {
        private const string KEY_HOOK = "h";
        private static readonly string[] _hooknames = { "call", "return", "line", "count", "tail return" };
        public const int LEVELS1 = 12;
        public const int LEVELS2 = 10;

        private static int DBGetRegistry(LuaState _l)
        {
            LuaPushValue(_l, LUA_REGISTRYINDEX);
            return 1;
        }

        private static int DBGetMetatable(LuaState _l)
        {
            LuaLCheckAny(_l, 1);
            if (LuaGetMetatable(_l, 1) == 0)
                LuaPushNil(_l);
            return 1;
        }

        private static int DBSetMetatable(LuaState _l)
        {
            int t = LuaType(_l, 2);
            LuaLArgCheck(_l, t == LUA_TNIL || t == LUA_TTABLE, 2,
                              "nil or table expected");
            LuaSetTop(_l, 2);
            LuaPushBoolean(_l, LuaSetMetatable(_l, 1));
            return 1;
        }

        private static int DBGetFEnv(LuaState _l)
        {
            LuaLCheckAny(_l, 1);
            LuaGetFEnv(_l, 1);
            return 1;
        }

        private static int DBSetFEnv(LuaState _l)
        {
            LuaLCheckType(_l, 2, LUA_TTABLE);
            LuaSetTop(_l, 2);
            if (LuaSetFEnv(_l, 1) == 0)
                LuaLError(_l, LUA_QL("setfenv") + " cannot change environment of given object");
            return 1;
        }

        private static void SetTabsS(LuaState _l, CharPtr _i, CharPtr _v)
        {
            LuaPushString(_l, _v);
            LuaSetField(_l, -2, _i);
        }

        private static void SetTabSI(LuaState _l, CharPtr _i, int _v)
        {
            LuaPushInteger(_l, _v);
            LuaSetField(_l, -2, _i);
        }

        private static LuaState GetThread(LuaState _l, out int _arg)
        {
            if (LuaIsThread(_l, 1))
            {
                _arg = 1;
                return LuaToThread(_l, 1);
            }
            else
            {
                _arg = 0;
                return _l;
            }
        }

        private static void TreatStackOption(LuaState _l, LuaState _l_1, CharPtr _f_name)
        {
            if (_l == _l_1)
            {
                LuaPushValue(_l, -2);
                LuaRemove(_l, -3);
            }
            else
                LuaXMove(_l_1, _l, 1);
            LuaSetField(_l, -2, _f_name);
        }

        private static int DBGetInfo(LuaState _l)
        {
            LuaDebug _ar = new LuaDebug();
            int _arg;
            LuaState L1 = GetThread(_l, out _arg);
            CharPtr options = LuaLOptString(_l, _arg + 2, "flnSu");
            if (LuaIsNumber(_l, _arg + 1) != 0)
            {
                if (LuaGetStack(L1, (int)LuaToInteger(_l, _arg + 1), ref _ar) == 0)
                {
                    LuaPushNil(_l);
                    return 1;
                }
            }
            else if (LuaIsFunction(_l, _arg + 1))
            {
                LuaPushFString(_l, ">%s", options);
                options = LuaToString(_l, -1);
                LuaPushValue(_l, _arg + 1);
                LuaXMove(_l, L1, 1);
            }
            else
                return LuaLArgError(_l, _arg + 1, "function or level expected");
            if (LuaGetInfo(L1, options, ref _ar) == 0)
                return LuaLArgError(_l, _arg + 2, "invalid option");
            LuaCreateTable(_l, 0, 2);
            if (StrChr(options, 'S') != null)
            {
                SetTabsS(_l, "source", _ar.source);
                SetTabsS(_l, "short_src", _ar.shortSrc);
                SetTabSI(_l, "linedefined", _ar.lineDefined);
                SetTabSI(_l, "lastlinedefined", _ar.lastLineDefined);
                SetTabsS(_l, "what", _ar.what);
            }
            if (StrChr(options, 'l') != null)
                SetTabSI(_l, "currentline", _ar.currentLine);
            if (StrChr(options, 'u') != null)
                SetTabSI(_l, "nups", _ar.nups);
            if (StrChr(options, 'n') != null)
            {
                SetTabsS(_l, "name", _ar.name);
                SetTabsS(_l, "namewhat", _ar.nameWhat);
            }
            if (StrChr(options, 'L') != null)
                TreatStackOption(_l, L1, "activelines");
            if (StrChr(options, 'f') != null)
                TreatStackOption(_l, L1, "func");
            return 1;
        }

        private static int DBGetLocal(LuaState _l)
        {
            int _arg;
            LuaState _l_1 = GetThread(_l, out _arg);
            LuaDebug _ar = new LuaDebug();
            CharPtr _name;
            if (LuaGetStack(_l_1, LuaLCheckInt(_l, _arg + 1), ref _ar) == 0)
                return LuaLArgError(_l, _arg + 1, "level out of range");
            _name = LuaGetLocal(_l_1, _ar, LuaLCheckInt(_l, _arg + 2));
            if (_name != null)
            {
                LuaXMove(_l_1, _l, 1);
                LuaPushString(_l, _name);
                LuaPushValue(_l, -2);
                return 2;
            }
            else
            {
                LuaPushNil(_l);
                return 1;
            }
        }

        private static int DBSetLocal(LuaState _l)
        {
            int _arg;
            LuaState _l_1 = GetThread(_l, out _arg);
            LuaDebug _ar = new LuaDebug();
            if (LuaGetStack(_l_1, LuaLCheckInt(_l, _arg + 1), ref _ar) == 0)
                return LuaLArgError(_l, _arg + 1, "level out of range");
            LuaLCheckAny(_l, _arg + 3);
            LuaSetTop(_l, _arg + 3);
            LuaXMove(_l, _l_1, 1);
            LuaPushString(_l, LuaSetLocal(_l_1, _ar, LuaLCheckInt(_l, _arg + 2)));
            return 1;
        }

        private static int AuxUpValue(LuaState _l, int _get)
        {
            CharPtr _name;
            int n = LuaLCheckInt(_l, 2);
            LuaLCheckType(_l, 1, LUA_TFUNCTION);
            if (LuaIsCFunction(_l, 1)) return 0;
            _name = (_get != 0) ? LuaGetUpValue(_l, 1, n) : LuaSetUpValue(_l, 1, n);
            if (_name == null) return 0;
            LuaPushString(_l, _name);
            LuaInsert(_l, -(_get + 1));
            return _get + 1;
        }

        private static int DBGetUpValue(LuaState _l)
        {
            return AuxUpValue(_l, 1);
        }

        private static int DBSetUpValue(LuaState _l)
        {
            LuaLCheckAny(_l, 3);
            return AuxUpValue(_l, 0);
        }

        private static void HookF(LuaState _l, LuaDebug _ar)
        {
            LuaPushLightUserData(_l, KEY_HOOK);
            LuaRawGet(_l, LUA_REGISTRYINDEX);
            LuaPushLightUserData(_l, _l);
            LuaRawGet(_l, -2);
            if (LuaIsFunction(_l, -1))
            {
                LuaPushString(_l, _hooknames[(int)_ar.event_]);
                if (_ar.currentLine >= 0)
                    LuaPushInteger(_l, _ar.currentLine);
                else LuaPushNil(_l);
                LuaAssert(LuaGetInfo(_l, "lS", ref _ar));
                LuaCall(_l, 2, 0);
            }
        }

        private static int MakeMask(CharPtr _s_mask, int _count)
        {
            int _mask = 0;
            if (StrChr(_s_mask, 'c') != null) _mask |= LUA_MASKCALL;
            if (StrChr(_s_mask, 'r') != null) _mask |= LUA_MASKRET;
            if (StrChr(_s_mask, 'l') != null) _mask |= LUA_MASKLINE;
            if (_count > 0) _mask |= LUA_MASKCOUNT;
            return _mask;
        }

        private static CharPtr UnmakeMask(int _mask, CharPtr _s_mask)
        {
            int _i = 0;
            if ((_mask & LUA_MASKCALL) != 0) _s_mask[_i++] = 'c';
            if ((_mask & LUA_MASKRET) != 0) _s_mask[_i++] = 'r';
            if ((_mask & LUA_MASKLINE) != 0) _s_mask[_i++] = 'l';
            _s_mask[_i] = '\0';
            return _s_mask;
        }

        private static void GetHookTable(LuaState _l)
        {
            LuaPushLightUserData(_l, KEY_HOOK);
            LuaRawGet(_l, LUA_REGISTRYINDEX);
            if (!LuaIsTable(_l, -1))
            {
                LuaPop(_l, 1);
                LuaCreateTable(_l, 0, 1);
                LuaPushLightUserData(_l, KEY_HOOK);
                LuaPushValue(_l, -2);
                LuaRawSet(_l, LUA_REGISTRYINDEX);
            }
        }

        private static int DBSetHook(LuaState _l)
        {
            int _arg, _mask, _count;
            LuaHook _func;
            LuaState _l_1 = GetThread(_l, out _arg);
            if (LuaIsNoneOrNil(_l, _arg + 1))
            {
                LuaSetTop(_l, _arg + 1);
                _func = null; _mask = 0; _count = 0;
            }
            else
            {
                CharPtr smask = LuaLCheckString(_l, _arg + 2);
                LuaLCheckType(_l, _arg + 1, LUA_TFUNCTION);
                _count = LuaLOptInt(_l, _arg + 3, 0);
                _func = HookF; _mask = MakeMask(smask, _count);
            }
            GetHookTable(_l);
            LuaPushLightUserData(_l, _l_1);
            LuaPushValue(_l, _arg + 1);
            LuaRawSet(_l, -3);
            LuaPop(_l, 1);
            LuaSetHook(_l_1, _func, _mask, _count);
            return 0;
        }

        private static int DBGetHook(LuaState _l)
        {
            int _arg;
            LuaState _l_1 = GetThread(_l, out _arg);
            CharPtr _buff = new char[5];
            int _mask = LuaGetHookMask(_l_1);
            LuaHook _hook = LuaGetHook(_l_1);
            if (_hook != null && _hook != HookF)
                LuaPushLiteral(_l, "external hook");
            else
            {
                GetHookTable(_l);
                LuaPushLightUserData(_l, _l_1);
                LuaRawGet(_l, -2);
                LuaRemove(_l, -2);
            }
            LuaPushString(_l, UnmakeMask(_mask, _buff));
            LuaPushInteger(_l, LuaGetHookCount(_l_1));
            return 3;
        }

        private static int DBDebug(LuaState _l)
        {
            for (; ; )
            {
                CharPtr _buffer = new char[250];
                FPuts("lua_debug> ", stdErr);
                if (FGets(_buffer, stdIn) == null || StrCmp(_buffer, "cont\n") == 0)
                    return 0;
                if (LuaLLoadBuffer(_l, _buffer, (uint)StrLen(_buffer), "=(debug command)") != 0 ||
                    LuaPCall(_l, 0, 0, 0) != 0)
                {
                    FPuts(LuaToString(_l, -1), stdErr);
                    FPuts("\n", stdErr);
                }
                LuaSetTop(_l, 0);
            }
        }

        private static int DBErrorFB(LuaState _l)
        {
            int _level;
            bool _first_part = true;
            int _arg;
            LuaState _l_1 = GetThread(_l, out _arg);
            LuaDebug _ar = new LuaDebug();
            if (LuaIsNumber(_l, _arg + 2) != 0)
            {
                _level = (int)LuaToInteger(_l, _arg + 2);
                LuaPop(_l, 1);
            }
            else
                _level = (_l == _l_1) ? 1 : 0;
            if (LuaGetTop(_l) == _arg)
                LuaPushLiteral(_l, "");
            else if (LuaIsString(_l, _arg + 1) == 0) return 1;
            else LuaPushLiteral(_l, "\n");
            LuaPushLiteral(_l, "stack traceback:");
            while (LuaGetStack(_l_1, _level++, ref _ar) != 0)
            {
                if (_level > LEVELS1 && _first_part)
                {
                    if (LuaGetStack(_l_1, _level + LEVELS2, ref _ar) == 0)
                        _level--;
                    else
                    {
                        LuaPushLiteral(_l, "\n\t...");
                        while (LuaGetStack(_l_1, _level + LEVELS2, ref _ar) != 0)
                            _level++;
                    }
                    _first_part = false;
                    continue;
                }
                LuaPushLiteral(_l, "\n\t");
                LuaGetInfo(_l_1, "Snl", ref _ar);
                LuaPushFString(_l, "%s:", _ar.shortSrc);
                if (_ar.currentLine > 0)
                    LuaPushFString(_l, "%d:", _ar.currentLine);
                if (_ar.nameWhat != '\0')
                    LuaPushFString(_l, " in function " + LUA_QS, _ar.name);
                else
                {
                    if (_ar.what == 'm')
                        LuaPushFString(_l, " in main chunk");
                    else if (_ar.what == 'C' || _ar.what == 't')
                        LuaPushLiteral(_l, " ?");
                    else
                        LuaPushFString(_l, " in function <%s:%d>",
                                           _ar.shortSrc, _ar.lineDefined);
                }
                LuaConcat(_l, LuaGetTop(_l) - _arg);
            }
            LuaConcat(_l, LuaGetTop(_l) - _arg);
            return 1;
        }

        private readonly static LuaLReg[] dblib = {
          new LuaLReg("debug", DBDebug),
          new LuaLReg("getfenv", DBGetFEnv),
          new LuaLReg("gethook", DBGetHook),
          new LuaLReg("getinfo", DBGetInfo),
          new LuaLReg("getlocal", DBGetLocal),
          new LuaLReg("getregistry", DBGetRegistry),
          new LuaLReg("getmetatable", DBGetMetatable),
          new LuaLReg("getupvalue", DBGetUpValue),
          new LuaLReg("setfenv", DBSetFEnv),
          new LuaLReg("sethook", DBSetHook),
          new LuaLReg("setlocal", DBSetLocal),
          new LuaLReg("setmetatable", DBSetMetatable),
          new LuaLReg("setupvalue", DBSetUpValue),
          new LuaLReg("traceback", DBErrorFB),
          new LuaLReg(null, null)
        };

        public static int LuaOpenDebug(LuaState _l)
        {
            LuaLRegister(_l, LUA_DBLIBNAME, dblib);
            return 1;
        }
    }
}