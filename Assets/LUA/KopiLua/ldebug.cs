﻿using System;
using System.Diagnostics;

namespace KopiLua
{
    using Instruction = System.UInt32;
    using StkId = Lua.LuaTypeValue;
    using TValue = Lua.LuaTypeValue;

    public partial class Lua
    {
        public static int PCRel(InstructionPtr _pc, Proto _p)
        {
            Debug.Assert(_pc.codes == _p.code);
            return _pc.pc - 1;
        }

        public static int GetLine(Proto _f, int _pc) { return (_f.lineInfo != null) ? _f.lineInfo[_pc] : 0; }

        public static void ResetHookCount(LuaState _l) { _l.hookCount = _l.baseHookCount; }

        private static int CurrentPC(LuaState _l, CallInfo _ci)
        {
            if (!IsLua(_ci)) return -1;
            if (_ci == _l.ci)
                _ci.saveDpc = InstructionPtr.Assign(_l.saveDpc);
            return PCRel(_ci.saveDpc, CIFunc(_ci).l.p);
        }

        private static int CurrentLine(LuaState _l, CallInfo _ci)
        {
            int _pc = CurrentPC(_l, _ci);
            if (_pc < 0)
                return -1;
            else
                return GetLine(CIFunc(_ci).l.p, _pc);
        }

        public static int LuaSetHook(LuaState _l, LuaHook _func, int _mask, int _count)
        {
            if (_func == null || _mask == 0)
            {
                _mask = 0;
                _func = null;
            }
            _l.hook = _func;
            _l.baseHookCount = _count;
            ResetHookCount(_l);
            _l.hookMask = CastByte(_mask);
            return 1;
        }

        public static LuaHook LuaGetHook(LuaState _l)
        {
            return _l.hook;
        }

        public static int LuaGetHookMask(LuaState _l)
        {
            return _l.hookMask;
        }

        public static int LuaGetHookCount(LuaState _l)
        {
            return _l.baseHookCount;
        }

        public static int LuaGetStack(LuaState _l, int _level, ref LuaDebug _ar)
        {
            int _status;
            CallInfo _ci;
            LuaLock(_l);
            for (_ci = _l.ci; _level > 0 && _ci > _l.baseCi[0]; CallInfo.Dec(ref _ci))
            {
                _level--;
                if (FIsLua(_ci))
                    _level -= _ci.tailCalls;
            }
            if (_level == 0 && _ci > _l.baseCi[0])
            {
                _status = 1;
                _ar.iCi = _ci - _l.baseCi[0];
            }
            else if (_level < 0)
            {
                _status = 1;
                _ar.iCi = 0;
            }
            else _status = 0;
            LuaUnlock(_l);
            return _status;
        }

        private static Proto GetLuaProto(CallInfo _ci)
        {
            return (IsLua(_ci) ? CIFunc(_ci).l.p : null);
        }

        private static CharPtr FindLocal(LuaState _l, CallInfo _ci, int _n)
        {
            CharPtr _name;
            Proto _fp = GetLuaProto(_ci);
            if ((_fp != null) && (_name = LuaFGetLocalName(_fp, _n, CurrentPC(_l, _ci))) != null)
                return _name;
            else
            {
                StkId _limit = (_ci == _l.ci) ? _l.top : (_ci + 1).func;
                if (_limit - _ci.base_ >= _n && _n > 0)
                    return "(*temporary)";
                else
                    return null;
            }
        }

        public static CharPtr LuaGetLocal(LuaState _l, LuaDebug _ar, int _n)
        {
            CallInfo _ci = _l.baseCi[_ar.iCi];
            CharPtr _name = FindLocal(_l, _ci, _n);
            LuaLock(_l);
            if (_name != null)
                LuaAPushObject(_l, _ci.base_[_n - 1]);
            LuaUnlock(_l);
            return _name;
        }

        public static CharPtr LuaSetLocal(LuaState _l, LuaDebug _ar, int _n)
        {
            CallInfo _ci = _l.baseCi[_ar.iCi];
            CharPtr _name = FindLocal(_l, _ci, _n);
            LuaLock(_l);
            if (_name != null)
                SetObj2S(_l, _ci.base_[_n - 1], _l.top - 1);
            StkId.Dec(ref _l.top);
            LuaUnlock(_l);
            return _name;
        }

        private static void FuncInfo(LuaDebug _ar, Closure _cl)
        {
            if (_cl.c.isC != 0)
            {
                _ar.source = "=[C]";
                _ar.lineDefined = -1;
                _ar.lastLineDefined = -1;
                _ar.what = "C";
            }
            else
            {
                _ar.source = GetStr(_cl.l.p.source);
                _ar.lineDefined = _cl.l.p.lineDefined;
                _ar.lastLineDefined = _cl.l.p.lastLineDefined;
                _ar.what = (_ar.lineDefined == 0) ? "main" : "Lua";
            }
            LuaOChunkID(_ar.shortSrc, _ar.source, LUA_IDSIZE);
        }

        private static void InfoTailCall(LuaDebug _ar)
        {
            _ar.name = _ar.nameWhat = "";
            _ar.what = "tail";
            _ar.lastLineDefined = _ar.lineDefined = _ar.currentLine = -1;
            _ar.source = "=(tail call)";
            LuaOChunkID(_ar.shortSrc, _ar.source, LUA_IDSIZE);
            _ar.nups = 0;
        }

        private static void CollectValidLines(LuaState _l, Closure _f)
        {
            if (_f == null || (_f.c.isC != 0))
                SetNilValue(_l.top);
            else
            {
                Table _t = LuaHNew(_l, 0, 0);
                int[] _line_info = _f.l.p.lineInfo;
                int _i;
                for (_i = 0; _i < _f.l.p.sizeLineInfo; _i++)
                    SetBValue(LuaHSetNum(_l, _t, _line_info[_i]), 1);
                SetHValue(_l, _l.top, _t);
            }
            IncrTop(_l);
        }

        private static int AuxGetInfo(LuaState _l, CharPtr _what, LuaDebug _ar, Closure _f, CallInfo _ci)
        {
            int _status = 1;
            if (_f == null)
            {
                InfoTailCall(_ar);
                return _status;
            }
            for (; _what[0] != 0; _what = _what.next())
            {
                switch (_what[0])
                {
                    case 'S':
                        {
                            FuncInfo(_ar, _f);
                            break;
                        }
                    case 'l':
                        {
                            _ar.currentLine = (_ci != null) ? CurrentLine(_l, _ci) : -1;
                            break;
                        }
                    case 'u':
                        {
                            _ar.nups = _f.c.nupvalues;
                            break;
                        }
                    case 'n':
                        {
                            _ar.nameWhat = (_ci != null) ? GetFuncName(_l, _ci, ref _ar.name) : null;
                            if (_ar.nameWhat == null)
                            {
                                _ar.nameWhat = "";
                                _ar.name = null;
                            }
                            break;
                        }
                    case 'L':
                    case 'f':
                        break;
                    default: _status = 0; break;
                }
            }
            return _status;
        }

        public static int LuaGetInfo(LuaState _l, CharPtr _what, ref LuaDebug _ar)
        {
            int _status;
            Closure _f = null;
            CallInfo _ci = null;
            LuaLock(_l);
            if (_what == '>')
            {
                StkId _func = _l.top - 1;
                LuaIApiCheck(_l, TTIsFunction(_func));
                _what = _what.next();
                _f = CLValue(_func);
                StkId.Dec(ref _l.top);
            }
            else if (_ar.iCi != 0)
            {
                _ci = _l.baseCi[_ar.iCi];
                LuaAssert(TTIsFunction(_ci.func));
                _f = CLValue(_ci.func);
            }
            _status = AuxGetInfo(_l, _what, _ar, _f, _ci);
            if (StrChr(_what, 'f') != null)
            {
                if (_f == null) SetNilValue(_l.top);
                else SetCLValue(_l, _l.top, _f);
                IncrTop(_l);
            }
            if (StrChr(_what, 'L') != null)
                CollectValidLines(_l, _f);
            LuaUnlock(_l);
            return _status;
        }

        private static int CheckJump(Proto _pt, int _pc) { if (!(0 <= _pc && _pc < _pt.sizeCode)) return 0; return 1; }

        private static int CheckReg(Proto _pt, int _reg) { if (!((_reg) < (_pt).maxStackSize)) return 0; return 1; }

        private static int PreCheck(Proto _pt)
        {
            if (!(_pt.maxStackSize <= MAXSTACK)) return 0;
            if (!(_pt.numParams + (_pt.isVarArg & VARARG_HASARG) <= _pt.maxStackSize)) return 0;
            if (!(((_pt.isVarArg & VARARG_NEEDSARG) == 0) || ((_pt.isVarArg & VARARG_HASARG) != 0))) return 0;
            if (!(_pt.sizeUpValues <= _pt.nups)) return 0;
            if (!(_pt.sizeLineInfo == _pt.sizeCode || _pt.sizeLineInfo == 0)) return 0;
            if (!(_pt.sizeCode > 0 && GET_OPCODE(_pt.code[_pt.sizeCode - 1]) == OpCode.OP_RETURN)) return 0;
            return 1;
        }

        public static int CheckOpenOp(Proto _pt, int _pc) { return LuaGCheckOpenOp(_pt.code[_pc + 1]); }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaGCheckOpenOp(Instruction _i)
        {
            switch (GET_OPCODE(_i))
            {
                case OpCode.OP_CALL:
                case OpCode.OP_TAILCALL:
                case OpCode.OP_RETURN:
                case OpCode.OP_SETLIST:
                    {
                        if (!(GETARG_B(_i) == 0)) return 0;
                        return 1;
                    }
                default: return 0;
            }
        }

        private static int CheckArgMode(Proto _pt, int _r, OpArgMask _mode)
        {
            switch (_mode)
            {
                case OpArgMask.OpArgN: if (_r != 0) return 0; break;
                case OpArgMask.OpArgU: break;
                case OpArgMask.OpArgR: CheckReg(_pt, _r); break;
                case OpArgMask.OpArgK:
                    if (!((ISK(_r) != 0) ? INDEXK(_r) < _pt.sizeK : _r < _pt.maxStackSize)) return 0;
                    break;
            }
            return 1;
        }

        private static Instruction SymbExec(Proto _pt, int _last_pc, int _reg)
        {
            int _pc;
            int _last;
            int _dest;
            _last = _pt.sizeCode - 1;
            if (PreCheck(_pt) == 0) return 0;
            for (_pc = 0; _pc < _last_pc; _pc++)
            {
                Instruction _i = _pt.code[_pc];
                OpCode _op = GET_OPCODE(_i);
                int _a = GETARG_A(_i);
                int _b = 0;
                int _c = 0;
                if (!((int)_op < NUM_OPCODES)) return 0;
                CheckReg(_pt, _a);
                switch (getOpMode(_op))
                {
                    case OpMode.iABC:
                        {
                            _b = GETARG_B(_i);
                            _c = GETARG_C(_i);
                            if (CheckArgMode(_pt, _b, getBMode(_op)) == 0) return 0;
                            if (CheckArgMode(_pt, _c, getCMode(_op)) == 0) return 0;
                            break;
                        }
                    case OpMode.iABx:
                        {
                            _b = GETARG_Bx(_i);
                            if (getBMode(_op) == OpArgMask.OpArgK) if (!(_b < _pt.sizeK)) return 0;
                            break;
                        }
                    case OpMode.iAsBx:
                        {
                            _b = GETARG_sBx(_i);
                            if (getBMode(_op) == OpArgMask.OpArgR)
                            {
                                _dest = _pc + 1 + _b;
                                if (!((0 <= _dest && _dest < _pt.sizeCode))) return 0;
                                if (_dest > 0)
                                {
                                    int _j;
                                    for (_j = 0; _j < _dest; _j++)
                                    {
                                        Instruction _d = _pt.code[_dest - 1 - _j];
                                        if (!(GET_OPCODE(_d) == OpCode.OP_SETLIST && GETARG_C(_d) == 0)) break;
                                    }
                                    if ((_j & 1) != 0) return 0;
                                }
                            }
                            break;
                        }
                }
                if (testAMode(_op) != 0)
                    if (_a == _reg) _last = _pc;
                if (testTMode(_op) != 0)
                {
                    if (!(_pc + 2 < _pt.sizeCode)) return 0;
                    if (!(GET_OPCODE(_pt.code[_pc + 1]) == OpCode.OP_JMP)) return 0;
                }
                switch (_op)
                {
                    case OpCode.OP_LOADBOOL:
                        {
                            if (_c == 1)
                            {
                                if (!(_pc + 2 < _pt.sizeCode)) return 0;
                                if (!(GET_OPCODE(_pt.code[_pc + 1]) != OpCode.OP_SETLIST ||
                                      GETARG_C(_pt.code[_pc + 1]) != 0)) return 0;
                            }
                            break;
                        }
                    case OpCode.OP_LOADNIL:
                        {
                            if (_a <= _reg && _reg <= _b)
                                _last = _pc;
                            break;
                        }
                    case OpCode.OP_GETUPVAL:
                    case OpCode.OP_SETUPVAL:
                        {
                            if (!(_b < _pt.nups)) return 0;
                            break;
                        }
                    case OpCode.OP_GETGLOBAL:
                    case OpCode.OP_SETGLOBAL:
                        {
                            if (!(TTIsString(_pt.k[_b]))) return 0;
                            break;
                        }
                    case OpCode.OP_SELF:
                        {
                            CheckReg(_pt, _a + 1);
                            if (_reg == _a + 1) _last = _pc;
                            break;
                        }
                    case OpCode.OP_CONCAT:
                        {
                            if (!(_b < _c)) return 0;
                            break;
                        }
                    case OpCode.OP_TFORLOOP:
                        {
                            if (!(_c >= 1)) return 0;
                            CheckReg(_pt, _a + 2 + _c);
                            if (_reg >= _a + 2) _last = _pc;
                            break;
                        }
                    case OpCode.OP_FORLOOP:
                    case OpCode.OP_FORPREP:
                        CheckReg(_pt, _a + 3);
                        _dest = _pc + 1 + _b;
                        if (_reg != NO_REG && _pc < _dest && _dest <= _last_pc)
                            _pc += _b;
                        break;

                    case OpCode.OP_JMP:
                        {
                            _dest = _pc + 1 + _b;
                            if (_reg != NO_REG && _pc < _dest && _dest <= _last_pc)
                                _pc += _b;
                            break;
                        }
                    case OpCode.OP_CALL:
                    case OpCode.OP_TAILCALL:
                        {
                            if (_b != 0)
                                CheckReg(_pt, _a + _b - 1);
                            _c--;
                            if (_c == LUA_MULTRET)
                                if (CheckOpenOp(_pt, _pc) == 0) return 0;
                                else if (_c != 0)
                                    CheckReg(_pt, _a + _c - 1);
                            if (_reg >= _a) _last = _pc;
                            break;
                        }
                    case OpCode.OP_RETURN:
                        {
                            _b--;
                            if (_b > 0) CheckReg(_pt, _a + _b - 1);
                            break;
                        }
                    case OpCode.OP_SETLIST:
                        {
                            if (_b > 0) CheckReg(_pt, _a + _b);
                            if (_c == 0)
                            {
                                _pc++;
                                if (!(_pc < _pt.sizeCode - 1)) return 0;
                            }
                            break;
                        }
                    case OpCode.OP_CLOSURE:
                        {
                            int _nup, _j;
                            if (!(_b < _pt.sizeP)) return 0;
                            _nup = _pt.p[_b].nups;
                            if (!(_pc + _nup < _pt.sizeCode)) return 0;
                            for (_j = 1; _j <= _nup; _j++)
                            {
                                OpCode op1 = GET_OPCODE(_pt.code[_pc + _j]);
                                if (!(op1 == OpCode.OP_GETUPVAL || op1 == OpCode.OP_MOVE)) return 0;
                            }
                            if (_reg != NO_REG)
                                _pc += _nup;
                            break;
                        }
                    case OpCode.OP_VARARG:
                        {
                            if (!((_pt.isVarArg & VARARG_ISVARARG) != 0 &&
                                    (_pt.isVarArg & VARARG_NEEDSARG) == 0)) return 0;
                            _b--;
                            if (_b == LUA_MULTRET) if (CheckOpenOp(_pt, _pc) == 0) return 0;
                            CheckReg(_pt, _a + _b - 1);
                            break;
                        }
                    default:
                        break;
                }
            }
            return _pt.code[_last];
        }

        public static int LuaGCheckCode(Proto _pt)
        {
            return (SymbExec(_pt, _pt.sizeCode, NO_REG) != 0) ? 1 : 0;
        }

        private static CharPtr KName(Proto _p, int _c)
        {
            if (ISK(_c) != 0 && TTIsString(_p.k[INDEXK(_c)]))
                return SValue(_p.k[INDEXK(_c)]);
            else
                return "?";
        }

        private static CharPtr GetObjName(LuaState _l, CallInfo _ci, int _stack_pos, ref CharPtr _name)
        {
            if (IsLua(_ci))
            {
                Proto _p = CIFunc(_ci).l.p;
                int _pc = CurrentPC(_l, _ci);
                Instruction _i;
                _name = LuaFGetLocalName(_p, _stack_pos + 1, _pc);
                if (_name != null)
                    return "local";
                _i = SymbExec(_p, _pc, _stack_pos);
                LuaAssert(_pc != -1);
                switch (GET_OPCODE(_i))
                {
                    case OpCode.OP_GETGLOBAL:
                        {
                            int _g = GETARG_Bx(_i);
                            LuaAssert(TTIsString(_p.k[_g]));
                            _name = SValue(_p.k[_g]);
                            return "global";
                        }
                    case OpCode.OP_MOVE:
                        {
                            int _a = GETARG_A(_i);
                            int _b = GETARG_B(_i);
                            if (_b < _a)
                                return GetObjName(_l, _ci, _b, ref _name);
                            break;
                        }
                    case OpCode.OP_GETTABLE:
                        {
                            int _k = GETARG_C(_i);
                            _name = KName(_p, _k);
                            return "field";
                        }
                    case OpCode.OP_GETUPVAL:
                        {
                            int _u = GETARG_B(_i);  /* upvalue index */
                            _name = (_p.upValues != null) ? GetStr(_p.upValues[_u]) : "?";
                            return "upvalue";
                        }
                    case OpCode.OP_SELF:
                        {
                            int _k = GETARG_C(_i);
                            _name = KName(_p, _k);
                            return "method";
                        }
                    default: break;
                }
            }
            return null;
        }

        private static CharPtr GetFuncName(LuaState _l, CallInfo _ci, ref CharPtr _name)
        {
            Instruction _i;
            if ((IsLua(_ci) && _ci.tailCalls > 0) || !IsLua(_ci - 1))
                return null;
            CallInfo.Dec(ref _ci);
            _i = CIFunc(_ci).l.p.code[CurrentPC(_l, _ci)];
            if (GET_OPCODE(_i) == OpCode.OP_CALL || GET_OPCODE(_i) == OpCode.OP_TAILCALL ||
                GET_OPCODE(_i) == OpCode.OP_TFORLOOP)
                return GetObjName(_l, _ci, GETARG_A(_i), ref _name);
            else
                return null;
        }

        private static int IsInStack(CallInfo _ci, TValue _o)
        {
            StkId _p;
            for (_p = _ci.base_; _p < _ci.top; StkId.Inc(ref _p))
                if (_o == _p) return 1;
            return 0;
        }

        public static void LuaGTypeError(LuaState _l, TValue _o, CharPtr _op)
        {
            CharPtr _name = null;
            CharPtr _t = LuaTTypenames[TType(_o)];
            CharPtr _kind = (IsInStack(_l.ci, _o)) != 0 ? GetObjName(_l, _l.ci, CastInt(_o - _l.base_), ref _name) : null;
            if (_kind != null)
                LuaGRunError(_l, "attempt to %s %s " + LUA_QS + " (a %s value)", _op, _kind, _name, _t);
            else
                LuaGRunError(_l, "attempt to %s a %s value", _op, _t);
        }

        public static void LuaGConcatError(LuaState _l, StkId _p_1, StkId _p_2)
        {
            if (TTIsString(_p_1) || TTIsNumber(_p_1)) _p_1 = _p_2;
            LuaAssert(!TTIsString(_p_1) && !TTIsNumber(_p_1));
            LuaGTypeError(_l, _p_1, "concatenate");
        }

        public static void LuaGArithError(LuaState _l, TValue _p_1, TValue _p_2)
        {
            TValue _temp = new LuaTypeValue();
            if (LuaVToNumber(_p_1, _temp) == null)
                _p_2 = _p_1;
            LuaGTypeError(_l, _p_2, "perform arithmetic on");
        }

        public static int LuaGOrderError(LuaState _l, TValue _p_1, TValue _p_2)
        {
            CharPtr _t_1 = LuaTTypenames[TType(_p_1)];
            CharPtr _t_2 = LuaTTypenames[TType(_p_2)];
            if (_t_1[2] == _t_2[2])
                LuaGRunError(_l, "attempt to compare two %s values", _t_1);
            else
                LuaGRunError(_l, "attempt to compare %s with %s", _t_1, _t_2);
            return 0;
        }

        private static void AddInfo(LuaState _l, CharPtr _msg)
        {
            CallInfo _ci = _l.ci;
            if (IsLua(_ci))
            {
                CharPtr _buff = new CharPtr(new char[LUA_IDSIZE]);
                int _line = CurrentLine(_l, _ci);
                LuaOChunkID(_buff, GetStr(GetLuaProto(_ci).source), LUA_IDSIZE);
                LuaOPushFString(_l, "%s:%d: %s", _buff, _line, _msg);
            }
        }

        public static void LuaGErrorMsg(LuaState _l)
        {
            if (_l.errFunc != 0)
            {
                StkId _err_func = RestoreStack(_l, _l.errFunc);
                if (!TTIsFunction(_err_func)) LuaDThrow(_l, LUA_ERRERR);
                SetObj2S(_l, _l.top, _l.top - 1);
                SetObj2S(_l, _l.top - 1, _err_func);
                IncrTop(_l);
                LuaDCall(_l, _l.top - 2, 1);
            }
            LuaDThrow(_l, LUA_ERRRUN);
        }

        public static void LuaGRunError(LuaState _l, CharPtr _fmt, params object[] _arg_p)
        {
            AddInfo(_l, LuaOPushVFString(_l, _fmt, _arg_p));
            LuaGErrorMsg(_l);
        }
    }
}