﻿using System;

#if XBOX
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
#endif

namespace KopiLua
{
    using LuaByteType = System.Byte;
    using LuaIntegerType = System.Int32;
    using ptrdiff_t = System.Int32;
    using StkId = Lua.LuaTypeValue;
    using TValue = Lua.LuaTypeValue;
    using ZIO = Lua.Zio;

    public partial class Lua
    {
        public const int PCRLUA = 0;
        public const int PCRC = 1;
        public const int PCRYIELD = 2;

        public static void LuaDCheckStack(LuaState _l, int _n)
        {
            if ((_l.stackLast - _l.top) <= _n)
                LuaDGrowStack(_l, _n);
            else
            {
#if HARDSTACKTESTS
				luaD_reallocstack(L, L.stacksize - EXTRA_STACK - 1);
#endif
            }
        }

        public static void IncrTop(LuaState _l)
        {
            LuaDCheckStack(_l, 1);
            StkId.Inc(ref _l.top);
        }

        public static int SaveStack(LuaState _l, StkId _p) { return _p; }

        public static StkId RestoreStack(LuaState _l, int _n) { return _l.stack[_n]; }

        public static int SaveCI(LuaState _l, CallInfo _p) { return _p - _l.baseCi; }
        public static CallInfo RestoreCI(LuaState _l, int _n) { return _l.baseCi[_n]; }

        public delegate void Pfunc(LuaState _l, object _ud);

        public delegate void LuaIJmpBuf(LuaIntegerType _b);

        public class LuaLongJmp
        {
            public LuaLongJmp _previous;
            public LuaIJmpBuf _b;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public volatile int status;
        };

        public static void LuaDSetErrorObj(LuaState _l, int _err_code, StkId _old_top)
        {
            switch (_err_code)
            {
                case LUA_ERRMEM:
                    {
                        SetSValue2S(_l, _old_top, LuaSNewLiteral(_l, MEMERRMSG));
                        break;
                    }
                case LUA_ERRERR:
                    {
                        SetSValue2S(_l, _old_top, LuaSNewLiteral(_l, "error in error handling"));
                        break;
                    }
                case LUA_ERRSYNTAX:
                case LUA_ERRRUN:
                    {
                        SetObj2S(_l, _old_top, _l.top - 1);
                        break;
                    }
            }
            _l.top = _old_top + 1;
        }

        private static void RestoreStackLimit(LuaState _l)
        {
            LuaAssert(_l.stackLast == _l.stackSize - EXTRASTACK - 1);
            if (_l.sizeCi > LUAI_MAXCALLS)
            {
                int _in_use = _l.ci - _l.baseCi;
                if (_in_use + 1 < LUAI_MAXCALLS)
                    LuaDReallocCI(_l, LUAI_MAXCALLS);
            }
        }

        private static void ResetStack(LuaState _l, int _status)
        {
            _l.ci = _l.baseCi[0];
            _l.base_ = _l.ci.base_;
            LuaFClose(_l, _l.base_);
            LuaDSetErrorObj(_l, _status, _l.base_);
            _l.nCcalls = _l.baseCCalls;
            _l.allowHook = 1;
            RestoreStackLimit(_l);
            _l.errFunc = 0;
            _l.errorJmp = null;
        }

        public static void LuaDThrow(LuaState _l, int _err_code)
        {
            if (_l.errorJmp != null)
            {
                _l.errorJmp.status = _err_code;
                LUAI_THROW(_l, _l.errorJmp);
            }
            else
            {
                _l.status = CastByte(_err_code);
                if (G(_l).panic != null)
                {
                    ResetStack(_l, _err_code);
                    LuaUnlock(_l);
                    G(_l).panic(_l);
                }
#if XBOX
			throw new ApplicationException();
#else
#if SILVERLIGHT
            throw new SystemException();
#else
                Environment.Exit(EXIT_FAILURE);
#endif
#endif
            }
        }

        public static int LuaDRawRunProtected(LuaState _l, Pfunc _f, object _ud)
        {
            LuaLongJmp _lj = new LuaLongJmp();
            _lj.status = 0;
            _lj._previous = _l.errorJmp;
#if CATCH_EXCEPTIONS
		  try
#endif
            {
                _f(_l, _ud);
            }
#if CATCH_EXCEPTIONS
		    catch
		    {
			    if (lj.status == 0)
				    lj.status = -1;
		    }
#endif
            _l.errorJmp = _lj._previous;
            return _lj.status;
        }

        private static void CorrectStack(LuaState _l, TValue[] _old_stack)
        {

        }

        public static void LuaDRealAllocStack(LuaState _l, int _new_size)
        {
            TValue[] _old_stack = _l.stack;
            int _real_size = _new_size + 1 + EXTRASTACK;
            LuaAssert(_l.stackLast == _l.stackSize - EXTRASTACK - 1);
            LuaMReallocVector(_l, ref _l.stack, _l.stackSize, _real_size);
            _l.stackSize = _real_size;
            _l.stackLast = _l.stack[_new_size];
            CorrectStack(_l, _old_stack);
        }

        public static void LuaDReallocCI(LuaState _l, int _new_size)
        {
            CallInfo oldci = _l.baseCi[0];
            LuaMReallocVector(_l, ref _l.baseCi, _l.sizeCi, _new_size);
            _l.sizeCi = _new_size;
            _l.ci = _l.baseCi[_l.ci - oldci];
            _l.endCi = _l.baseCi[_l.sizeCi - 1];
        }

        public static void LuaDGrowStack(LuaState _l, int _n)
        {
            if (_n <= _l.stackSize)
                LuaDRealAllocStack(_l, 2 * _l.stackSize);
            else
                LuaDRealAllocStack(_l, _l.stackSize + _n);
        }

        private static CallInfo GrowCI(LuaState _l)
        {
            if (_l.sizeCi > LUAI_MAXCALLS)
                LuaDThrow(_l, LUA_ERRERR);
            else
            {
                LuaDReallocCI(_l, 2 * _l.sizeCi);
                if (_l.sizeCi > LUAI_MAXCALLS)
                    LuaGRunError(_l, "stack overflow");
            }
            CallInfo.Inc(ref _l.ci);
            return _l.ci;
        }

        public static void LuaDCallHook(LuaState _l, int _event, int _line)
        {
            LuaHook _hook = _l.hook;
            if ((_hook != null) && (_l.allowHook != 0))
            {
                ptrdiff_t _top = SaveStack(_l, _l.top);
                ptrdiff_t _ci_top = SaveStack(_l, _l.ci.top);
                LuaDebug _ar = new LuaDebug();
                _ar.event_ = _event;
                _ar.currentLine = _line;
                if (_event == LUA_HOOKTAILRET)
                    _ar.iCi = 0;
                else
                    _ar.iCi = _l.ci - _l.baseCi;
                LuaDCheckStack(_l, LUA_MINSTACK);
                _l.ci.top = _l.top + LUA_MINSTACK;
                LuaAssert(_l.ci.top <= _l.stackLast);
                _l.allowHook = 0;
                LuaUnlock(_l);
                _hook(_l, _ar);
                LuaLock(_l);
                LuaAssert(_l.allowHook == 0);
                _l.allowHook = 1;
                _l.ci.top = RestoreStack(_l, _ci_top);
                _l.top = RestoreStack(_l, _top);
            }
        }

        private static StkId AdjustVarArgs(LuaState _l, Proto _p, int _actual)
        {
            int _i;
            int _n_fix_args = _p.numParams;
            Table _htab = null;
            StkId _base, _fixed;
            for (; _actual < _n_fix_args; ++_actual)
                SetNilValue(StkId.Inc(ref _l.top));
#if LUA_COMPAT_VARARG
		    if ((_p.is_vararg & VARARG_NEEDSARG) != 0)
            { 
			    int _nvar = _actual - _n_fix_args;  
			    lua_assert(_p.is_vararg & VARARG_HASARG);
			    luaC_checkGC(_l);
			    luaD_checkstack(_l, _p.maxstacksize);
                _htab = luaH_new(_l, _nvar, 1); 
			    for (i=0; i<_nvar; i++)  
			        setobj2n(_l, luaH_setnum(_l, _htab, i+1), _l.top - _nvar + i);
			    setnvalue(luaH_setstr(_l, _htab, luaS_newliteral(_l, "n")), cast_num(_nvar));
		    }
#endif
            _fixed = _l.top - _actual;
            _base = _l.top;
            for (_i = 0; _i < _n_fix_args; _i++)
            {
                SetObj2S(_l, StkId.Inc(ref _l.top), _fixed + _i);
                SetNilValue(_fixed + _i);
            }
            if (_htab != null)
            {
                StkId top = _l.top;
                StkId.Inc(ref _l.top);
                SetHValue(_l, top, _htab);
                LuaAssert(IsWhite(obj2gco(_htab)));
            }
            return _base;
        }

        static StkId TryFuncTM(LuaState _l, StkId _func)
        {
            TValue _tm = LuaTGetTmByObj(_l, _func, TMS.TM_CALL);
            StkId _p;
            ptrdiff_t _func_r = SaveStack(_l, _func);
            if (!TTIsFunction(_tm))
                LuaGTypeError(_l, _func, "call");
            for (_p = _l.top; _p > _func; StkId.Dec(ref _p)) SetObj2S(_l, _p, _p - 1);
            IncrTop(_l);
            _func = RestoreStack(_l, _func_r);
            SetObj2S(_l, _func, _tm);
            return _func;
        }

        public static CallInfo IncCI(LuaState _l)
        {
            if (_l.ci == _l.endCi) return GrowCI(_l);
            CallInfo.Inc(ref _l.ci);
            return _l.ci;
        }

        public static int LuaDPreCall(LuaState _l, StkId _func, int _n_results)
        {
            LClosure _cl;
            ptrdiff_t _func_r;
            if (!TTIsFunction(_func))
                _func = TryFuncTM(_l, _func);

            _func_r = SaveStack(_l, _func);
            _cl = CLValue(_func).l;
            _l.ci.saveDpc = InstructionPtr.Assign(_l.saveDpc);
            if (_cl.isC == 0)
            {
                CallInfo _ci;
                StkId _st, _base;
                Proto _p = _cl.p;
                LuaDCheckStack(_l, _p.maxStackSize);
                _func = RestoreStack(_l, _func_r);
                if (_p.isVarArg == 0)
                {
                    _base = _l.stack[_func + 1];
                    if (_l.top > _base + _p.numParams)
                        _l.top = _base + _p.numParams;
                }
                else
                {
                    int _n_args = _l.top - _func - 1;
                    _base = AdjustVarArgs(_l, _p, _n_args);
                    _func = RestoreStack(_l, _func_r);
                }
                _ci = IncCI(_l);
                _ci.func = _func;
                _l.base_ = _ci.base_ = _base;
                _ci.top = _l.base_ + _p.maxStackSize;
                LuaAssert(_ci.top <= _l.stackLast);
                _l.saveDpc = new InstructionPtr(_p.code, 0);
                _ci.tailCalls = 0;
                _ci.nResults = _n_results;
                for (_st = _l.top; _st < _ci.top; StkId.Inc(ref _st))
                    SetNilValue(_st);
                _l.top = _ci.top;
                if ((_l.hookMask & LUA_MASKCALL) != 0)
                {
                    InstructionPtr.inc(ref _l.saveDpc);
                    LuaDCallHook(_l, LUA_HOOKCALL, -1);
                    InstructionPtr.dec(ref _l.saveDpc);
                }
                return PCRLUA;
            }
            else
            {
                CallInfo _ci;
                int _n;
                LuaDCheckStack(_l, LUA_MINSTACK);
                _ci = IncCI(_l);
                _ci.func = RestoreStack(_l, _func_r);
                _l.base_ = _ci.base_ = _ci.func + 1;
                _ci.top = _l.top + LUA_MINSTACK;
                LuaAssert(_ci.top <= _l.stackLast);
                _ci.nResults = _n_results;
                if ((_l.hookMask & LUA_MASKCALL) != 0)
                    LuaDCallHook(_l, LUA_HOOKCALL, -1);
                LuaUnlock(_l);
                _n = CurrFunc(_l).c.f(_l);
                LuaLock(_l);
                if (_n < 0)
                    return PCRYIELD;
                else
                {
                    LuaDPosCall(_l, _l.top - _n);
                    return PCRC;
                }
            }
        }

        private static StkId CallRetHooks(LuaState _l, StkId _first_result)
        {
            ptrdiff_t _fr = SaveStack(_l, _first_result);
            LuaDCallHook(_l, LUA_HOOKRET, -1);
            if (FIsLua(_l.ci))
            {
                while (((_l.hookMask & LUA_MASKRET) != 0) && (_l.ci.tailCalls-- != 0))
                    LuaDCallHook(_l, LUA_HOOKTAILRET, -1);
            }
            return RestoreStack(_l, _fr);
        }

        public static int LuaDPosCall(LuaState _l, StkId _first_result)
        {
            StkId _res;
            int _wanted, _i;
            CallInfo _ci;
            if ((_l.hookMask & LUA_MASKRET) != 0)
                _first_result = CallRetHooks(_l, _first_result);
            _ci = CallInfo.Dec(ref _l.ci);
            _res = _ci.func;
            _wanted = _ci.nResults;
            _l.base_ = (_ci - 1).base_;
            _l.saveDpc = InstructionPtr.Assign((_ci - 1).saveDpc);
            for (_i = _wanted; _i != 0 && _first_result < _l.top; _i--)
            {
                SetObj2S(_l, _res, _first_result);
                _res = _res + 1;
                _first_result = _first_result + 1;
            }
            while (_i-- > 0)
                SetNilValue(StkId.Inc(ref _res));
            _l.top = _res;
            return (_wanted - LUA_MULTRET);
        }

        private static void LuaDCall(LuaState _l, StkId _func, int _n_results)
        {
            if (++_l.nCcalls >= LUAI_MAXCCALLS)
            {
                if (_l.nCcalls == LUAI_MAXCCALLS)
                    LuaGRunError(_l, "C stack overflow");
                else if (_l.nCcalls >= (LUAI_MAXCCALLS + (LUAI_MAXCCALLS >> 3)))
                    LuaDThrow(_l, LUA_ERRERR);
            }
            if (LuaDPreCall(_l, _func, _n_results) == PCRLUA)
                LuaVExecute(_l, 1);
            _l.nCcalls--;
            LuaCCheckGC(_l);
        }

        private static void Resume(LuaState _l, object _ud)
        {
            StkId _first_arg = (StkId)_ud;
            CallInfo _ci = _l.ci;
            if (_l.status == 0)
            {
                LuaAssert(_ci == _l.baseCi[0] && _first_arg > _l.base_);
                if (LuaDPreCall(_l, _first_arg - 1, LUA_MULTRET) != PCRLUA)
                    return;
            }
            else
            {
                LuaAssert(_l.status == LUA_YIELD);
                _l.status = 0;
                if (!FIsLua(_ci))
                {
                    LuaAssert(GET_OPCODE((_ci - 1).saveDpc[-1]) == OpCode.OP_CALL || GET_OPCODE((_ci - 1).saveDpc[-1]) == OpCode.OP_TAILCALL);
                    if (LuaDPosCall(_l, _first_arg) != 0)
                        _l.top = _l.ci.top;
                }
                else
                    _l.base_ = _l.ci.base_;
            }
            LuaVExecute(_l, _l.ci - _l.baseCi);
        }

        private static int ResumeError(LuaState _l, CharPtr _msg)
        {
            _l.top = _l.ci.base_;
            SetSValue2S(_l, _l.top, LuaSNew(_l, _msg));
            IncrTop(_l);
            LuaUnlock(_l);
            return LUA_ERRRUN;
        }

        public static int LuaResume(LuaState _l, int _n_args)
        {
            int _status;
            LuaLock(_l);
            if (_l.status != LUA_YIELD && (_l.status != 0 || (_l.ci != _l.baseCi[0])))
                return ResumeError(_l, "cannot resume non-suspended coroutine");
            if (_l.nCcalls >= LUAI_MAXCCALLS)
                return ResumeError(_l, "C stack overflow");
            LuaIUserStateResume(_l, _n_args);
            LuaAssert(_l.errFunc == 0);
            _l.baseCCalls = ++_l.nCcalls;
            _status = LuaDRawRunProtected(_l, Resume, _l.top - _n_args);
            if (_status != 0)
            {
                _l.status = CastByte(_status);
                LuaDSetErrorObj(_l, _status, _l.top);
                _l.ci.top = _l.top;
            }
            else
            {
                LuaAssert(_l.nCcalls == _l.baseCCalls);
                _status = _l.status;
            }
            --_l.nCcalls;
            LuaUnlock(_l);
            return _status;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaYield(LuaState _l, int _n_results)
        {
            LuaIUserStateYield(_l, _n_results);
            LuaLock(_l);
            if (_l.nCcalls > _l.baseCCalls)
                LuaGRunError(_l, "attempt to yield across metamethod/C-call boundary");
            _l.base_ = _l.top - _n_results;
            _l.status = LUA_YIELD;
            LuaUnlock(_l);
            return -1;
        }

        public static int LuaDPCall(LuaState _l, Pfunc _func, object _u, ptrdiff_t _old_top, ptrdiff_t _ef)
        {
            int _status;
            ushort _old_nc_calls = _l.nCcalls;
            ptrdiff_t _old_ci = SaveCI(_l, _l.ci);
            LuaByteType _old_allow_hooks = _l.allowHook;
            ptrdiff_t _old_err_func = _l.errFunc;
            _l.errFunc = _ef;
            _status = LuaDRawRunProtected(_l, _func, _u);
            if (_status != 0)
            {
                StkId __old_top = RestoreStack(_l, _old_top);
                LuaFClose(_l, __old_top);
                LuaDSetErrorObj(_l, _status, __old_top);
                _l.nCcalls = _old_nc_calls;
                _l.ci = RestoreCI(_l, _old_ci);
                _l.base_ = _l.ci.base_;
                _l.saveDpc = InstructionPtr.Assign(_l.ci.saveDpc);
                _l.allowHook = _old_allow_hooks;
                RestoreStackLimit(_l);
            }
            _l.errFunc = _old_err_func;
            return _status;
        }

        public class SParser
        {
            public ZIO z;
            public Mbuffer buff = new Mbuffer();
            public CharPtr name;
        };

        private static void FParser(LuaState _l, object _ud)
        {
            int _i;
            Proto _tf;
            Closure _cl;
            SParser _p = (SParser)_ud;
            int _c = LuaZLookAhead(_p.z);
            LuaCCheckGC(_l);
            _tf = (_c == LUA_SIGNATURE[0]) ? LuaUUndump(_l, _p.z, _p.buff, _p.name) : LuaYParser(_l, _p.z, _p.buff, _p.name);
            _cl = LuaFNewLClosure(_l, _tf.nups, HValue(Gt(_l)));
            _cl.l.p = _tf;
            for (_i = 0; _i < _tf.nups; _i++)
                _cl.l.upvals[_i] = LuaFNewUpVal(_l);
            SetCLValue(_l, _l.top, _cl);
            IncrTop(_l);
        }

        public static int LuaDProtectedParser(LuaState _l, ZIO _z, CharPtr _name)
        {
            SParser _p = new SParser();
            int _status;
            _p.z = _z; _p.name = new CharPtr(_name);
            LuaZInitBuffer(_l, _p.buff);
            _status = LuaDPCall(_l, FParser, _p, SaveStack(_l, _l.top), _l.errFunc);
            LuaZFreeBuffer(_l, _p.buff);
            return _status;
        }
    }
}