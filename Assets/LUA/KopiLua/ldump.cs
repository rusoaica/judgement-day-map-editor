﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace KopiLua
{
    using LuaNumberType = System.Double;
    using TValue = Lua.LuaTypeValue;

    public partial class Lua
    {
        public class DumpState
        {
            public LuaState l;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public LuaWriter writer;
            public object data;
            public int strip;
            public int status;
        };

        public static void DumpMem(object _b, DumpState _d)
        {
#if XBOX
			Debug.Assert(false);
#endif

#if SILVERLIGHT
			int _size = 0;
			byte[] _bytes;
			Type _t = _b.GetType();
			if (_t.Equals(typeof(UInt32)))
			{
				_size = 4;
				_bytes = BitConverter.GetBytes((uint)_b);
			}
			else if (_t.Equals(typeof(Int32)))
			{
				_size = 4;
				_bytes = BitConverter.GetBytes((int)_b);
			}
			else if (_t.Equals(typeof(Char)))
			{
				_size = 1;
				_bytes = new byte[1] { BitConverter.GetBytes((char)_b)[0] };
			}
			else if (_t.Equals(typeof(Byte)))
			{
				_size = 1;
				_bytes = new byte[1] { (byte)_b };
			}
			else if (_t.Equals(typeof(Double)))
			{
				_size = 8;
				_bytes = BitConverter.GetBytes((double)_b);
			}
			else
			{
				throw new NotImplementedException("Invalid type: " + _t.FullName);
			}
#else
            int _size = Marshal.SizeOf(_b);
            IntPtr _ptr = Marshal.AllocHGlobal(_size);
            Marshal.StructureToPtr(_b, _ptr, false);
            byte[] _bytes = new byte[_size];
            Marshal.Copy(_ptr, _bytes, 0, _size);
#endif
            char[] _ch = new char[_bytes.Length];
            for (int i = 0; i < _bytes.Length; i++)
                _ch[i] = (char)_bytes[i];
            CharPtr str = _ch;
            DumpBlock(str, (uint)str.chars.Length, _d);
#if !SILVERLIGHT
            Marshal.Release(_ptr);
#endif
        }

        public static void DumpMem(object _b, int _n, DumpState _d)
        {
            Array _array = _b as Array;
            Debug.Assert(_array.Length == _n);
            for (int i = 0; i < _n; i++)
                DumpMem(_array.GetValue(i), _d);
        }

        public static void DumpVar(object _x, DumpState _d)
        {
            DumpMem(_x, _d);
        }

        private static void DumpBlock(CharPtr _b, uint _size, DumpState _d)
        {
            if (_d.status == 0)
            {
                LuaUnlock(_d.l);
                _d.status = _d.writer(_d.l, _b, _size, _d.data);
                LuaLock(_d.l);
            }
        }

        private static void DumpChar(int _y, DumpState _d)
        {
            char _x = (char)_y;
            DumpVar(_x, _d);
        }

        private static void DumpInt(int _x, DumpState _d)
        {
            DumpVar(_x, _d);
        }

        private static void DumpNumber(LuaNumberType _x, DumpState _d)
        {
            DumpVar(_x, _d);
        }

        static void DumpVector(object _b, int _n, DumpState _d)
        {
            DumpInt(_n, _d);
            DumpMem(_b, _n, _d);
        }

        private static void DumpString(TString _s, DumpState _d)
        {
            if (_s == null || GetStr(_s) == null)
            {
                uint _size = 0;
                DumpVar(_size, _d);
            }
            else
            {
                uint _size = _s.tsv.len + 1;
                DumpVar(_size, _d);
                DumpBlock(GetStr(_s), _size, _d);
            }
        }

        private static void DumpCode(Proto _f, DumpState _d)
        {
            DumpVector(_f.code, _f.sizeCode, _d);
        }

        private static void DumpConstants(Proto _f, DumpState _d)
        {
            int _i, _n = _f.sizeK;
            DumpInt(_n, _d);
            for (_i = 0; _i < _n; _i++)
            {
                TValue _o = _f.k[_i];
                DumpChar(TType(_o), _d);
                switch (TType(_o))
                {
                    case LUA_TNIL:
                        break;
                    case LUA_TBOOLEAN:
                        DumpChar(BValue(_o), _d);
                        break;
                    case LUA_TNUMBER:
                        DumpNumber(NValue(_o), _d);
                        break;
                    case LUA_TSTRING:
                        DumpString(RawTSValue(_o), _d);
                        break;
                    default:
                        LuaAssert(0);
                        break;
                }
            }
            _n = _f.sizeP;
            DumpInt(_n, _d);
            for (_i = 0; _i < _n; _i++) DumpFunction(_f.p[_i], _f.source, _d);
        }

        private static void DumpDebug(Proto _f, DumpState _d)
        {
            int _i, _n;
            _n = (_d.strip != 0) ? 0 : _f.sizeLineInfo;
            DumpVector(_f.lineInfo, _n, _d);
            _n = (_d.strip != 0) ? 0 : _f.sizeLocVars;
            DumpInt(_n, _d);
            for (_i = 0; _i < _n; _i++)
            {
                DumpString(_f.locVars[_i].varname, _d);
                DumpInt(_f.locVars[_i].startpc, _d);
                DumpInt(_f.locVars[_i].endpc, _d);
            }
            _n = (_d.strip != 0) ? 0 : _f.sizeUpValues;
            DumpInt(_n, _d);
            for (_i = 0; _i < _n; _i++) DumpString(_f.upValues[_i], _d);
        }

        private static void DumpFunction(Proto _f, TString _p, DumpState _d)
        {
            DumpString(((_f.source == _p) || (_d.strip != 0)) ? null : _f.source, _d);
            DumpInt(_f.lineDefined, _d);
            DumpInt(_f.lastLineDefined, _d);
            DumpChar(_f.nups, _d);
            DumpChar(_f.numParams, _d);
            DumpChar(_f.isVarArg, _d);
            DumpChar(_f.maxStackSize, _d);
            DumpCode(_f, _d);
            DumpConstants(_f, _d);
            DumpDebug(_f, _d);
        }

        private static void DumpHeader(DumpState _d)
        {
            CharPtr _h = new char[LUAC_HEADERSIZE];
            LuaUHeader(_h);
            DumpBlock(_h, LUAC_HEADERSIZE, _d);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaUDump(LuaState _l, Proto _f, LuaWriter _w, object _data, int _strip)
        {
            DumpState _d = new DumpState();
            _d.l = _l;
            _d.writer = _w;
            _d.data = _data;
            _d.strip = _strip;
            _d.status = 0;
            DumpHeader(_d);
            DumpFunction(_f, null, _d);
            return _d.status;
        }
    }
}