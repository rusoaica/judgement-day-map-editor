﻿using System;

namespace KopiLua
{
    using Instruction = System.UInt32;
    using StkId = Lua.LuaTypeValue;
    using TValue = Lua.LuaTypeValue;

    public partial class Lua
    {
        public static int SizeCclosure(int _n)
        {
            return GetUnmanagedSize(typeof(CClosure)) + GetUnmanagedSize(typeof(TValue)) * (_n - 1);
        }

        public static int SizeLclosure(int _n)
        {
            return GetUnmanagedSize(typeof(LClosure)) + GetUnmanagedSize(typeof(TValue)) * (_n - 1);
        }

        public static Closure LuaFNewCclosure(LuaState _l, int _n_elems, Table _e)
        {
            Closure _c = LuaMNew<Closure>(_l);
            AddTotalBytes(_l, SizeCclosure(_n_elems));
            LuaCLink(_l, obj2gco(_c), LUA_TFUNCTION);
            _c.c.isC = 1;
            _c.c.env = _e;
            _c.c.nupvalues = CastByte(_n_elems);
            _c.c.upvalue = new TValue[_n_elems];
            for (int i = 0; i < _n_elems; i++)
                _c.c.upvalue[i] = new LuaTypeValue();
            return _c;
        }

        public static Closure LuaFNewLClosure(LuaState _l, int _n_elems, Table _e)
        {
            Closure _c = LuaMNew<Closure>(_l);
            AddTotalBytes(_l, SizeLclosure(_n_elems));
            LuaCLink(_l, obj2gco(_c), LUA_TFUNCTION);
            _c.l.isC = 0;
            _c.l.env = _e;
            _c.l.nupvalues = CastByte(_n_elems);
            _c.l.upvals = new UpVal[_n_elems];
            for (int i = 0; i < _n_elems; i++)
                _c.l.upvals[i] = new UpVal();
            while (_n_elems-- > 0) _c.l.upvals[_n_elems] = null;
            return _c;
        }

        public static UpVal LuaFNewUpVal(LuaState _l)
        {
            UpVal _uv = LuaMNew<UpVal>(_l);
            LuaCLink(_l, obj2gco(_uv), LUATUPVAL);
            _uv.v = _uv.u.value;
            SetNilValue(_uv.v);
            return _uv;
        }

        public static UpVal LuaFindUpVal(LuaState _l, StkId _level)
        {
            GlobalState _g = G(_l);
            GCObjectRef _pp = new OpenValRef(_l);
            UpVal _p;
            UpVal _uv;
            while (_pp.get() != null && (_p = ngcotouv(_pp.get())).v >= _level)
            {
                LuaAssert(_p.v != _p.u.value);
                if (_p.v == _level)
                {
                    if (IsDead(_g, obj2gco(_p)))
                        ChangeWhite(obj2gco(_p));
                    return _p;
                }
                _pp = new NextRef(_p);
            }
            _uv = LuaMNew<UpVal>(_l);
            _uv.tt = LUATUPVAL;
            _uv.marked = LuaCWhite(_g);
            _uv.v = _level;
            _uv.next = _pp.get();
            _pp.set(obj2gco(_uv));
            _uv.u.l.prev = _g.uvHead;
            _uv.u.l.next = _g.uvHead.u.l.next;
            _uv.u.l.next.u.l.prev = _uv;
            _g.uvHead.u.l.next = _uv;
            LuaAssert(_uv.u.l.next.u.l.prev == _uv && _uv.u.l.prev.u.l.next == _uv);
            return _uv;
        }

        private static void UnlinkUpVal(UpVal _uv)
        {
            LuaAssert(_uv.u.l.next.u.l.prev == _uv && _uv.u.l.prev.u.l.next == _uv);
            _uv.u.l.next.u.l.prev = _uv.u.l.prev;
            _uv.u.l.prev.u.l.next = _uv.u.l.next;
        }

        public static void LuaFreeUpVal(LuaState _l, UpVal _uv)
        {
            if (_uv.v != _uv.u.value)
                UnlinkUpVal(_uv);
            LuaMFree(_l, _uv);
        }

        public static void LuaFClose(LuaState _l, StkId _level)
        {
            UpVal _uv;
            GlobalState _g = G(_l);
            while (_l.openUpVal != null && (_uv = ngcotouv(_l.openUpVal)).v >= _level)
            {
                GCObject _o = obj2gco(_uv);
                LuaAssert(!IsBlack(_o) && _uv.v != _uv.u.value);
                _l.openUpVal = _uv.next;
                if (IsDead(_g, _o))
                    LuaFreeUpVal(_l, _uv);
                else
                {
                    UnlinkUpVal(_uv);
                    SetObj(_l, _uv.u.value, _uv.v);
                    _uv.v = _uv.u.value;
                    LuaCLinkUpVal(_l, _uv);
                }
            }
        }

        public static Proto LuaFNewProto(LuaState _l)
        {
            Proto _f = LuaMNew<Proto>(_l);
            LuaCLink(_l, obj2gco(_f), LUATPROTO);
            _f.k = null;
            _f.sizeK = 0;
            _f.p = null;
            _f.sizeP = 0;
            _f.code = null;
            _f.sizeCode = 0;
            _f.sizeLineInfo = 0;
            _f.sizeUpValues = 0;
            _f.nups = 0;
            _f.upValues = null;
            _f.numParams = 0;
            _f.isVarArg = 0;
            _f.maxStackSize = 0;
            _f.lineInfo = null;
            _f.sizeLocVars = 0;
            _f.locVars = null;
            _f.lineDefined = 0;
            _f.lastLineDefined = 0;
            _f.source = null;
            return _f;
        }

        public static void LuaFFreeProto(LuaState _l, Proto _f)
        {
            LuaMFreeArray<Instruction>(_l, _f.code);
            LuaMFreeArray<Proto>(_l, _f.p);
            LuaMFreeArray<TValue>(_l, _f.k);
            LuaMFreeArray<Int32>(_l, _f.lineInfo);
            LuaMFreeArray<LocVar>(_l, _f.locVars);
            LuaMFreeArray<TString>(_l, _f.upValues);
            LuaMFree(_l, _f);
        }

        public static void LuaFFreeClosure(LuaState _l, Closure _c)
        {
            int _size = (_c.c.isC != 0) ? SizeCclosure(_c.c.nupvalues) : SizeLclosure(_c.l.nupvalues);
            SubtractTotalBytes(_l, _size);
        }

        public static CharPtr LuaFGetLocalName(Proto _f, int _local_number, int _pc)
        {
            int _i;
            for (_i = 0; _i < _f.sizeLocVars && _f.locVars[_i].startpc <= _pc; _i++)
            {
                if (_pc < _f.locVars[_i].endpc)
                {
                    _local_number--;
                    if (_local_number == 0)
                        return GetStr(_f.locVars[_i].varname);
                }
            }
            return null;
        }
    }
}