﻿using System;

namespace KopiLua
{
    using Instruction = System.UInt32;
    using l_mem = System.Int32;
    using lu_mem = System.UInt32;
    using LuaByteType = System.Byte;
    using LuaIntegerType = System.UInt32;
    using StkId = Lua.LuaTypeValue;
    using TValue = Lua.LuaTypeValue;

    public partial class Lua
    {
        public const int GCSpause = 0;
        public const int GCSpropagate = 1;
        public const int GCSsweepstring = 2;
        public const int GCSsweep = 3;
        public const int GCSfinalize = 4;
        public const int WHITE0BIT = 0;
        public const int WHITE1BIT = 1;
        public const int BLACKBIT = 2;
        public const int FINALIZEDBIT = 3;
        public const int KEYWEAKBIT = 3;
        public const int VALUEWEAKBIT = 4;
        public const int FIXEDBIT = 5;
        public const int SFIXEDBIT = 6;
        public const int GCSWEEPMAX = 40;
        public const int GCSWEEPCOST = 10;
        public const int GCFINALIZECOST = 100;
        public const uint GCSTEPSIZE = 1024;
        public static int KEYWEAK = BitMask(KEYWEAKBIT);
        public static int VALUEWEAK = BitMask(VALUEWEAKBIT);

        public readonly static int WHITEBITS = Bit2Mask(WHITE0BIT, WHITE1BIT);


        public static byte maskmarks = (byte)(~(BitMask(BLACKBIT) | WHITEBITS));


        public static int ResetBits(ref LuaByteType _x, int _m) { _x &= (LuaByteType)~_m; return _x; }

        public static int SetBits(ref LuaByteType _x, int _m) { _x |= (LuaByteType)_m; return _x; }

        public static bool TestBits(LuaByteType _x, int _m) { return (_x & (LuaByteType)_m) != 0; }

        public static int BitMask(int _b) { return 1 << _b; }

        public static int Bit2Mask(int _b_1, int _b_2) { return (BitMask(_b_1) | BitMask(_b_2)); }

        public static int LSetBit(ref LuaByteType _x, int _b) { return SetBits(ref _x, BitMask(_b)); }

        public static int ResetBit(ref LuaByteType _x, int _b) { return ResetBits(ref _x, BitMask(_b)); }

        public static bool TestBit(LuaByteType _x, int _b) { return TestBits(_x, BitMask(_b)); }

        public static int Set2Bits(ref LuaByteType _x, int _b_1, int _b_2) { return SetBits(ref _x, (Bit2Mask(_b_1, _b_2))); }

        public static int Reset2Bits(ref LuaByteType _x, int _b_1, int _b_2) { return ResetBits(ref _x, (Bit2Mask(_b_1, _b_2))); }

        public static bool Test2Bits(LuaByteType _x, int _b_1, int _b_2) { return TestBits(_x, (Bit2Mask(_b_1, _b_2))); }

        public static bool IsWhite(GCObject _x) { return Test2Bits(_x.gch.marked, WHITE0BIT, WHITE1BIT); }

        public static bool IsBlack(GCObject _x) { return TestBit(_x.gch.marked, BLACKBIT); }

        public static bool IsGray(GCObject _x) { return (!IsBlack(_x) && !IsWhite(_x)); }

        public static int OtherWhite(GlobalState _g) { return _g.currentWhite ^ WHITEBITS; }

        public static bool IsDead(GlobalState _g, GCObject _v) { return (_v.gch.marked & OtherWhite(_g) & WHITEBITS) != 0; }

        public static void ChangeWhite(GCObject _x) { _x.gch.marked ^= (byte)WHITEBITS; }

        public static void Gray2Black(GCObject _x) { LSetBit(ref _x.gch.marked, BLACKBIT); }

        public static bool ValIsWhite(TValue _x) { return (IsCollectable(_x) && IsWhite(GCValue(_x))); }

        public static byte LuaCWhite(GlobalState _g) { return (byte)(_g.currentWhite & WHITEBITS); }

        public static void LuaCCheckGC(LuaState _l)
        {
            if (G(_l).totalBytes >= G(_l).gcThreshold)
                LuaCStep(_l);
        }

        public static void LuaCBarrier(LuaState _l, object _p, TValue _v)
        {
            if (ValIsWhite(_v) && IsBlack(obj2gco(_p)))
                LuaCBarrierF(_l, obj2gco(_p), GCValue(_v));
        }

        public static void LuaCBarrierT(LuaState _l, Table _t, TValue _v)
        {
            if (ValIsWhite(_v) && IsBlack(obj2gco(_t)))
                LuaCBarrierBack(_l, _t);
        }

        public static void LuaCObjBarrier(LuaState _l, object _p, object _o)
        {
            if (IsWhite(obj2gco(_o)) && IsBlack(obj2gco(_p)))
                LuaCBarrierF(_l, obj2gco(_p), obj2gco(_o));
        }

        public static void LuaCObjBarrierT(LuaState _l, Table _t, object _o)
        { if (IsWhite(obj2gco(_o)) && IsBlack(obj2gco(_t))) LuaCBarrierBack(_l, _t); }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static void MakeWhite(GlobalState _g, GCObject _x)
        {
            _x.gch.marked = (byte)(_x.gch.marked & maskmarks | LuaCWhite(_g));
        }

        public static void White2Gray(GCObject _x) { Reset2Bits(ref _x.gch.marked, WHITE0BIT, WHITE1BIT); }

        public static void Black2Gray(GCObject _x) { ResetBit(ref _x.gch.marked, BLACKBIT); }

        public static void StringMark(TString _s) { Reset2Bits(ref _s.tsv.marked, WHITE0BIT, WHITE1BIT); }

        public static bool IsFinalized(UdataUV _u) { return TestBit(_u.marked, FINALIZEDBIT); }

        public static void MarkFinalized(UdataUV _u)
        {
            LuaByteType _marked = _u.marked;
            LSetBit(ref _marked, FINALIZEDBIT);
            _u.marked = _marked;
        }

        public static void MarkValue(GlobalState _g, TValue _o)
        {
            CheckConsistency(_o);
            if (IsCollectable(_o) && IsWhite(GCValue(_o)))
                ReallyMarkObject(_g, GCValue(_o));
        }

        public static void MarkObject(GlobalState _g, object _t)
        {
            if (IsWhite(obj2gco(_t)))
                ReallyMarkObject(_g, obj2gco(_t));
        }

        public static void SetThreshold(GlobalState _g)
        {
            _g.gcThreshold = (uint)((_g.estimate / 100) * _g.gcPause);
        }

        private static void RemoveEntry(Node _n)
        {
            LuaAssert(TTIsNil(GVal(_n)));
            if (IsCollectable(GKey(_n)))
                SetTType(GKey(_n), LUATDEADKEY);
        }

        private static void ReallyMarkObject(GlobalState _g, GCObject _o)
        {
            LuaAssert(IsWhite(_o) && !IsDead(_g, _o));
            White2Gray(_o);
            switch (_o.gch.tt)
            {
                case LUA_TSTRING:
                    {
                        return;
                    }
                case LUA_TUSERDATA:
                    {
                        Table _mt = gco2u(_o).metatable;
                        Gray2Black(_o);
                        if (_mt != null) MarkObject(_g, _mt);
                        MarkObject(_g, gco2u(_o).env);
                        return;
                    }
                case LUATUPVAL:
                    {
                        UpVal _uv = gco2uv(_o);
                        MarkValue(_g, _uv.v);
                        if (_uv.v == _uv.u.value)
                            Gray2Black(_o);
                        return;
                    }
                case LUA_TFUNCTION:
                    {
                        gco2cl(_o).c.gclist = _g.gray;
                        _g.gray = _o;
                        break;
                    }
                case LUA_TTABLE:
                    {
                        gco2h(_o).gcList = _g.gray;
                        _g.gray = _o;
                        break;
                    }
                case LUA_TTHREAD:
                    {
                        gco2th(_o).gcList = _g.gray;
                        _g.gray = _o;
                        break;
                    }
                case LUATPROTO:
                    {
                        gco2p(_o).gclist = _g.gray;
                        _g.gray = _o;
                        break;
                    }
                default: LuaAssert(0); break;
            }
        }

        private static void MarkTMU(GlobalState _g)
        {
            GCObject _u = _g.tmuData;
            if (_u != null)
            {
                do
                {
                    _u = _u.gch.next;
                    MakeWhite(_g, _u);
                    ReallyMarkObject(_g, _u);
                } while (_u != _g.tmuData);
            }
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static uint LuaCSeparateUData(LuaState _l, int _all)
        {
            GlobalState _g = G(_l);
            uint _dead_mem = 0;
            GCObjectRef _p = new NextRef(_g.mainThread);
            GCObject _curr;
            while ((_curr = _p.get()) != null)
            {
                if (!(IsWhite(_curr) || (_all != 0)) || IsFinalized(gco2u(_curr)))
                    _p = new NextRef(_curr.gch);
                else if (FastTm(_l, gco2u(_curr).metatable, TMS.TM_GC) == null)
                {
                    MarkFinalized(gco2u(_curr));
                    _p = new NextRef(_curr.gch);
                }
                else
                {
                    _dead_mem += (uint)SizeUData(gco2u(_curr));
                    MarkFinalized(gco2u(_curr));
                    _p.set(_curr.gch.next);
                    if (_g.tmuData == null)
                        _g.tmuData = _curr.gch.next = _curr;
                    else
                    {
                        _curr.gch.next = _g.tmuData.gch.next;
                        _g.tmuData.gch.next = _curr;
                        _g.tmuData = _curr;
                    }
                }
            }
            return _dead_mem;
        }

        private static int TraverseTable(GlobalState _g, Table _h)
        {
            int _i;
            int _weak_key = 0;
            int _weak_value = 0;
            TValue _mode;
            if (_h.metatable != null)
                MarkObject(_g, _h.metatable);
            _mode = GFastTm(_g, _h.metatable, TMS.TM_MODE);
            if ((_mode != null) && TTIsString(_mode))
            {
                _weak_key = (StrChr(SValue(_mode), 'k') != null) ? 1 : 0;
                _weak_value = (StrChr(SValue(_mode), 'v') != null) ? 1 : 0;
                if ((_weak_key != 0) || (_weak_value != 0))
                {
                    _h.marked &= (byte)~(KEYWEAK | VALUEWEAK);
                    _h.marked |= CastByte((_weak_key << KEYWEAKBIT) | (_weak_value << VALUEWEAKBIT));
                    _h.gcList = _g.weak;
                    _g.weak = obj2gco(_h);
                }
            }
            if ((_weak_key != 0) && (_weak_value != 0)) return 1;
            if (_weak_value == 0)
            {
                _i = _h.sizeArray;
                while ((_i--) != 0)
                    MarkValue(_g, _h.array[_i]);
            }
            _i = SizeNode(_h);
            while ((_i--) != 0)
            {
                Node _n = GNode(_h, _i);
                LuaAssert(TType(GKey(_n)) != LUATDEADKEY || TTIsNil(GVal(_n)));
                if (TTIsNil(GVal(_n)))
                    RemoveEntry(_n);
                else
                {
                    LuaAssert(!TTIsNil(GKey(_n)));
                    if (_weak_key == 0) MarkValue(_g, GKey(_n));
                    if (_weak_value == 0) MarkValue(_g, GVal(_n));
                }
            }
            return ((_weak_key != 0) || (_weak_value != 0)) ? 1 : 0;
        }

        private static void TraverseProto(GlobalState _g, Proto _f)
        {
            int _i;
            if (_f.source != null) StringMark(_f.source);
            for (_i = 0; _i < _f.sizeK; _i++)
                MarkValue(_g, _f.k[_i]);
            for (_i = 0; _i < _f.sizeUpValues; _i++)
            {
                if (_f.upValues[_i] != null)
                    StringMark(_f.upValues[_i]);
            }
            for (_i = 0; _i < _f.sizeP; _i++)
            {
                if (_f.p[_i] != null)
                    MarkObject(_g, _f.p[_i]);
            }
            for (_i = 0; _i < _f.sizeLocVars; _i++)
            {
                if (_f.locVars[_i].varname != null)
                    StringMark(_f.locVars[_i].varname);
            }
        }

        private static void TraverseClosure(GlobalState _g, Closure _cl)
        {
            MarkObject(_g, _cl.c.env);
            if (_cl.c.isC != 0)
            {
                int _i;
                for (_i = 0; _i < _cl.c.nupvalues; _i++)
                    MarkValue(_g, _cl.c.upvalue[_i]);
            }
            else
            {
                int _i;
                LuaAssert(_cl.l.nupvalues == _cl.l.p.nups);
                MarkObject(_g, _cl.l.p);
                for (_i = 0; _i < _cl.l.nupvalues; _i++)
                    MarkObject(_g, _cl.l.upvals[_i]);
            }
        }

        private static void CheckStackSizes(LuaState _l, StkId _max)
        {
            int _ci_used = CastInt(_l.ci - _l.baseCi[0]);
            int _s_used = CastInt(_max - _l.stack);
            if (_l.sizeCi > LUAI_MAXCALLS)
                return;
            if (4 * _ci_used < _l.sizeCi && 2 * BASICCISIZE < _l.sizeCi)
                LuaDReallocCI(_l, _l.sizeCi / 2);
            if (4 * _s_used < _l.stackSize && 2 * (BASICSTACKSIZE + EXTRASTACK) < _l.stackSize)
                LuaDRealAllocStack(_l, _l.stackSize / 2);
        }

        private static void TraverseStack(GlobalState _g, LuaState _l)
        {
            StkId _o, _lim;
            CallInfo _ci;
            MarkValue(_g, Gt(_l));
            _lim = _l.top;
            for (_ci = _l.baseCi[0]; _ci <= _l.ci; CallInfo.Inc(ref _ci))
            {
                LuaAssert(_ci.top <= _l.stackLast);
                if (_lim < _ci.top) _lim = _ci.top;
            }
            for (_o = _l.stack[0]; _o < _l.top; StkId.Inc(ref _o))
                MarkValue(_g, _o);
            for (; _o <= _lim; StkId.Inc(ref _o))
                SetNilValue(_o);
            CheckStackSizes(_l, _lim);
        }

        private static l_mem PropagateMark(GlobalState _g)
        {
            GCObject _o = _g.gray;
            LuaAssert(IsGray(_o));
            Gray2Black(_o);
            switch (_o.gch.tt)
            {
                case LUA_TTABLE:
                    {
                        Table _h = gco2h(_o);
                        _g.gray = _h.gcList;
                        if (TraverseTable(_g, _h) != 0)
                            Black2Gray(_o);
                        return GetUnmanagedSize(typeof(Table)) +
                                GetUnmanagedSize(typeof(TValue)) * _h.sizeArray +
                                GetUnmanagedSize(typeof(Node)) * SizeNode(_h);
                    }
                case LUA_TFUNCTION:
                    {
                        Closure _cl = gco2cl(_o);
                        _g.gray = _cl.c.gclist;
                        TraverseClosure(_g, _cl);
                        return (_cl.c.isC != 0) ? SizeCclosure(_cl.c.nupvalues) : SizeLclosure(_cl.l.nupvalues);
                    }
                case LUA_TTHREAD:
                    {
                        LuaState _th = gco2th(_o);
                        _g.gray = _th.gcList;
                        _th.gcList = _g.grayAgain;
                        _g.grayAgain = _o;
                        Black2Gray(_o);
                        TraverseStack(_g, _th);
                        return GetUnmanagedSize(typeof(LuaState)) + GetUnmanagedSize(typeof(TValue)) * _th.stackSize + GetUnmanagedSize(typeof(CallInfo)) * _th.sizeCi;
                    }
                case LUATPROTO:
                    {
                        Proto _p = gco2p(_o);
                        _g.gray = _p.gclist;
                        TraverseProto(_g, _p);
                        return GetUnmanagedSize(typeof(Proto)) + GetUnmanagedSize(typeof(Instruction)) * _p.sizeCode + GetUnmanagedSize(typeof(Proto)) * _p.sizeP + GetUnmanagedSize(typeof(TValue)) * _p.sizeK +
                                  GetUnmanagedSize(typeof(int)) * _p.sizeLineInfo + GetUnmanagedSize(typeof(LocVar)) * _p.sizeLocVars + GetUnmanagedSize(typeof(TString)) * _p.sizeUpValues;
                    }
                default: LuaAssert(0); return 0;
            }
        }

        private static uint PropagateAll(GlobalState _g)
        {
            uint _m = 0;
            while (_g.gray != null) _m += (uint)PropagateMark(_g);
            return _m;
        }

        private static bool IsCleared(TValue _o, bool _is_key)
        {
            if (!IsCollectable(_o)) return false;
            if (TTIsString(_o))
            {
                StringMark(RawTSValue(_o));
                return false;
            }
            return IsWhite(GCValue(_o)) ||
              (TTIsUserData(_o) && (!_is_key && IsFinalized(UValue(_o))));
        }

        private static void ClearTable(GCObject _l)
        {
            while (_l != null)
            {
                Table _h = gco2h(_l);
                int _i = _h.sizeArray;
                LuaAssert(TestBit(_h.marked, VALUEWEAKBIT) || TestBit(_h.marked, KEYWEAKBIT));
                if (TestBit(_h.marked, VALUEWEAKBIT))
                {
                    while (_i-- != 0)
                    {
                        TValue _o = _h.array[_i];
                        if (IsCleared(_o, false))
                            SetNilValue(_o);
                    }
                }
                _i = SizeNode(_h);
                while (_i-- != 0)
                {
                    Node _n = GNode(_h, _i);
                    if (!TTIsNil(GVal(_n)) &&
                        (IsCleared(Key2TVal(_n), true) || IsCleared(GVal(_n), false)))
                    {
                        SetNilValue(GVal(_n));
                        RemoveEntry(_n);
                    }
                }
                _l = _h.gcList;
            }
        }

        private static void FreeObj(LuaState _l, GCObject _o)
        {
            switch (_o.gch.tt)
            {
                case LUATPROTO: LuaFFreeProto(_l, gco2p(_o)); break;
                case LUA_TFUNCTION: LuaFFreeClosure(_l, gco2cl(_o)); break;
                case LUATUPVAL: LuaFreeUpVal(_l, gco2uv(_o)); break;
                case LUA_TTABLE: LuaHFree(_l, gco2h(_o)); break;
                case LUA_TTHREAD:
                    {
                        LuaAssert(gco2th(_o) != _l && gco2th(_o) != G(_l).mainThread);
                        luaE_freethread(_l, gco2th(_o));
                        break;
                    }
                case LUA_TSTRING:
                    {
                        G(_l).strT.nuse--;
                        SubtractTotalBytes(_l, SizeString(gco2ts(_o)));
                        LuaMFreeMem(_l, gco2ts(_o));
                        break;
                    }
                case LUA_TUSERDATA:
                    {
                        SubtractTotalBytes(_l, SizeUData(gco2u(_o)));
                        LuaMFreeMem(_l, gco2u(_o));
                        break;
                    }
                default: LuaAssert(0); break;
            }
        }

        public static void SweepWholeList(LuaState _l, GCObjectRef _p) { SweepList(_l, _p, MAXLUMEM); }

        private static GCObjectRef SweepList(LuaState _l, GCObjectRef _p, lu_mem _count)
        {
            GCObject _curr;
            GlobalState _g = G(_l);
            int _dead_mask = OtherWhite(_g);
            while ((_curr = _p.get()) != null && _count-- > 0)
            {
                if (_curr.gch.tt == LUA_TTHREAD)
                    SweepWholeList(_l, new OpenValRef(gco2th(_curr)));
                if (((_curr.gch.marked ^ WHITEBITS) & _dead_mask) != 0)
                {
                    LuaAssert(!IsDead(_g, _curr) || TestBit(_curr.gch.marked, FIXEDBIT));
                    MakeWhite(_g, _curr);
                    _p = new NextRef(_curr.gch);
                }
                else
                {
                    LuaAssert(IsDead(_g, _curr) || _dead_mask == BitMask(SFIXEDBIT));
                    _p.set(_curr.gch.next);
                    if (_curr == _g.rootGc)
                        _g.rootGc = _curr.gch.next;
                    FreeObj(_l, _curr);
                }
            }
            return _p;
        }

        private static void CheckSizes(LuaState _l)
        {
            GlobalState _g = G(_l);
            if (_g.strT.nuse < (LuaIntegerType)(_g.strT.size / 4) && _g.strT.size > MINSTRTABSIZE * 2)
                LuaSResize(_l, _g.strT.size / 2);
            if (LuaZSizeBuffer(_g.buff) > LUAMINBUFFER * 2)
            {
                uint _new_size = LuaZSizeBuffer(_g.buff) / 2;
                LuaZResizeBuffer(_l, _g.buff, (int)_new_size);
            }
        }

        private static void GCTM(LuaState _l)
        {
            GlobalState _g = G(_l);
            GCObject _o = _g.tmuData.gch.next;
            Udata _udata = rawgco2u(_o);
            TValue _tm;
            if (_o == _g.tmuData)
                _g.tmuData = null;
            else
                _g.tmuData.gch.next = _udata.uv.next;
            _udata.uv.next = _g.mainThread.next;
            _g.mainThread.next = _o;
            MakeWhite(_g, _o);
            _tm = FastTm(_l, _udata.uv.metatable, TMS.TM_GC);
            if (_tm != null)
            {
                LuaByteType _old_ah = _l.allowHook;
                lu_mem _old_t = (lu_mem)_g.gcThreshold;
                _l.allowHook = 0;
                _g.gcThreshold = 2 * _g.totalBytes;
                SetObj2S(_l, _l.top, _tm);
                SetUValue(_l, _l.top + 1, _udata);
                _l.top += 2;
                LuaDCall(_l, _l.top - 2, 0);
                _l.allowHook = _old_ah;
                _g.gcThreshold = (uint)_old_t;
            }
        }

        public static void LuaCCallGCTM(LuaState _l)
        {
            while (G(_l).tmuData != null)
                GCTM(_l);
        }

        public static void LuaCFreeAll(LuaState _l)
        {
            GlobalState g = G(_l);
            int _i;
            g.currentWhite = (byte)(WHITEBITS | BitMask(SFIXEDBIT));
            SweepWholeList(_l, new RootGCRef(g));
            for (_i = 0; _i < g.strT.size; _i++)
                SweepWholeList(_l, new ArrayRef(g.strT.hash, _i));
        }

        private static void MarkMT(GlobalState _g)
        {
            int _i;
            for (_i = 0; _i < NUMTAGS; _i++)
                if (_g.mt[_i] != null) MarkObject(_g, _g.mt[_i]);
        }

        private static void MarkRoot(LuaState _l)
        {
            GlobalState _g = G(_l);
            _g.gray = null;
            _g.grayAgain = null;
            _g.weak = null;
            MarkObject(_g, _g.mainThread);
            MarkValue(_g, Gt(_g.mainThread));
            MarkValue(_g, Registry(_l));
            MarkMT(_g);
            _g.gcState = GCSpropagate;
        }

        private static void RemarkUpVals(GlobalState _g)
        {
            UpVal _uv;
            for (_uv = _g.uvHead.u.l.next; _uv != _g.uvHead; _uv = _uv.u.l.next)
            {
                LuaAssert(_uv.u.l.next.u.l.prev == _uv && _uv.u.l.prev.u.l.next == _uv);
                if (IsGray(obj2gco(_uv)))
                    MarkValue(_g, _uv.v);
            }
        }

        private static void Atomic(LuaState _l)
        {
            GlobalState _g = G(_l);
            uint _ud_size;
            RemarkUpVals(_g);
            PropagateAll(_g);
            _g.gray = _g.weak;
            _g.weak = null;
            LuaAssert(!IsWhite(obj2gco(_g.mainThread)));
            MarkObject(_g, _l);
            MarkMT(_g);
            PropagateAll(_g);
            _g.gray = _g.grayAgain;
            _g.grayAgain = null;
            PropagateAll(_g);
            _ud_size = LuaCSeparateUData(_l, 0);
            MarkTMU(_g);
            _ud_size += PropagateAll(_g);
            ClearTable(_g.weak);
            _g.currentWhite = CastByte(OtherWhite(_g));
            _g.sweepStrGc = 0;
            _g.sweepGc = new RootGCRef(_g);
            _g.gcState = GCSsweepstring;
            _g.estimate = _g.totalBytes - _ud_size;  /* first estimate */
        }

        private static l_mem SingleStep(LuaState _l)
        {
            GlobalState _g = G(_l);
            switch (_g.gcState)
            {
                case GCSpause:
                    {
                        MarkRoot(_l);
                        return 0;
                    }
                case GCSpropagate:
                    {
                        if (_g.gray != null)
                            return PropagateMark(_g);
                        else
                        {
                            Atomic(_l);
                            return 0;
                        }
                    }
                case GCSsweepstring:
                    {
                        lu_mem _old = (lu_mem)_g.totalBytes;
                        SweepWholeList(_l, new ArrayRef(_g.strT.hash, _g.sweepStrGc++));
                        if (_g.sweepStrGc >= _g.strT.size)
                            _g.gcState = GCSsweep;
                        LuaAssert(_old >= _g.totalBytes);
                        _g.estimate -= (uint)(_old - _g.totalBytes);
                        return GCSWEEPCOST;
                    }
                case GCSsweep:
                    {
                        lu_mem _old = (lu_mem)_g.totalBytes;
                        _g.sweepGc = SweepList(_l, _g.sweepGc, GCSWEEPMAX);
                        if (_g.sweepGc.get() == null)
                        {
                            CheckSizes(_l);
                            _g.gcState = GCSfinalize;
                        }
                        LuaAssert(_old >= _g.totalBytes);
                        _g.estimate -= (uint)(_old - _g.totalBytes);
                        return GCSWEEPMAX * GCSWEEPCOST;
                    }
                case GCSfinalize:
                    {
                        if (_g.tmuData != null)
                        {
                            GCTM(_l);
                            if (_g.estimate > GCFINALIZECOST)
                                _g.estimate -= GCFINALIZECOST;
                            return GCFINALIZECOST;
                        }
                        else
                        {
                            _g.gcState = GCSpause;
                            _g.gcDept = 0;
                            return 0;
                        }
                    }
                default: LuaAssert(0); return 0;
            }
        }

        public static void LuaCStep(LuaState _l)
        {
            GlobalState _g = G(_l);
            l_mem _lim = (l_mem)((GCSTEPSIZE / 100) * _g.gcStepMul);
            if (_lim == 0)
                _lim = (l_mem)((MAXLUMEM - 1) / 2);
            _g.gcDept += _g.totalBytes - _g.gcThreshold;
            do
            {
                _lim -= SingleStep(_l);
                if (_g.gcState == GCSpause)
                    break;
            } while (_lim > 0);
            if (_g.gcState != GCSpause)
            {
                if (_g.gcDept < GCSTEPSIZE)
                    _g.gcThreshold = _g.totalBytes + GCSTEPSIZE;
                else
                {
                    _g.gcDept -= GCSTEPSIZE;
                    _g.gcThreshold = _g.totalBytes;
                }
            }
            else
            {
                SetThreshold(_g);
            }
        }

        public static void LuaCFullGC(LuaState _l)
        {
            GlobalState _g = G(_l);
            if (_g.gcState <= GCSpropagate)
            {
                _g.sweepStrGc = 0;
                _g.sweepGc = new RootGCRef(_g);
                _g.gray = null;
                _g.grayAgain = null;
                _g.weak = null;
                _g.gcState = GCSsweepstring;
            }
            LuaAssert(_g.gcState != GCSpause && _g.gcState != GCSpropagate);
            while (_g.gcState != GCSfinalize)
            {
                LuaAssert(_g.gcState == GCSsweepstring || _g.gcState == GCSsweep);
                SingleStep(_l);
            }
            MarkRoot(_l);
            while (_g.gcState != GCSpause)
                SingleStep(_l);
            SetThreshold(_g);
        }

        public static void LuaCBarrierF(LuaState _l, GCObject _o, GCObject _v)
        {
            GlobalState _g = G(_l);
            LuaAssert(IsBlack(_o) && IsWhite(_v) && !IsDead(_g, _v) && !IsDead(_g, _o));
            LuaAssert(_g.gcState != GCSfinalize && _g.gcState != GCSpause);
            LuaAssert(TType(_o.gch) != LUA_TTABLE);
            if (_g.gcState == GCSpropagate)
                ReallyMarkObject(_g, _v);
            else
                MakeWhite(_g, _o);
        }

        public static void LuaCBarrierBack(LuaState _l, Table _t)
        {
            GlobalState _g = G(_l);
            GCObject _o = obj2gco(_t);
            LuaAssert(IsBlack(_o) && !IsDead(_g, _o));
            LuaAssert(_g.gcState != GCSfinalize && _g.gcState != GCSpause);
            Black2Gray(_o);
            _t.gcList = _g.grayAgain;
            _g.grayAgain = _o;
        }

        public static void LuaCLink(LuaState _l, GCObject _o, LuaByteType _tt)
        {
            GlobalState _g = G(_l);
            _o.gch.next = _g.rootGc;
            _g.rootGc = _o;
            _o.gch.marked = LuaCWhite(_g);
            _o.gch.tt = _tt;
        }

        public static void LuaCLinkUpVal(LuaState _l, UpVal _uv)
        {
            GlobalState _g = G(_l);
            GCObject _o = obj2gco(_uv);
            _o.gch.next = _g.rootGc;
            _g.rootGc = _o;
            if (IsGray(_o))
            {
                if (_g.gcState == GCSpropagate)
                {
                    Gray2Black(_o);
                    LuaCBarrier(_l, _uv, _uv.v);
                }
                else
                {
                    MakeWhite(_g, _o);
                    LuaAssert(_g.gcState != GCSfinalize && _g.gcState != GCSpause);
                }
            }
        }
    }
}