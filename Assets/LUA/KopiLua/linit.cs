﻿namespace KopiLua
{
    public partial class Lua
    {
        private readonly static LuaLReg[] _lualibs = {
          new LuaLReg("", LuaOpenBase),
          new LuaLReg(LUA_LOADLIBNAME, LuaOpenPackage),
          new LuaLReg(LUA_TABLIBNAME, LuaOpenTable),
          new LuaLReg(LUA_IOLIBNAME, LuaOpenIo),
          new LuaLReg(LUA_OSLIBNAME, LuaOpenOS),
          new LuaLReg(LUA_STRLIBNAME, LuaOpenString),
          new LuaLReg(LUA_MATHLIBNAME, LuaOpenMath),
          new LuaLReg(LUA_DBLIBNAME, LuaOpenDebug),
          new LuaLReg(null, null)
        };

        public static void LuaLOpenLibs(LuaState _l)
        {
            for (int i = 0; i < _lualibs.Length - 1; i++)
            {
                LuaLReg lib = _lualibs[i];
                LuaPushCFunction(_l, lib.func);
                LuaPushString(_l, lib.name);
                LuaCall(_l, 1, 0);
            }
        }
    }
}