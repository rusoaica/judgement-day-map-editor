﻿using System.IO;

namespace KopiLua
{
    using LuaIntegerType = System.Int32;

    public class FilePtr
    {
        public Stream file;
    }

    public partial class Lua
    {
        public const int IOINPUT = 1;
        public const int IOOUTPUT = 2;
        private static readonly string[] fnames = { "input", "output" };

        private static int PushResult(LuaState _l, int _i, CharPtr _filename)
        {
            int _en = ErrNo();
            if (_i != 0)
            {
                LuaPushBoolean(_l, 1);
                return 1;
            }
            else
            {
                LuaPushNil(_l);
                if (_filename != null)
                    LuaPushFString(_l, "%s: %s", _filename, StrError(_en));
                else
                    LuaPushFString(_l, "%s", StrError(_en));
                LuaPushInteger(_l, _en);
                return 3;
            }
        }

        private static void FileError(LuaState _l, int _arg, CharPtr _filename)
        {
            LuaPushFString(_l, "%s: %s", _filename, StrError(ErrNo()));
            LuaLArgError(_l, _arg, LuaToString(_l, -1));
        }

        public static FilePtr ToFilePointer(LuaState _l) { return (FilePtr)LuaLCheckUData(_l, 1, LUA_FILEHANDLE); }

        private static int GetIOType(LuaState _l)
        {
            object _ud;
            LuaLCheckAny(_l, 1);
            _ud = LuaToUserData(_l, 1);
            LuaGetField(_l, LUA_REGISTRYINDEX, LUA_FILEHANDLE);
            if (_ud == null || (LuaGetMetatable(_l, 1) == 0) || (LuaRawEqual(_l, -2, -1) == 0))
                LuaPushNil(_l);
            else if ((_ud as FilePtr).file == null)
                LuaPushLiteral(_l, "closed file");
            else
                LuaPushLiteral(_l, "file");
            return 1;
        }

        private static Stream ToFile(LuaState _l)
        {
            FilePtr _f = ToFilePointer(_l);
            if (_f.file == null)
                LuaLError(_l, "attempt to use a closed file");
            return _f.file;
        }

        private static FilePtr NewFile(LuaState _l)
        {

            FilePtr _pf = (FilePtr)LuaNewUserData(_l, typeof(FilePtr));
            _pf.file = null;
            LuaLGetMetatable(_l, LUA_FILEHANDLE);
            LuaSetMetatable(_l, -2);
            return _pf;
        }

        private static int IoNoClose(LuaState _l)
        {
            LuaPushNil(_l);
            LuaPushLiteral(_l, "cannot close standard file");
            return 2;
        }

        private static int IoPClose(LuaState _l)
        {
            FilePtr _p = ToFilePointer(_l);
            int _ok = (LuaPClose(_l, _p.file) == 0) ? 1 : 0;
            _p.file = null;
            return PushResult(_l, _ok, null);
        }

        private static int IoFClose(LuaState _l)
        {
            FilePtr _p = ToFilePointer(_l);
            int _ok = (FClose(_p.file) == 0) ? 1 : 0;
            _p.file = null;
            return PushResult(_l, _ok, null);
        }

        private static int AuxClose(LuaState _l)
        {
            LuaGetFEnv(_l, 1);
            LuaGetField(_l, -1, "__close");
            return (LuaToCFunction(_l, -1))(_l);
        }

        private static int IoClose(LuaState _l)
        {
            if (LuaIsNone(_l, 1))
                LuaRawGetI(_l, LUA_ENVIRONINDEX, IOOUTPUT);
            ToFile(_l);
            return AuxClose(_l);
        }

        private static int IoGC(LuaState _l)
        {
            Stream _f = ToFilePointer(_l).file;
            if (_f != null)
                AuxClose(_l);
            return 0;
        }

        private static int IoToString(LuaState _l)
        {
            Stream _f = ToFilePointer(_l).file;
            if (_f == null)
                LuaPushLiteral(_l, "file (closed)");
            else
                LuaPushFString(_l, "file (%p)", _f);
            return 1;
        }

        private static int IoOpen(LuaState _l)
        {
            CharPtr _filename = LuaLCheckString(_l, 1);
            CharPtr _mode = LuaLOptString(_l, 2, "r");
            FilePtr _pf = NewFile(_l);
            _pf.file = FOpen(_filename, _mode);
            return (_pf.file == null) ? PushResult(_l, 0, _filename) : 1;
        }

        private static int IoPopen(LuaState _l)
        {
            CharPtr _filename = LuaLCheckString(_l, 1);
            CharPtr _mode = LuaLOptString(_l, 2, "r");
            FilePtr _pf = NewFile(_l);
            _pf.file = LuaPopen(_l, _filename, _mode);
            return (_pf.file == null) ? PushResult(_l, 0, _filename) : 1;
        }

        private static int IoTmpFile(LuaState _l)
        {
            FilePtr _pf = NewFile(_l);
#if XBOX
			LuaLError(_l, "io_tmpfile not supported on Xbox360");
#else
            _pf.file = TmpFile();
#endif
            return (_pf.file == null) ? PushResult(_l, 0, null) : 1;
        }

        private static Stream GetIOFile(LuaState _l, int _f_index)
        {
            Stream _f;
            LuaRawGetI(_l, LUA_ENVIRONINDEX, _f_index);
            _f = (LuaToUserData(_l, -1) as FilePtr).file;
            if (_f == null)
                LuaLError(_l, "standard %s file is closed", fnames[_f_index - 1]);
            return _f;
        }

        private static int GIOFile(LuaState _l, int _f, CharPtr _mode)
        {
            if (!LuaIsNoneOrNil(_l, 1))
            {
                CharPtr _filename = LuaToString(_l, 1);
                if (_filename != null)
                {
                    FilePtr _pf = NewFile(_l);
                    _pf.file = FOpen(_filename, _mode);
                    if (_pf.file == null)
                        FileError(_l, 1, _filename);
                }
                else
                {
                    ToFile(_l);
                    LuaPushValue(_l, 1);
                }
                LuaRawSetI(_l, LUA_ENVIRONINDEX, _f);
            }
            LuaRawGetI(_l, LUA_ENVIRONINDEX, _f);
            return 1;
        }

        private static int IoInput(LuaState _l)
        {
            return GIOFile(_l, IOINPUT, "r");
        }

        private static int IoOutput(LuaState _l)
        {
            return GIOFile(_l, IOOUTPUT, "w");
        }

        private static void AuxLines(LuaState _l, int _idx, int _to_close)
        {
            LuaPushValue(_l, _idx);
            LuaPushBoolean(_l, _to_close);
            LuaPushCClosure(_l, IoReadLine, 2);
        }

        private static int FLines(LuaState _l)
        {
            ToFile(_l);
            AuxLines(_l, 1, 0);
            return 1;
        }

        private static int IoLines(LuaState _l)
        {
            if (LuaIsNoneOrNil(_l, 1))
            {
                LuaRawGetI(_l, LUA_ENVIRONINDEX, IOINPUT);
                return FLines(_l);
            }
            else
            {
                CharPtr _filename = LuaLCheckString(_l, 1);
                FilePtr _pf = NewFile(_l);
                _pf.file = FOpen(_filename, "r");
                if (_pf.file == null)
                    FileError(_l, 1, _filename);
                AuxLines(_l, LuaGetTop(_l), 1);
                return 1;
            }
        }

        private static int ReadNumber(LuaState _l, Stream _f)
        {
            object[] _parms = { (object)(double)0.0 };
            if (FScanF(_f, LUA_NUMBER_SCAN, _parms) == 1)
            {
                LuaPushNumber(_l, (double)_parms[0]);
                return 1;
            }
            else
            {
                LuaPushNil(_l);
                return 0;
            }
        }

        private static int TestEof(LuaState _l, Stream _f)
        {
            int _c = GetC(_f);
            UngetC(_c, _f);
            LuaPushLString(_l, null, 0);
            return (_c != EOF) ? 1 : 0;
        }


        private static int ReadLine(LuaState _l, Stream _f)
        {
            LuaLBuffer _b = new LuaLBuffer();
            LuaLBuffInit(_l, _b);
            for (; ; )
            {
                uint __l;
                CharPtr _p = LuaLPrepBuffer(_b);
                if (FGets(_p, _f) == null)
                {
                    LuaLPushResult(_b);
                    return (LuaObjectLen(_l, -1) > 0) ? 1 : 0;
                }
                __l = (uint)StrLen(_p);
                if (__l == 0 || _p[__l - 1] != '\n')
                    LuaLAddSize(_b, (int)__l);
                else
                {
                    LuaLAddSize(_b, (int)(__l - 1));
                    LuaLPushResult(_b);
                    return 1;
                }
            }
        }

        private static int ReadChars(LuaState _l, Stream _f, uint _n)
        {
            uint _r_len;
            uint _nr;
            LuaLBuffer _b = new LuaLBuffer();
            LuaLBuffInit(_l, _b);
            _r_len = LUAL_BUFFERSIZE;
            do
            {
                CharPtr _p = LuaLPrepBuffer(_b);
                if (_r_len > _n) _r_len = _n;
                _nr = (uint)FRead(_p, GetUnmanagedSize(typeof(char)), (int)_r_len, _f);
                LuaLAddSize(_b, (int)_nr);
                _n -= _nr;
            } while (_n > 0 && _nr == _r_len);
            LuaLPushResult(_b);
            return (_n == 0 || LuaObjectLen(_l, -1) > 0) ? 1 : 0;
        }

        private static int GRead(LuaState _l, Stream _f, int _first)
        {
            int _n_args = LuaGetTop(_l) - 1;
            int _success;
            int _n;
            ClearErr(_f);
            if (_n_args == 0)
            {
                _success = ReadLine(_l, _f);
                _n = _first + 1;
            }
            else
            {
                LuaLCheckStack(_l, _n_args + LUA_MINSTACK, "too many arguments");
                _success = 1;
                for (_n = _first; (_n_args-- != 0) && (_success != 0); _n++)
                {
                    if (LuaType(_l, _n) == LUA_TNUMBER)
                    {
                        uint __l = (uint)LuaToInteger(_l, _n);
                        _success = (__l == 0) ? TestEof(_l, _f) : ReadChars(_l, _f, __l);
                    }
                    else
                    {
                        CharPtr _p = LuaToString(_l, _n);
                        LuaLArgCheck(_l, (_p != null) && (_p[0] == '*'), _n, "invalid option");
                        switch (_p[1])
                        {
                            case 'n':
                                _success = ReadNumber(_l, _f);
                                break;
                            case 'l':
                                _success = ReadLine(_l, _f);
                                break;
                            case 'a':
                                ReadChars(_l, _f, ~((uint)0));
                                _success = 1;
                                break;
                            default:
                                return LuaLArgError(_l, _n, "invalid format");
                        }
                    }
                }
            }
            if (FError(_f) != 0)
                return PushResult(_l, 0, null);
            if (_success == 0)
            {
                LuaPop(_l, 1);
                LuaPushNil(_l);
            }
            return _n - _first;
        }

        private static int IoRead(LuaState _l)
        {
            return GRead(_l, GetIOFile(_l, IOINPUT), 1);
        }

        private static int FRead(LuaState _l)
        {
            return GRead(_l, ToFile(_l), 2);
        }

        private static int IoReadLine(LuaState _l)
        {
            Stream _f = (LuaToUserData(_l, LuaUpValueIndex(1)) as FilePtr).file;
            int _sucess;
            if (_f == null)
                LuaLError(_l, "file is already closed");
            _sucess = ReadLine(_l, _f);
            if (FError(_f) != 0)
                return LuaLError(_l, "%s", StrError(ErrNo()));
            if (_sucess != 0) return 1;
            else
            {
                if (LuaToBoolean(_l, LuaUpValueIndex(2)) != 0)
                {
                    LuaSetTop(_l, 0);
                    LuaPushValue(_l, LuaUpValueIndex(1));
                    AuxClose(_l);
                }
                return 0;
            }
        }

        private static int GWrite(LuaState _l, Stream _f, int _arg)
        {
            int _n_args = LuaGetTop(_l) - 1;
            int _status = 1;
            for (; (_n_args--) != 0; _arg++)
            {
                if (LuaType(_l, _arg) == LUA_TNUMBER)
                {
                    _status = ((_status != 0) &&
                        (FPrintF(_f, LUA_NUMBER_FMT, LuaToNumber(_l, _arg)) > 0)) ? 1 : 0;
                }
                else
                {
                    uint __l;
                    CharPtr _s = LuaLCheckLString(_l, _arg, out __l);
                    _status = ((_status != 0) && (FWrite(_s, GetUnmanagedSize(typeof(char)), (int)__l, _f) == __l)) ? 1 : 0;
                }
            }
            return PushResult(_l, _status, null);
        }

        private static int IoWrite(LuaState _l)
        {
            return GWrite(_l, GetIOFile(_l, IOOUTPUT), 1);
        }

        private static int FWrite(LuaState _l)
        {
            return GWrite(_l, ToFile(_l), 2);
        }

        private static int FSeek(LuaState _l)
        {
            int[] _mode = { SEEK_SET, SEEK_CUR, SEEK_END };
            CharPtr[] _mode_names = { "set", "cur", "end", null };
            Stream _f = ToFile(_l);
            int _op = LuaLCheckOption(_l, 2, "cur", _mode_names);
            long _offset = LuaLOptLong(_l, 3, 0);
            _op = FSeek(_f, _offset, _mode[_op]);
            if (_op != 0)
                return PushResult(_l, 0, null);
            else
            {
                LuaPushInteger(_l, FTell(_f));
                return 1;
            }
        }

        private static int FSetVBuf(LuaState _l)
        {
            CharPtr[] _mode_names = { "no", "full", "line", null };
            int[] _mode = { _IONBF, _IOFBF, _IOLBF };
            Stream _f = ToFile(_l);
            int _op = LuaLCheckOption(_l, 2, null, _mode_names);
            LuaIntegerType _sz = LuaLOptInteger(_l, 3, LUAL_BUFFERSIZE);
            int _res = SetVBuf(_f, null, _mode[_op], (uint)_sz);
            return PushResult(_l, (_res == 0) ? 1 : 0, null);
        }

        private static int IoFlush(LuaState _l)
        {
            int _result = 1;
            try { GetIOFile(_l, IOOUTPUT).Flush(); } catch { _result = 0; }
            return PushResult(_l, _result, null);
        }

        private static int FFlush(LuaState _l)
        {
            int _result = 1;
            try { ToFile(_l).Flush(); } catch { _result = 0; }
            return PushResult(_l, _result, null);
        }

        private readonly static LuaLReg[] _iolib = {
          new LuaLReg("close", IoClose),
          new LuaLReg("flush", IoFlush),
          new LuaLReg("input", IoInput),
          new LuaLReg("lines", IoLines),
          new LuaLReg("open", IoOpen),
          new LuaLReg("output", IoOutput),
          new LuaLReg("popen", IoPopen),
          new LuaLReg("read", IoRead),
          new LuaLReg("tmpfile", IoTmpFile),
          new LuaLReg("type", GetIOType),
          new LuaLReg("write", IoWrite),
          new LuaLReg(null, null)
        };

        private readonly static LuaLReg[] _flib = {
          new LuaLReg("close", IoClose),
          new LuaLReg("flush", FFlush),
          new LuaLReg("lines", FLines),
          new LuaLReg("read", FRead),
          new LuaLReg("seek", FSeek),
          new LuaLReg("setvbuf", FSetVBuf),
          new LuaLReg("write", FWrite),
          new LuaLReg("__gc", IoGC),
          new LuaLReg("__tostring", IoToString),
          new LuaLReg(null, null)
        };

        private static void CreateMeta(LuaState _l)
        {
            LuaLNewMetatable(_l, LUA_FILEHANDLE);
            LuaPushValue(_l, -1);
            LuaSetField(_l, -2, "__index");
            LuaLRegister(_l, null, _flib);
        }

        private static void CreateStdFile(LuaState _l, Stream _f, int _k, CharPtr _f_name)
        {
            NewFile(_l).file = _f;
            if (_k > 0)
            {
                LuaPushValue(_l, -1);
                LuaRawSetI(_l, LUA_ENVIRONINDEX, _k);
            }
            LuaPushValue(_l, -2);
            LuaSetFEnv(_l, -2);
            LuaSetField(_l, -3, _f_name);
        }

        private static void NewFEnv(LuaState _l, LuaNativeFunction _cls)
        {
            LuaCreateTable(_l, 0, 1);
            LuaPushCFunction(_l, _cls);
            LuaSetField(_l, -2, "__close");
        }

        public static int LuaOpenIo(LuaState _l)
        {
            CreateMeta(_l);
            NewFEnv(_l, IoFClose);
            LuaReplace(_l, LUA_ENVIRONINDEX);
            LuaLRegister(_l, LUA_IOLIBNAME, _iolib);
            NewFEnv(_l, IoNoClose);
            CreateStdFile(_l, stdIn, IOINPUT, "stdin");
            CreateStdFile(_l, stdOut, IOOUTPUT, "stdout");
            CreateStdFile(_l, stdErr, 0, "stderr");
            LuaPop(_l, 1);
            LuaGetField(_l, -1, "popen");
            NewFEnv(_l, IoPClose);
            LuaSetFEnv(_l, -2);
            LuaPop(_l, 1);
            return 1;
        }
    }
}