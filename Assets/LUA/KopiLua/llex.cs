﻿using System;

namespace KopiLua
{
    using LuaNumberType = System.Double;
    using TValue = Lua.LuaTypeValue;
    using ZIO = Lua.Zio;

    public partial class Lua
    {
        public const int FIRSTRESERVED = 257;
        public const int TOKENLEN = 9;
        public const int NUMRESERVED = (int)RESERVED.TK_WHILE - FIRSTRESERVED + 1;
        public const int MAXSRC = 80;

        public enum RESERVED
        {
            TK_AND = FIRSTRESERVED, TK_BREAK,
            TK_DO, TK_ELSE, TK_ELSEIF, TK_END, TK_FALSE, TK_FOR, TK_FUNCTION,
            TK_IF, TK_IN, TK_LOCAL, TK_NIL, TK_NOT, TK_OR, TK_REPEAT,
            TK_RETURN, TK_THEN, TK_TRUE, TK_UNTIL, TK_WHILE,
            TK_CONCAT, TK_DOTS, TK_EQ, TK_GE, TK_LE, TK_NE, TK_NUMBER,
            TK_NAME, TK_STRING, TK_EOS
        };

        public class SemInfo
        {
            public SemInfo() { }

            public SemInfo(SemInfo copy)
            {
                this.r = copy.r;
                this.ts = copy.ts;
            }

            public LuaNumberType r;
            public TString ts;
        };

        public class Token
        {
            public Token() { }

            public Token(Token copy)
            {
                this.token = copy.token;
                this.seminfo = new SemInfo(copy.seminfo);
            }

            public int token;
            public SemInfo seminfo = new SemInfo();
        };

        public class LexState
        {
            public int current;
            public int lineNumber;
            public int lastLine;
            public Token t = new Token();
            public Token lookAhead = new Token();
            public FuncState fs;
            public LuaState l;
            public ZIO z;
            public Mbuffer buff;
            public TString source;
            public char decpoint;
        };

        public static void Next(LexState _ls) { _ls.current = zgetc(_ls.z); }

        public static bool CurrIsNewline(LexState _ls) { return (_ls.current == '\n' || _ls.current == '\r'); }

        public static readonly string[] LuaXTokens = {
            "and", "break", "do", "else", "elseif",
            "end", "false", "for", "function", "if",
            "in", "local", "nil", "not", "or", "repeat",
            "return", "then", "true", "until", "while",
            "..", "...", "==", ">=", "<=", "~=",
            "<number>", "<name>", "<string>", "<eof>"
        };

        public static void SaveAndNext(LexState _ls) { Save(_ls, _ls.current); Next(_ls); }

        private static void Save(LexState _ls, int c)
        {
            Mbuffer _b = _ls.buff;
            if (_b.n + 1 > _b.buffSize)
            {
                uint _new_size;
                if (_b.buffSize >= MAXSIZET / 2)
                    LuaXLexError(_ls, "lexical element too long", 0);
                _new_size = _b.buffSize * 2;
                LuaZResizeBuffer(_ls.l, _b, (int)_new_size);
            }
            _b.buffer[_b.n++] = (char)c;
        }

        public static void LuaXInit(LuaState _l)
        {
            int _i;
            for (_i = 0; _i < NUMRESERVED; _i++)
            {
                TString _ts = LuaSNew(_l, LuaXTokens[_i]);
                LuaSFix(_ts);
                LuaAssert(LuaXTokens[_i].Length + 1 <= TOKENLEN);
                _ts.tsv.reserved = CastByte(_i + 1);
            }
        }

        public static CharPtr LuaXTokenToString(LexState _ls, int _token)
        {
            if (_token < FIRSTRESERVED)
            {
                LuaAssert(_token == (byte)_token);
                return (IsCntrl(_token)) ? LuaOPushFString(_ls.l, "char(%d)", _token) : LuaOPushFString(_ls.l, "%c", _token);
            }
            else
                return LuaXTokens[(int)_token - FIRSTRESERVED];
        }

        public static CharPtr TextToken(LexState _ls, int _token)
        {
            switch (_token)
            {
                case (int)RESERVED.TK_NAME:
                case (int)RESERVED.TK_STRING:
                case (int)RESERVED.TK_NUMBER:
                    Save(_ls, '\0');
                    return LuaZBuffer(_ls.buff);
                default:
                    return LuaXTokenToString(_ls, _token);
            }
        }

        public static void LuaXLexError(LexState _ls, CharPtr _msg, int _token)
        {
            CharPtr _buff = new char[MAXSRC];
            LuaOChunkID(_buff, GetStr(_ls.source), MAXSRC);
            _msg = LuaOPushFString(_ls.l, "%s:%d: %s", _buff, _ls.lineNumber, _msg);
            if (_token != 0)
                LuaOPushFString(_ls.l, "%s near " + LUA_QS, _msg, TextToken(_ls, _token));
            LuaDThrow(_ls.l, LUA_ERRSYNTAX);
        }

        public static void LuaXSyntaxError(LexState _ls, CharPtr _msg)
        {
            LuaXLexError(_ls, _msg, _ls.t.token);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static TString LuaXNewString(LexState _ls, CharPtr _str, uint _l)
        {
            LuaState __l = _ls.l;
            TString _ts = LuaSNewLStr(__l, _str, _l);
            TValue _o = LuaHSetStr(__l, _ls.fs.h, _ts);
            if (TTIsNil(_o))
            {
                SetBValue(_o, 1);
                LuaCCheckGC(__l);
            }
            return _ts;
        }

        private static void IncLineNumber(LexState _ls)
        {
            int _old = _ls.current;
            LuaAssert(CurrIsNewline(_ls));
            Next(_ls);
            if (CurrIsNewline(_ls) && _ls.current != _old)
                Next(_ls);
            if (++_ls.lineNumber >= MAXINT)
                LuaXSyntaxError(_ls, "chunk has too many lines");
        }

        public static void LuaXSetInput(LuaState _l, LexState _ls, ZIO _z, TString _source)
        {
            _ls.decpoint = '.';
            _ls.l = _l;
            _ls.lookAhead.token = (int)RESERVED.TK_EOS;  /* no look-ahead token */
            _ls.z = _z;
            _ls.fs = null;
            _ls.lineNumber = 1;
            _ls.lastLine = 1;
            _ls.source = _source;
            LuaZResizeBuffer(_ls.l, _ls.buff, LUAMINBUFFER);
            Next(_ls);
        }

        private static int CheckNext(LexState _ls, CharPtr _set)
        {
            if (StrChr(_set, (char)_ls.current) == null)
                return 0;
            SaveAndNext(_ls);
            return 1;
        }

        private static void BufferReplace(LexState _ls, char _from, char _to)
        {
            uint _n = LuaZBuffLen(_ls.buff);
            CharPtr p = LuaZBuffer(_ls.buff);
            while ((_n--) != 0)
                if (p[_n] == _from) p[_n] = _to;
        }

        private static void TryDecPoint(LexState _ls, SemInfo _seminfo)
        {
            char _old = _ls.decpoint;
            _ls.decpoint = '.';
            BufferReplace(_ls, _old, _ls.decpoint);
            if (LuaOStr2d(LuaZBuffer(_ls.buff), out _seminfo.r) == 0)
            {
                BufferReplace(_ls, _ls.decpoint, '.');
                LuaXLexError(_ls, "malformed number", (int)RESERVED.TK_NUMBER);
            }
        }

        private static void ReadNumeral(LexState _ls, SemInfo _seminfo)
        {
            LuaAssert(IsDigit(_ls.current));
            do
            {
                SaveAndNext(_ls);
            } while (IsDigit(_ls.current) || _ls.current == '.');
            if (CheckNext(_ls, "Ee") != 0)
                CheckNext(_ls, "+-");
            while (IsAlnum(_ls.current) || _ls.current == '_')
                SaveAndNext(_ls);
            Save(_ls, '\0');
            BufferReplace(_ls, '.', _ls.decpoint);
            if (LuaOStr2d(LuaZBuffer(_ls.buff), out _seminfo.r) == 0)
                TryDecPoint(_ls, _seminfo);
        }


        private static int SkipSep(LexState _ls)
        {
            int _count = 0;
            int _s = _ls.current;
            LuaAssert(_s == '[' || _s == ']');
            SaveAndNext(_ls);
            while (_ls.current == '=')
            {
                SaveAndNext(_ls);
                _count++;
            }
            return (_ls.current == _s) ? _count : (-_count) - 1;
        }


        private static void ReadLongString(LexState _ls, SemInfo _seminfo, int _sep)
        {
            SaveAndNext(_ls);
            if (CurrIsNewline(_ls))
                IncLineNumber(_ls);
            for (; ; )
            {
                switch (_ls.current)
                {
                    case EOZ:
                        LuaXLexError(_ls, (_seminfo != null) ? "unfinished long string" : "unfinished long comment", (int)RESERVED.TK_EOS);
                        break;
#if LUA_COMPAT_LSTR
			        case '[':
                        {
				        if (skip_sep(_ls) == _sep)
                        {
				            save_and_next(_ls);  
				            cont++;
#if LUA_COMPAT_LSTR
				            if (_sep == 0)
					            luaX_lexerror(_ls, "nesting of [[...]] is deprecated", '[');
#endif
				        }
				        break;
			        }
#endif
                    case ']':
                        if (SkipSep(_ls) == _sep)
                        {
                            SaveAndNext(_ls);
                            goto endloop;
                        }
                        break;
                    case '\n':
                    case '\r':
                        Save(_ls, '\n');
                        IncLineNumber(_ls);
                        if (_seminfo == null) LuaZResetBuffer(_ls.buff);
                        break;
                    default:
                        {
                            if (_seminfo != null) SaveAndNext(_ls);
                            else Next(_ls);
                        }
                        break;
                }
            }
            endloop:
            if (_seminfo != null)
            {
                _seminfo.ts = LuaXNewString(_ls, LuaZBuffer(_ls.buff) + (2 + _sep), (uint)(LuaZBuffLen(_ls.buff) - 2 * (2 + _sep)));
            }
        }

        static void ReadString(LexState _ls, int _del, SemInfo _seminfo)
        {
            SaveAndNext(_ls);
            while (_ls.current != _del)
            {
                switch (_ls.current)
                {
                    case EOZ:
                        LuaXLexError(_ls, "unfinished string", (int)RESERVED.TK_EOS);
                        continue;
                    case '\n':
                    case '\r':
                        LuaXLexError(_ls, "unfinished string", (int)RESERVED.TK_STRING);
                        continue;
                    case '\\':
                        {
                            int c;
                            Next(_ls);
                            switch (_ls.current)
                            {
                                case 'a': c = '\a'; break;
                                case 'b': c = '\b'; break;
                                case 'f': c = '\f'; break;
                                case 'n': c = '\n'; break;
                                case 'r': c = '\r'; break;
                                case 't': c = '\t'; break;
                                case 'v': c = '\v'; break;
                                case '\n':
                                case '\r': Save(_ls, '\n'); IncLineNumber(_ls); continue;
                                case EOZ: continue;
                                default:
                                    {
                                        if (!IsDigit(_ls.current))
                                            SaveAndNext(_ls);
                                        else
                                        {
                                            int i = 0;
                                            c = 0;
                                            do
                                            {
                                                c = 10 * c + (_ls.current - '0');
                                                Next(_ls);
                                            } while (++i < 3 && IsDigit(_ls.current));
                                            if (c > System.Byte.MaxValue)
                                                LuaXLexError(_ls, "escape sequence too large", (int)RESERVED.TK_STRING);
                                            Save(_ls, c);
                                        }
                                        continue;
                                    }
                            }
                            Save(_ls, c);
                            Next(_ls);
                            continue;
                        }
                    default:
                        SaveAndNext(_ls);
                        break;
                }
            }
            SaveAndNext(_ls);
            _seminfo.ts = LuaXNewString(_ls, LuaZBuffer(_ls.buff) + 1, LuaZBuffLen(_ls.buff) - 2);
        }

        private static int LLex(LexState _ls, SemInfo _seminfo)
        {
            LuaZResetBuffer(_ls.buff);
            for (; ; )
            {
                switch (_ls.current)
                {
                    case '\n':
                    case '\r':
                        {
                            IncLineNumber(_ls);
                            continue;
                        }
                    case '-':
                        {
                            Next(_ls);
                            if (_ls.current != '-') return '-';
                            Next(_ls);
                            if (_ls.current == '[')
                            {
                                int sep = SkipSep(_ls);
                                LuaZResetBuffer(_ls.buff);
                                if (sep >= 0)
                                {
                                    ReadLongString(_ls, null, sep);
                                    LuaZResetBuffer(_ls.buff);
                                    continue;
                                }
                            }
                            while (!CurrIsNewline(_ls) && _ls.current != EOZ)
                                Next(_ls);
                            continue;
                        }
                    case '[':
                        {
                            int _sep = SkipSep(_ls);
                            if (_sep >= 0)
                            {
                                ReadLongString(_ls, _seminfo, _sep);
                                return (int)RESERVED.TK_STRING;
                            }
                            else if (_sep == -1) return '[';
                            else LuaXLexError(_ls, "invalid long string delimiter", (int)RESERVED.TK_STRING);
                        }
                        break;
                    case '=':
                        {
                            Next(_ls);
                            if (_ls.current != '=') return '=';
                            else { Next(_ls); return (int)RESERVED.TK_EQ; }
                        }
                    case '<':
                        {
                            Next(_ls);
                            if (_ls.current != '=') return '<';
                            else { Next(_ls); return (int)RESERVED.TK_LE; }
                        }
                    case '>':
                        {
                            Next(_ls);
                            if (_ls.current != '=') return '>';
                            else { Next(_ls); return (int)RESERVED.TK_GE; }
                        }
                    case '~':
                        {
                            Next(_ls);
                            if (_ls.current != '=') return '~';
                            else { Next(_ls); return (int)RESERVED.TK_NE; }
                        }
                    case '"':
                    case '\'':
                        {
                            ReadString(_ls, _ls.current, _seminfo);
                            return (int)RESERVED.TK_STRING;
                        }
                    case '.':
                        {
                            SaveAndNext(_ls);
                            if (CheckNext(_ls, ".") != 0)
                            {
                                if (CheckNext(_ls, ".") != 0)
                                    return (int)RESERVED.TK_DOTS;   /* ... */
                                else return (int)RESERVED.TK_CONCAT;   /* .. */
                            }
                            else if (!IsDigit(_ls.current)) return '.';
                            else
                            {
                                ReadNumeral(_ls, _seminfo);
                                return (int)RESERVED.TK_NUMBER;
                            }
                        }
                    case EOZ:
                        {
                            return (int)RESERVED.TK_EOS;
                        }
                    default:
                        {
                            if (IsSpace(_ls.current))
                            {
                                LuaAssert(!CurrIsNewline(_ls));
                                Next(_ls);
                                continue;
                            }
                            else if (IsDigit(_ls.current))
                            {
                                ReadNumeral(_ls, _seminfo);
                                return (int)RESERVED.TK_NUMBER;
                            }
                            else if (IsAlpha(_ls.current) || _ls.current == '_')
                            {
                                TString _ts;
                                do
                                {
                                    SaveAndNext(_ls);
                                } while (IsAlnum(_ls.current) || _ls.current == '_');
                                _ts = LuaXNewString(_ls, LuaZBuffer(_ls.buff), LuaZBuffLen(_ls.buff));
                                if (_ts.tsv.reserved > 0)
                                    return _ts.tsv.reserved - 1 + FIRSTRESERVED;
                                else
                                {
                                    _seminfo.ts = _ts;
                                    return (int)RESERVED.TK_NAME;
                                }
                            }
                            else
                            {
                                int _c = _ls.current;
                                Next(_ls);
                                return _c;
                            }
                        }
                }
            }
        }

        public static void LuaXNext(LexState _ls)
        {
            _ls.lastLine = _ls.lineNumber;
            if (_ls.lookAhead.token != (int)RESERVED.TK_EOS)
            {
                _ls.t = new Token(_ls.lookAhead);
                _ls.lookAhead.token = (int)RESERVED.TK_EOS;
            }
            else
                _ls.t.token = LLex(_ls, _ls.t.seminfo);
        }

        public static void LuaXLookAhead(LexState _ls)
        {
            LuaAssert(_ls.lookAhead.token == (int)RESERVED.TK_EOS);
            _ls.lookAhead.token = LLex(_ls, _ls.lookAhead.seminfo);
        }
    }
}