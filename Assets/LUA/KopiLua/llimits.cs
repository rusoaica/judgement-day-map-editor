﻿using System;
using System.Diagnostics;

namespace KopiLua
{
    using lu_byte = System.Byte;
    using lu_mem = System.UInt32;
    using lua_Number = System.Double;

    public partial class Lua
    {
        public const int MAXSTACK = 250;
        public const int MINSTRTABSIZE = 32;
        public const int LUAMINBUFFER = 32;

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const uint MAXSIZET = uint.MaxValue - 2;
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const lu_mem MAXLUMEM = lu_mem.MaxValue - 2;
        public const int MAXINT = (Int32.MaxValue - 2);
        
#if lua_assert
		[Conditional("DEBUG")]
		public static void LuaAssert(bool _c) {Debug.Assert(_c);}

		[Conditional("DEBUG")]
		public static void LuaAssert(int _c) { Debug.Assert(_c != 0); }

		internal static object CheckExp(bool _c, object _e)		{LuaAssert(_c); return _e;}
		public static object CheckExp(int _c, object _e) { LuaAssert(_c != 0); return _e; }
#else
        [Conditional("DEBUG")]
        public static void LuaAssert(bool _c) { }

        [Conditional("DEBUG")]
        public static void LuaAssert(int _c) { }

        public static object CheckExp(bool _c, object _e) { return _e; }
        public static object CheckExp(int _c, object _e) { return _e; }
#endif

        [Conditional("DEBUG")]
        internal static void ApiCheck(object _o, bool _e) { LuaAssert(_e); }
        internal static void ApiCheck(object _o, int _e) { LuaAssert(_e != 0); }
        internal static lu_byte CastByte(int _i) { return (lu_byte)_i; }
        internal static lu_byte CastByte(long _i) { return (lu_byte)(int)_i; }
        internal static lu_byte CastByte(bool _i) { return _i ? (lu_byte)1 : (lu_byte)0; }
        internal static lu_byte CastByte(lua_Number _i) { return (lu_byte)_i; }
        internal static lu_byte CastByte(object _i) { return (lu_byte)(int)(_i); }

        internal static int CastInt(int _i) { return (int)_i; }
        internal static int CastInt(uint _i) { return (int)_i; }
        internal static int CastInt(long _i) { return (int)(int)_i; }
        internal static int CastInt(ulong _i) { return (int)(int)_i; }
        internal static int CastInt(bool _i) { return _i ? (int)1 : (int)0; }
        internal static int CastInt(lua_Number _i) { return (int)_i; }
        internal static int CastInt(object _i) { Debug.Assert(false, "Can't convert int."); return Convert.ToInt32(_i); }

        internal static lua_Number CastNum(int _i) { return (lua_Number)_i; }
        internal static lua_Number CastNum(uint _i) { return (lua_Number)_i; }
        internal static lua_Number CastNum(long _i) { return (lua_Number)_i; }
        internal static lua_Number CastNum(ulong _i) { return (lua_Number)_i; }
        internal static lua_Number CastNum(bool _i) { return _i ? (lua_Number)1 : (lua_Number)0; }
        internal static lua_Number CastNum(object _i) { Debug.Assert(false, "Can't convert number."); return Convert.ToSingle(_i); }

#if !lua_lock
        public static void LuaLock(LuaState _l) { }
        public static void LuaUnlock(LuaState _l) { }
#endif

#if !luai_threadyield
        public static void LuaIThreadYield(LuaState _l) { LuaUnlock(_l); LuaLock(_l); }
#endif
    }
}