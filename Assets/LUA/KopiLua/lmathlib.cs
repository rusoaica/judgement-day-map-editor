﻿using System;

namespace KopiLua
{
    using LuaNumberType = System.Double;

    public partial class Lua
    {
        public const double PI = 3.14159265358979323846;
        public const double RADIANS_PER_DEGREE = PI / 180.0;
        private static Random _rng = new Random();

        private static int MathAbs(LuaState _l)
        {
            LuaPushNumber(_l, Math.Abs(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathSin(LuaState _l)
        {
            LuaPushNumber(_l, Math.Sin(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathSinH(LuaState _l)
        {
            LuaPushNumber(_l, Math.Sinh(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathCos(LuaState _l)
        {
            LuaPushNumber(_l, Math.Cos(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathCosH(LuaState _l)
        {
            LuaPushNumber(_l, Math.Cosh(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathTan(LuaState _l)
        {
            LuaPushNumber(_l, Math.Tan(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathTanH(LuaState _l)
        {
            LuaPushNumber(_l, Math.Tanh(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathASin(LuaState _l)
        {
            LuaPushNumber(_l, Math.Asin(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathACos(LuaState _l)
        {
            LuaPushNumber(_l, Math.Acos(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathATan(LuaState _l)
        {
            LuaPushNumber(_l, Math.Atan(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathATan2(LuaState _l)
        {
            LuaPushNumber(_l, Math.Atan2(LuaLCheckNumber(_l, 1), LuaLCheckNumber(_l, 2)));
            return 1;
        }

        private static int MathCeil(LuaState _l)
        {
            LuaPushNumber(_l, Math.Ceiling(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathFloor(LuaState _l)
        {
            LuaPushNumber(_l, Math.Floor(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathFMod(LuaState _l)
        {
            LuaPushNumber(_l, FMod(LuaLCheckNumber(_l, 1), LuaLCheckNumber(_l, 2)));
            return 1;
        }

        private static int MathModF(LuaState _l)
        {
            double _ip;
            double _fp = ModF(LuaLCheckNumber(_l, 1), out _ip);
            LuaPushNumber(_l, _ip);
            LuaPushNumber(_l, _fp);
            return 2;
        }

        private static int MathSqrt(LuaState _l)
        {
            LuaPushNumber(_l, Math.Sqrt(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathPow(LuaState _l)
        {
            LuaPushNumber(_l, Math.Pow(LuaLCheckNumber(_l, 1), LuaLCheckNumber(_l, 2)));
            return 1;
        }

        private static int MathLog(LuaState _l)
        {
            LuaPushNumber(_l, Math.Log(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathLog10(LuaState _l)
        {
            LuaPushNumber(_l, Math.Log10(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathExp(LuaState _l)
        {
            LuaPushNumber(_l, Math.Exp(LuaLCheckNumber(_l, 1)));
            return 1;
        }

        private static int MathDeg(LuaState _l)
        {
            LuaPushNumber(_l, LuaLCheckNumber(_l, 1) / RADIANS_PER_DEGREE);
            return 1;
        }

        private static int MathRad(LuaState _l)
        {
            LuaPushNumber(_l, LuaLCheckNumber(_l, 1) * RADIANS_PER_DEGREE);
            return 1;
        }

        private static int MathFRExp(LuaState _l)
        {
            int _e;
            LuaPushNumber(_l, FRExp(LuaLCheckNumber(_l, 1), out _e));
            LuaPushInteger(_l, _e);
            return 2;
        }

        private static int MathLDExp(LuaState _l)
        {
            LuaPushNumber(_l, LDExp(LuaLCheckNumber(_l, 1), LuaLCheckInt(_l, 2)));
            return 1;
        }

        private static int MahMin(LuaState _l)
        {
            int _n = LuaGetTop(_l);  
            LuaNumberType _dmin = LuaLCheckNumber(_l, 1);
            int _i;
            for (_i = 2; _i <= _n; _i++)
            {
                LuaNumberType _d = LuaLCheckNumber(_l, _i);
                if (_d < _dmin)
                    _dmin = _d;
            }
            LuaPushNumber(_l, _dmin);
            return 1;
        }

        private static int MathMax(LuaState _l)
        {
            int _n = LuaGetTop(_l);  
            LuaNumberType _dmax = LuaLCheckNumber(_l, 1);
            int _i;
            for (_i = 2; _i <= _n; _i++)
            {
                LuaNumberType _d = LuaLCheckNumber(_l, _i);
                if (_d > _dmax)
                    _dmax = _d;
            }
            LuaPushNumber(_l, _dmax);
            return 1;
        }

        private static int MathRandom(LuaState _l)
        {
            LuaNumberType _r = (LuaNumberType)_rng.NextDouble();
            switch (LuaGetTop(_l))
            {  
                case 0:
                    { 
                        LuaPushNumber(_l, _r);  
                        break;
                    }
                case 1:
                    {  
                        int _u = LuaLCheckInt(_l, 1);
                        LuaLArgCheck(_l, 1 <= _u, 1, "interval is empty");
                        LuaPushNumber(_l, Math.Floor(_r * _u) + 1);  
                        break;
                    }
                case 2:
                    { 
                        int __l = LuaLCheckInt(_l, 1);
                        int _u = LuaLCheckInt(_l, 2);
                        LuaLArgCheck(_l, __l <= _u, 2, "interval is empty");
                        LuaPushNumber(_l, Math.Floor(_r * (_u - __l + 1)) + __l); 
                        break;
                    }
                default: return LuaLError(_l, "wrong number of arguments");
            }
            return 1;
        }

        private static int MathRandomSeed(LuaState _l)
        {
            _rng = new Random(LuaLCheckInt(_l, 1));
            return 0;
        }

        private readonly static LuaLReg[] _mathlib = {
          new LuaLReg("abs",   MathAbs),
          new LuaLReg("acos",  MathACos),
          new LuaLReg("asin",  MathASin),
          new LuaLReg("atan2", MathATan2),
          new LuaLReg("atan",  MathATan),
          new LuaLReg("ceil",  MathCeil),
          new LuaLReg("cosh",   MathCosH),
          new LuaLReg("cos",   MathCos),
          new LuaLReg("deg",   MathDeg),
          new LuaLReg("exp",   MathExp),
          new LuaLReg("floor", MathFloor),
          new LuaLReg("fmod",   MathFMod),
          new LuaLReg("frexp", MathFRExp),
          new LuaLReg("ldexp", MathLDExp),
          new LuaLReg("log10", MathLog10),
          new LuaLReg("log",   MathLog),
          new LuaLReg("max",   MathMax),
          new LuaLReg("min",   MahMin),
          new LuaLReg("modf",   MathModF),
          new LuaLReg("pow",   MathPow),
          new LuaLReg("rad",   MathRad),
          new LuaLReg("random",     MathRandom),
          new LuaLReg("randomseed", MathRandomSeed),
          new LuaLReg("sinh",   MathSinH),
          new LuaLReg("sin",   MathSin),
          new LuaLReg("sqrt",  MathSqrt),
          new LuaLReg("tanh",   MathTanH),
          new LuaLReg("tan",   MathTan),
          new LuaLReg(null, null)
        };

        public static int LuaOpenMath(LuaState _l)
        {
            LuaLRegister(_l, LUA_MATHLIBNAME, _mathlib);
            LuaPushNumber(_l, PI);
            LuaSetField(_l, -2, "pi");
            LuaPushNumber(_l, HUGE_VAL);
            LuaSetField(_l, -2, "huge");
#if LUA_COMPAT_MOD
		    LuaGetField(L, -1, "fmod");
		    LuaSetField(L, -2, "mod");
#endif
            return 1;
        }
    }
}