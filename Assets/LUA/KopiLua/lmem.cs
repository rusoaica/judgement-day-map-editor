﻿using System;
using System.Diagnostics;

namespace KopiLua
{
    public partial class Lua
    {
        public const string MEMERRMSG = "not enough memory";
        public const int MINSIZEARRAY = 4;

        public static T[] LuaMReallocV<T>(LuaState _l, T[] _block, int _new_size)
        {
            return (T[])LuaMRealloc(_l, _block, _new_size);
        }

        public static void LuaMFreeMem<T>(LuaState _l, T _b) { LuaMRealloc<T>(_l, new T[] { _b }, 0); }
        public static void LuaMFree<T>(LuaState _l, T _b) { LuaMRealloc<T>(_l, new T[] { _b }, 0); }
        public static void LuaMFreeArray<T>(LuaState _l, T[] _b) { LuaMReallocV(_l, _b, 0); }

        private static void AddTotalBytes(LuaState _lL, int _num_bytes) { G(_lL).totalBytes += (uint)_num_bytes; }
        private static void SubtractTotalBytes(LuaState _l, int _num_bytes) { G(_l).totalBytes -= (uint)_num_bytes; }
        private static void AddTotalBytes(LuaState _l, uint _num_bytes) { G(_l).totalBytes += _num_bytes; }
        private static void SubtractTotalBytes(LuaState _l, uint _num_bytes) { G(_l).totalBytes -= _num_bytes; }

        public static T LuaMMalloc<T>(LuaState _l) { return (T)LuaMRealloc<T>(_l); }
        public static T LuaMNew<T>(LuaState _l) { return (T)LuaMRealloc<T>(_l); }
        public static T[] LuaMNewVector<T>(LuaState _l, int _n)
        {
            return LuaMReallocV<T>(_l, null, _n);
        }

        public static void LuaMGrowVector<T>(LuaState _l, ref T[] _v, int _n_elems, ref int _size, int _limit, CharPtr _e)
        {
            if (_n_elems + 1 > _size)
                _v = (T[])LuaMGrowAux(_l, ref _v, ref _size, _limit, _e);
        }

        public static T[] LuaMReallocVector<T>(LuaState _l, ref T[] _v, int _old_n, int _n)
        {
            Debug.Assert((_v == null && _old_n == 0) || (_v.Length == _old_n));
            _v = LuaMReallocV<T>(_l, _v, _n);
            return _v;
        }

        public static T[] LuaMGrowAux<T>(LuaState _l, ref T[] _block, ref int _size, int _limit, CharPtr _error_msg)
        {
            T[] _new_block;
            int _new_size;
            if (_size >= _limit / 2)
            {  
                if (_size >= _limit) 
                    LuaGRunError(_l, _error_msg);
                _new_size = _limit;  
            }
            else
            {
                _new_size = _size * 2;
                if (_new_size < MINSIZEARRAY)
                    _new_size = MINSIZEARRAY;  
            }
            _new_block = LuaMReallocV<T>(_l, _block, _new_size);
            _size = _new_size;  
            return _new_block;
        }

        public static object LuaMTooBig(LuaState _l)
        {
            LuaGRunError(_l, "memory allocation error: block too big");
            return null; 
        }

        public static object LuaMRealloc(LuaState _l, Type _t)
        {
            int _unmanaged_size = (int)GetUnmanagedSize(_t);
            int _n_size = _unmanaged_size;
            object _new_obj = System.Activator.CreateInstance(_t);
            AddTotalBytes(_l, _n_size);
            return _new_obj;
        }

        public static object LuaMRealloc<T>(LuaState _l)
        {
            int _unmanaged_size = (int)GetUnmanagedSize(typeof(T));
            int _n_size = _unmanaged_size;
            T _new_obj = (T)System.Activator.CreateInstance(typeof(T));
            AddTotalBytes(_l, _n_size);
            return _new_obj;
        }

        public static object LuaMRealloc<T>(LuaState _l, T _obj)
        {
            int _unmanaged_size = (int)GetUnmanagedSize(typeof(T));
            int _old_size = (_obj == null) ? 0 : _unmanaged_size;
            int _o_size = _old_size * _unmanaged_size;
            int _n_size = _unmanaged_size;
            T _new_obj = (T)System.Activator.CreateInstance(typeof(T));
            SubtractTotalBytes(_l, _o_size);
            AddTotalBytes(_l, _n_size);
            return _new_obj;
        }

        public static object LuaMRealloc<T>(LuaState _l, T[] _old_block, int _new_size)
        {
            int _unmanaged_size = (int)GetUnmanagedSize(typeof(T));
            int _old_size = (_old_block == null) ? 0 : _old_block.Length;
            int _o_size = _old_size * _unmanaged_size;
            int _n_size = _new_size * _unmanaged_size;
            T[] _new_block = new T[_new_size];
            for (int i = 0; i < Math.Min(_old_size, _new_size); i++)
                _new_block[i] = _old_block[i];
            for (int i = _old_size; i < _new_size; i++)
                _new_block[i] = (T)System.Activator.CreateInstance(typeof(T));
            if (CanIndex(typeof(T)))
                for (int i = 0; i < _new_size; i++)
                {
                    ArrayElement _elem = _new_block[i] as ArrayElement;
                    Debug.Assert(_elem != null, String.Format("Need to derive type {0} from ArrayElement", typeof(T).ToString()));
                    _elem.SetIndex(i);
                    _elem.SetArray(_new_block);
                }
            SubtractTotalBytes(_l, _o_size);
            AddTotalBytes(_l, _n_size);
            return _new_block;
        }

        public static bool CanIndex(Type _t)
        {
            if (_t == typeof(char))
                return false;
            if (_t == typeof(byte))
                return false;
            if (_t == typeof(int))
                return false;
            if (_t == typeof(uint))
                return false;
            if (_t == typeof(LocVar))
                return false;
            return true;
        }
    }
}