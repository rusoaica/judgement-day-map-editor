﻿using System;

namespace KopiLua
{
    public partial class Lua
    {
        private static object _tag = 0;

        public static void LuaPushStdCallCFunction(LuaState _lua_state, LuaNativeFunction _function)
        {
            LuaPushCFunction(_lua_state, _function);
        }

        public static bool LuaLCheckMetatable(LuaState _lua_state, int _index)
        {
            bool _ret_val = false;
            if (LuaGetMetatable(_lua_state, _index) != 0)
            {
                LuaPushLightUserData(_lua_state, _tag);
                LuaRawGet(_lua_state, -2);
                _ret_val = !LuaIsNil(_lua_state, -1);
                LuaSetTop(_lua_state, -3);
            }
            return _ret_val;
        }

        public static LuaTag LuaNetGetTag()
        {
            return new LuaTag(_tag);
        }

        public static void LuaPushLightUserData(LuaState _l, LuaTag _p)
        {
            LuaPushLightUserData(_l, _p.Tag);
        }

        private static object CheckUserDataRaw(LuaState _l, int _ud, string _t_name)
        {
            object _p = LuaToUserData(_l, _ud);
            if (_p != null)
            {
                if (LuaGetMetatable(_l, _ud) != 0)
                {
                    bool _is_equal;
                    LuaGetField(_l, LUA_REGISTRYINDEX, _t_name); 
                    _is_equal = LuaRawEqual(_l, -1, -2) != 0;
                    LuaSetTop(_l, -(2) - 1);
                    if (_is_equal)    
                        return _p;
                }
            }
            return null;
        }

        public static int LuaNetCheckUData(LuaState _lua_state, int _ud, string _t_name)
        {
            object _u_data = CheckUserDataRaw(_lua_state, _ud, _t_name);
            return _u_data != null ? FourBytesToInt(_u_data as byte[]) : -1;
        }

        public static int LuaNetToNetObject(LuaState _lua_state, int _index)
        {
            byte[] _u_data;
            if (LuaType(_lua_state, _index) == LUA_TUSERDATA)
            {
                if (LuaLCheckMetatable(_lua_state, _index))
                {
                    _u_data = LuaToUserData(_lua_state, _index) as byte[];
                    if (_u_data != null)
                        return FourBytesToInt(_u_data);
                }
                _u_data = CheckUserDataRaw(_lua_state, _index, "luaNet_class") as byte[];
                if (_u_data != null)
                    return FourBytesToInt(_u_data);
                _u_data = CheckUserDataRaw(_lua_state, _index, "luaNet_searchbase") as byte[];
                if (_u_data != null)
                    return FourBytesToInt(_u_data);
                _u_data = CheckUserDataRaw(_lua_state, _index, "luaNet_function") as byte[];
                if (_u_data != null)
                    return FourBytesToInt(_u_data);
            }
            return -1;
        }

        public static void LuaNetNewUData(LuaState _lua_state, int _val)
        {
            var _user_data = LuaNewUserData(_lua_state, sizeof(int)) as byte[];
            IntToFourBytes(_val, _user_data);
        }

        public static int LuaNetRawNetObj(LuaState _lua_state, int _obj)
        {
            byte[] _bytes = LuaToUserData(_lua_state, _obj) as byte[];
            if (_bytes == null)
                return -1;
            return FourBytesToInt(_bytes);
        }

        private static int FourBytesToInt(byte[] _bytes)
        {
            return _bytes[0] + (_bytes[1] << 8) + (_bytes[2] << 16) + (_bytes[3] << 24);
        }

        private static void IntToFourBytes(int _val, byte[] _bytes)
        {
            _bytes[0] = (byte)_val;
            _bytes[1] = (byte)(_val >> 8);
            _bytes[2] = (byte)(_val >> 16);
            _bytes[3] = (byte)(_val >> 24);
        }

        public static int LuaNetRegistryIndex()
        {
            return LUA_REGISTRYINDEX;
        }

        public static void LuaNetPushGlobalTable(LuaState _l)
        {
            LuaPushValue(_l, LUA_GLOBALSINDEX);
        }

        public static void LuaNetPopGlobalTable(LuaState _l)
        {
            LuaReplace(_l, LUA_GLOBALSINDEX);
        }

        public static void LuaNetSetGlobal(LuaState _l, string _name)
        {
            LuaSetGlobal(_l, _name);
        }

        public static void LuaNetGetGlobal(LuaState _l, string _name)
        {
            LuaGetGlobal(_l, _name);
        }

        public static int LuaNetPCall(LuaState _l, int _n_args, int _n_results, int _err_func)
        {
            return LuaPCall(_l, _n_args, _n_results, _err_func);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaNetLoadBuffer(LuaState _l, string _buff, uint _sz, string _name)
        {
            if (_sz == 0)
                _sz = (uint)StrLen(_buff);
            return LuaLLoadBuffer(_l, _buff, _sz, _name);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaNetLoadBuffer(LuaState _l, byte[] _buff, uint _sz, string _name)
        {
            return LuaLLoadBuffer(_l, _buff, _sz, _name);
        }

        public static int LuaNetLoadFile(LuaState _l, string _file)
        {
            return LuaLLoadFile(_l, _file);
        }

        public static double LuaNetToNumber(LuaState _l, int _idx)
        {
            return LuaToNumber(_l, _idx);
        }

        public static int LuaNetEqual(LuaState _l, int _idx_1, int _idx_2)
        {
            return LuaEqual(_l, _idx_1, _idx_2);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static void LuaNetPushLString(LuaState _l, string _s, uint _len)
        {
            LuaPushLString(_l, _s, _len);
        }

        public static int LuaNetIsStringStrict(LuaState _l, int _idx)
        {
            int _t = LuaType(_l, _idx);
            return (_t == LUA_TSTRING) ? 1 : 0;
        }

        public static LuaState LuaNetGetMainState(LuaState _l_1)
        {
            LuaGetField(_l_1, LUA_REGISTRYINDEX, "main_state");
            LuaState main = LuaToThread(_l_1, -1);
            LuaPop(_l_1, 1);
            return main;
        }
    }
}