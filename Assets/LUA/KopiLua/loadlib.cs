﻿using System;
using System.IO;

namespace KopiLua
{
    public partial class Lua
    {
        public static object sentinel = new object();
        public const string LUAPOF = "luaopen_";
        public const string LUAOFSEP = "_";
        public const string LIBPREFIX = "LOADLIB: ";
        public const string POF = LUAPOF;
        public const string LIBFAIL = "open";
        public const int ERRLIB = 1;
        public const int ERRFUNC = 2;
        public readonly static string AUXMARK = String.Format("{0}", (char)1);

        public static void SetProgDir(LuaState _l)
        {
#if WINDOWS_PHONE
			CharPtr _buff = "/";
#elif SILVERLIGHT
			CharPtr _buff;
			try
			{
                _buff = Directory.GetCurrentDirectory(); 
			}
			catch (MethodAccessException)
			{
                _buff = "/";
			}
#else
            CharPtr _buff = Directory.GetCurrentDirectory();
#endif
            LuaLGSub(_l, LuaToString(_l, -1), LUA_EXECDIR, _buff);
            LuaRemove(_l, -2);
        }

#if LUA_DL_DLOPEN		
        static void ll_unloadlib(void *_lib)
        {
            dlclose(_lib);
        }

        static void* ll_load(LuaState _l, readonly CharPtr _path) 
        {

          void *_lib = dlopen(_path, RTLD_NOW);
		  if (_lib == null) lua_pushstring(_l, dlerror());
		  return _lib;
		}


        static lua_CFunction ll_sym(LuaState _l, void *_lib, readonly CharPtr _sym)
        {
            lua_CFunction _f = (lua_CFunction)dlsym(_lib, _sym);
            if (_f == null) lua_pushstring(_l, dlerror());
            return _f;
        }

        static void setprogdir(LuaState _l)
        {
            char _buff[MAX_PATH + 1];
            char *_lb;
            DWORD _n_size = sizeof(buff) / GetUnmanagedSize(typeof(char));
            DWORD _n = GetModuleFileNameA(null, _buff, _n_size);
            if (_n == 0 || _n == _n_size || (_lb = strrchr(_buff, '\\')) == null)
                luaL_error(_l, "unable to get ModuleFileName");
            else
            {
                *_lb = '\0';
                luaL_gsub(_l, lua_tostring(_l, -1), LUA_EXECDIR, _buff);
                lua_remove(_l, -2);  
            }
        }

        static void pusherror(LuaState _l)
        {
            int _error = GetLastError();
            char _buffer[128];
            if (FormatMessageA(FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM, null, _error, 0, _buffer, sizeof(buffer), null))
                lua_pushstring(_l, _buffer);
            else
                lua_pushfstring(_l, "system error %d\n", _error);
        }

        static void ll_unloadlib(void *_lib)
        {
            FreeLibrary((HINSTANCE)_lib);
        }

        static void* ll_load(LuaState _l, readonly CharPtr _path)
        {
            HINSTANCE _lib = LoadLibraryA(_path);
            if (_lib == null) pusherror(_l);
            return _lib;
        }

        static lua_CFunction ll_sym(LuaState _l, void *_lib, readonly CharPtr _sym)
        {
            lua_CFunction _f = (lua_CFunction)GetProcAddress((HINSTANCE)_lib, _sym);
            if (_f == null) pusherror(_l);
            return _f;
        }
#elif LUA_DL_DYLD    
        static void pusherror(LuaState _l)
        {
            CharPtr _err_str;
            CharPtr _err_file;
            NSLinkEditErrors _err;
            int _err_num;
            NSLinkEditError(_err, _err_num, _err_file, _err_str);
            lua_pushstring(_l, _err_str);
        }

        static CharPtr errorfromcode(NSObjectFileImageReturnCode _ret)
        {
            switch (_ret)
            {
                case NSObjectFileImageInappropriateFile:
                    return "file is not a bundle";
                case NSObjectFileImageArch:
                    return "library is for wrong CPU type";
                case NSObjectFileImageFormat:
                    return "bad format";
                case NSObjectFileImageAccess:
                    return "cannot access file";
                case NSObjectFileImageFailure:
                default:
                    return "unable to load library";
            }
        }

        static void ll_unloadlib(void *_lib)
        {
            NSUnLinkModule((NSModule)_lib, NSUNLINKMODULE_OPTION_RESET_LAZY_REFERENCES);
        }

        static void* ll_load(LuaState _l, readonly CharPtr _path)
        {
            NSObjectFileImage _img;
            NSObjectFileImageReturnCode _ret;
            if (!_dyld_present())
            {
                lua_pushliteral(_l, "dyld not present");
                return null;
            }
            _ret = NSCreateObjectFileImageFromFile(_path, _img);
            if (_ret == NSObjectFileImageSuccess)
            {
                NSModule _mod = NSLinkModule(_img, _path, NSLINKMODULE_OPTION_PRIVATE | NSLINKMODULE_OPTION_RETURN_ON_ERROR);
                NSDestroyObjectFileImage(_img);
                if (_mod == null) pusherror(_l);
                return _mod;
            }
            lua_pushstring(_l, errorfromcode(_ret));
            return null;
        }

        static lua_CFunction ll_sym(LuaState _l, void *_lib, readonly CharPtr _sym)
        {
            NSSymbol _nss = NSLookupSymbolInModule((NSModule)_lib, _sym);
            if (_nss == null)
            {
                lua_pushfstring(_l, "symbol " + LUA_QS + " not found", _sym);
                return null;
            }
            return (lua_CFunction)NSAddressOfSymbol(_nss);
        }
#else    
        public const string DLMSG = "dynamic libraries not enabled; check your Lua installation";

        public static void LLUnloadLib(object _lib)
        {
            
        }

        public static object LLLoad(LuaState _l, CharPtr _path)
        {
            LuaPushLiteral(_l, DLMSG);
            return null;
        }

        public static LuaNativeFunction LLSym(LuaState _l, object _lib, CharPtr _sym)
        {
            LuaPushLiteral(_l, DLMSG);
            return null;
        }
#endif

        private static object LLRegister(LuaState _l, CharPtr _path)
        {
            object _p_lib = null;
            LuaPushFString(_l, "%s%s", LIBPREFIX, _path);
            LuaGetTable(_l, LUA_REGISTRYINDEX);  
            if (!LuaIsNil(_l, -1)) 
                _p_lib = LuaToUserData(_l, -1);
            else
            {  
                LuaPop(_l, 1);
                LuaLGetMetatable(_l, "_LOADLIB");
                LuaSetMetatable(_l, -2);
                LuaPushFString(_l, "%s%s", LIBPREFIX, _path);
                LuaPushValue(_l, -2);
                LuaSetTable(_l, LUA_REGISTRYINDEX);
            }
            return _p_lib;
        }

        private static int Gctm(LuaState _l)
        {
            object _lib = LuaLCheckUData(_l, 1, "_LOADLIB");
            if (_lib != null) LLUnloadLib(_lib);
            _lib = null;  
            return 0;
        }

        private static int LLLoadFunc(LuaState _l, CharPtr _path, CharPtr _sym)
        {
            object _reg = LLRegister(_l, _path);
            if (_reg == null) _reg = LLLoad(_l, _path);
            if (_reg == null)
                return ERRLIB; 
            else
            {
                LuaNativeFunction _f = LLSym(_l, _reg, _sym);
                if (_f == null)
                    return ERRFUNC; 
                LuaPushCFunction(_l, _f);
                return 0;  
            }
        }

        private static int LLLoadLib(LuaState _l)
        {
            CharPtr _path = LuaLCheckString(_l, 1);
            CharPtr _init = LuaLCheckString(_l, 2);
            int _stat = LLLoadFunc(_l, _path, _init);
            if (_stat == 0) 
                return 1;  
            else
            {  
                LuaPushNil(_l);
                LuaInsert(_l, -2);
                LuaPushString(_l, (_stat == ERRLIB) ? LIBFAIL : "init");
                return 3;  
            }
        }

        private static int Readable(CharPtr _filename)
        {
            Stream _f = FOpen(_filename, "r"); 
            if (_f == null) return 0;  
            FClose(_f);
            return 1;
        }

        private static CharPtr PushNextTemplate(LuaState _l, CharPtr _path)
        {
            CharPtr __l;
            while (_path[0] == LUA_PATHSEP[0]) _path = _path.next();  
            if (_path[0] == '\0') return null;  
            __l = StrChr(_path, LUA_PATHSEP[0]); 
            if (__l == null) __l = _path + StrLen(_path);
            LuaPushLString(_l, _path, (uint)(__l - _path));
            return __l;
        }

        private static CharPtr FindFile(LuaState _l, CharPtr _name, CharPtr _p_name)
        {
            CharPtr _path;
            _name = LuaLGSub(_l, _name, ".", LUA_DIRSEP);
            LuaGetField(_l, LUA_ENVIRONINDEX, _p_name);
            _path = LuaToString(_l, -1);
            if (_path == null)
                LuaLError(_l, LUA_QL("package.%s") + " must be a string", _p_name);
            LuaPushLiteral(_l, "");  
            while ((_path = PushNextTemplate(_l, _path)) != null)
            {
                CharPtr _filename;
                _filename = LuaLGSub(_l, LuaToString(_l, -1), LUA_PATH_MARK, _name);
                LuaRemove(_l, -2);  
                if (Readable(_filename) != 0)  
                    return _filename;  
                LuaPushFString(_l, "\n\tno file " + LUA_QS, _filename);
                LuaRemove(_l, -2);  
                LuaConcat(_l, 2); 
            }
            return null; 
        }

        private static void LoadError(LuaState _l, CharPtr _filename)
        {
            LuaLError(_l, "error loading module " + LUA_QS + " from file " + LUA_QS + ":\n\t%s", LuaToString(_l, 1), _filename, LuaToString(_l, -1));
        }

        private static int LoaderLua(LuaState _l)
        {
            CharPtr _filename;
            CharPtr _name = LuaLCheckString(_l, 1);
            _filename = FindFile(_l, _name, "path");
            if (_filename == null) return 1;  
            if (LuaLLoadFile(_l, _filename) != 0)
                LoadError(_l, _filename);
            return 1;  
        }

        private static CharPtr MakeFuncName(LuaState _l, CharPtr _mod_name)
        {
            CharPtr _func_name;
            CharPtr _mark = StrChr(_mod_name, LUA_IGMARK[0]);
            if (_mark != null) _mod_name = _mark + 1;
            _func_name = LuaLGSub(_l, _mod_name, ".", LUAOFSEP);
            _func_name = LuaPushFString(_l, POF + "%s", _func_name);
            LuaRemove(_l, -2);  
            return _func_name;
        }

        private static int LoaderC(LuaState _l)
        {
            CharPtr _func_name;
            CharPtr _name = LuaLCheckString(_l, 1);
            CharPtr _filename = FindFile(_l, _name, "cpath");
            if (_filename == null) return 1; 
            _func_name = MakeFuncName(_l, _name);
            if (LLLoadFunc(_l, _filename, _func_name) != 0)
                LoadError(_l, _filename);
            return 1; 
        }

        private static int LoaderCRoot(LuaState _l)
        {
            CharPtr _func_name;
            CharPtr _file_name;
            CharPtr _name = LuaLCheckString(_l, 1);
            CharPtr _p = StrChr(_name, '.');
            int _stat;
            if (_p == null) return 0; 
            LuaPushLString(_l, _name, (uint)(_p - _name));
            _file_name = FindFile(_l, LuaToString(_l, -1), "cpath");
            if (_file_name == null) return 1;  
            _func_name = MakeFuncName(_l, _name);
            if ((_stat = LLLoadFunc(_l, _file_name, _func_name)) != 0)
            {
                if (_stat != ERRFUNC) LoadError(_l, _file_name); 
                LuaPushFString(_l, "\n\tno module " + LUA_QS + " in file " + LUA_QS, _name, _file_name);
                return 1;  
            }
            return 1;
        }

        private static int LoaderPreLoad(LuaState _l)
        {
            CharPtr _name = LuaLCheckString(_l, 1);
            LuaGetField(_l, LUA_ENVIRONINDEX, "preload");
            if (!LuaIsTable(_l, -1))
                LuaLError(_l, LUA_QL("package.preload") + " must be a table");
            LuaGetField(_l, -1, _name);
            if (LuaIsNil(_l, -1))  
                LuaPushFString(_l, "\n\tno field package.preload['%s']", _name);
            return 1;
        }

        public static int LLRequire(LuaState _l)
        {
            CharPtr _name = LuaLCheckString(_l, 1);
            int _i;
            LuaSetTop(_l, 1);  
            LuaGetField(_l, LUA_REGISTRYINDEX, "_LOADED");
            LuaGetField(_l, 2, _name);
            if (LuaToBoolean(_l, -1) != 0)
            {  
                if (LuaToUserData(_l, -1) == sentinel)  
                    LuaLError(_l, "loop or previous error loading module " + LUA_QS, _name);
                return 1; 
            }
            LuaGetField(_l, LUA_ENVIRONINDEX, "loaders");
            if (!LuaIsTable(_l, -1))
                LuaLError(_l, LUA_QL("package.loaders") + " must be a table");
            LuaPushLiteral(_l, ""); 
            for (_i = 1; ; _i++)
            {
                LuaRawGetI(_l, -2, _i); 
                if (LuaIsNil(_l, -1))
                    LuaLError(_l, "module " + LUA_QS + " not found:%s", _name, LuaToString(_l, -2));
                LuaPushString(_l, _name);
                LuaCall(_l, 1, 1); 
                if (LuaIsFunction(_l, -1))  
                    break; 
                else if (LuaIsString(_l, -1) != 0) 
                    LuaConcat(_l, 2);  
                else
                    LuaPop(_l, 1);
            }
            LuaPushLightUserData(_l, sentinel);
            LuaSetField(_l, 2, _name);  
            LuaPushString(_l, _name); 
            LuaCall(_l, 1, 1);  
            if (!LuaIsNil(_l, -1))  
                LuaSetField(_l, 2, _name); 
            LuaGetField(_l, 2, _name);
            if (LuaToUserData(_l, -1) == sentinel)
            {   
                LuaPushBoolean(_l, 1);  
                LuaPushValue(_l, -1);  
                LuaSetField(_l, 2, _name);  
            }
            return 1;
        }

        private static void SetFEnv(LuaState _l)
        {
            LuaDebug _ar = new LuaDebug();
            if (LuaGetStack(_l, 1, ref _ar) == 0 || LuaGetInfo(_l, "f", ref _ar) == 0 || LuaIsCFunction(_l, -1))
                LuaLError(_l, LUA_QL("module") + " not called from a Lua function");
            LuaPushValue(_l, -2);
            LuaSetFEnv(_l, -2);
            LuaPop(_l, 1);
        }

        private static void DoOptions(LuaState _l, int _n)
        {
            int _i;
            for (_i = 2; _i <= _n; _i++)
            {
                LuaPushValue(_l, _i);  
                LuaPushValue(_l, -2);  
                LuaCall(_l, 1, 0);
            }
        }

        private static void ModInit(LuaState _l, CharPtr _mod_name)
        {
            CharPtr _dot;
            LuaPushValue(_l, -1);
            LuaSetField(_l, -2, "_M");  
            LuaPushString(_l, _mod_name);
            LuaSetField(_l, -2, "_NAME");
            _dot = StrRChr(_mod_name, '.'); 
            if (_dot == null) _dot = _mod_name;
            else _dot = _dot.next();
            LuaPushLString(_l, _mod_name, (uint)(_dot - _mod_name));
            LuaSetField(_l, -2, "_PACKAGE");
        }

        private static int LLModule(LuaState _l)
        {
            CharPtr _mod_name = LuaLCheckString(_l, 1);
            int _loaded = LuaGetTop(_l) + 1;  
            LuaGetField(_l, LUA_REGISTRYINDEX, "_LOADED");
            LuaGetField(_l, _loaded, _mod_name); 
            if (!LuaIsTable(_l, -1))
            {  
                LuaPop(_l, 1); 
                if (LuaLFindTable(_l, LUA_GLOBALSINDEX, _mod_name, 1) != null)
                    return LuaLError(_l, "name conflict for module " + LUA_QS, _mod_name);
                LuaPushValue(_l, -1);
                LuaSetField(_l, _loaded, _mod_name);  
            }
            LuaGetField(_l, -1, "_NAME");
            if (!LuaIsNil(_l, -1))  
                LuaPop(_l, 1);
            else
            {  
                LuaPop(_l, 1);
                ModInit(_l, _mod_name);
            }
            LuaPushValue(_l, -1);
            SetFEnv(_l);
            DoOptions(_l, _loaded - 1);
            return 0;
        }

        private static int LLSeeAll(LuaState _l)
        {
            LuaLCheckType(_l, 1, LUA_TTABLE);
            if (LuaGetMetatable(_l, 1) == 0)
            {
                LuaCreateTable(_l, 0, 1); 
                LuaPushValue(_l, -1);
                LuaSetMetatable(_l, 1);
            }
            LuaPushValue(_l, LUA_GLOBALSINDEX);
            LuaSetField(_l, -2, "__index"); 
            return 0;
        }

        private static void SetPath(LuaState _l, CharPtr _field_name, CharPtr _env_name, CharPtr _def)
        {
            CharPtr _path = GetEnv(_env_name);
            if (_path == null) 
                LuaPushString(_l, _def);  
            else
            {
                _path = LuaLGSub(_l, _path, LUA_PATHSEP + LUA_PATHSEP, LUA_PATHSEP + AUXMARK + LUA_PATHSEP);
                LuaLGSub(_l, _path, AUXMARK, _def);
                LuaRemove(_l, -2);
            }
            SetProgDir(_l);
            LuaSetField(_l, -2, _field_name);
        }

        private readonly static LuaLReg[] PKFuncs = {
              new LuaLReg("loadlib", LLLoadLib),
              new LuaLReg("seeall", LLSeeAll),
              new LuaLReg(null, null)
            };

        private readonly static LuaLReg[] LLFuncs = {
              new LuaLReg("module", LLModule),
              new LuaLReg("require", LLRequire),
              new LuaLReg(null, null)
            };

        public readonly static LuaNativeFunction[] loaders = {LoaderPreLoad, LoaderLua, LoaderC, LoaderCRoot, null};


        public static int LuaOpenPackage(LuaState _l)
        {
            int _i;
            LuaLNewMetatable(_l, "_LOADLIB");
            LuaPushCFunction(_l, Gctm);
            LuaSetField(_l, -2, "__gc");
            LuaLRegister(_l, LUA_LOADLIBNAME, PKFuncs);
#if LUA_COMPAT_LOADLIB
		      lua_getfield(_l, -1, "loadlib");
		      lua_setfield(_l, LUA_GLOBALSINDEX, "loadlib");
#endif
            LuaPushValue(_l, -1);
            LuaReplace(_l, LUA_ENVIRONINDEX);
            LuaCreateTable(_l, loaders.Length - 1, 0);
            for (_i = 0; loaders[_i] != null; _i++)
            {
                LuaPushCFunction(_l, loaders[_i]);
                LuaRawSetI(_l, -2, _i + 1);
            }
            LuaSetField(_l, -2, "loaders");  
            SetPath(_l, "path", LUA_PATH, LUA_PATH_DEFAULT);  
            SetPath(_l, "cpath", LUA_CPATH, LUA_CPATH_DEFAULT); 
            LuaPushLiteral(_l, LUA_DIRSEP + "\n" + LUA_PATHSEP + "\n" + LUA_PATH_MARK + "\n" + LUA_EXECDIR + "\n" + LUA_IGMARK);
            LuaSetField(_l, -2, "config");
            LuaLFindTable(_l, LUA_REGISTRYINDEX, "_LOADED", 2);
            LuaSetField(_l, -2, "loaded");
            LuaNewTable(_l);
            LuaSetField(_l, -2, "preload");
            LuaPushValue(_l, LUA_GLOBALSINDEX);
            LuaLRegister(_l, null, LLFuncs); 
            LuaPop(_l, 1);
            return 1;  
        }
    }
}
