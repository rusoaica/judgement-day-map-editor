﻿using System;
using System.Diagnostics;

namespace KopiLua
{
    using Instruction = System.UInt32;
    using l_uacNumber = System.Double;
    using LuaByteType = System.Byte;
    using LuaNumberType = System.Double;
    using StkId = Lua.LuaTypeValue;

    public partial class Lua
    {
        public const int LASTTAG = LUA_TTHREAD;
        public const int NUMTAGS = (LASTTAG + 1);
        public const int LUATPROTO = (LASTTAG + 1);
        public const int LUATUPVAL = (LASTTAG + 2);
        public const int LUATDEADKEY = (LASTTAG + 3);


        public interface ArrayElement
        {
            void SetIndex(int index);
            void SetArray(object array);
        }

        public class CommonHeader
        {
            public GCObject next;
            public LuaByteType tt;
            public LuaByteType marked;
        }

        public class GCheader : CommonHeader { };

        public class Value
        {
            public object p;

            public void Copy(Value _copy)
            {
                this.p = _copy.p;
            }

            public GCObject _gc
            {
                get { return (GCObject)this.p; }
                set { this.p = value; }
            }

            public LuaNumberType n
            {
                get { return (LuaNumberType)this.p; }
                set { this.p = (object)value; }
            }

            public int b
            {
                get { return (int)this.p; }
                set { this.p = (object)value; }
            }
        };

        public class LuaTypeValue : ArrayElement
        {
            public Value value = new Value();
            public int tt;
            private LuaTypeValue[] _values = null;
            private int _index = -1;

            public void SetIndex(int __index)
            {
                this._index = __index;
            }

            public void SetArray(object _array)
            {
                this._values = (LuaTypeValue[])_array;
                Debug.Assert(this._values != null);
            }

            public LuaTypeValue this[int _offset]
            {
                get { return this._values[this._index + _offset]; }
            }

#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public LuaTypeValue this[uint _offset]
            {
                get { return this._values[this._index + (int)_offset]; }
            }

            public static LuaTypeValue operator +(LuaTypeValue _value, int _offset)
            {
                return _value._values[_value._index + _offset];
            }

            public static LuaTypeValue operator +(int _offset, LuaTypeValue _value)
            {
                return _value._values[_value._index + _offset];
            }

            public static LuaTypeValue operator -(LuaTypeValue _value, int _offset)
            {
                return _value._values[_value._index - _offset];
            }

            public static int operator -(LuaTypeValue _value, LuaTypeValue[] _array)
            {
                Debug.Assert(_value._values == _array);
                return _value._index;
            }

            public static int operator -(LuaTypeValue _a, LuaTypeValue _b)
            {
                Debug.Assert(_a._values == _b._values);
                return _a._index - _b._index;
            }

            public static bool operator <(LuaTypeValue _a, LuaTypeValue _b)
            {
                Debug.Assert(_a._values == _b._values);
                return _a._index < _b._index;
            }

            public static bool operator <=(LuaTypeValue _a, LuaTypeValue _b)
            {
                Debug.Assert(_a._values == _b._values);
                return _a._index <= _b._index;
            }

            public static bool operator >(LuaTypeValue _a, LuaTypeValue _b)
            {
                Debug.Assert(_a._values == _b._values);
                return _a._index > _b._index;
            }

            public static bool operator >=(LuaTypeValue _a, LuaTypeValue _b)
            {
                Debug.Assert(_a._values == _b._values);
                return _a._index >= _b._index;
            }

            public static LuaTypeValue Inc(ref LuaTypeValue _value)
            {
                _value = _value[1];
                return _value[-1];
            }

            public static LuaTypeValue Dec(ref LuaTypeValue _value)
            {
                _value = _value[-1];
                return _value[1];
            }

            public static implicit operator int(LuaTypeValue _value)
            {
                return _value._index;
            }

            public LuaTypeValue()
            {

            }

            public LuaTypeValue(LuaTypeValue _copy)
            {
                this._values = _copy._values;
                this._index = _copy._index;
                this.value.Copy(_copy.value);
                this.tt = _copy.tt;
            }

            public LuaTypeValue(Value _value, int _tt)
            {
                this._values = null;
                this._index = 0;
                this.value.Copy(_value);
                this.tt = _tt;
            }

            public override string ToString()
            {
                string _typename = null;
                string _val = null;
                switch (tt)
                {
                    case LUA_TNIL: _typename = "LUA_TNIL"; _val = string.Empty; break;
                    case LUA_TNUMBER: _typename = "LUA_TNUMBER"; _val = value.n.ToString(); break;
                    case LUA_TSTRING: _typename = "LUA_TSTRING"; _val = value._gc.ts.ToString(); break;
                    case LUA_TTABLE: _typename = "LUA_TTABLE"; break;
                    case LUA_TFUNCTION: _typename = "LUA_TFUNCTION"; break;
                    case LUA_TBOOLEAN: _typename = "LUA_TBOOLEAN"; break;
                    case LUA_TUSERDATA: _typename = "LUA_TUSERDATA"; break;
                    case LUA_TTHREAD: _typename = "LUA_TTHREAD"; break;
                    case LUA_TLIGHTUSERDATA: _typename = "LUA_TLIGHTUSERDATA"; break;
                    default: _typename = "unknown"; break;
                }
                return string.Format("Lua.LuaTypeValue<{0}>({1})", _typename, _val);
            }
        };

        internal static bool TTIsNil(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TNIL); }
        internal static bool TTIsNumber(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TNUMBER); }
        internal static bool TTIsString(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TSTRING); }
        internal static bool TTIsTable(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TTABLE); }
        internal static bool TTIsFunction(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TFUNCTION); }
        internal static bool TTIsBoolean(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TBOOLEAN); }
        internal static bool TTIsUserData(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TUSERDATA); }
        internal static bool TTIsThread(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TTHREAD); }
        internal static bool TTIsLightUserData(Lua.LuaTypeValue _o) { return (TType(_o) == LUA_TLIGHTUSERDATA); }

#if DEBUG
        internal static int TType(Lua.LuaTypeValue _o) { return _o.tt; }
        internal static int TType(CommonHeader _o) { return _o.tt; }
        internal static GCObject GCValue(Lua.LuaTypeValue _o) { return (GCObject)CheckExp(IsCollectable(_o), _o.value._gc); }
        internal static object PValue(Lua.LuaTypeValue _o) { return (object)CheckExp(TTIsLightUserData(_o), _o.value.p); }
        internal static LuaNumberType NValue(Lua.LuaTypeValue _o) { return (LuaNumberType)CheckExp(TTIsNumber(_o), _o.value.n); }
        internal static TString RawTSValue(Lua.LuaTypeValue _o) { return (TString)CheckExp(TTIsString(_o), _o.value._gc.ts); }
        internal static TStringTSV TSValue(Lua.LuaTypeValue _o) { return RawTSValue(_o).tsv; }
        internal static Udata RawUValue(Lua.LuaTypeValue _o) { return (Udata)CheckExp(TTIsUserData(_o), _o.value._gc.u); }
        internal static UdataUV UValue(Lua.LuaTypeValue _o) { return RawUValue(_o).uv; }
        internal static Closure CLValue(Lua.LuaTypeValue _o) { return (Closure)CheckExp(TTIsFunction(_o), _o.value._gc.cl); }
        internal static Table HValue(Lua.LuaTypeValue _o) { return (Table)CheckExp(TTIsTable(_o), _o.value._gc.h); }
        internal static int BValue(Lua.LuaTypeValue _o) { return (int)CheckExp(TTIsBoolean(_o), _o.value.b); }
        internal static LuaState THValue(Lua.LuaTypeValue _o) { return (LuaState)CheckExp(TTIsThread(_o), _o.value._gc.th); }
#else
		internal static int TType(Lua.LuaTypeValue _o) { return _o.tt; }
		internal static int TType(CommonHeader _o) { return _o.tt; }
		internal static GCObject GCValue(Lua.LuaTypeValue _o) { return _o.value.gc; }
		internal static object PValue(Lua.LuaTypeValue _o) { return _o.value.p; }
		internal static LuaNumberType NValue(Lua.LuaTypeValue _o) { return _o.value.n; }
		internal static TString RawTSValue(Lua.LuaTypeValue _o) { return _o.value.gc.ts; }
		internal static TStringTSV TSValue(Lua.LuaTypeValue _o) { return RawTSValue(_o).tsv; }
		internal static Udata RawUValue(Lua.LuaTypeValue _o) { return _o.value.gc.u; }
		internal static UdataUV UValue(Lua.LuaTypeValue _o) { return RawUValue(_o).uv; }
		internal static Closure CLValue(Lua.LuaTypeValue _o) { return _o.value.gc.cl; }
		internal static Table HValue(Lua.LuaTypeValue _o) { return _o.value.gc.h; }
		internal static int BValue(Lua.LuaTypeValue _o) { return _o.value.b; }
		internal static LuaState THValue(Lua.LuaTypeValue _o) { return (LuaState)CheckExp(TTIsThread(_o), _o.value.gc.th); }
#endif
        public static int LIsFalse(Lua.LuaTypeValue _o) { return ((TTIsNil(_o) || (TTIsBoolean(_o) && BValue(_o) == 0))) ? 1 : 0; }

        [Conditional("DEBUG")]
        internal static void CheckConsistency(Lua.LuaTypeValue _obj)
        {
            LuaAssert(!IsCollectable(_obj) || (TType(_obj) == (_obj).value._gc.gch.tt));
        }

        [Conditional("DEBUG")]
        internal static void CheckLiveness(GlobalState _g, Lua.LuaTypeValue _obj)
        {
            LuaAssert(!IsCollectable(_obj) ||
            ((TType(_obj) == _obj.value._gc.gch.tt) && !IsDead(_g, _obj.value._gc)));
        }

        internal static void SetNilValue(Lua.LuaTypeValue _obj)
        {
            _obj.tt = LUA_TNIL;
        }

        internal static void SetNValue(Lua.LuaTypeValue _obj, LuaNumberType _x)
        {
            _obj.value.n = _x;
            _obj.tt = LUA_TNUMBER;
        }

        internal static void SetPValue(Lua.LuaTypeValue _obj, object _x)
        {
            _obj.value.p = _x;
            _obj.tt = LUA_TLIGHTUSERDATA;
        }

        internal static void SetBValue(Lua.LuaTypeValue _obj, int _x)
        {
            _obj.value.b = _x;
            _obj.tt = LUA_TBOOLEAN;
        }

        internal static void SetSValue(LuaState _l, Lua.LuaTypeValue _obj, GCObject _x)
        {
            _obj.value._gc = _x;
            _obj.tt = LUA_TSTRING;
            CheckLiveness(G(_l), _obj);
        }

        internal static void SetUValue(LuaState _l, Lua.LuaTypeValue _obj, GCObject _x)
        {
            _obj.value._gc = _x;
            _obj.tt = LUA_TUSERDATA;
            CheckLiveness(G(_l), _obj);
        }

        internal static void SetTTHValue(LuaState _l, Lua.LuaTypeValue _obj, GCObject _x)
        {
            _obj.value._gc = _x;
            _obj.tt = LUA_TTHREAD;
            CheckLiveness(G(_l), _obj);
        }

        internal static void SetCLValue(LuaState _l, Lua.LuaTypeValue _obj, Closure _x)
        {
            _obj.value._gc = _x;
            _obj.tt = LUA_TFUNCTION;
            CheckLiveness(G(_l), _obj);
        }

        internal static void SetHValue(LuaState _l, Lua.LuaTypeValue _obj, Table _x)
        {
            _obj.value._gc = _x;
            _obj.tt = LUA_TTABLE;
            CheckLiveness(G(_l), _obj);
        }

        internal static void SetPTValue(LuaState _l, Lua.LuaTypeValue _obj, Proto _x)
        {
            _obj.value._gc = _x;
            _obj.tt = LUATPROTO;
            CheckLiveness(G(_l), _obj);
        }

        internal static void SetObj(LuaState _l, Lua.LuaTypeValue _obj_1, Lua.LuaTypeValue _obj_2)
        {
            _obj_1.value.Copy(_obj_2.value);
            _obj_1.tt = _obj_2.tt;
            CheckLiveness(G(_l), _obj_1);
        }

        internal static void SetObjs2S(LuaState _l, Lua.LuaTypeValue _obj, Lua.LuaTypeValue _x) { SetObj(_l, _obj, _x); }

        internal static void SetObj2S(LuaState _l, Lua.LuaTypeValue _obj, Lua.LuaTypeValue _x) { SetObj(_l, _obj, _x); }

        internal static void SetSValue2S(LuaState _l, Lua.LuaTypeValue _obj, TString _x) { SetSValue(_l, _obj, _x); }

        internal static void SetHValue2S(LuaState _l, Lua.LuaTypeValue _obj, Table _x) { SetHValue(_l, _obj, _x); }

        internal static void SetPTValue2S(LuaState _l, Lua.LuaTypeValue _obj, Proto _x) { SetPTValue(_l, _obj, _x); }

        internal static void SetObjT2T(LuaState _l, Lua.LuaTypeValue _obj, Lua.LuaTypeValue _x) { SetObj(_l, _obj, _x); }

        internal static void SetObj2T(LuaState _l, Lua.LuaTypeValue _obj, Lua.LuaTypeValue _x) { SetObj(_l, _obj, _x); }

        internal static void SetObj2N(LuaState _l, Lua.LuaTypeValue _obj, Lua.LuaTypeValue _x) { SetObj(_l, _obj, _x); }

        internal static void SetSValue2N(LuaState _l, Lua.LuaTypeValue _obj, TString _x) { SetSValue(_l, _obj, _x); }

        internal static void SetTType(Lua.LuaTypeValue _obj, int _tt) { _obj.tt = _tt; }


        internal static bool IsCollectable(Lua.LuaTypeValue _o) { return (TType(_o) >= LUA_TSTRING); }

        public class TStringTSV : GCObject
        {
            public LuaByteType reserved;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public uint hash;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public uint len;
        };
        public class TString : TStringTSV
        {
            public TStringTSV tsv { get { return this; } }

            public TString() { }

            public TString(CharPtr _str) { this.str = _str; }

            public CharPtr str;

            public override string ToString() { return str.ToString(); } 
        };

        public static CharPtr GetStr(TString _ts) { return _ts.str; }
        public static CharPtr SValue(StkId _o) { return GetStr(RawTSValue(_o)); }

        public class UdataUV : GCObject
        {
            public Table metatable;
            public Table env;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public uint len;
        };

        public class Udata : UdataUV
        {
            public Udata() { this.uv = this; }

            public new UdataUV uv;

            public object user_data;
        };

        public class Proto : GCObject
        {

            public Proto[] protos = null;
            public int index = 0;
            public Proto this[int _offset] { get { return this.protos[this.index + _offset]; } }

            public Lua.LuaTypeValue[] k;  
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public Instruction[] code;
            public new Proto[] p; 
            public int[] lineInfo; 
            public LocVar[] locVars;  
            public TString[] upValues; 
            public TString source;
            public int sizeUpValues;
            public int sizeK;  
            public int sizeCode;
            public int sizeLineInfo;
            public int sizeP;  
            public int sizeLocVars;
            public int lineDefined;
            public int lastLineDefined;
            public GCObject gclist;
            public LuaByteType nups;  
            public LuaByteType numParams;
            public LuaByteType isVarArg;
            public LuaByteType maxStackSize;
        };

        public const int VARARG_HASARG = 1;
        public const int VARARG_ISVARARG = 2;
        public const int VARARG_NEEDSARG = 4;

        public class LocVar
        {
            public TString varname;
            public int startpc;  
            public int endpc;   
        };

        public class UpVal : GCObject
        {
            public Lua.LuaTypeValue v;  
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public class Uinternal
            {
                public Lua.LuaTypeValue value = new LuaTypeValue();  
#if !UNITY_3D
                [CLSCompliantAttribute(false)]
#endif
                public class _l
                { 
                    public UpVal prev;
                    public UpVal next;
                };

                public _l l = new _l();
            }
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public new Uinternal u = new Uinternal();
        };

        public class ClosureHeader : GCObject
        {
            public LuaByteType isC;
            public LuaByteType nupvalues;
            public GCObject gclist;
            public Table env;
        };

        public class ClosureType
        {

            private ClosureHeader _header;

            public static implicit operator ClosureHeader(ClosureType _ctype) { return _ctype._header; }
            public ClosureType(ClosureHeader _header) { this._header = _header; }

            public LuaByteType isC { get { return _header.isC; } set { _header.isC = value; } }
            public LuaByteType nupvalues { get { return _header.nupvalues; } set { _header.nupvalues = value; } }
            public GCObject gclist { get { return _header.gclist; } set { _header.gclist = value; } }
            public Table env { get { return _header.env; } set { _header.env = value; } }
        }

        public class CClosure : ClosureType
        {
            public CClosure(ClosureHeader _header) : base(_header) { }
            public LuaNativeFunction f;
            public Lua.LuaTypeValue[] upvalue;
        };

        public class LClosure : ClosureType
        {
            public LClosure(ClosureHeader _header) : base(_header) { }
            public Proto p;
            public UpVal[] upvals;
        };

        public class Closure : ClosureHeader
        {
            public Closure()
            {
                c = new CClosure(this);
                l = new LClosure(this);
            }

            public CClosure c;
            public LClosure l;
        };

        public static bool IsCFunction(Lua.LuaTypeValue _o) { return ((TType(_o) == LUA_TFUNCTION) && (CLValue(_o).c.isC != 0)); }
        public static bool IsLfunction(Lua.LuaTypeValue _o) { return ((TType(_o) == LUA_TFUNCTION) && (CLValue(_o).c.isC == 0)); }

        public class TKeyNK : Lua.LuaTypeValue
        {
            public TKeyNK() { }
            public TKeyNK(Value _value, int _tt, Node _next) : base(_value, _tt)
            {
                this.next = _next;
            }
            public Node next;  
        };

        public class TKey
        {
            public TKey()
            {
                this.nk = new TKeyNK();
            }
            public TKey(TKey copy)
            {
                this.nk = new TKeyNK(copy.nk.value, copy.nk.tt, copy.nk.next);
            }
            public TKey(Value value, int tt, Node next)
            {
                this.nk = new TKeyNK(value, tt, next);
            }

            public TKeyNK nk = new TKeyNK();
            public Lua.LuaTypeValue tvk { get { return this.nk; } }
        };

        public class Node : ArrayElement
        {
            private Node[] _values = null;
            private int _index = -1;

            public void SetIndex(int _index)
            {
                this._index = _index;
            }

            public void SetArray(object _array)
            {
                this._values = (Node[])_array;
                Debug.Assert(this._values != null);
            }

            public Node()
            {
                this.i_val = new LuaTypeValue();
                this.i_key = new TKey();
            }

            public Node(Node _copy)
            {
                this._values = _copy._values;
                this._index = _copy._index;
                this.i_val = new LuaTypeValue(_copy.i_val);
                this.i_key = new TKey(_copy.i_key);
            }

            public Node(Lua.LuaTypeValue _i_val, TKey _i_key)
            {
                this._values = new Node[] { this };
                this._index = 0;
                this.i_val = _i_val;
                this.i_key = _i_key;
            }

            public Lua.LuaTypeValue i_val;
            public TKey i_key;

#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public Node this[uint _offset]
            {
                get { return this._values[this._index + (int)_offset]; }
            }

            public Node this[int _offset]
            {
                get { return this._values[this._index + _offset]; }
            }

            public static int operator -(Node _n_1, Node _n_2)
            {
                Debug.Assert(_n_1._values == _n_2._values);
                return _n_1._index - _n_2._index;
            }

            public static Node Inc(ref Node _node)
            {
                _node = _node[1];
                return _node[-1];
            }

            public static Node Dec(ref Node _node)
            {
                _node = _node[-1];
                return _node[1];
            }

            public static bool operator >(Node _n_1, Node _n_2) { Debug.Assert(_n_1._values == _n_2._values); return _n_1._index > _n_2._index; }
            public static bool operator >=(Node _n_1, Node _n_2) { Debug.Assert(_n_1._values == _n_2._values); return _n_1._index >= _n_2._index; }
            public static bool operator <(Node _n_1, Node _n_2) { Debug.Assert(_n_1._values == _n_2._values); return _n_1._index < _n_2._index; }
            public static bool operator <=(Node _n_1, Node _n_2) { Debug.Assert(_n_1._values == _n_2._values); return _n_1._index <= _n_2._index; }
            public static bool operator !=(Node _n_1, Node _n_2) { return !(_n_1 == _n_2); }
            public static bool operator ==(Node _n_1, Node _n_2)
            {
                object _o_1 = _n_1 as Node;
                object _o_2 = _n_2 as Node;
                if ((_o_1 == null) && (_o_2 == null)) return true;
                if (_o_1 == null) return false;
                if (_o_2 == null) return false;
                if (_n_1._values != _n_2._values) return false;
                return _n_1._index == _n_2._index;
            }
            public override bool Equals(object _o) { return this == (Node)_o; }
            public override int GetHashCode() { return 0; }
        };

        public class Table : GCObject
        {
            public LuaByteType flags; 
            public LuaByteType lSizeNode; 
            public Table metatable;
            public Lua.LuaTypeValue[] array; 
            public Node[] node;
            public int lastFree;  
            public GCObject gcList;
            public int sizeArray;  
        };

        internal static int TwoTo(int _x) { return 1 << _x; }
        internal static int SizeNode(Table _t) { return TwoTo(_t.lSizeNode); }

        public static Lua.LuaTypeValue LuaONilObjectX = new LuaTypeValue(new Value(), LUA_TNIL);
        public static Lua.LuaTypeValue LuaONilObject = LuaONilObjectX;

        public static int CeilLog2(int _x) { return LuaOLog2((uint)(_x - 1)) + 1; }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaOInt2FB(uint _x)
        {
            int _e = 0; 
            while (_x >= 16)
            {
                _x = (_x + 1) >> 1;
                _e++;
            }
            if (_x < 8) return (int)_x;
            else return ((_e + 1) << 3) | (CastInt(_x) - 8);
        }

        public static int LuaOFBInt(int _x)
        {
            int e = (_x >> 3) & 31;
            if (e == 0) return _x;
            else return ((_x & 7) + 8) << (e - 1);
        }

        private readonly static LuaByteType[] log2 = {
            0,1,2,2,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
            6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
            7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
            7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
            8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8
          };

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int LuaOLog2(uint _x)
        {
            int _l = -1;
            while (_x >= 256) { _l += 8; _x >>= 8; }
            return _l + log2[_x];

        }

        public static int LuaORawEqualObj(Lua.LuaTypeValue _t_1, Lua.LuaTypeValue _t_2)
        {
            if (TType(_t_1) != TType(_t_2)) return 0;
            else switch (TType(_t_1))
                {
                    case LUA_TNIL:
                        return 1;
                    case LUA_TNUMBER:
                        return LuaINumEq(NValue(_t_1), NValue(_t_2)) ? 1 : 0;
                    case LUA_TBOOLEAN:
                        return BValue(_t_1) == BValue(_t_2) ? 1 : 0; 
                    case LUA_TLIGHTUSERDATA:
                        return PValue(_t_1) == PValue(_t_2) ? 1 : 0;
                    default:
                        LuaAssert(IsCollectable(_t_1));
                        return GCValue(_t_1) == GCValue(_t_2) ? 1 : 0;
                }
        }

        public static int LuaOStr2d(CharPtr _s, out LuaNumberType _result)
        {
            CharPtr _end_ptr;
            _result = LuaStr2Number(_s, out _end_ptr);
            if (_end_ptr == _s) return 0; 
            if (_end_ptr[0] == 'x' || _end_ptr[0] == 'X')  
                _result = CastNum(StrToUl(_s, out _end_ptr, 16));
            if (_end_ptr[0] == '\0') return 1;  
            while (IsSpace(_end_ptr[0])) _end_ptr = _end_ptr.next();
            if (_end_ptr[0] != '\0') return 0; 
            return 1;
        }

        private static void PushStr(LuaState _l, CharPtr _str)
        {
            SetSValue2S(_l, _l.top, LuaSNew(_l, _str));
            IncrTop(_l);
        }

        public static CharPtr LuaOPushVFString(LuaState _l, CharPtr _fmt, params object[] _arg_p)
        {
            int _parm_index = 0;
            int _n = 1;
            PushStr(_l, "");
            for (;;)
            {
                CharPtr _e = StrChr(_fmt, '%');
                if (_e == null) break;
                SetSValue2S(_l, _l.top, LuaSNewLStr(_l, _fmt, (uint)(_e - _fmt)));
                IncrTop(_l);
                switch (_e[1])
                {
                    case 's':
                        {
                            object _o = _arg_p[_parm_index++];
                            CharPtr _s = _o as CharPtr;
                            if (_s == null)
                                _s = (string)_o;
                            if (_s == null) _s = "(null)";
                            PushStr(_l, _s);
                            break;
                        }
                    case 'c':
                        {
                            CharPtr _buff = new char[2];
                            _buff[0] = (char)(int)_arg_p[_parm_index++];
                            _buff[1] = '\0';
                            PushStr(_l, _buff);
                            break;
                        }
                    case 'd':
                        {
                            SetNValue(_l.top, (int)_arg_p[_parm_index++]);
                            IncrTop(_l);
                            break;
                        }
                    case 'f':
                        {
                            SetNValue(_l.top, (l_uacNumber)_arg_p[_parm_index++]);
                            IncrTop(_l);
                            break;
                        }
                    case 'p':
                        {
                            CharPtr _buff = new char[32];
                            SPrintF(_buff, "0x%08x", _arg_p[_parm_index++].GetHashCode());
                            PushStr(_l, _buff);
                            break;
                        }
                    case '%':
                        {
                            PushStr(_l, "%");
                            break;
                        }
                    default:
                        {
                            CharPtr _buff = new char[3];
                            _buff[0] = '%';
                            _buff[1] = _e[1];
                            _buff[2] = '\0';
                            PushStr(_l, _buff);
                            break;
                        }
                }
                _n += 2;
                _fmt = _e + 2;
            }
            PushStr(_l, _fmt);
            LuaVConcat(_l, _n + 1, CastInt(_l.top - _l.base_) - 1);
            _l.top -= _n;
            return SValue(_l.top - 1);
        }

        public static CharPtr LuaOPushFString(LuaState _l, CharPtr _fmt, params object[] _args)
        {
            return LuaOPushVFString(_l, _fmt, _args);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static void LuaOChunkID(CharPtr _out, CharPtr _source, uint _buff_len)
        {
            if (_source[0] == '=')
            {
                StrNCpy(_out, _source + 1, (int)_buff_len);
                _out[_buff_len - 1] = '\0';  
            }
            else
            { 
                if (_source[0] == '@')
                {
                    uint _l;
                    _source = _source.next();  
                    _buff_len -= (uint)(" '...' ".Length + 1);
                    _l = (uint)StrLen(_source);
                    StrCpy(_out, "");
                    if (_l > _buff_len)
                    {
                        _source += (_l - _buff_len);  
                        StrCat(_out, "...");
                    }
                    StrCat(_out, _source);
                }
                else
                {  
                    uint _len = StrCspn(_source, "\n\r");  
                    _buff_len -= (uint)(" [string \"...\"] ".Length + 1);
                    if (_len > _buff_len) _len = _buff_len;
                    StrCpy(_out, "[string \"");
                    if (_source[_len] != '\0')
                    { 
                        StrNCat(_out, _source, (int)_len);
                        StrCat(_out, "...");
                    }
                    else
                        StrCat(_out, _source);
                    StrCat(_out, "\"]");
                }
            }
        }
    }
}