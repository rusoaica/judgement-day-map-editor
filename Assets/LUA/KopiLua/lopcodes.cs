﻿using System;

namespace KopiLua
{
    using Instruction = System.UInt32;
    using LuaByteType = System.Byte;

    public partial class Lua
    {
        public enum OpMode { iABC, iABx, iAsBx }; 
        public const int SIZE_C = 9;
        public const int SIZE_B = 9;
        public const int SIZE_Bx = (SIZE_C + SIZE_B);
        public const int SIZE_A = 8;
        public const int SIZE_OP = 6;
        public const int POS_OP = 0;
        public const int POS_A = (POS_OP + SIZE_OP);
        public const int POS_C = (POS_A + SIZE_A);
        public const int POS_B = (POS_C + SIZE_C);
        public const int POS_Bx = POS_C;
        public const int MAXARG_Bx = ((1 << SIZE_Bx) - 1);
        public const int MAXARG_sBx = (MAXARG_Bx >> 1); 

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const uint MAXARG_A = (uint)((1 << (int)SIZE_A) - 1);
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const uint MAXARG_B = (uint)((1 << (int)SIZE_B) - 1);
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const uint MAXARG_C = (uint)((1 << (int)SIZE_C) - 1);

        internal static uint MASK1(int _n, int _p) { return (uint)((~((~0) << _n)) << _p); }

        internal static uint MASK0(int _n, int _p) { return (uint)(~MASK1(_n, _p)); }

        internal static OpCode GET_OPCODE(Instruction _i)
        {
            return (OpCode)((_i >> POS_OP) & MASK1(SIZE_OP, 0));
        }

        internal static OpCode GET_OPCODE(InstructionPtr _i) { return GET_OPCODE(_i[0]); }

        internal static void SET_OPCODE(ref Instruction _i, Instruction _o)
        {
            _i = (Instruction)(_i & MASK0(SIZE_OP, POS_OP)) | ((_o << POS_OP) & MASK1(SIZE_OP, POS_OP));
        }

        internal static void SET_OPCODE(ref Instruction _i, OpCode _op_code)
        {
            _i = (Instruction)(_i & MASK0(SIZE_OP, POS_OP)) | (((uint)_op_code << POS_OP) & MASK1(SIZE_OP, POS_OP));
        }

        internal static void SET_OPCODE(InstructionPtr _i, OpCode _op_code) { SET_OPCODE(ref _i.codes[_i.pc], _op_code); }

        internal static int GETARG_A(Instruction _i)
        {
            return (int)((_i >> POS_A) & MASK1(SIZE_A, 0));
        }

        internal static int GETARG_A(InstructionPtr _i) { return GETARG_A(_i[0]); }

        internal static void SETARG_A(InstructionPtr _i, int _u)
        {
            _i[0] = (Instruction)((_i[0] & MASK0(SIZE_A, POS_A)) | ((_u << POS_A) & MASK1(SIZE_A, POS_A)));
        }

        internal static int GETARG_B(Instruction _i)
        {
            return (int)((_i >> POS_B) & MASK1(SIZE_B, 0));
        }

        internal static int GETARG_B(InstructionPtr _i) { return GETARG_B(_i[0]); }

        internal static void SETARG_B(InstructionPtr _i, int _b)
        {
            _i[0] = (Instruction)((_i[0] & MASK0(SIZE_B, POS_B)) | ((_b << POS_B) & MASK1(SIZE_B, POS_B)));
        }

        internal static int GETARG_C(Instruction _i)
        {
            return (int)((_i >> POS_C) & MASK1(SIZE_C, 0));
        }

        internal static int GETARG_C(InstructionPtr _i) { return GETARG_C(_i[0]); }

        internal static void SETARG_C(InstructionPtr _i, int _b)
        {
            _i[0] = (Instruction)((_i[0] & MASK0(SIZE_C, POS_C)) | ((_b << POS_C) & MASK1(SIZE_C, POS_C)));
        }

        internal static int GETARG_Bx(Instruction _i)
        {
            return (int)((_i >> POS_Bx) & MASK1(SIZE_Bx, 0));
        }

        internal static int GETARG_Bx(InstructionPtr _i) { return GETARG_Bx(_i[0]); }

        internal static void SETARG_Bx(InstructionPtr _i, int _b)
        {
            _i[0] = (Instruction)((_i[0] & MASK0(SIZE_Bx, POS_Bx)) | ((_b << POS_Bx) & MASK1(SIZE_Bx, POS_Bx)));
        }

        internal static int GETARG_sBx(Instruction _i)
        {
            return (GETARG_Bx(_i) - MAXARG_sBx);
        }

        internal static int GETARG_sBx(InstructionPtr _i) { return GETARG_sBx(_i[0]); }

        internal static void SETARG_sBx(InstructionPtr _i, int _b)
        {
            SETARG_Bx(_i, _b + MAXARG_sBx);
        }

        internal static int CREATE_ABC(OpCode _o, int _a, int _b, int _c)
        {
            return (int)(((int)_o << POS_OP) | (_a << POS_A) | (_b << POS_B) | (_c << POS_C));
        }

        internal static int CREATE_ABx(OpCode _o, int _a, int _bc)
        {
            int _result = (int)(((int)_o << POS_OP) | (_a << POS_A) | (_bc << POS_Bx));
            return _result;
        }

        internal readonly static int BITRK = (1 << (SIZE_B - 1));

        internal static int ISK(int _x) { return _x & BITRK; }

        internal static int INDEXK(int _r) { return _r & (~BITRK); }

        internal static readonly int MAXINDEXRK = BITRK - 1;

        internal static int RKASK(int _x) { return _x | BITRK; }

        internal static readonly int NO_REG = (int)MAXARG_A;

        public enum OpCode
        {
            OP_MOVE,
            OP_LOADK,
            OP_LOADBOOL,
            OP_LOADNIL,
            OP_GETUPVAL,
            OP_GETGLOBAL,
            OP_GETTABLE,
            OP_SETGLOBAL,
            OP_SETUPVAL,
            OP_SETTABLE,
            OP_NEWTABLE,
            OP_SELF,
            OP_ADD,
            OP_SUB,
            OP_MUL,
            OP_DIV,
            OP_MOD,
            OP_POW,
            OP_UNM,
            OP_NOT,
            OP_LEN,
            OP_CONCAT,
            OP_JMP,
            OP_EQ,
            OP_LT,
            OP_LE,
            OP_TEST,
            OP_TESTSET,
            OP_CALL,
            OP_TAILCALL,
            OP_RETURN,
            OP_FORLOOP,
            OP_FORPREP,
            OP_TFORLOOP,
            OP_SETLIST,
            OP_CLOSE,
            OP_CLOSURE,
            OP_VARARG
        };

        public const int NUM_OPCODES = (int)OpCode.OP_VARARG;

        public enum OpArgMask
        {
            OpArgN,  
            OpArgU,  
            OpArgR,  
            OpArgK   
        };

        public static OpMode getOpMode(OpCode _m) { return (OpMode)(_luaPOpModes[(int)_m] & 3); }
        public static OpArgMask getBMode(OpCode _m) { return (OpArgMask)((_luaPOpModes[(int)_m] >> 4) & 3); }
        public static OpArgMask getCMode(OpCode _m) { return (OpArgMask)((_luaPOpModes[(int)_m] >> 2) & 3); }
        public static int testAMode(OpCode _m) { return _luaPOpModes[(int)_m] & (1 << 6); }
        public static int testTMode(OpCode _m) { return _luaPOpModes[(int)_m] & (1 << 7); }
        
        public const int LFIELDS_PER_FLUSH = 50;

        private readonly static CharPtr[] _luaPOpNames = {
          "MOVE",
          "LOADK",
          "LOADBOOL",
          "LOADNIL",
          "GETUPVAL",
          "GETGLOBAL",
          "GETTABLE",
          "SETGLOBAL",
          "SETUPVAL",
          "SETTABLE",
          "NEWTABLE",
          "SELF",
          "ADD",
          "SUB",
          "MUL",
          "DIV",
          "MOD",
          "POW",
          "UNM",
          "NOT",
          "LEN",
          "CONCAT",
          "JMP",
          "EQ",
          "LT",
          "LE",
          "TEST",
          "TESTSET",
          "CALL",
          "TAILCALL",
          "RETURN",
          "FORLOOP",
          "FORPREP",
          "TFORLOOP",
          "SETLIST",
          "CLOSE",
          "CLOSURE",
          "VARARG",
        };

        private static LuaByteType opmode(LuaByteType _t, LuaByteType _a, OpArgMask _b, OpArgMask _c, OpMode _m)
        {
            return (LuaByteType)(((_t) << 7) | ((_a) << 6) | (((LuaByteType)_b) << 4) | (((LuaByteType)_c) << 2) | ((LuaByteType)_m));
        }

        private readonly static LuaByteType[] _luaPOpModes = {
		
		  opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgN, OpMode.iABC) 		
		 ,opmode(0, 1, OpArgMask.OpArgK, OpArgMask.OpArgN, OpMode.iABx)		
		 ,opmode(0, 1, OpArgMask.OpArgU, OpArgMask.OpArgU, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgN, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgU, OpArgMask.OpArgN, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgK, OpArgMask.OpArgN, OpMode.iABx)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 0, OpArgMask.OpArgK, OpArgMask.OpArgN, OpMode.iABx)		
		 ,opmode(0, 0, OpArgMask.OpArgU, OpArgMask.OpArgN, OpMode.iABC)		
		 ,opmode(0, 0, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgU, OpArgMask.OpArgU, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgN, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgN, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgN, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgR, OpMode.iABC)		
		 ,opmode(0, 0, OpArgMask.OpArgR, OpArgMask.OpArgN, OpMode.iAsBx)		
		 ,opmode(1, 0, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(1, 0, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(1, 0, OpArgMask.OpArgK, OpArgMask.OpArgK, OpMode.iABC)		
		 ,opmode(1, 1, OpArgMask.OpArgR, OpArgMask.OpArgU, OpMode.iABC)		
		 ,opmode(1, 1, OpArgMask.OpArgR, OpArgMask.OpArgU, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgU, OpArgMask.OpArgU, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgU, OpArgMask.OpArgU, OpMode.iABC)		
		 ,opmode(0, 0, OpArgMask.OpArgU, OpArgMask.OpArgN, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgN, OpMode.iAsBx)		
		 ,opmode(0, 1, OpArgMask.OpArgR, OpArgMask.OpArgN, OpMode.iAsBx)		
		 ,opmode(1, 0, OpArgMask.OpArgN, OpArgMask.OpArgU, OpMode.iABC)		
		 ,opmode(0, 0, OpArgMask.OpArgU, OpArgMask.OpArgU, OpMode.iABC)		
		 ,opmode(0, 0, OpArgMask.OpArgN, OpArgMask.OpArgN, OpMode.iABC)		
		 ,opmode(0, 1, OpArgMask.OpArgU, OpArgMask.OpArgN, OpMode.iABx)		
		 ,opmode(0, 1, OpArgMask.OpArgU, OpArgMask.OpArgN, OpMode.iABC)		
		};
    }
}