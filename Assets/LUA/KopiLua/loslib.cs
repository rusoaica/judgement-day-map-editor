﻿using System;
using System.IO;

namespace KopiLua
{
    using lua_Number = System.Double;

    public partial class Lua
    {
        private static int OSPushResult(LuaState _l, int _i, CharPtr _filename)
        {
            int _en = ErrNo();
            if (_i != 0)
            {
                LuaPushBoolean(_l, 1);
                return 1;
            }
            else
            {
                LuaPushNil(_l);
                LuaPushFString(_l, "%s: %s", _filename, StrError(_en));
                LuaPushInteger(_l, _en);
                return 3;
            }
        }

        private static int OSExecute(LuaState _l)
        {
#if XBOX || SILVERLIGHT
			LuaLError(_l, "os_execute not supported on XBox360");
#else
            CharPtr _param = LuaLOptString(_l, 1, null);
            if (_param == null)
            {
                LuaPushInteger(_l, 1);
                return 1;
            }
            CharPtr _str_cmd_line = "/C regenresx " + LuaLOptString(_l, 1, null);
            System.Diagnostics.Process _proc = new System.Diagnostics.Process();
            _proc.EnableRaisingEvents = false;
            _proc.StartInfo.FileName = "CMD.exe";
            _proc.StartInfo.Arguments = _str_cmd_line.ToString();
            _proc.Start();
            _proc.WaitForExit();
            LuaPushInteger(_l, _proc.ExitCode);
#endif
            return 1;
        }

        private static int OSRemove(LuaState _l)
        {
            CharPtr _filename = LuaLCheckString(_l, 1);
            int _result = 1;
            try { File.Delete(_filename.ToString()); } catch { _result = 0; }
            return OSPushResult(_l, _result, _filename);
        }

        private static int OSRename(LuaState _)
        {
            CharPtr _from_name = LuaLCheckString(_, 1);
            CharPtr _to_name = LuaLCheckString(_, 2);
            int _result;
            try
            {
                File.Move(_from_name.ToString(), _to_name.ToString());
                _result = 0;
            }
            catch
            {
                _result = 1;
            }
            return OSPushResult(_, _result, _from_name);
        }

        private static int OSTmpName(LuaState _l)
        {
#if XBOX
			LuaLError(_l, "os_tmpname not supported on Xbox360");
#else
            LuaPushString(_l, Path.GetTempFileName());
#endif
            return 1;
        }

        private static int OSGetEnv(LuaState _l)
        {
            LuaPushString(_l, GetEnv(LuaLCheckString(_l, 1)));
            return 1;
        }

        private static int OSClock(LuaState _l)
        {
            long _ticks = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            LuaPushNumber(_l, ((lua_Number)_ticks) / (lua_Number)1000);
            return 1;
        }

        private static void SetField(LuaState _l, CharPtr _key, int _value)
        {
            LuaPushInteger(_l, _value);
            LuaSetField(_l, -2, _key);
        }

        private static void SetBoolField(LuaState _l, CharPtr _key, int _value)
        {
            if (_value < 0)
                return;
            LuaPushBoolean(_l, _value);
            LuaSetField(_l, -2, _key);
        }

        private static int GetBoolField(LuaState _l, CharPtr _key)
        {
            int _res;
            LuaGetField(_l, -1, _key);
            _res = LuaIsNil(_l, -1) ? -1 : LuaToBoolean(_l, -1);
            LuaPop(_l, 1);
            return _res;
        }

        private static int GetField(LuaState _l, CharPtr _key, int _d)
        {
            int _res;
            LuaGetField(_l, -1, _key);
            if (LuaIsNumber(_l, -1) != 0)
                _res = (int)LuaToInteger(_l, -1);
            else
            {
                if (_d < 0)
                    return LuaLError(_l, "field " + LUA_QS + " missing in date table", _key);
                _res = _d;
            }
            LuaPop(_l, 1);
            return _res;
        }

        private static int OSDate(LuaState _l)
        {
            CharPtr _s = LuaLOptString(_l, 1, "%c");
            DateTime _stm;
            if (_s[0] == '!')
            {
                _stm = DateTime.UtcNow;
                _s.inc();
            }
            else
                _stm = DateTime.Now;
            if (StrCmp(_s, "*t") == 0)
            {
                LuaCreateTable(_l, 0, 9);
                SetField(_l, "sec", _stm.Second);
                SetField(_l, "min", _stm.Minute);
                SetField(_l, "hour", _stm.Hour);
                SetField(_l, "day", _stm.Day);
                SetField(_l, "month", _stm.Month);
                SetField(_l, "year", _stm.Year);
                SetField(_l, "wday", (int)_stm.DayOfWeek);
                SetField(_l, "yday", _stm.DayOfYear);
                SetBoolField(_l, "isdst", _stm.IsDaylightSavingTime() ? 1 : 0);
            }
            else
            {
                CharPtr _cc = new char[3];
                LuaLBuffer _b = new LuaLBuffer();
                _cc[0] = '%'; _cc[2] = '\0';
                LuaLBuffInit(_l, _b);
                for (; _s[0] != 0; _s.inc())
                {
                    if (_s[0] != '%' || _s[1] == '\0')
                        LuaLAddChar(_b, _s[0]);
                    else
                    {
                        uint _reslen;
                        CharPtr _buff = new char[200];
                        _s.inc();
                        _cc[1] = _s[0];
                        _reslen = strftime(_buff, (uint)_buff.chars.Length, _cc, _stm);
                        _buff.index = 0;
                        LuaLAddLString(_b, _buff, _reslen);
                    }
                }
                LuaLPushResult(_b);
            }
            return 1;
        }

        #region strftime c# implementation
        private static uint strftime(CharPtr _s, uint _max_size, CharPtr _format, DateTime _t)
        {
            int _s_index = _s.index;
            CharPtr _p = StrFTimeFmt((_format as object) == null ? "%c" : _format, _t, _s, _s.add((int)_max_size));
            if (_p == _s + _max_size) return 0;
            _p[0] = '\0';
            return (uint)Math.Abs(_s.index - _s_index);
        }

        private static CharPtr StrFTimeFmt(CharPtr _base_format, DateTime _t, CharPtr _pt, CharPtr _ptlim)
        {
            CharPtr _format = new CharPtr(_base_format);
            for (; _format[0] != 0; _format.inc())
            {
                if (_format == '%')
                {
                    _format.inc();
                    if (_format == 'E')
                        _format.inc();
                    else if (_format == 'O')
                        _format.inc();              
                    switch (_format[0])
                    {
                        case '\0':
                            _format.dec();
                            break;
                        case 'A':
                            _pt = StrFTimeAdd(_t.ToString("dddd"), _pt, _ptlim);
                            continue;
                        case 'a':
                            _pt = StrFTimeAdd(_t.ToString("ddd"), _pt, _ptlim);
                            continue;
                        case 'B':
                            _pt = StrFTimeAdd(_t.ToString("MMMM"), _pt, _ptlim);
                            continue;
                        case 'b':
                        case 'h':
                            _pt = StrFTimeAdd(_t.ToString("MMM"), _pt, _ptlim);
                            continue;
                        case 'C':
                            _pt = StrFTimeAdd(_t.ToString("yyyy").Substring(0, 2), _pt, _ptlim);
                            continue;
                        case 'c':
                            _pt = StrFTimeFmt("%a %b %e %H:%M:%S %Y", _t, _pt, _ptlim);
                            continue;
                        case 'D':
                            _pt = StrFTimeFmt("%m/%d/%y", _t, _pt, _ptlim);
                            continue;
                        case 'd':
                            _pt = StrFTimeAdd(_t.ToString("dd"), _pt, _ptlim);
                            continue;
                        case 'e':
                            _pt = StrFTimeAdd(_t.Day.ToString().PadLeft(2, ' '), _pt, _ptlim);
                            continue;
                        case 'F':
                            _pt = StrFTimeFmt("%Y-%m-%d", _t, _pt, _ptlim);
                            continue;
                        case 'H':
                            _pt = StrFTimeAdd(_t.ToString("HH"), _pt, _ptlim);
                            continue;
                        case 'I':
                            _pt = StrFTimeAdd(_t.ToString("hh"), _pt, _ptlim);
                            continue;
                        case 'j':
                            _pt = StrFTimeAdd(_t.DayOfYear.ToString().PadLeft(3, ' '), _pt, _ptlim);
                            continue;
                        case 'k':
                            _pt = StrFTimeAdd(_t.ToString("%H").PadLeft(2, ' '), _pt, _ptlim);
                            continue;
                        case 'l':
                            _pt = StrFTimeAdd(_t.ToString("%h").PadLeft(2, ' '), _pt, _ptlim);
                            continue;
                        case 'M':
                            _pt = StrFTimeAdd(_t.ToString("mm"), _pt, _ptlim);
                            continue;
                        case 'm':
                            _pt = StrFTimeAdd(_t.ToString("MM"), _pt, _ptlim);
                            continue;
                        case 'n':
                            _pt = StrFTimeAdd(Environment.NewLine, _pt, _ptlim);
                            continue;
                        case 'p':
                            _pt = StrFTimeAdd(_t.ToString("tt"), _pt, _ptlim);
                            continue;
                        case 'R':
                            _pt = StrFTimeFmt("%H:%M", _t, _pt, _ptlim);
                            continue;
                        case 'r':
                            _pt = StrFTimeFmt("%I:%M:%S %p", _t, _pt, _ptlim);
                            continue;
                        case 'S':
                            _pt = StrFTimeAdd(_t.ToString("ss"), _pt, _ptlim);
                            continue;
                        case 'T':
                            _pt = StrFTimeFmt("%H:%M:%S", _t, _pt, _ptlim);
                            continue;
                        case 't':
                            _pt = StrFTimeAdd("\t", _pt, _ptlim);
                            continue;
                        case 'U':
                            _pt = StrFTimeAdd(System.Globalization.CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(_t, System.Globalization.CalendarWeekRule.FirstFullWeek, DayOfWeek.Sunday).ToString(), _pt, _ptlim);
                            continue;
                        case 'u':
                            _pt = StrFTimeAdd(_t.DayOfWeek == DayOfWeek.Sunday ? "7" : ((int)_t.DayOfWeek).ToString(), _pt, _ptlim);
                            continue;
                        case 'G':
                        case 'g':
                        case 'V':
                            DateTime _iso_time = _t;
                            DayOfWeek _day = System.Globalization.CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(_iso_time);
                            if (_day >= DayOfWeek.Monday && _day <= DayOfWeek.Wednesday)
                                _iso_time = _iso_time.AddDays(3);
                            if (_format[0] == 'V')
                            {
                                int _iso_week = System.Globalization.CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(_iso_time, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                                _pt = StrFTimeAdd(_iso_week.ToString(), _pt, _ptlim);
                            }
                            else
                            {
                                string _iso_year = System.Globalization.CultureInfo.InvariantCulture.Calendar.GetYear(_iso_time).ToString();
                                if (_format[0] == 'g')
                                    _iso_year = _iso_year.Substring(_iso_year.Length - 2, 2);
                                _pt = StrFTimeAdd(_iso_year, _pt, _ptlim);
                            }
                            continue;
                        case 'W':
                            _pt = StrFTimeAdd(System.Globalization.CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(_t, System.Globalization.CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday).ToString(), _pt, _ptlim);
                            continue;
                        case 'w':
                            _pt = StrFTimeAdd(((int)_t.DayOfWeek).ToString(), _pt, _ptlim);
                            continue;
                        case 'X':
                            _pt = StrFTimeAdd(_t.ToString("%T"), _pt, _ptlim);
                            continue;
                        case 'x':
                            _pt = StrFTimeAdd(_t.ToString("%d"), _pt, _ptlim);
                            continue;
                        case 'y':
                            _pt = StrFTimeAdd(_t.ToString("yy"), _pt, _ptlim);
                            continue;
                        case 'Y':
                            _pt = StrFTimeAdd(_t.Year.ToString(), _pt, _ptlim);
                            continue;
                        case 'Z':
                            _pt = StrFTimeAdd(TimeZoneInfo.Local.StandardName, _pt, _ptlim);
                            continue;
                        case 'z':
                            TimeSpan _ts = TimeZoneInfo.Local.GetUtcOffset(_t);
                            string _offset = (_ts.Ticks < 0 ? "-" : "+") + _ts.TotalHours.ToString("#00") + _ts.Minutes.ToString("00");
                            _pt = StrFTimeAdd(_offset, _pt, _ptlim);
                            continue;
                        case '%':
                            _pt = StrFTimeAdd("%", _pt, _ptlim);
                            continue;
                        default:
                            break;
                    }
                }
                if (_pt == _ptlim) break;
                _pt[0] = _format[0];
                _pt.inc();
            }
            return _pt;
        }

        private static CharPtr StrFTimeAdd(CharPtr _str, CharPtr _pt, CharPtr _ptlim)
        {
            _pt[0] = _str[0];
            _str = _str.next();
            while (_pt < _ptlim && _pt[0] != 0)
            {
                _pt.inc();
                _pt[0] = _str[0];
                _str = _str.next();
            }
            return _pt;
        }
        #endregion

        private static int OSTime(LuaState _l)
        {
            DateTime _t;
            if (LuaIsNoneOrNil(_l, 1))
                _t = DateTime.Now;
            else
            {
                LuaLCheckType(_l, 1, LUA_TTABLE);
                LuaSetTop(_l, 1);
                int _sec = GetField(_l, "sec", 0);
                int _min = GetField(_l, "min", 0);
                int _hour = GetField(_l, "hour", 12);
                int _day = GetField(_l, "day", -1);
                int _month = GetField(_l, "month", -1) - 1;
                int _year = GetField(_l, "year", -1) - 1900;
                GetBoolField(_l, "isdst");
                _t = new DateTime(_year, _month, _day, _hour, _min, _sec);
            }
            LuaPushNumber(_l, _t.Ticks);
            return 1;
        }

        private static int OSDiffTime(LuaState _l)
        {
            long _ticks = (long)LuaLCheckNumber(_l, 1) - (long)LuaLOptNumber(_l, 2, 0);
            LuaPushNumber(_l, _ticks / TimeSpan.TicksPerSecond);
            return 1;
        }

        private static int OSSetLocale(LuaState _l)
        {
            CharPtr __l = LuaLOptString(_l, 1, null);
            LuaPushString(_l, "C");
            return (__l.ToString() == "C") ? 1 : 0;
        }
        
        private static int OSExit(LuaState _l)
        {
#if XBOX
			LuaLError(_l, "os_exit not supported on XBox360");
#else
#if SILVERLIGHT
            throw new SystemException();
#else
            Environment.Exit(EXIT_SUCCESS);
#endif
#endif
            return 0;
        }

        private readonly static LuaLReg[] _sys_lib = {
          new LuaLReg("clock",     OSClock),
          new LuaLReg("date",      OSDate),
          new LuaLReg("difftime",  OSDiffTime),
          new LuaLReg("execute",   OSExecute),
          new LuaLReg("exit",      OSExit),
          new LuaLReg("getenv",    OSGetEnv),
          new LuaLReg("remove",    OSRemove),
          new LuaLReg("rename",    OSRename),
          new LuaLReg("setlocale", OSSetLocale),
          new LuaLReg("time",      OSTime),
          new LuaLReg("tmpname",   OSTmpName),
          new LuaLReg(null, null)
        };

        public static int LuaOpenOS(LuaState _l)
        {
            LuaLRegister(_l, LUA_OSLIBNAME, _sys_lib);
            return 1;
        }
    }
}