﻿using System;

namespace KopiLua
{
    using lu_byte = System.Byte;
    using lua_Number = System.Double;
    using ZIO = Lua.Zio;

    public partial class Lua
    {
        public enum expkind
        {
            VVOID,
            VNIL,
            VTRUE,
            VFALSE,
            VK,
            VKNUM,
            VLOCAL,
            VUPVAL,
            VGLOBAL,
            VINDEXED,
            VJMP,
            VRELOCABLE,
            VNONRELOC,
            VCALL,
            VVARARG
        };

        public class expdesc
        {
            public expkind k;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public _u u = new _u();
            public int t;
            public int f;

            public void Copy(expdesc _e)
            {
                this.k = _e.k;
                this.u.Copy(_e.u);
                this.t = _e.t;
                this.f = _e.f;
            }

#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public class _u
            {
                public _s __s = new _s();
                public lua_Number nval;

                public void Copy(_u __u)
                {
                    this.__s.Copy(__u.__s);
                    this.nval = __u.nval;
                }

#if !UNITY_3D
                [CLSCompliantAttribute(false)]
#endif
                public class _s
                {
                    public void Copy(_s __s)
                    {
                        this.info = __s.info;
                        this.aux = __s.aux;
                    }
                    public int info, aux;
                };
            };
        };

        public class upvaldesc
        {
            public lu_byte k;
            public lu_byte info;
        };

        public class FuncState
        {
            public FuncState()
            {
                for (int i = 0; i < this.upValues.Length; i++)
                    this.upValues[i] = new upvaldesc();
            }

            public Proto f;
            public Table h;
            public FuncState prev;
            public LexState ls;
            public LuaState L;
            public BlockCnt bl;
            public int pc;
            public int lastTarget;
            public int jpc;
            public int freeReg;
            public int nk;
            public int np;
            public short nLocVars;
            public lu_byte nActVar;
            public upvaldesc[] upValues = new upvaldesc[LUAI_MAXUPVALUES];
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public ushort[] actVar = new ushort[LUAI_MAXVARS];
        };

        public static int HasMultret(expkind _k) { return ((_k) == expkind.VCALL || (_k) == expkind.VVARARG) ? 1 : 0; }

        public static LocVar GetLocVar(FuncState _fs, int _i) { return _fs.f.locVars[_fs.actVar[_i]]; }

        public static void LuaYCheckLimit(FuncState _fs, int _v, int _l, CharPtr _m) { if ((_v) > (_l)) ErrorLimit(_fs, _l, _m); }

        public class BlockCnt
        {
            public BlockCnt previous;
            public int breakList;
            public lu_byte nActVar;
            public lu_byte upVal;
            public lu_byte isBreakable;
        };

        private static void AnchorToken(LexState _ls)
        {
            if (_ls.t.token == (int)RESERVED.TK_NAME || _ls.t.token == (int)RESERVED.TK_STRING)
            {
                TString _ts = _ls.t.seminfo.ts;
                LuaXNewString(_ls, GetStr(_ts), _ts.tsv.len);
            }
        }

        private static void ErrorExpected(LexState _ls, int _token)
        {
            LuaXSyntaxError(_ls, LuaOPushFString(_ls.l, LUA_QS + " expected", LuaXTokenToString(_ls, _token)));
        }

        private static void ErrorLimit(FuncState _fs, int _limit, CharPtr _what)
        {
            CharPtr msg = (_fs.f.lineDefined == 0) ? LuaOPushFString(_fs.L, "main function has more than %d %s", _limit, _what) : LuaOPushFString(_fs.L, "function at line %d has more than %d %s", _fs.f.lineDefined, _limit, _what);
            LuaXLexError(_fs.ls, msg, 0);
        }

        private static int TestNext(LexState _ls, int _c)
        {
            if (_ls.t.token == _c)
            {
                LuaXNext(_ls);
                return 1;
            }
            else return 0;
        }

        private static void Check(LexState _ls, int _c)
        {
            if (_ls.t.token != _c)
                ErrorExpected(_ls, _c);
        }

        private static void CheckNext(LexState _ls, int _c)
        {
            Check(_ls, _c);
            LuaXNext(_ls);
        }

        public static void CheckCondition(LexState _ls, bool _c, CharPtr _msg)
        {
            if (!(_c)) LuaXSyntaxError(_ls, _msg);
        }

        private static void CheckMatch(LexState _ls, int _what, int _who, int _where)
        {
            if (TestNext(_ls, _what) == 0)
            {
                if (_where == _ls.lineNumber)
                    ErrorExpected(_ls, _what);
                else
                    LuaXSyntaxError(_ls, LuaOPushFString(_ls.l, LUA_QS + " expected (to close " + LUA_QS + " at line %d)", LuaXTokenToString(_ls, _what), LuaXTokenToString(_ls, _who), _where));
            }
        }

        private static TString StrCheckName(LexState _ls)
        {
            TString _ts;
            Check(_ls, (int)RESERVED.TK_NAME);
            _ts = _ls.t.seminfo.ts;
            LuaXNext(_ls);
            return _ts;
        }

        private static void InitExp(expdesc _e, expkind _k, int _i)
        {
            _e.f = _e.t = NO_JUMP;
            _e.k = _k;
            _e.u.__s.info = _i;
        }

        private static void CodeString(LexState _ls, expdesc _e, TString _s)
        {
            InitExp(_e, expkind.VK, LuaKStringK(_ls.fs, _s));
        }

        private static void CheckName(LexState _ls, expdesc _e)
        {
            CodeString(_ls, _e, StrCheckName(_ls));
        }

        private static int RegisterLocalVar(LexState _ls, TString _var_name)
        {
            FuncState _fs = _ls.fs;
            Proto _f = _fs.f;
            int _old_size = _f.sizeLocVars;
            LuaMGrowVector(_ls.l, ref _f.locVars, _fs.nLocVars, ref _f.sizeLocVars, (int)SHRT_MAX, "too many local variables");
            while (_old_size < _f.sizeLocVars) _f.locVars[_old_size++].varname = null;
            _f.locVars[_fs.nLocVars].varname = _var_name;
            LuaCObjBarrier(_ls.l, _f, _var_name);
            return _fs.nLocVars++;
        }

        public static void NewLocalVarLiteral(LexState _ls, CharPtr _v, int _n)
        {
            NewLocalVar(_ls, LuaXNewString(_ls, "" + _v, (uint)(_v.chars.Length - 1)), _n);
        }

        private static void NewLocalVar(LexState _ls, TString _name, int _n)
        {
            FuncState _fs = _ls.fs;
            LuaYCheckLimit(_fs, _fs.nActVar + _n + 1, LUAI_MAXVARS, "local variables");
            _fs.actVar[_fs.nActVar + _n] = (ushort)RegisterLocalVar(_ls, _name);
        }

        private static void AdjustLocalVars(LexState _ls, int _n_vars)
        {
            FuncState _fs = _ls.fs;
            _fs.nActVar = CastByte(_fs.nActVar + _n_vars);
            for (; _n_vars != 0; _n_vars--)
                GetLocVar(_fs, _fs.nActVar - _n_vars).startpc = _fs.pc;
        }

        private static void RemoveVars(LexState _ls, int _to_level)
        {
            FuncState _fs = _ls.fs;
            while (_fs.nActVar > _to_level)
                GetLocVar(_fs, --_fs.nActVar).endpc = _fs.pc;
        }

        private static int IndexUpValue(FuncState _fs, TString _name, expdesc _v)
        {
            int _i;
            Proto _f = _fs.f;
            int _old_size = _f.sizeUpValues;
            for (_i = 0; _i < _f.nups; _i++)
            {
                if ((int)_fs.upValues[_i].k == (int)_v.k && _fs.upValues[_i].info == _v.u.__s.info)
                {
                    LuaAssert(_f.upValues[_i] == _name);
                    return _i;
                }
            }
            LuaYCheckLimit(_fs, _f.nups + 1, LUAI_MAXUPVALUES, "upvalues");
            LuaMGrowVector(_fs.L, ref _f.upValues, _f.nups, ref _f.sizeUpValues, MAXINT, "");
            while (_old_size < _f.sizeUpValues) _f.upValues[_old_size++] = null;
            _f.upValues[_f.nups] = _name;
            LuaCObjBarrier(_fs.L, _f, _name);
            LuaAssert(_v.k == expkind.VLOCAL || _v.k == expkind.VUPVAL);
            _fs.upValues[_f.nups].k = CastByte(_v.k);
            _fs.upValues[_f.nups].info = CastByte(_v.u.__s.info);
            return _f.nups++;
        }

        private static int SearchVar(FuncState _fs, TString _n)
        {
            int _i;
            for (_i = _fs.nActVar - 1; _i >= 0; _i--)
                if (_n == GetLocVar(_fs, _i).varname)
                    return _i;
            return -1;
        }

        private static void markupval(FuncState _fs, int _level)
        {
            BlockCnt _bl = _fs.bl;
            while ((_bl != null) && _bl.nActVar > _level) _bl = _bl.previous;
            if (_bl != null) _bl.upVal = 1;
        }

        private static expkind SingleVarAux(FuncState _fs, TString _n, expdesc _var, int _base)
        {
            if (_fs == null)
            {
                InitExp(_var, expkind.VGLOBAL, NO_REG);
                return expkind.VGLOBAL;
            }
            else
            {
                int _v = SearchVar(_fs, _n);
                if (_v >= 0)
                {
                    InitExp(_var, expkind.VLOCAL, _v);
                    if (_base == 0)
                        markupval(_fs, _v);
                    return expkind.VLOCAL;
                }
                else
                {
                    if (SingleVarAux(_fs.prev, _n, _var, 0) == expkind.VGLOBAL)
                        return expkind.VGLOBAL;
                    _var.u.__s.info = IndexUpValue(_fs, _n, _var);
                    _var.k = expkind.VUPVAL;
                    return expkind.VUPVAL;
                }
            }
        }

        private static void SingleVar(LexState _ls, expdesc _var)
        {
            TString _var_name = StrCheckName(_ls);
            FuncState _fs = _ls.fs;
            if (SingleVarAux(_fs, _var_name, _var, 1) == expkind.VGLOBAL)
                _var.u.__s.info = LuaKStringK(_fs, _var_name);
        }

        private static void AdjustAssign(LexState _ls, int _n_vars, int _n_exps, expdesc _e)
        {
            FuncState _fs = _ls.fs;
            int _extra = _n_vars - _n_exps;
            if (HasMultret(_e.k) != 0)
            {
                _extra++;
                if (_extra < 0) _extra = 0;
                LuaKSetReturns(_fs, _e, _extra);
                if (_extra > 1) LuaKReserveRegs(_fs, _extra - 1);
            }
            else
            {
                if (_e.k != expkind.VVOID) LuaKExp2NextReg(_fs, _e);
                if (_extra > 0)
                {
                    int _reg = _fs.freeReg;
                    LuaKReserveRegs(_fs, _extra);
                    LuaKNil(_fs, _reg, _extra);
                }
            }
        }

        private static void EnterLevel(LexState _ls)
        {
            if (++_ls.l.nCcalls > LUAI_MAXCCALLS)
                LuaXLexError(_ls, "chunk has too many syntax levels", 0);
        }

        private static void LeaveLevel(LexState _ls) { _ls.l.nCcalls--; }

        private static void EnterBlock(FuncState _fs, BlockCnt _bl, lu_byte _is_breakable)
        {
            _bl.breakList = NO_JUMP;
            _bl.isBreakable = _is_breakable;
            _bl.nActVar = _fs.nActVar;
            _bl.upVal = 0;
            _bl.previous = _fs.bl;
            _fs.bl = _bl;
            LuaAssert(_fs.freeReg == _fs.nActVar);
        }

        private static void LeaveBlock(FuncState _fs)
        {
            BlockCnt _bl = _fs.bl;
            _fs.bl = _bl.previous;
            RemoveVars(_fs.ls, _bl.nActVar);
            if (_bl.upVal != 0)
                LuaKCodeABC(_fs, OpCode.OP_CLOSE, _bl.nActVar, 0, 0);
            LuaAssert((_bl.isBreakable == 0) || (_bl.upVal == 0));
            LuaAssert(_bl.nActVar == _fs.nActVar);
            _fs.freeReg = _fs.nActVar;
            LuaKPatchToHere(_fs, _bl.breakList);
        }

        private static void PushClosure(LexState _ls, FuncState _func, expdesc _v)
        {
            FuncState _fs = _ls.fs;
            Proto _f = _fs.f;
            int _old_size = _f.sizeP;
            int _i;
            LuaMGrowVector(_ls.l, ref _f.p, _fs.np, ref _f.sizeP, MAXARG_Bx, "constant table overflow");
            while (_old_size < _f.sizeP) _f.p[_old_size++] = null;
            _f.p[_fs.np++] = _func.f;
            LuaCObjBarrier(_ls.l, _f, _func.f);
            InitExp(_v, expkind.VRELOCABLE, LuaKCodeABx(_fs, OpCode.OP_CLOSURE, 0, _fs.np - 1));
            for (_i = 0; _i < _func.f.nups; _i++)
            {
                OpCode _o = ((int)_func.upValues[_i].k == (int)expkind.VLOCAL) ? OpCode.OP_MOVE : OpCode.OP_GETUPVAL;
                LuaKCodeABC(_fs, _o, 0, _func.upValues[_i].info, 0);
            }
        }

        private static void OpenFunc(LexState _ls, FuncState _fs)
        {
            LuaState _l = _ls.l;
            Proto _f = LuaFNewProto(_l);
            _fs.f = _f;
            _fs.prev = _ls.fs;
            _fs.ls = _ls;
            _fs.L = _l;
            _ls.fs = _fs;
            _fs.pc = 0;
            _fs.lastTarget = -1;
            _fs.jpc = NO_JUMP;
            _fs.freeReg = 0;
            _fs.nk = 0;
            _fs.np = 0;
            _fs.nLocVars = 0;
            _fs.nActVar = 0;
            _fs.bl = null;
            _f.source = _ls.source;
            _f.maxStackSize = 2;
            _fs.h = LuaHNew(_l, 0, 0);
            SetHValue2S(_l, _l.top, _fs.h);
            IncrTop(_l);
            SetPTValue2S(_l, _l.top, _f);
            IncrTop(_l);
        }

        private static void CloseFunc(LexState _ls)
        {
            LuaState _l = _ls.l;
            FuncState _fs = _ls.fs;
            Proto _f = _fs.f;
            RemoveVars(_ls, 0);
            LuaKRet(_fs, 0, 0);
            LuaMReallocVector(_l, ref _f.code, _f.sizeCode, _fs.pc);
            _f.sizeCode = _fs.pc;
            LuaMReallocVector(_l, ref _f.lineInfo, _f.sizeLineInfo, _fs.pc);
            _f.sizeLineInfo = _fs.pc;
            LuaMReallocVector(_l, ref _f.k, _f.sizeK, _fs.nk);
            _f.sizeK = _fs.nk;
            LuaMReallocVector(_l, ref _f.p, _f.sizeP, _fs.np);
            _f.sizeP = _fs.np;
            for (int i = 0; i < _f.p.Length; i++)
            {
                _f.p[i].protos = _f.p;
                _f.p[i].index = i;
            }
            LuaMReallocVector(_l, ref _f.locVars, _f.sizeLocVars, _fs.nLocVars);
            _f.sizeLocVars = _fs.nLocVars;
            LuaMReallocVector(_l, ref _f.upValues, _f.sizeUpValues, _f.nups);
            _f.sizeUpValues = _f.nups;
            LuaAssert(LuaGCheckCode(_f));
            LuaAssert(_fs.bl == null);
            _ls.fs = _fs.prev;
            if (_fs != null) AnchorToken(_ls);
            _l.top -= 2;
        }

        public static Proto LuaYParser(LuaState _l, ZIO _z, Mbuffer _buff, CharPtr _name)
        {
            LexState _lex_state = new LexState();
            FuncState _func_state = new FuncState();
            _lex_state.buff = _buff;
            LuaXSetInput(_l, _lex_state, _z, LuaSNew(_l, _name));
            OpenFunc(_lex_state, _func_state);
            _func_state.f.isVarArg = VARARG_ISVARARG;
            LuaXNext(_lex_state);
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            Chunk(_lex_state);
            Check(_lex_state, (int)RESERVED.TK_EOS);
            CloseFunc(_lex_state);
            LuaAssert(_func_state.prev == null);
            LuaAssert(_func_state.f.nups == 0);
            LuaAssert(_lex_state.fs == null);
            return _func_state.f;
        }

        private static void Field(LexState _ls, expdesc _v)
        {
            FuncState _fs = _ls.fs;
            expdesc _key = new expdesc();
            LuaKExp2AnyReg(_fs, _v);
            LuaXNext(_ls);
            CheckName(_ls, _key);
            LuaKIndexed(_fs, _v, _key);
        }

        private static void YIndex(LexState _ls, expdesc _v)
        {
            LuaXNext(_ls);
            Expr(_ls, _v);
            LuaKExp2Val(_ls.fs, _v);
            CheckNext(_ls, ']');
        }

        public class ConsControl
        {
            public expdesc v = new expdesc();
            public expdesc t;
            public int nh;
            public int na;
            public int tostore;
        };

        private static void RecField(LexState _ls, ConsControl _cc)
        {
            FuncState _fs = _ls.fs;
            int _reg = _ls.fs.freeReg;
            expdesc _key = new expdesc(), _val = new expdesc();
            int _rk_key;
            if (_ls.t.token == (int)RESERVED.TK_NAME)
            {
                LuaYCheckLimit(_fs, _cc.nh, MAXINT, "items in a constructor");
                CheckName(_ls, _key);
            }
            else
                YIndex(_ls, _key);
            _cc.nh++;
            CheckNext(_ls, '=');
            _rk_key = LuaKExp2RK(_fs, _key);
            Expr(_ls, _val);
            LuaKCodeABC(_fs, OpCode.OP_SETTABLE, _cc.t.u.__s.info, _rk_key, LuaKExp2RK(_fs, _val));
            _fs.freeReg = _reg;
        }

        private static void CloseListField(FuncState _fs, ConsControl _cc)
        {
            if (_cc.v.k == expkind.VVOID) return;
            LuaKExp2NextReg(_fs, _cc.v);
            _cc.v.k = expkind.VVOID;
            if (_cc.tostore == LFIELDS_PER_FLUSH)
            {
                LuaKSetList(_fs, _cc.t.u.__s.info, _cc.na, _cc.tostore);
                _cc.tostore = 0;
            }
        }

        private static void LastListField(FuncState _fs, ConsControl _cc)
        {
            if (_cc.tostore == 0) return;
            if (HasMultret(_cc.v.k) != 0)
            {
                LuaKSetMultRet(_fs, _cc.v);
                LuaKSetList(_fs, _cc.t.u.__s.info, _cc.na, LUA_MULTRET);
                _cc.na--;
            }
            else
            {
                if (_cc.v.k != expkind.VVOID)
                    LuaKExp2NextReg(_fs, _cc.v);
                LuaKSetList(_fs, _cc.t.u.__s.info, _cc.na, _cc.tostore);
            }
        }

        private static void ListField(LexState _ls, ConsControl _cc)
        {
            Expr(_ls, _cc.v);
            LuaYCheckLimit(_ls.fs, _cc.na, MAXINT, "items in a constructor");
            _cc.na++;
            _cc.tostore++;
        }

        private static void Constructor(LexState _ls, expdesc _t)
        {
            FuncState _fs = _ls.fs;
            int _line = _ls.lineNumber;
            int _pc = LuaKCodeABC(_fs, OpCode.OP_NEWTABLE, 0, 0, 0);
            ConsControl _cc = new ConsControl();
            _cc.na = _cc.nh = _cc.tostore = 0;
            _cc.t = _t;
            InitExp(_t, expkind.VRELOCABLE, _pc);
            InitExp(_cc.v, expkind.VVOID, 0);
            LuaKExp2NextReg(_ls.fs, _t);
            CheckNext(_ls, '{');
            do
            {
                LuaAssert(_cc.v.k == expkind.VVOID || _cc.tostore > 0);
                if (_ls.t.token == '}') break;
                CloseListField(_fs, _cc);
                switch (_ls.t.token)
                {
                    case (int)RESERVED.TK_NAME:
                        {
                            LuaXLookAhead(_ls);
                            if (_ls.lookAhead.token != '=')
                                ListField(_ls, _cc);
                            else
                                RecField(_ls, _cc);
                            break;
                        }
                    case '[':
                        {
                            RecField(_ls, _cc);
                            break;
                        }
                    default:
                        {
                            ListField(_ls, _cc);
                            break;
                        }
                }
            } while ((TestNext(_ls, ',') != 0) || (TestNext(_ls, ';') != 0));
            CheckMatch(_ls, '}', '{', _line);
            LastListField(_fs, _cc);
            SETARG_B(new InstructionPtr(_fs.f.code, _pc), LuaOInt2FB((uint)_cc.na));
            SETARG_C(new InstructionPtr(_fs.f.code, _pc), LuaOInt2FB((uint)_cc.nh));
        }

        private static void Parlist(LexState _ls)
        {
            FuncState _fs = _ls.fs;
            Proto _f = _fs.f;
            int _n_params = 0;
            _f.isVarArg = 0;
            if (_ls.t.token != ')')
            {
                do
                {
                    switch (_ls.t.token)
                    {
                        case (int)RESERVED.TK_NAME:
                            {
                                NewLocalVar(_ls, StrCheckName(_ls), _n_params++);
                                break;
                            }
                        case (int)RESERVED.TK_DOTS:
                            {
                                LuaXNext(_ls);
#if LUA_COMPAT_VARARG
		                        new_localvarliteral(_ls, "arg", nparams++);
		                        _f.is_vararg = VARARG_HASARG | VARARG_NEEDSARG;
#endif
                                _f.isVarArg |= VARARG_ISVARARG;
                                break;
                            }
                        default: LuaXSyntaxError(_ls, "<name> or " + LUA_QL("...") + " expected"); break;
                    }
                } while ((_f.isVarArg == 0) && (TestNext(_ls, ',') != 0));
            }
            AdjustLocalVars(_ls, _n_params);
            _f.numParams = CastByte(_fs.nActVar - (_f.isVarArg & VARARG_HASARG));
            LuaKReserveRegs(_fs, _fs.nActVar);
        }

        private static void Body(LexState _ls, expdesc _e, int _need_self, int _line)
        {
            FuncState _new_fs = new FuncState();
            OpenFunc(_ls, _new_fs);
            _new_fs.f.lineDefined = _line;
            CheckNext(_ls, '(');
            if (_need_self != 0)
            {
                NewLocalVarLiteral(_ls, "self", 0);
                AdjustLocalVars(_ls, 1);
            }
            Parlist(_ls);
            CheckNext(_ls, ')');
            Chunk(_ls);
            _new_fs.f.lastLineDefined = _ls.lineNumber;
            CheckMatch(_ls, (int)RESERVED.TK_END, (int)RESERVED.TK_FUNCTION, _line);
            CloseFunc(_ls);
            PushClosure(_ls, _new_fs, _e);
        }

        private static int explist1(LexState _ls, expdesc _v)
        {
            int _n = 1;
            Expr(_ls, _v);
            while (TestNext(_ls, ',') != 0)
            {
                LuaKExp2NextReg(_ls.fs, _v);
                Expr(_ls, _v);
                _n++;
            }
            return _n;
        }

        private static void Funcargs(LexState _ls, expdesc _f)
        {
            FuncState _fs = _ls.fs;
            expdesc _args = new expdesc();
            int _base, _n_params;
            int _line = _ls.lineNumber;
            switch (_ls.t.token)
            {
                case '(':
                    {
                        if (_line != _ls.lastLine)
                            LuaXSyntaxError(_ls, "ambiguous syntax (function call x new statement)");
                        LuaXNext(_ls);
                        if (_ls.t.token == ')')
                            _args.k = expkind.VVOID;
                        else
                        {
                            explist1(_ls, _args);
                            LuaKSetMultRet(_fs, _args);
                        }
                        CheckMatch(_ls, ')', '(', _line);
                        break;
                    }
                case '{':
                    {
                        Constructor(_ls, _args);
                        break;
                    }
                case (int)RESERVED.TK_STRING:
                    {
                        CodeString(_ls, _args, _ls.t.seminfo.ts);
                        LuaXNext(_ls);
                        break;
                    }
                default:
                    {
                        LuaXSyntaxError(_ls, "function arguments expected");
                        return;
                    }
            }
            LuaAssert(_f.k == expkind.VNONRELOC);
            _base = _f.u.__s.info;
            if (HasMultret(_args.k) != 0)
                _n_params = LUA_MULTRET;
            else
            {
                if (_args.k != expkind.VVOID)
                    LuaKExp2NextReg(_fs, _args);
                _n_params = _fs.freeReg - (_base + 1);
            }
            InitExp(_f, expkind.VCALL, LuaKCodeABC(_fs, OpCode.OP_CALL, _base, _n_params + 1, 2));
            LuaKFixLine(_fs, _line);
            _fs.freeReg = _base + 1;
        }

        private static void Prefixexp(LexState _ls, expdesc _v)
        {
            switch (_ls.t.token)
            {
                case '(':
                    {
                        int _line = _ls.lineNumber;
                        LuaXNext(_ls);
                        Expr(_ls, _v);
                        CheckMatch(_ls, ')', '(', _line);
                        LuaKDischargeVars(_ls.fs, _v);
                        return;
                    }
                case (int)RESERVED.TK_NAME:
                    {
                        SingleVar(_ls, _v);
                        return;
                    }
                default:
                    {
                        LuaXSyntaxError(_ls, "unexpected symbol");
                        return;
                    }
            }
        }

        private static void Primaryexp(LexState _ls, expdesc _v)
        {
            FuncState _fs = _ls.fs;
            Prefixexp(_ls, _v);
            for (;;)
            {
                switch (_ls.t.token)
                {
                    case '.':
                        {
                            Field(_ls, _v);
                            break;
                        }
                    case '[':
                        {
                            expdesc _key = new expdesc();
                            LuaKExp2AnyReg(_fs, _v);
                            YIndex(_ls, _key);
                            LuaKIndexed(_fs, _v, _key);
                            break;
                        }
                    case ':':
                        {
                            expdesc _key = new expdesc();
                            LuaXNext(_ls);
                            CheckName(_ls, _key);
                            LuaKSelf(_fs, _v, _key);
                            Funcargs(_ls, _v);
                            break;
                        }
                    case '(':
                    case (int)RESERVED.TK_STRING:
                    case '{':
                        {
                            LuaKExp2NextReg(_fs, _v);
                            Funcargs(_ls, _v);
                            break;
                        }
                    default: return;
                }
            }
        }

        private static void Simpleexp(LexState _ls, expdesc _v)
        {
            switch (_ls.t.token)
            {
                case (int)RESERVED.TK_NUMBER:
                    {
                        InitExp(_v, expkind.VKNUM, 0);
                        _v.u.nval = _ls.t.seminfo.r;
                        break;
                    }
                case (int)RESERVED.TK_STRING:
                    {
                        CodeString(_ls, _v, _ls.t.seminfo.ts);
                        break;
                    }
                case (int)RESERVED.TK_NIL:
                    {
                        InitExp(_v, expkind.VNIL, 0);
                        break;
                    }
                case (int)RESERVED.TK_TRUE:
                    {
                        InitExp(_v, expkind.VTRUE, 0);
                        break;
                    }
                case (int)RESERVED.TK_FALSE:
                    {
                        InitExp(_v, expkind.VFALSE, 0);
                        break;
                    }
                case (int)RESERVED.TK_DOTS:
                    {
                        FuncState _fs = _ls.fs;
                        CheckCondition(_ls, _fs.f.isVarArg != 0, "cannot use " + LUA_QL("...") + " outside a vararg function");
                        _fs.f.isVarArg &= unchecked((lu_byte)(~VARARG_NEEDSARG));
                        InitExp(_v, expkind.VVARARG, LuaKCodeABC(_fs, OpCode.OP_VARARG, 0, 1, 0));
                        break;
                    }
                case '{':
                    {
                        Constructor(_ls, _v);
                        return;
                    }
                case (int)RESERVED.TK_FUNCTION:
                    {
                        LuaXNext(_ls);
                        Body(_ls, _v, 0, _ls.lineNumber);
                        return;
                    }
                default:
                    {
                        Primaryexp(_ls, _v);
                        return;
                    }
            }
            LuaXNext(_ls);
        }

        private static UnOpr Getunopr(int _op)
        {
            switch (_op)
            {
                case (int)RESERVED.TK_NOT: return UnOpr.OPR_NOT;
                case '-': return UnOpr.OPR_MINUS;
                case '#': return UnOpr.OPR_LEN;
                default: return UnOpr.OPR_NOUNOPR;
            }
        }

        private static BinOpr Getbinopr(int _op)
        {
            switch (_op)
            {
                case '+': return BinOpr.OPR_ADD;
                case '-': return BinOpr.OPR_SUB;
                case '*': return BinOpr.OPR_MUL;
                case '/': return BinOpr.OPR_DIV;
                case '%': return BinOpr.OPR_MOD;
                case '^': return BinOpr.OPR_POW;
                case (int)RESERVED.TK_CONCAT: return BinOpr.OPR_CONCAT;
                case (int)RESERVED.TK_NE: return BinOpr.OPR_NE;
                case (int)RESERVED.TK_EQ: return BinOpr.OPR_EQ;
                case '<': return BinOpr.OPR_LT;
                case (int)RESERVED.TK_LE: return BinOpr.OPR_LE;
                case '>': return BinOpr.OPR_GT;
                case (int)RESERVED.TK_GE: return BinOpr.OPR_GE;
                case (int)RESERVED.TK_AND: return BinOpr.OPR_AND;
                case (int)RESERVED.TK_OR: return BinOpr.OPR_OR;
                default: return BinOpr.OPR_NOBINOPR;
            }
        }

        private class priority_
        {
            public lu_byte left;
            public lu_byte right;

            public priority_(lu_byte _left, lu_byte _right)
            {
                this.left = _left;
                this.right = _right;
            }
        }

        private static priority_[] _priority = {
			new priority_(6, 6),
            new priority_(6, 6),
            new priority_(7, 7),
            new priority_(7, 7),
            new priority_(7, 7),
			new priority_(10, 9),
            new priority_(5, 4),
			new priority_(3, 3),
            new priority_(3, 3),
			new priority_(3, 3),
            new priority_(3, 3),
            new priority_(3, 3),
            new priority_(3, 3),
			new priority_(2, 2),
            new priority_(1, 1)
		};

        public const int UNARY_PRIORITY = 8;
        private static BinOpr Subexpr(LexState _ls, expdesc _v, uint _limit)
        {
            BinOpr _op = new BinOpr();
            UnOpr _uop = new UnOpr();
            EnterLevel(_ls);
            _uop = Getunopr(_ls.t.token);
            if (_uop != UnOpr.OPR_NOUNOPR)
            {
                LuaXNext(_ls);
                Subexpr(_ls, _v, UNARY_PRIORITY);
                LuaKPrefix(_ls.fs, _uop, _v);
            }
            else Simpleexp(_ls, _v);
            _op = Getbinopr(_ls.t.token);
            while (_op != BinOpr.OPR_NOBINOPR && _priority[(int)_op].left > _limit)
            {
                expdesc _v2 = new expdesc();
                BinOpr _next_op;
                LuaXNext(_ls);
                LuaKInfix(_ls.fs, _op, _v);
                _next_op = Subexpr(_ls, _v2, _priority[(int)_op].right);
                LuaKPosFix(_ls.fs, _op, _v, _v2);
                _op = _next_op;
            }
            LeaveLevel(_ls);
            return _op;
        }

        private static void Expr(LexState _ls, expdesc _v)
        {
            Subexpr(_ls, _v, 0);
        }

        private static int Block_follow(int _token)
        {
            switch (_token)
            {
                case (int)RESERVED.TK_ELSE:
                case (int)RESERVED.TK_ELSEIF:
                case (int)RESERVED.TK_END:
                case (int)RESERVED.TK_UNTIL:
                case (int)RESERVED.TK_EOS:
                    return 1;
                default: return 0;
            }
        }

        private static void Block(LexState _ls)
        {
            FuncState _fs = _ls.fs;
            BlockCnt _bl = new BlockCnt();
            EnterBlock(_fs, _bl, 0);
            Chunk(_ls);
            LuaAssert(_bl.breakList == NO_JUMP);
            LeaveBlock(_fs);
        }

        public class LHS_assign
        {
            public LHS_assign prev;
            public expdesc _v = new expdesc();
        };

        private static void Check_conflict(LexState _ls, LHS_assign _lh, expdesc _v)
        {
            FuncState _fs = _ls.fs;
            int _extra = _fs.freeReg;
            int _conflict = 0;
            for (; _lh != null; _lh = _lh.prev)
            {
                if (_lh._v.k == expkind.VINDEXED)
                {
                    if (_lh._v.u.__s.info == _v.u.__s.info)
                    {
                        _conflict = 1;
                        _lh._v.u.__s.info = _extra;
                    }
                    if (_lh._v.u.__s.aux == _v.u.__s.info)
                    {
                        _conflict = 1;
                        _lh._v.u.__s.aux = _extra;
                    }
                }
            }
            if (_conflict != 0)
            {
                LuaKCodeABC(_fs, OpCode.OP_MOVE, _fs.freeReg, _v.u.__s.info, 0);
                LuaKReserveRegs(_fs, 1);
            }
        }

        private static void Assignment(LexState _ls, LHS_assign _lh, int _n_vars)
        {
            expdesc _e = new expdesc();
            CheckCondition(_ls, expkind.VLOCAL <= _lh._v.k && _lh._v.k <= expkind.VINDEXED, "syntax error");
            if (TestNext(_ls, ',') != 0)
            {
                LHS_assign _nv = new LHS_assign();
                _nv.prev = _lh;
                Primaryexp(_ls, _nv._v);
                if (_nv._v.k == expkind.VLOCAL)
                    Check_conflict(_ls, _lh, _nv._v);
                LuaYCheckLimit(_ls.fs, _n_vars, LUAI_MAXCCALLS - _ls.l.nCcalls, "variables in assignment");
                Assignment(_ls, _nv, _n_vars + 1);
            }
            else
            {
                int _nex_ps;
                CheckNext(_ls, '=');
                _nex_ps = explist1(_ls, _e);
                if (_nex_ps != _n_vars)
                {
                    AdjustAssign(_ls, _n_vars, _nex_ps, _e);
                    if (_nex_ps > _n_vars)
                        _ls.fs.freeReg -= _nex_ps - _n_vars;
                }
                else
                {
                    LuaKSetOneRet(_ls.fs, _e);
                    LuaKStoreVar(_ls.fs, _lh._v, _e);
                    return;
                }
            }
            InitExp(_e, expkind.VNONRELOC, _ls.fs.freeReg - 1);
            LuaKStoreVar(_ls.fs, _lh._v, _e);
        }

        private static int Cond(LexState _ls)
        {
            expdesc _v = new expdesc();
            Expr(_ls, _v);
            if (_v.k == expkind.VNIL) _v.k = expkind.VFALSE;
            LuaKGoIfTrue(_ls.fs, _v);
            return _v.f;
        }

        private static void Breakstat(LexState _ls)
        {
            FuncState _fs = _ls.fs;
            BlockCnt _bl = _fs.bl;
            int _up_val = 0;
            while ((_bl != null) && (_bl.isBreakable == 0))
            {
                _up_val |= _bl.upVal;
                _bl = _bl.previous;
            }
            if (_bl == null)
                LuaXSyntaxError(_ls, "no loop to break");
            if (_up_val != 0)
                LuaKCodeABC(_fs, OpCode.OP_CLOSE, _bl.nActVar, 0, 0);
            LuaKConcat(_fs, ref _bl.breakList, LuaKJump(_fs));
        }

        private static void Whilestat(LexState _ls, int _line)
        {
            FuncState _fs = _ls.fs;
            int _while_init;
            int _cond_exit;
            BlockCnt _bl = new BlockCnt();
            LuaXNext(_ls);
            _while_init = LuaKGetLabel(_fs);
            _cond_exit = Cond(_ls);
            EnterBlock(_fs, _bl, 1);
            CheckNext(_ls, (int)RESERVED.TK_DO);
            Block(_ls);
            LuaKPatchList(_fs, LuaKJump(_fs), _while_init);
            CheckMatch(_ls, (int)RESERVED.TK_END, (int)RESERVED.TK_WHILE, _line);
            LeaveBlock(_fs);
            LuaKPatchToHere(_fs, _cond_exit);
        }

        private static void Repeatstat(LexState _ls, int _line)
        {
            int _cond_exit;
            FuncState _fs = _ls.fs;
            int _repeat_init = LuaKGetLabel(_fs);
            BlockCnt _bl_1 = new BlockCnt(), _bl_2 = new BlockCnt();
            EnterBlock(_fs, _bl_1, 1);
            EnterBlock(_fs, _bl_2, 0);
            LuaXNext(_ls);
            Chunk(_ls);
            CheckMatch(_ls, (int)RESERVED.TK_UNTIL, (int)RESERVED.TK_REPEAT, _line);
            _cond_exit = Cond(_ls);
            if (_bl_2.upVal == 0)
            {
                LeaveBlock(_fs);
                LuaKPatchList(_ls.fs, _cond_exit, _repeat_init);
            }
            else
            {
                Breakstat(_ls);
                LuaKPatchToHere(_ls.fs, _cond_exit);
                LeaveBlock(_fs);
                LuaKPatchList(_ls.fs, LuaKJump(_fs), _repeat_init);
            }
            LeaveBlock(_fs);
        }

        private static int Exp1(LexState _ls)
        {
            expdesc _e = new expdesc();
            int _k;
            Expr(_ls, _e);
            _k = (int)_e.k;
            LuaKExp2NextReg(_ls.fs, _e);
            return _k;
        }

        private static void ForFody(LexState _ls, int _base, int _line, int _n_vars, int _is_num)
        {
            BlockCnt _bl = new BlockCnt();
            FuncState _fs = _ls.fs;
            int _prep, _end_for;
            AdjustLocalVars(_ls, 3);
            CheckNext(_ls, (int)RESERVED.TK_DO);
            _prep = (_is_num != 0) ? LuaKCodeAsBx(_fs, OpCode.OP_FORPREP, _base, NO_JUMP) : LuaKJump(_fs);
            EnterBlock(_fs, _bl, 0);
            AdjustLocalVars(_ls, _n_vars);
            LuaKReserveRegs(_fs, _n_vars);
            Block(_ls);
            LeaveBlock(_fs);
            LuaKPatchToHere(_fs, _prep);
            _end_for = (_is_num != 0) ? LuaKCodeAsBx(_fs, OpCode.OP_FORLOOP, _base, NO_JUMP) : LuaKCodeABC(_fs, OpCode.OP_TFORLOOP, _base, 0, _n_vars);
            LuaKFixLine(_fs, _line);
            LuaKPatchList(_fs, ((_is_num != 0) ? _end_for : LuaKJump(_fs)), _prep + 1);
        }

        private static void Fornum(LexState _ls, TString _var_name, int _line)
        {
            FuncState _fs = _ls.fs;
            int _base = _fs.freeReg;
            NewLocalVarLiteral(_ls, "(for index)", 0);
            NewLocalVarLiteral(_ls, "(for limit)", 1);
            NewLocalVarLiteral(_ls, "(for step)", 2);
            NewLocalVar(_ls, _var_name, 3);
            CheckNext(_ls, '=');
            Exp1(_ls);
            CheckNext(_ls, ',');
            Exp1(_ls);
            if (TestNext(_ls, ',') != 0)
                Exp1(_ls);
            else
            {
                LuaKCodeABx(_fs, OpCode.OP_LOADK, _fs.freeReg, LuaKNumberK(_fs, 1));
                LuaKReserveRegs(_fs, 1);
            }
            ForFody(_ls, _base, _line, 1, 1);
        }

        private static void Forlist(LexState _ls, TString _index_name)
        {
            FuncState _fs = _ls.fs;
            expdesc _e = new expdesc();
            int _n_vars = 0;
            int _line;
            int _base = _fs.freeReg;
            NewLocalVarLiteral(_ls, "(for generator)", _n_vars++);
            NewLocalVarLiteral(_ls, "(for state)", _n_vars++);
            NewLocalVarLiteral(_ls, "(for control)", _n_vars++);
            NewLocalVar(_ls, _index_name, _n_vars++);
            while (TestNext(_ls, ',') != 0)
                NewLocalVar(_ls, StrCheckName(_ls), _n_vars++);
            CheckNext(_ls, (int)RESERVED.TK_IN);
            _line = _ls.lineNumber;
            AdjustAssign(_ls, 3, explist1(_ls, _e), _e);
            LuaKCheckStack(_fs, 3);
            ForFody(_ls, _base, _line, _n_vars - 3, 0);
        }

        private static void Forstat(LexState _ls, int _line)
        {
            FuncState _fs = _ls.fs;
            TString _var_name;
            BlockCnt _bl = new BlockCnt();
            EnterBlock(_fs, _bl, 1);
            LuaXNext(_ls);
            _var_name = StrCheckName(_ls);
            switch (_ls.t.token)
            {
                case '=': Fornum(_ls, _var_name, _line); break;
                case ',':
                case (int)RESERVED.TK_IN:
                    Forlist(_ls, _var_name);
                    break;
                default: LuaXSyntaxError(_ls, LUA_QL("=") + " or " + LUA_QL("in") + " expected"); break;
            }
            CheckMatch(_ls, (int)RESERVED.TK_END, (int)RESERVED.TK_FOR, _line);
            LeaveBlock(_fs);
        }

        private static int TestThenBlock(LexState _ls)
        {
            int _cond_exit;
            LuaXNext(_ls);
            _cond_exit = Cond(_ls);
            CheckNext(_ls, (int)RESERVED.TK_THEN);
            Block(_ls);
            return _cond_exit;
        }

        private static void Ifstat(LexState _ls, int _line)
        {
            FuncState _fs = _ls.fs;
            int _f_list;
            int _escape_list = NO_JUMP;
            _f_list = TestThenBlock(_ls);
            while (_ls.t.token == (int)RESERVED.TK_ELSEIF)
            {
                LuaKConcat(_fs, ref _escape_list, LuaKJump(_fs));
                LuaKPatchToHere(_fs, _f_list);
                _f_list = TestThenBlock(_ls);
            }
            if (_ls.t.token == (int)RESERVED.TK_ELSE)
            {
                LuaKConcat(_fs, ref _escape_list, LuaKJump(_fs));
                LuaKPatchToHere(_fs, _f_list);
                LuaXNext(_ls);
                Block(_ls);
            }
            else
                LuaKConcat(_fs, ref _escape_list, _f_list);
            LuaKPatchToHere(_fs, _escape_list);
            CheckMatch(_ls, (int)RESERVED.TK_END, (int)RESERVED.TK_IF, _line);
        }

        private static void Localfunc(LexState _ls)
        {
            expdesc _v = new expdesc(), b = new expdesc();
            FuncState _fs = _ls.fs;
            NewLocalVar(_ls, StrCheckName(_ls), 0);
            InitExp(_v, expkind.VLOCAL, _fs.freeReg);
            LuaKReserveRegs(_fs, 1);
            AdjustLocalVars(_ls, 1);
            Body(_ls, b, 0, _ls.lineNumber);
            LuaKStoreVar(_fs, _v, b);
            GetLocVar(_fs, _fs.nActVar - 1).startpc = _fs.pc;
        }

        private static void Localstat(LexState _ls)
        {
            int _n_vars = 0;
            int _n_exps;
            expdesc _e = new expdesc();
            do
            {
                NewLocalVar(_ls, StrCheckName(_ls), _n_vars++);
            } while (TestNext(_ls, ',') != 0);
            if (TestNext(_ls, '=') != 0)
                _n_exps = explist1(_ls, _e);
            else
            {
                _e.k = expkind.VVOID;
                _n_exps = 0;
            }
            AdjustAssign(_ls, _n_vars, _n_exps, _e);
            AdjustLocalVars(_ls, _n_vars);
        }

        private static int FuncName(LexState _ls, expdesc _v)
        {
            int _need_self = 0;
            SingleVar(_ls, _v);
            while (_ls.t.token == '.')
                Field(_ls, _v);
            if (_ls.t.token == ':')
            {
                _need_self = 1;
                Field(_ls, _v);
            }
            return _need_self;
        }

        private static void Funcstat(LexState _ls, int _line)
        {
            int _need_self;
            expdesc _v = new expdesc(), _b = new expdesc();
            LuaXNext(_ls);
            _need_self = FuncName(_ls, _v);
            Body(_ls, _b, _need_self, _line);
            LuaKStoreVar(_ls.fs, _v, _b);
            LuaKFixLine(_ls.fs, _line);
        }

        private static void Exprstat(LexState _ls)
        {
            FuncState _fs = _ls.fs;
            LHS_assign _v = new LHS_assign();
            Primaryexp(_ls, _v._v);
            if (_v._v.k == expkind.VCALL)
                SETARG_C(GetCode(_fs, _v._v), 1);
            else
            {
                _v.prev = null;
                Assignment(_ls, _v, 1);
            }
        }

        private static void Retstat(LexState _ls)
        {
            FuncState _fs = _ls.fs;
            expdesc _e = new expdesc();
            int _first, _nret;
            LuaXNext(_ls);
            if ((Block_follow(_ls.t.token) != 0) || _ls.t.token == ';')
                _first = _nret = 0;
            else
            {
                _nret = explist1(_ls, _e);
                if (HasMultret(_e.k) != 0)
                {
                    LuaKSetMultRet(_fs, _e);
                    if (_e.k == expkind.VCALL && _nret == 1)
                    {
                        SET_OPCODE(GetCode(_fs, _e), OpCode.OP_TAILCALL);
                        LuaAssert(GETARG_A(GetCode(_fs, _e)) == _fs.nActVar);
                    }
                    _first = _fs.nActVar;
                    _nret = LUA_MULTRET;
                }
                else
                {
                    if (_nret == 1)
                        _first = LuaKExp2AnyReg(_fs, _e);
                    else
                    {
                        LuaKExp2NextReg(_fs, _e);
                        _first = _fs.nActVar;
                        LuaAssert(_nret == _fs.freeReg - _first);
                    }
                }
            }
            LuaKRet(_fs, _first, _nret);
        }

        private static int Statement(LexState _ls)
        {
            int _line = _ls.lineNumber;
            switch (_ls.t.token)
            {
                case (int)RESERVED.TK_IF:
                    {
                        Ifstat(_ls, _line);
                        return 0;
                    }
                case (int)RESERVED.TK_WHILE:
                    {
                        Whilestat(_ls, _line);
                        return 0;
                    }
                case (int)RESERVED.TK_DO:
                    {
                        LuaXNext(_ls);
                        Block(_ls);
                        CheckMatch(_ls, (int)RESERVED.TK_END, (int)RESERVED.TK_DO, _line);
                        return 0;
                    }
                case (int)RESERVED.TK_FOR:
                    {
                        Forstat(_ls, _line);
                        return 0;
                    }
                case (int)RESERVED.TK_REPEAT:
                    {
                        Repeatstat(_ls, _line);
                        return 0;
                    }
                case (int)RESERVED.TK_FUNCTION:
                    {
                        Funcstat(_ls, _line);
                        return 0;
                    }
                case (int)RESERVED.TK_LOCAL:
                    {
                        LuaXNext(_ls);
                        if (TestNext(_ls, (int)RESERVED.TK_FUNCTION) != 0)
                            Localfunc(_ls);
                        else
                            Localstat(_ls);
                        return 0;
                    }
                case (int)RESERVED.TK_RETURN:
                    {
                        Retstat(_ls);
                        return 1;
                    }
                case (int)RESERVED.TK_BREAK:
                    {
                        LuaXNext(_ls);
                        Breakstat(_ls);
                        return 1;
                    }
                default:
                    {
                        Exprstat(_ls);
                        return 0;
                    }
            }
        }

        private static void Chunk(LexState _ls)
        {
            int _is_last = 0;
            EnterLevel(_ls);
            while ((_is_last == 0) && (Block_follow(_ls.t.token) == 0))
            {
                _is_last = Statement(_ls);
                TestNext(_ls, ';');
                LuaAssert(_ls.fs.f.maxStackSize >= _ls.fs.freeReg &&
                           _ls.fs.freeReg >= _ls.fs.nActVar);
                _ls.fs.freeReg = _ls.fs.nActVar;
            }
            LeaveLevel(_ls);
        }
    }
}