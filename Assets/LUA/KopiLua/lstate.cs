﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace KopiLua
{
    using lu_byte = System.Byte;
    using lu_int32 = System.Int32;
    using lu_mem = System.UInt32;
    using StkId = Lua.LuaTypeValue;
    using TValue = Lua.LuaTypeValue;

    public partial class Lua
    {
        public static TValue Gt(LuaState _l) { return _l.lGt; }
        public static TValue Registry(LuaState _l) { return G(_l).lRegistry; }
        public const int EXTRASTACK = 5;
        public const int BASICCISIZE = 8;
        public const int BASICSTACKSIZE = (2 * LUA_MINSTACK);

        public class stringtable
        {
            public GCObject[] hash;
            public lu_int32 nuse;
            public int size;
        };

        public class CallInfo : ArrayElement
        {
            public StkId base_;
            public StkId func;
            public StkId top;
            public InstructionPtr saveDpc;
            public int nResults;
            public int tailCalls; 
            private CallInfo[] _values = null;
            private int _index = -1;

            public void SetIndex(int __index)
            {
                this._index = __index;
            }

            public void SetArray(object _array)
            {
                this._values = (CallInfo[])_array;
                Debug.Assert(this._values != null);
            }

            public CallInfo this[int _offset]
            {
                get { return _values[_index + _offset]; }
            }

            public static CallInfo operator +(CallInfo _value, int _offset)
            {
                return _value._values[_value._index + _offset];
            }

            public static CallInfo operator -(CallInfo _value, int _offset)
            {
                return _value._values[_value._index - _offset];
            }

            public static int operator -(CallInfo _ci, CallInfo[] _values)
            {
                Debug.Assert(_ci._values == _values);
                return _ci._index;
            }

            public static int operator -(CallInfo _ci_1, CallInfo _ci_2)
            {
                Debug.Assert(_ci_1._values == _ci_2._values);
                return _ci_1._index - _ci_2._index;
            }

            public static bool operator <(CallInfo _ci_1, CallInfo _ci_2)
            {
                Debug.Assert(_ci_1._values == _ci_2._values);
                return _ci_1._index < _ci_2._index;
            }

            public static bool operator <=(CallInfo _ci_1, CallInfo _ci_2)
            {
                Debug.Assert(_ci_1._values == _ci_2._values);
                return _ci_1._index <= _ci_2._index;
            }

            public static bool operator >(CallInfo _ci_1, CallInfo _ci_2)
            {
                Debug.Assert(_ci_1._values == _ci_2._values);
                return _ci_1._index > _ci_2._index;
            }

            public static bool operator >=(CallInfo _ci_1, CallInfo _ci_2)
            {
                Debug.Assert(_ci_1._values == _ci_2._values);
                return _ci_1._index >= _ci_2._index;
            }

            public static CallInfo Inc(ref CallInfo _value)
            {
                _value = _value[1];
                return _value[-1];
            }

            public static CallInfo Dec(ref CallInfo _value)
            {
                _value = _value[-1];
                return _value[1];
            }
        };

        public static Closure CurrFunc(LuaState _l) { return (CLValue(_l.ci.func)); }

        public static Closure CIFunc(CallInfo _ci) { return (CLValue(_ci.func)); }

        public static bool FIsLua(CallInfo _ci) { return CIFunc(_ci).c.isC == 0; }

        public static bool IsLua(CallInfo _ci) { return (TTIsFunction((_ci).func) && FIsLua(_ci)); }

        public class GlobalState
        {
            public stringtable strT = new stringtable();
            public luaAlloc freAlloc;
            public object ud;
            public lu_byte currentWhite;
            public lu_byte gcState;
            public int sweepStrGc;
            public GCObject rootGc;
            public GCObjectRef sweepGc;
            public GCObject gray;
            public GCObject grayAgain;
            public GCObject weak;
            public GCObject tmuData;
            public Mbuffer buff = new Mbuffer();
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public lu_mem gcThreshold;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public lu_mem totalBytes;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public lu_mem estimate;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public lu_mem gcDept;
            public int gcPause;
            public int gcStepMul;
            public LuaNativeFunction panic;
            public TValue lRegistry = new LuaTypeValue();
            public LuaState mainThread;
            public UpVal uvHead = new UpVal();
            public Table[] mt = new Table[NUMTAGS];
            public TString[] tmName = new TString[(int)TMS.TM_N];
            internal stringtable strt;
        };
        
        public static GlobalState G(LuaState _l) { return _l.lG; }

        public static void G_set(LuaState _l, GlobalState _s) { _l.lG = _s; }

        public class GCObject : GCheader, ArrayElement
        {
            public void SetIndex(int _index) { }

            public void SetArray(object _array) { }

            public GCheader gch { get { return (GCheader)this; } }
            public TString ts { get { return (TString)this; } }
            public Udata u { get { return (Udata)this; } }
            public Closure cl { get { return (Closure)this; } }
            public Table h { get { return (Table)this; } }
            public Proto p { get { return (Proto)this; } }
            public UpVal uv { get { return (UpVal)this; } }
            public LuaState th { get { return (LuaState)this; } }
        };

        public interface GCObjectRef
        {
            void set(GCObject _value);
            GCObject get();
        }

        public class ArrayRef : GCObjectRef, ArrayElement
        {
            GCObject[] _arrayElements;
            private int _arrayIndex;

            public ArrayRef()
            {
                this._arrayElements = null;
                this._arrayIndex = 0;
            }

            public ArrayRef(GCObject[] array_elements, int array_index)
            {
                this._arrayElements = array_elements;
                this._arrayIndex = array_index;
            }

            public void set(GCObject _value) { _arrayElements[_arrayIndex] = _value; }

            public GCObject get() { return _arrayElements[_arrayIndex]; }

            public void SetIndex(int _index) { }

            public void SetArray(object _vals) { }
        }

        public class OpenValRef : GCObjectRef
        {
            LuaState _l;

            public OpenValRef(LuaState _l) { this._l = _l; }

            public void set(GCObject _value) { this._l.openUpVal = _value; }

            public GCObject get() { return this._l.openUpVal; }
        }

        public class RootGCRef : GCObjectRef
        {
            private GlobalState _g;

            public RootGCRef(GlobalState _g) { this._g = _g; }

            public void set(GCObject _value) { this._g.rootGc = _value; }

            public GCObject get() { return this._g.rootGc; }
        }

        public class NextRef : GCObjectRef
        {
            private GCheader _header;

            public NextRef(GCheader _header) { this._header = _header; } 

            public void set(GCObject _value) { this._header.next = _value; }

            public GCObject get() { return this._header.next; }
        }

        public static TString rawgco2ts(GCObject _o) { return (TString)CheckExp(_o.gch.tt == LUA_TSTRING, _o.ts); }
        public static TString gco2ts(GCObject _o) { return (TString)(rawgco2ts(_o).tsv); }
        public static Udata rawgco2u(GCObject _o) { return (Udata)CheckExp(_o.gch.tt == LUA_TUSERDATA, _o.u); }
        public static Udata gco2u(GCObject _o) { return (Udata)(rawgco2u(_o).uv); }
        public static Closure gco2cl(GCObject _o) { return (Closure)CheckExp(_o.gch.tt == LUA_TFUNCTION, _o.cl); }
        public static Table gco2h(GCObject _o) { return (Table)CheckExp(_o.gch.tt == LUA_TTABLE, _o.h); }
        public static Proto gco2p(GCObject _o) { return (Proto)CheckExp(_o.gch.tt == LUATPROTO, _o.p); }
        public static UpVal gco2uv(GCObject _o) { return (UpVal)CheckExp(_o.gch.tt == LUATUPVAL, _o.uv); }
        public static UpVal ngcotouv(GCObject _o) { return (UpVal)CheckExp((_o == null) || (_o.gch.tt == LUATUPVAL), _o.uv); }
        public static LuaState gco2th(GCObject _o) { return (LuaState)CheckExp(_o.gch.tt == LUA_TTHREAD, _o.th); }
        public static GCObject obj2gco(object _v) { return (GCObject)_v; }
        public static int state_size(object _x) { return Marshal.SizeOf(_x) + LUAI_EXTRASPACE; }

        public static LuaState tostate(object _l)
        {
            Debug.Assert(LUAI_EXTRASPACE == 0, "LUAI_EXTRASPACE not supported");
            return (LuaState)_l;
        }

        public class LG : LuaState
        {
            public LuaState l { get { return this; } }
            public GlobalState g = new GlobalState();
        };

        private static void stack_init(LuaState _l_1, LuaState _l)
        {
            _l_1.baseCi = LuaMNewVector<CallInfo>(_l, BASICCISIZE);
            _l_1.ci = _l_1.baseCi[0];
            _l_1.sizeCi = BASICCISIZE;
            _l_1.endCi = _l_1.baseCi[_l_1.sizeCi - 1];
            _l_1.stack = LuaMNewVector<TValue>(_l, BASICSTACKSIZE + EXTRASTACK);
            _l_1.stackSize = BASICSTACKSIZE + EXTRASTACK;
            _l_1.top = _l_1.stack[0];
            _l_1.stackLast = _l_1.stack[_l_1.stackSize - EXTRASTACK - 1];
            _l_1.ci.func = _l_1.top;
            SetNilValue(StkId.Inc(ref _l_1.top));
            _l_1.base_ = _l_1.ci.base_ = _l_1.top;
            _l_1.ci.top = _l_1.top + LUA_MINSTACK;
        }

        private static void freestack(LuaState _l, LuaState _l_1)
        {
            LuaMFreeArray(_l, _l_1.baseCi);
            LuaMFreeArray(_l, _l_1.stack);
        }

        private static void f_luaopen(LuaState _l, object _ud)
        {
            GlobalState _g = G(_l);
            stack_init(_l, _l);
            SetHValue(_l, Gt(_l), LuaHNew(_l, 0, 2));
            SetHValue(_l, Registry(_l), LuaHNew(_l, 0, 2));
            LuaSResize(_l, MINSTRTABSIZE);
            LuaTInit(_l);
            LuaXInit(_l);
            LuaSFix(LuaSNewLiteral(_l, MEMERRMSG));
            _g.gcThreshold = 4 * _g.totalBytes;
        }

        private static void preinit_state(LuaState _l, GlobalState _g)
        {
            G_set(_l, _g);
            _l.stack = null;
            _l.stackSize = 0;
            _l.errorJmp = null;
            _l.hook = null;
            _l.hookMask = 0;
            _l.baseHookCount = 0;
            _l.allowHook = 1;
            ResetHookCount(_l);
            _l.openUpVal = null;
            _l.sizeCi = 0;
            _l.nCcalls = _l.baseCCalls = 0;
            _l.status = 0;
            _l.baseCi = null;
            _l.ci = null;
            _l.saveDpc = new InstructionPtr();
            _l.errFunc = 0;
            SetNilValue(Gt(_l));
        }

        private static void close_state(LuaState _l)
        {
            GlobalState _g = G(_l);
            LuaFClose(_l, _l.stack[0]);
            LuaCFreeAll(_l);
            LuaAssert(_g.rootGc == obj2gco(_l));
            LuaAssert(_g.strT.nuse == 0);
            LuaMFreeArray(_l, G(_l).strT.hash);
            LuaZFreeBuffer(_l, _g.buff);
            freestack(_l, _l);
            LuaAssert(_g.totalBytes == GetUnmanagedSize(typeof(LG)));
        }

        private static LuaState luaE_newthread(LuaState _l)
        {
            LuaState _l_1 = LuaMNew<LuaState>(_l);
            LuaCLink(_l, obj2gco(_l_1), LUA_TTHREAD);
            preinit_state(_l_1, G(_l));
            stack_init(_l_1, _l);
            SetObj2N(_l, Gt(_l_1), Gt(_l));
            _l_1.hookMask = _l.hookMask;
            _l_1.baseHookCount = _l.baseHookCount;
            _l_1.hook = _l.hook;
            ResetHookCount(_l_1);
            LuaAssert(IsWhite(obj2gco(_l_1)));
            return _l_1;
        }

        private static void luaE_freethread(LuaState _l, LuaState _l_1)
        {
            LuaFClose(_l_1, _l_1.stack[0]);
            LuaAssert(_l_1.openUpVal == null);
            LuaIUserStateFree(_l_1);
            freestack(_l, _l_1);
        }

        public static LuaState LuaNewState(luaAlloc _f, object _ud)
        {
            int _i;
            LuaState _l;
            GlobalState _g;
            object __l = _f(typeof(LG));
            if (__l == null) return null;
            _l = tostate(__l);
            _g = (_l as LG).g;
            _l.next = null;
            _l.tt = LUA_TTHREAD;
            _g.currentWhite = (lu_byte)Bit2Mask(WHITE0BIT, FIXEDBIT);
            _l.marked = LuaCWhite(_g);
            lu_byte _marked = _l.marked;
            Set2Bits(ref _marked, FIXEDBIT, SFIXEDBIT);
            _l.marked = _marked;
            preinit_state(_l, _g);
            _g.freAlloc = _f;
            _g.ud = _ud;
            _g.mainThread = _l;
            _g.uvHead.u.l.prev = _g.uvHead;
            _g.uvHead.u.l.next = _g.uvHead;
            _g.gcThreshold = 0;
            _g.strT.size = 0;
            _g.strT.nuse = 0;
            _g.strT.hash = null;
            SetNilValue(Registry(_l));
            LuaZInitBuffer(_l, _g.buff);
            _g.panic = null;
            _g.gcState = GCSpause;
            _g.rootGc = obj2gco(_l);
            _g.sweepStrGc = 0;
            _g.sweepGc = new RootGCRef(_g);
            _g.gray = null;
            _g.grayAgain = null;
            _g.weak = null;
            _g.tmuData = null;
            _g.totalBytes = (uint)GetUnmanagedSize(typeof(LG));
            _g.gcPause = LUAI_GCPAUSE;
            _g.gcStepMul = LUAI_GCMUL;
            _g.gcDept = 0;
            for (_i = 0; _i < NUMTAGS; _i++) _g.mt[_i] = null;
            if (LuaDRawRunProtected(_l, f_luaopen, null) != 0)
            {
                close_state(_l);
                _l = null;
            }
            else
                LuaIUserStateOpen(_l);
            return _l;
        }

        private static void callallgcTM(LuaState _l, object _ud)
        {
            LuaCCallGCTM(_l);
        }

        public static void LuaClose(LuaState _l)
        {
            _l = G(_l).mainThread;
            LuaLock(_l);
            LuaFClose(_l, _l.stack[0]);
            LuaCSeparateUData(_l, 1);
            _l.errFunc = 0;
            do
            {
                _l.ci = _l.baseCi[0];
                _l.base_ = _l.top = _l.ci.base_;
                _l.nCcalls = _l.baseCCalls = 0;
            } while (LuaDRawRunProtected(_l, callallgcTM, null) != 0);
            LuaAssert(G(_l).tmuData == null);
            LuaIUserStateClose(_l);
            close_state(_l);
        }
    }
}