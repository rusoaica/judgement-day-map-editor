﻿using System;

namespace KopiLua
{
    using lu_byte = System.Byte;

    public partial class Lua
    {
        public static int SizeString(TString _s) { return ((int)_s.len + 1) * GetUnmanagedSize(typeof(char)); }

        public static int SizeUData(Udata _u) { return (int)_u.len; }

        public static TString LuaSNew(LuaState _l, CharPtr _s) { return LuaSNewLStr(_l, _s, (uint)StrLen(_s)); }

        public static TString LuaSNewLiteral(LuaState _l, CharPtr _s) { return LuaSNewLStr(_l, _s, (uint)StrLen(_s)); }

        public static void LuaSFix(TString _s)
        {
            lu_byte _marked = _s.tsv.marked;
            LSetBit(ref _marked, FIXEDBIT);
            _s.tsv.marked = _marked;
        }

        public static void LuaSResize(LuaState _l, int _new_size)
        {
            GCObject[] _new_hash;
            stringtable _tb;
            int _i;
            if (G(_l).gcState == GCSsweepstring)
                return;
            _new_hash = new GCObject[_new_size];
            AddTotalBytes(_l, _new_size * GetUnmanagedSize(typeof(GCObjectRef)));
            _tb = G(_l).strt;
            for (_i = 0; _i < _new_size; _i++) _new_hash[_i] = null;
            for (_i = 0; _i < _tb.size; _i++)
            {
                GCObject _p = _tb.hash[_i];
                while (_p != null)
                {
                    GCObject _next = _p.gch.next;
                    uint _h = gco2ts(_p).hash;
                    int _h_1 = (int)LMod(_h, _new_size);
                    LuaAssert((int)(_h % _new_size) == LMod(_h, _new_size));
                    _p.gch.next = _new_hash[_h_1];
                    _new_hash[_h_1] = _p;
                    _p = _next;
                }
            }
            if (_tb.hash != null)
                SubtractTotalBytes(_l, _tb.hash.Length * GetUnmanagedSize(typeof(GCObjectRef)));
            _tb.size = _new_size;
            _tb.hash = _new_hash;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static TString NewLStr(LuaState _l, CharPtr _str, uint __l, uint _h)
        {
            TString _ts;
            stringtable _tb;
            if (__l + 1 > MAXSIZET / GetUnmanagedSize(typeof(char)))
                LuaMTooBig(_l);
            _ts = new TString(new char[__l + 1]);
            AddTotalBytes(_l, (int)(__l + 1) * GetUnmanagedSize(typeof(char)) + GetUnmanagedSize(typeof(TString)));
            _ts.tsv.len = __l;
            _ts.tsv.hash = _h;
            _ts.tsv.marked = LuaCWhite(G(_l));
            _ts.tsv.tt = LUA_TSTRING;
            _ts.tsv.reserved = 0;
            MemCpy(_ts.str.chars, _str.chars, _str.index, (int)__l);
            _ts.str[__l] = '\0';
            _tb = G(_l).strt;
            _h = (uint)LMod(_h, _tb.size);
            _ts.tsv.next = _tb.hash[_h];
            _tb.hash[_h] = obj2gco(_ts);
            _tb.nuse++;
            if ((_tb.nuse > (int)_tb.size) && (_tb.size <= MAXINT / 2))
                LuaSResize(_l, _tb.size * 2);
            return _ts;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static TString LuaSNewLStr(LuaState _l, CharPtr _str, uint __l)
        {
            GCObject _o;
            uint _h = (uint)__l;
            uint _step = (__l >> 5) + 1;
            uint _l_1;
            for (_l_1 = __l; _l_1 >= _step; _l_1 -= _step)
                _h = _h ^ ((_h << 5) + (_h >> 2) + (byte)_str[_l_1 - 1]);
            for (_o = G(_l).strt.hash[LMod(_h, G(_l).strt.size)]; _o != null; _o = _o.gch.next)
            {
                TString _ts = rawgco2ts(_o);
                if (_ts.tsv.len == __l && (MemCmp(_str, GetStr(_ts), __l) == 0))
                {
                    if (IsDead(G(_l), _o)) ChangeWhite(_o);
                    return _ts;
                }
            }
            TString _res = NewLStr(_l, _str, __l, _h);
            return _res;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static Udata LuaSNewUData(LuaState _l, uint _s, Table _e)
        {
            Udata _u = new Udata();
            _u.uv.marked = LuaCWhite(G(_l));
            _u.uv.tt = LUA_TUSERDATA;
            _u.uv.len = _s;
            _u.uv.metatable = null;
            _u.uv.env = _e;
            _u.user_data = new byte[_s];
            AddTotalBytes(_l, GetUnmanagedSize(typeof(Udata)) + SizeUData(_u));
            _u.uv.next = G(_l).mainThread.next;
            G(_l).mainThread.next = obj2gco(_u);
            return _u;
        }

        internal static Udata LuaSNewUData(LuaState _l, Type _t, Table _e)
        {
            Udata _u = new Udata();
            _u.uv.marked = LuaCWhite(G(_l));
            _u.uv.tt = LUA_TUSERDATA;
            _u.uv.len = 0;
            _u.uv.metatable = null;
            _u.uv.env = _e;
            _u.user_data = LuaMRealloc(_l, _t);
            AddTotalBytes(_l, GetUnmanagedSize(typeof(Udata)));
            _u.uv.next = G(_l).mainThread.next;
            G(_l).mainThread.next = obj2gco(_u);
            return _u;
        }
    }
}