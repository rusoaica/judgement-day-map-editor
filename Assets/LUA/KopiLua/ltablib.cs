﻿namespace KopiLua
{
    using lua_Number = System.Double;

    public partial class Lua
    {
        private static int AuxGetn(LuaState _l, int _n) { LuaLCheckType(_l, _n, LUA_TTABLE); return LuaLGetN(_l, _n); }

        private static int ForEachI(LuaState _l)
        {
            int _i;
            int _n = AuxGetn(_l, 1);
            LuaLCheckType(_l, 2, LUA_TFUNCTION);
            for (_i = 1; _i <= _n; _i++)
            {
                LuaPushValue(_l, 2);
                LuaPushInteger(_l, _i);
                LuaRawGetI(_l, 1, _i);
                LuaCall(_l, 2, 1);
                if (!LuaIsNil(_l, -1))
                    return 1;
                LuaPop(_l, 1);
            }
            return 0;
        }

        private static int ForEach(LuaState _l)
        {
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaLCheckType(_l, 2, LUA_TFUNCTION);
            LuaPushNil(_l);
            while (LuaNext(_l, 1) != 0)
            {
                LuaPushValue(_l, 2);
                LuaPushValue(_l, -3);
                LuaPushValue(_l, -3);
                LuaCall(_l, 2, 1);
                if (!LuaIsNil(_l, -1))
                    return 1;
                LuaPop(_l, 2);
            }
            return 0;
        }

        private static int MaxN(LuaState _l)
        {
            lua_Number _max = 0;
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaPushNil(_l);
            while (LuaNext(_l, 1) != 0)
            {
                LuaPop(_l, 1);
                if (LuaType(_l, -1) == LUA_TNUMBER)
                {
                    lua_Number _v = LuaToNumber(_l, -1);
                    if (_v > _max) _max = _v;
                }
            }
            LuaPushNumber(_l, _max);
            return 1;
        }

        private static int GetN(LuaState _l)
        {
            LuaPushInteger(_l, AuxGetn(_l, 1));
            return 1;
        }

        private static int SetN(LuaState _l)
        {
            LuaLCheckType(_l, 1, LUA_TTABLE);
            LuaLError(_l, LUA_QL("setn") + " is obsolete");
            LuaPushValue(_l, 1);
            return 1;
        }

        private static int TInsert(LuaState _l)
        {
            int _e = AuxGetn(_l, 1) + 1;
            int _pos;
            switch (LuaGetTop(_l))
            {
                case 2:
                    {
                        _pos = _e;
                        break;
                    }
                case 3:
                    {
                        int _i;
                        _pos = LuaLCheckInt(_l, 2);
                        if (_pos > _e) _e = _pos;
                        for (_i = _e; _i > _pos; _i--)
                        {
                            LuaRawGetI(_l, 1, _i - 1);
                            LuaRawSetI(_l, 1, _i);
                        }
                        break;
                    }
                default:
                    {
                        return LuaLError(_l, "wrong number of arguments to " + LUA_QL("insert"));
                    }
            }
            LuaLSetN(_l, 1, _e);
            LuaRawSetI(_l, 1, _pos);
            return 0;
        }

        private static int TRemove(LuaState _l)
        {
            int _e = AuxGetn(_l, 1);
            int _pos = LuaLOptInt(_l, 2, _e);
            if (!(1 <= _pos && _pos <= _e))
                return 0;
            LuaLSetN(_l, 1, _e - 1);
            LuaRawGetI(_l, 1, _pos);
            for (; _pos < _e; _pos++)
            {
                LuaRawGetI(_l, 1, _pos + 1);
                LuaRawSetI(_l, 1, _pos);
            }
            LuaPushNil(_l);
            LuaRawSetI(_l, 1, _e);
            return 1;
        }

        private static void AddField(LuaState _l, LuaLBuffer _b, int _i)
        {
            LuaRawGetI(_l, 1, _i);
            if (LuaIsString(_l, -1) == 0)
                LuaLError(_l, "invalid value (%s) at index %d in table for " + LUA_QL("concat"), LuaLTypeName(_l, -1), _i);
            LuaLAddValue(_b);
        }

        private static int TConcat(LuaState _l)
        {
            LuaLBuffer _b = new LuaLBuffer();
            uint _lsep;
            int _i, _last;
            CharPtr sep = LuaLOptLString(_l, 2, "", out _lsep);
            LuaLCheckType(_l, 1, LUA_TTABLE);
            _i = LuaLOptInt(_l, 3, 1);
            _last = LuaLOptInteger(_l, LuaLCheckInt, 4, LuaLGetN(_l, 1));
            LuaLBuffInit(_l, _b);
            for (; _i < _last; _i++)
            {
                AddField(_l, _b, _i);
                LuaLAddLString(_b, sep, _lsep);
            }
            if (_i == _last)
                AddField(_l, _b, _i);
            LuaLPushResult(_b);
            return 1;
        }

        private static void Set2(LuaState _l, int _i, int _j)
        {
            LuaRawSetI(_l, 1, _i);
            LuaRawSetI(_l, 1, _j);
        }

        private static int SortComp(LuaState _l, int _a, int _b)
        {
            if (!LuaIsNil(_l, 2))
            {
                int _res;
                LuaPushValue(_l, 2);
                LuaPushValue(_l, _a - 1);
                LuaPushValue(_l, _b - 2);
                LuaCall(_l, 2, 1);
                _res = LuaToBoolean(_l, -1);
                LuaPop(_l, 1);
                return _res;
            }
            else
                return LuaLessThan(_l, _a, _b);
        }

        private static int AuxSortLoop1(LuaState _l, ref int _i)
        {
            LuaRawGetI(_l, 1, ++_i);
            return SortComp(_l, -1, -2);
        }

        private static int AuxSortLoop2(LuaState _l, ref int _j)
        {
            LuaRawGetI(_l, 1, --_j);
            return SortComp(_l, -3, -1);
        }

        private static void AuxSort(LuaState _l, int __l, int _u)
        {
            while (__l < _u)
            {
                int _i, _j;
                LuaRawGetI(_l, 1, __l);
                LuaRawGetI(_l, 1, _u);
                if (SortComp(_l, -1, -2) != 0)
                    Set2(_l, __l, _u);
                else
                    LuaPop(_l, 2);
                if (_u - __l == 1) break;
                _i = (__l + _u) / 2;
                LuaRawGetI(_l, 1, _i);
                LuaRawGetI(_l, 1, __l);
                if (SortComp(_l, -2, -1) != 0)
                    Set2(_l, _i, __l);
                else
                {
                    LuaPop(_l, 1);
                    LuaRawGetI(_l, 1, _u);
                    if (SortComp(_l, -1, -2) != 0)
                        Set2(_l, _i, _u);
                    else
                        LuaPop(_l, 2);
                }
                if (_u - __l == 2) break;
                LuaRawGetI(_l, 1, _i);
                LuaPushValue(_l, -1);
                LuaRawGetI(_l, 1, _u - 1);
                Set2(_l, _i, _u - 1);
                _i = __l; _j = _u - 1;
                for (;;)
                {
                    while (AuxSortLoop1(_l, ref _i) != 0)
                    {
                        if (_i > _u) LuaLError(_l, "invalid order function for sorting");
                        LuaPop(_l, 1);
                    }
                    while (AuxSortLoop2(_l, ref _j) != 0)
                    {
                        if (_j < __l) LuaLError(_l, "invalid order function for sorting");
                        LuaPop(_l, 1);
                    }
                    if (_j < _i)
                    {
                        LuaPop(_l, 3);
                        break;
                    }
                    Set2(_l, _i, _j);
                }
                LuaRawGetI(_l, 1, _u - 1);
                LuaRawGetI(_l, 1, _i);
                Set2(_l, _u - 1, _i);
                if (_i - __l < _u - _i)
                {
                    _j = __l;
                    _i = _i - 1;
                    __l = _i + 2;
                }
                else
                {
                    _j = _i + 1;
                    _i = _u;
                    _u = _j - 2;
                }
                AuxSort(_l, _j, _i);
            }
        }

        private static int Sort(LuaState _l)
        {
            int _n = AuxGetn(_l, 1);
            LuaLCheckStack(_l, 40, "");
            if (!LuaIsNoneOrNil(_l, 2))
                LuaLCheckType(_l, 2, LUA_TFUNCTION);
            LuaSetTop(_l, 2);
            AuxSort(_l, 1, _n);
            return 0;
        }

        private readonly static LuaLReg[] _tab_funcs = {
          new LuaLReg("concat", TConcat),
          new LuaLReg("foreach", ForEach),
          new LuaLReg("foreachi", ForEachI),
          new LuaLReg("getn", GetN),
          new LuaLReg("maxn", MaxN),
          new LuaLReg("insert", TInsert),
          new LuaLReg("remove", TRemove),
          new LuaLReg("setn", SetN),
          new LuaLReg("sort", Sort),
          new LuaLReg(null, null)
        };


        public static int LuaOpenTable(LuaState _l)
        {
            LuaLRegister(_l, LUA_TABLIBNAME, _tab_funcs);
            return 1;
        }
    }
}