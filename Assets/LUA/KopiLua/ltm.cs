﻿namespace KopiLua
{
    using TValue = Lua.LuaTypeValue;

    public partial class Lua
    {
        public enum TMS
        {
            TM_INDEX,
            TM_NEWINDEX,
            TM_GC,
            TM_MODE,
            TM_EQ,
            TM_ADD,
            TM_SUB,
            TM_MUL,
            TM_DIV,
            TM_MOD,
            TM_POW,
            TM_UNM,
            TM_LEN,
            TM_LT,
            TM_LE,
            TM_CONCAT,
            TM_CALL,
            TM_N
        };

        public static TValue GFastTm(GlobalState _g, Table _et, TMS _e)
        {
            return (_et == null) ? null :
            ((_et.flags & (1 << (int)_e)) != 0) ? null :
            LuaTGetTm(_et, _e, _g.tmName[(int)_e]);
        }

        public static TValue FastTm(LuaState _l, Table _et, TMS _e) { return GFastTm(G(_l), _et, _e); }

        public readonly static CharPtr[] LuaTTypenames = {
          "nil", "boolean", "userdata", "number",
          "string", "table", "function", "userdata", "thread",
          "proto", "upval"
        };

        private readonly static CharPtr[] LuaTEventname = {
            "__index", "__newindex",
            "__gc", "__mode", "__eq",
            "__add", "__sub", "__mul", "__div", "__mod",
            "__pow", "__unm", "__len", "__lt", "__le",
            "__concat", "__call"
        };

        public static void LuaTInit(LuaState _l)
        {
            int _i;
            for (_i = 0; _i < (int)TMS.TM_N; _i++)
            {
                G(_l).tmName[_i] = LuaSNew(_l, LuaTEventname[_i]);
                LuaSFix(G(_l).tmName[_i]);
            }
        }

        public static TValue LuaTGetTm(Table _events, TMS _event, TString _e_name)
        {
            TValue _tm = LuaHGetStr(_events, _e_name);
            LuaAssert(_event <= TMS.TM_EQ);
            if (TTIsNil(_tm))
            {
                _events.flags |= (byte)(1 << (int)_event);
                return null;
            }
            else return _tm;
        }

        public static TValue LuaTGetTmByObj(LuaState _l, TValue _o, TMS _event)
        {
            Table _mt;
            switch (TType(_o))
            {
                case LUA_TTABLE:
                    _mt = HValue(_o).metatable;
                    break;
                case LUA_TUSERDATA:
                    _mt = UValue(_o).metatable;
                    break;
                default:
                    _mt = G(_l).mt[TType(_o)];
                    break;
            }
            return ((_mt != null) ? LuaHGetStr(_mt, G(_l).tmName[(int)_event]) : LuaONilObject);
        }
    }
}