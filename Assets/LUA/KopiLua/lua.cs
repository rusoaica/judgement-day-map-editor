﻿using System;

namespace KopiLua
{
    using LuaNumberType = Double;
    public delegate void LuaHook(LuaState _l, LuaDebug _ar);

    public delegate int LuaNativeFunction(LuaState _l);

#if !UNITY_3D
    [CLSCompliantAttribute(true)]
#endif
    public partial class Lua
    {
        public const string LUA_VERSION = "Lua 5.1";
        public const string LUA_RELEASE = "Lua 5.1.5";
        public const int LUA_VERSION_NUM = 501;
        public const string LUA_COPYRIGHT = "Copyright (C) 1994-2012 Lua.org, PUC-Rio";
        public const string LUA_AUTHORS = "R. Ierusalimschy, L. H. de Figueiredo & W. Celes";
        public const string LUA_SIGNATURE = "\x01bLua";
        public const int LUA_MULTRET = (-1);
        public const int LUA_REGISTRYINDEX = (-10000);
        public const int LUA_ENVIRONINDEX = (-10001);
        public const int LUA_GLOBALSINDEX = (-10002);
        public const int LUA_YIELD = 1;
        public const int LUA_ERRRUN = 2;
        public const int LUA_ERRSYNTAX = 3;
        public const int LUA_ERRMEM = 4;
        public const int LUA_ERRERR = 5;
        public const int LUA_TNONE = -1;
        public const int LUA_TNIL = 0;
        public const int LUA_TBOOLEAN = 1;
        public const int LUA_TLIGHTUSERDATA = 2;
        public const int LUA_TNUMBER = 3;
        public const int LUA_TSTRING = 4;
        public const int LUA_TTABLE = 5;
        public const int LUA_TFUNCTION = 6;
        public const int LUA_TUSERDATA = 7;
        public const int LUA_TTHREAD = 8;
        public const int LUA_MINSTACK = 20;
        public const int LUA_GCSTOP = 0;
        public const int LUA_GCRESTART = 1;
        public const int LUA_GCCOLLECT = 2;
        public const int LUA_GCCOUNT = 3;
        public const int LUA_GCCOUNTB = 4;
        public const int LUA_GCSTEP = 5;
        public const int LUA_GCSETPAUSE = 6;
        public const int LUA_GCSETSTEPMUL = 7;
        public const int LUA_HOOKCALL = 0;
        public const int LUA_HOOKRET = 1;
        public const int LUA_HOOKLINE = 2;
        public const int LUA_HOOKCOUNT = 3;
        public const int LUA_HOOKTAILRET = 4;
        public const int LUA_MASKCALL = (1 << LUA_HOOKCALL);
        public const int LUA_MASKRET = (1 << LUA_HOOKRET);
        public const int LUA_MASKLINE = (1 << LUA_HOOKLINE);
        public const int LUA_MASKCOUNT = (1 << LUA_HOOKCOUNT);

        public static int LuaUpValueIndex(int _i) { return LUA_GLOBALSINDEX - _i; }

        private static bool RunningOnUnix
        {
            get
            {
                var _platform = (int)Environment.OSVersion.Platform;
                return (_platform == 4) || (_platform == 6) || (_platform == 128);
            }
        }

        static Lua()
        {
            if (RunningOnUnix)
            {
                LUA_ROOT = UNIX_LUA_ROOT;
                LUA_LDIR = UNIX_LUA_LDIR;
                LUA_CDIR = UNIX_LUA_CDIR;
                LUA_PATH_DEFAULT = UNIX_LUA_PATH_DEFAULT;
                LUA_CPATH_DEFAULT = UNIX_LUA_CPATH_DEFAULT;
            }
            else
            {
                LUA_ROOT = null;
                LUA_LDIR = WIN32_LUA_LDIR;
                LUA_CDIR = WIN32_LUA_CDIR;
                LUA_PATH_DEFAULT = WIN32_LUA_PATH_DEFAULT;
                LUA_CPATH_DEFAULT = WIN32_LUA_CPATH_DEFAULT;
            }
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public delegate CharPtr LuaReader(LuaState _l, object _ud, out uint _sz);
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public delegate int LuaWriter(LuaState _l, CharPtr _p, uint _sz, object _ud);
        public delegate object luaAlloc(Type _t);

        public static void LuaPop(LuaState _l, int _n)
        {
            LuaSetTop(_l, -(_n) - 1);
        }

        public static void LuaNewTable(LuaState _l)
        {
            LuaCreateTable(_l, 0, 0);
        }

        public static void LuaRegister(LuaState _l, CharPtr _n, LuaNativeFunction _f)
        {
            LuaPushCFunction(_l, _f);
            LuaSetGlobal(_l, _n);
        }

        public static void LuaPushCFunction(LuaState _l, LuaNativeFunction _f)
        {
            LuaPushCClosure(_l, _f, 0);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static uint LuaStrLen(LuaState _l, int _i)
        {
            return LuaObjectLen(_l, _i);
        }

        public static bool LuaIsFunction(LuaState _l, int _n)
        {
            return LuaType(_l, _n) == LUA_TFUNCTION;
        }

        public static bool LuaIsTable(LuaState _l, int _n)
        {
            return LuaType(_l, _n) == LUA_TTABLE;
        }

        public static bool LuaIsLightUserData(LuaState _l, int _n)
        {
            return LuaType(_l, _n) == LUA_TLIGHTUSERDATA;
        }

        public static bool LuaIsNil(LuaState _l, int _n)
        {
            return LuaType(_l, _n) == LUA_TNIL;
        }

        public static bool LuaIsBoolean(LuaState _l, int _n)
        {
            return LuaType(_l, _n) == LUA_TBOOLEAN;
        }

        public static bool LuaIsThread(LuaState _l, int _n)
        {
            return LuaType(_l, _n) == LUA_TTHREAD;
        }

        public static bool LuaIsNone(LuaState _l, int _n)
        {
            return LuaType(_l, _n) == LUA_TNONE;
        }

        public static bool LuaIsNoneOrNil(LuaState _l, LuaNumberType _n)
        {
            return LuaType(_l, (int)_n) <= 0;
        }

        public static void LuaPushLiteral(LuaState _l, CharPtr _s)
        {
            LuaPushString(_l, _s);
        }

        public static void LuaSetGlobal(LuaState _l, CharPtr _s)
        {
            LuaSetField(_l, LUA_GLOBALSINDEX, _s);
        }

        public static void LuaGetGlobal(LuaState _l, CharPtr _s)
        {
            LuaGetField(_l, LUA_GLOBALSINDEX, _s);
        }

        public static CharPtr LuaToString(LuaState _l, int _i)
        {
            uint _blah;
            return LuaToLString(_l, _i, out _blah);
        }

        public static LuaState LuaOpen()
        {
            return LuaLNewState();
        }

        public static void LuaGetRegistry(LuaState _l)
        {
            LuaPushValue(_l, LUA_REGISTRYINDEX);
        }

        public static int LuaGetGCCount(LuaState _l)
        {
            return LuaGC(_l, LUA_GCCOUNT, 0);
        }
    }
}