﻿using System;
using System.Diagnostics;
using System.IO;

namespace KopiLua
{
    using AT.MIN;
    using System.Globalization;
    using LuaNumber = System.Double;

    public partial class Lua
    {
        public static CharPtr LUA_QL(string x) { return "'" + x + "'"; }
        public static CharPtr LUA_QS { get { return LUA_QL("%s"); } }
        public static readonly string LUA_ROOT;
        public static readonly string LUA_LDIR;
        public static readonly string LUA_CDIR;
        public static readonly string LUA_PATH_DEFAULT;
        public static readonly string LUA_CPATH_DEFAULT;
        public static readonly string LUA_DIRSEP = Path.DirectorySeparatorChar.ToString();
        public const string LUA_PATHSEP = ";";
        public const string LUA_PATH_MARK = "?";
        public const string LUA_EXECDIR = "!";
        public const string LUA_IGMARK = "-";
        public const string LUA_PROMPT = "> ";
        public const string LUA_PROMPT2 = ">> ";
        public const string LUA_PROGNAME = "lua";
        public const string LUA_PATH = "LUA_PATH";
        public const string LUA_CPATH = "LUA_CPATH";
        public const string LUA_INIT = "LUA_INIT";
        public const string LUA_NUMBER_SCAN = "%lf";
        public const string LUA_NUMBER_FMT = "%.14g";
        public const int LUA_MAXINPUT = 512;
        public const int LUA_IDSIZE = 60;
        public const int LUAI_GCPAUSE = 200;
        public const int LUAI_GCMUL = 200;
        public const int LUAI_BITSINT = 32;
        public const int LUAI_MAXCALLS = 20000;
        public const int LUAI_MAXCSTACK = 8000;
        public const int LUAI_MAXCCALLS = 200;
        public const int LUAI_MAXVARS = 200;
        public const int LUAI_MAXUPVALUES = 60;
        public const int LUAL_BUFFERSIZE = 1024;
        public const int LUAI_MAXNUMBER2STR = 32;
        public const int LUAI_EXTRASPACE = 0;
        public const int EXIT_SUCCESS = 0;
        public const int EXIT_FAILURE = 1;
        private const string number_chars = "0123456789+-eE.";
        private const string WIN32_LUA_LDIR = "!\\lua\\";
        private const string WIN32_LUA_CDIR = "!\\";
        private const string WIN32_LUA_PATH_DEFAULT = ".\\?.lua;" + WIN32_LUA_LDIR + "?.lua;" + WIN32_LUA_LDIR + "?\\init.lua;" + WIN32_LUA_CDIR + "?.lua;" + WIN32_LUA_CDIR + "?\\init.lua";
        private const string WIN32_LUA_CPATH_DEFAULT = ".\\?.dll;" + WIN32_LUA_CDIR + "?.dll;" + WIN32_LUA_CDIR + "loadall.dll";
        private const string UNIX_LUA_ROOT = "/usr/local/";
        private const string UNIX_LUA_LDIR = UNIX_LUA_ROOT + "share/lua/5.1/";
        private const string UNIX_LUA_CDIR = UNIX_LUA_ROOT + "lib/lua/5.1/";
        private const string UNIX_LUA_PATH_DEFAULT = "./?.lua;" + UNIX_LUA_LDIR + "?.lua;" + UNIX_LUA_LDIR + "?/init.lua;" + UNIX_LUA_CDIR + "?.lua;" + UNIX_LUA_CDIR + "?/init.lua";
        private const string UNIX_LUA_CPATH_DEFAULT = "./?.so;" + UNIX_LUA_CDIR + "?.so;" + UNIX_LUA_CDIR + "loadall.so";
#if LUA_USE_ISATTY
#elif LUA_WIN
#else
        public static int LuaStdinIsTty() { return 1; }
#endif
#if LUA_USE_READLINE
#else
        public static bool LuaReadLine(LuaState _l, CharPtr _b, CharPtr _p)
        {
            FPuts(_p, stdOut);
            FFlush(stdOut);
            return (FGets(_b, stdIn) != null);
        }

        public static void LuaSaveLine(LuaState _l, int _idx) { }

        public static void LuaFreeLine(LuaState _l, CharPtr _b) { }
#endif
#if LUA_USE_APICHECK
		public static void LuaIApiCheck(LuaState _l, bool _o)	{Debug.Assert(_o);}
		public static void LuaApiCheck(LuaState _l, int _o) {Debug.Assert(_o != 0);}
#else
        public static void LuaIApiCheck(LuaState _l, bool _o) { }

        public static void LuaIApiCheck(LuaState _l, int _o) { }
#endif
        public static CharPtr LuaNumber2Str(double _n) { return String.Format(CultureInfo.InvariantCulture, "{0}", _n); }

        public static double LuaStr2Number(CharPtr _s, out CharPtr _end)
        {
            _end = new CharPtr(_s.chars, _s.index);
            string _str = "";
            while (_end[0] == ' ')
                _end = _end.next();
            while (number_chars.IndexOf(_end[0]) >= 0)
            {
                _str += _end[0];
                _end = _end.next();
            }
            try
            {
                return Convert.ToDouble(_str.ToString(), Culture("en-US"));
            }
            catch (System.OverflowException)
            {
                if (_str[0] == '-')
                    return System.Double.NegativeInfinity;
                else
                    return System.Double.PositiveInfinity;
            }
            catch
            {
                _end = new CharPtr(_s.chars, _s.index);
                return 0;
            }
        }

        private static IFormatProvider Culture(string _p)
        {
#if SILVERLIGHT
            return new CultureInfo(_p);
#else
            return CultureInfo.GetCultureInfo(_p);
#endif
        }
#if LUA_CORE
		public delegate LuaNumber OpDelegate(LuaNumber _a, LuaNumber _b);

		public static LuaNumber LuaINumAdd(LuaNumber _a, LuaNumber _b) { return ((_a) + (_b)); }

		public static LuaNumber LuaINumSub(LuaNumber _a, LuaNumber _b) { return ((_a) - (_b)); }

		public static LuaNumber LuaINumMul(LuaNumber _a, LuaNumber _b) { return ((_a) * (_b)); }

		public static LuaNumber LuaINumDiv(LuaNumber _a, LuaNumber _b) { return ((_a) / (_b)); }

		public static LuaNumber LuaINumMod(LuaNumber _a, LuaNumber _b) { return ((_a) - Math.Floor((_a) / (_b)) * (_b)); }

		public static LuaNumber LuaINumPow(LuaNumber _a, LuaNumber _b) { return (Math.Pow(_a, _b)); }

		public static LuaNumber LuaINumUnm(LuaNumber _a) { return (-(_a)); }

		public static bool LuaINumEq(LuaNumber _a, LuaNumber _b) { return ((_a) == (_b)); }

		public static bool LuaINumLt(LuaNumber _a, LuaNumber _b) { return ((_a) < (_b)); }

		public static bool LuaINumLe(LuaNumber _a, LuaNumber _b) { return ((_a) <= (_b)); }

		public static bool LuaINumIsNaN(LuaNumber _a) { return LuaNumber.IsNaN(_a); }
#endif

        private static void LuaNumber2Int(out int _i, LuaNumber _d) { _i = (int)_d; }

        private static void LuaNumber2Integer(out int _i, LuaNumber _n) { _i = (int)_n; }

        public class LuaException : Exception
        {
            public LuaState l;
            public LuaLongJmp c;

            public LuaException(LuaState _l, LuaLongJmp _c) { this.l = _l; this.c = _c; }
        }

        public static void LUAI_THROW(LuaState _l, LuaLongJmp _c) { throw new LuaException(_l, _c); }

        public static void LUAI_TRY(LuaState _l, LuaLongJmp _c, object _a)
        {
            if (_c.status == 0) _c.status = -1;
        }

        public const int LUA_MAXCAPTURES = 32;
#if loslib_c || luaall_c

#if LUA_USE_MKSTEMP
		public const int LUA_TMPNAMBUFSIZE	= 32;
#else
			public const int LUA_TMPNAMBUFSIZE	= L_tmpnam;

			public static void LuaTmpName(CharPtr _b, int _e) { _e = (tmpnam(_b) == null) ? 1 : 0; }
#endif
#endif

        public static Stream LuaPopen(LuaState _l, CharPtr _c, CharPtr _m) { LuaLError(_l, LUA_QL("popen") + " not supported"); return null; }

        public static int LuaPClose(LuaState _l, Stream _file) { return 0; }

        public static void LuaIUserStateOpen(LuaState _l) { }

        public static void LuaIUserStateClose(LuaState _l) { }

        public static void LuaIUserStateThread(LuaState _l, LuaState _l_1) { }

        public static void LuaIUserStateFree(LuaState _l) { }

        public static void LuaIUserStateResume(LuaState _l, int _n) { }

        public static void LuaIUserStateYield(LuaState _l, int _n) { }

#if LUA_USELONGLONG
		public const string LUA_INTFRMLEN = "ll";
#else
        public const string LUA_INTFRMLEN = "l";
#endif
        public static bool IsAlpha(char _c) { return Char.IsLetter(_c); }

        public static bool IsCntrl(char _c) { return Char.IsControl(_c); }

        public static bool IsDigit(char _c) { return Char.IsDigit(_c); }

        public static bool IsLower(char _c) { return Char.IsLower(_c); }

        public static bool IsPunct(char _c) { return Char.IsPunctuation(_c); }

        public static bool IsSpace(char _c) { return (_c == ' ') || (_c >= (char)0x09 && _c <= (char)0x0D); }

        public static bool IsUpper(char _c) { return Char.IsUpper(_c); }

        public static bool IsAlnum(char _c) { return Char.IsLetterOrDigit(_c); }

        public static bool IsXDigit(char _c) { return "0123456789ABCDEFabcdef".IndexOf(_c) >= 0; }

        public static bool IsAlpha(int _c) { return Char.IsLetter((char)_c); }

        public static bool IsCntrl(int _c) { return Char.IsControl((char)_c); }

        public static bool IsDigit(int _c) { return Char.IsDigit((char)_c); }

        public static bool IsLower(int _c) { return Char.IsLower((char)_c); }

        public static bool IsPunct(int _c) { return ((char)_c != ' ') && !IsAlnum((char)_c); }

        public static bool IsSpace(int _c) { return ((char)_c == ' ') || ((char)_c >= (char)0x09 && (char)_c <= (char)0x0D); }

        public static bool IsUpper(int _c) { return Char.IsUpper((char)_c); }

        public static bool IsAlnum(int _c) { return Char.IsLetterOrDigit((char)_c); }

        public static char ToLower(char _c) { return Char.ToLower(_c); }

        public static char ToUpper(char _c) { return Char.ToUpper(_c); }

        public static char ToLower(int _c) { return Char.ToLower((char)_c); }

        public static char ToUpper(int _c) { return Char.ToUpper((char)_c); }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static ulong StrToUl(CharPtr _s, out CharPtr _end, int _base)
        {
            try
            {
                _end = new CharPtr(_s.chars, _s.index);
                while (_end[0] == ' ')
                    _end = _end.next();
                if ((_end[0] == '0') && (_end[1] == 'x'))
                    _end = _end.next().next();
                else if ((_end[0] == '0') && (_end[1] == 'X'))
                    _end = _end.next().next();
                bool negate = false;
                if (_end[0] == '+')
                    _end = _end.next();
                else if (_end[0] == '-')
                {
                    negate = true;
                    _end = _end.next();
                }
                bool _invalid = false;
                bool _had_digits = false;
                ulong _result = 0;
                while (true)
                {
                    char _ch = _end[0];
                    int _this_digit = 0;
                    if (IsDigit(_ch))
                        _this_digit = _ch - '0';
                    else if (IsAlpha(_ch))
                        _this_digit = ToLower(_ch) - 'a' + 10;
                    else
                        break;
                    if (_this_digit >= _base)
                        _invalid = true;
                    else
                    {
                        _had_digits = true;
                        _result = _result * (ulong)_base + (ulong)_this_digit;
                    }
                    _end = _end.next();
                }
                if (_invalid || (!_had_digits))
                {
                    _end = _s;
                    return System.UInt64.MaxValue;
                }
                if (negate)
                    _result = (ulong)-(long)_result;
                return (ulong)_result;
            }
            catch
            {
                _end = _s;
                return 0;
            }
        }

        public static void PutChar(char _ch)
        {
            Console.Write(_ch);
        }

        public static void PutChar(int _ch)
        {
            Console.Write((char)_ch);
        }

        public static bool IsPrint(byte _c)
        {
            return (_c >= (byte)' ') && (_c <= (byte)127);
        }

        public static int ParseScanf(string _str, CharPtr _fmt, params object[] _arg_p)
        {
            int _param_index = 0;
            int _index = 0;
            while (_fmt[_index] != 0)
            {
                if (_fmt[_index++] == '%')
                    switch (_fmt[_index++])
                    {
                        case 's':
                            {
                                _arg_p[_param_index++] = _str;
                                break;
                            }
                        case 'c':
                            {
                                _arg_p[_param_index++] = Convert.ToChar(_str, Culture("en-US"));
                                break;
                            }
                        case 'd':
                            {
                                _arg_p[_param_index++] = Convert.ToInt32(_str, Culture("en-US"));
                                break;
                            }
                        case 'l':
                            {
                                _arg_p[_param_index++] = Convert.ToDouble(_str, Culture("en-US"));
                                break;
                            }
                        case 'f':
                            {
                                _arg_p[_param_index++] = Convert.ToDouble(_str, Culture("en-US"));
                                break;
                            }
                    }
            }
            return _param_index;
        }

        public static void PrintF(CharPtr _str, params object[] _arg_v)
        {
            Tools.PrintF(_str.ToString(), _arg_v);
        }

        public static void SPrintF(CharPtr _buffer, CharPtr _str, params object[] _arg_v)
        {
            string _temp = Tools.SPrintF(_str.ToString(), _arg_v);
            StrCpy(_buffer, _temp);
        }

        public static int FPrintF(Stream _stream, CharPtr _str, params object[] _arg_v)
        {
            string _result = Tools.SPrintF(_str.ToString(), _arg_v);
            char[] _chars = _result.ToCharArray();
            byte[] _bytes = new byte[_chars.Length];
            for (int i = 0; i < _chars.Length; i++)
                _bytes[i] = (byte)_chars[i];
            _stream.Write(_bytes, 0, _bytes.Length);
            return 1;
        }

        public static int ErrNo()
        {
            return -1;
        }

        public static CharPtr StrError(int _error)
        {
            return String.Format("error #{0}", _error);
        }

        public static CharPtr GetEnv(CharPtr _env_name)
        {
            return null;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int MemCmp(CharPtr _ptr_1, CharPtr _ptr_2, uint _size) { return MemCmp(_ptr_1, _ptr_2, (int)_size); }
        public static int MemCmp(CharPtr _ptr_1, CharPtr _ptr_2, int _size)
        {
            for (int i = 0; i < _size; i++)
                if (_ptr_1[i] != _ptr_2[i])
                {
                    if (_ptr_1[i] < _ptr_2[i])
                        return -1;
                    else
                        return 1;
                }
            return 0;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr MemChr(CharPtr _ptr, char _c, uint _count)
        {
            for (uint i = 0; i < _count; i++)
                if (_ptr[i] == _c)
                    return new CharPtr(_ptr.chars, (int)(_ptr.index + i));
            return null;
        }

        public static CharPtr StrPbrk(CharPtr _str, CharPtr _charset)
        {
            for (int i = 0; _str[i] != '\0'; i++)
                for (int j = 0; _charset[j] != '\0'; j++)
                    if (_str[i] == _charset[j])
                        return new CharPtr(_str.chars, _str.index + i);
            return null;
        }

        public static CharPtr StrChr(CharPtr _str, char _c)
        {
            for (int index = _str.index; _str.chars[index] != 0; index++)
                if (_str.chars[index] == _c)
                    return new CharPtr(_str.chars, index);
            return null;
        }

        public static CharPtr StrCpy(CharPtr _dest, CharPtr _src)
        {
            int _i;
            for (_i = 0; _src[_i] != '\0'; _i++)
                _dest[_i] = _src[_i];
            _dest[_i] = '\0';
            return _dest;
        }

        public static CharPtr StrCat(CharPtr _dest, CharPtr _src)
        {
            int _dest_index = 0;
            while (_dest[_dest_index] != '\0')
                _dest_index++;
            int _src_index = 0;
            while (_src[_src_index] != '\0')
                _dest[_dest_index++] = _src[_src_index++];
            _dest[_dest_index++] = '\0';
            return _dest;
        }

        public static CharPtr StrNCat(CharPtr _dest, CharPtr _src, int _count)
        {
            int _dest_index = 0;
            while (_dest[_dest_index] != '\0')
                _dest_index++;
            int _src_index = 0;
            while ((_src[_src_index] != '\0') && (_count-- > 0))
                _dest[_dest_index++] = _src[_src_index++];
            return _dest;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static uint StrCspn(CharPtr _str, CharPtr _charset)
        {
            int _index = _str.ToString().IndexOfAny(_charset.ToString().ToCharArray());
            if (_index < 0)
                _index = _str.ToString().Length;
            return (uint)_index;
        }

        public static CharPtr StrNCpy(CharPtr _dest, CharPtr _src, int _length)
        {
            int _index = 0;
            while ((_src[_index] != '\0') && (_index < _length))
            {
                _dest[_index] = _src[_index];
                _index++;
            }
            while (_index < _length)
                _dest[_index++] = '\0';
            return _dest;
        }

        public static int StrLen(CharPtr _str)
        {
            int _index = 0;
            while (_str[_index] != '\0')
                _index++;
            return _index;
        }

        public static LuaNumber FMod(LuaNumber _a, LuaNumber _b)
        {
            float _quotient = (int)Math.Floor(_a / _b);
            return _a - _quotient * _b;
        }

        public static LuaNumber ModF(LuaNumber _a, out LuaNumber _b)
        {
            _b = Math.Floor(_a);
            return _a - Math.Floor(_a);
        }

        public static long LMod(LuaNumber _a, LuaNumber _b)
        {
            return (long)_a % (long)_b;
        }

        public static int GetC(Stream _f)
        {
            return _f.ReadByte();
        }

        public static void UngetC(int _c, Stream _f)
        {
            if (_f.Position > 0)
                _f.Seek(-1, SeekOrigin.Current);
        }

#if XBOX || SILVERLIGHT
		public static Stream stdout;
		public static Stream stdin;
		public static Stream stderr;
#else
        public static Stream stdOut = Console.OpenStandardOutput();
        public static Stream stdIn = Console.OpenStandardInput();
        public static Stream stdErr = Console.OpenStandardError();
#endif
        public static int EOF = -1;

        public static void FPuts(CharPtr _str, Stream _stream)
        {
            Console.Write(_str.ToString());
        }

        public static int FEof(Stream _s)
        {
            return (_s.Position >= _s.Length) ? 1 : 0;
        }

        public static int FRead(CharPtr _ptr, int _size, int _num, Stream _stream)
        {
            int _num_bytes = _num * _size;
            byte[] _bytes = new byte[_num_bytes];
            try
            {
                int _result = _stream.Read(_bytes, 0, _num_bytes);
                for (int i = 0; i < _result; i++)
                    _ptr[i] = (char)_bytes[i];
                return _result / _size;
            }
            catch
            {
                return 0;
            }
        }

        public static int FWrite(CharPtr _ptr, int _size, int _num, Stream _stream)
        {
            int _num_bytes = _num * _size;
            byte[] _bytes = new byte[_num_bytes];
            for (int i = 0; i < _num_bytes; i++)
                _bytes[i] = (byte)_ptr[i];
            try
            {
                _stream.Write(_bytes, 0, _num_bytes);
            }
            catch
            {
                return 0;
            }
            return _num;
        }

        public static int StrCmp(CharPtr _s_1, CharPtr _s_2)
        {
            if (_s_1 == _s_2)
                return 0;
            if (_s_1 == null)
                return -1;
            if (_s_2 == null)
                return 1;
            for (int i = 0; ; i++)
            {
                if (_s_1[i] != _s_2[i])
                {
                    if (_s_1[i] < _s_2[i])
                        return -1;
                    else
                        return 1;
                }
                if (_s_1[i] == '\0')
                    return 0;
            }
        }

        public static CharPtr FGets(CharPtr _str, Stream _stream)
        {
            int _index = 0;
            try
            {
                while (true)
                {
                    _str[_index] = (char)_stream.ReadByte();
                    if (_str[_index] == '\n')
                        break;
                    if (_index >= _str.chars.Length)
                        break;
                    _index++;
                }
            }
            catch
            {
            }
            return _str;
        }

        public static double FRExp(double _x, out int _exp_ptr)
        {
#if XBOX
			_exp_ptr = (int)(Math.Log(_x) / Math.Log(2)) + 1;
#else
            _exp_ptr = (int)Math.Log(_x, 2) + 1;
#endif
            double _s = _x / Math.Pow(2, _exp_ptr);
            return _s;
        }

        public static double LDExp(double x, int expptr)
        {
            return x * Math.Pow(2, expptr);
        }

        public static CharPtr StrStr(CharPtr _str, CharPtr _substr)
        {
            int _index = _str.ToString().IndexOf(_substr.ToString());
            if (_index < 0)
                return null;
            return new CharPtr(_str + _index);
        }

        public static CharPtr StrRChr(CharPtr _str, char _ch)
        {
            int _index = _str.ToString().LastIndexOf(_ch);
            if (_index < 0)
                return null;
            return _str + _index;
        }

        public static Stream FOpen(CharPtr _filename, CharPtr _mode)
        {
            string _str = _filename.ToString();
            FileMode _file_mode = FileMode.Open;
            FileAccess _file_access = (FileAccess)0;
            for (int i = 0; _mode[i] != '\0'; i++)
                switch (_mode[i])
                {
                    case 'r':
                        _file_access = _file_access | FileAccess.Read;
                        if (!File.Exists(_str))
                            return null;
                        break;

                    case 'w':
                        _file_mode = FileMode.Create;
                        _file_access = _file_access | FileAccess.Write;
                        break;
                }
            try
            {
                return new FileStream(_str, _file_mode, _file_access);
            }
            catch
            {
                return null;
            }
        }

        public static Stream FReopen(CharPtr _filename, CharPtr _mode, Stream _stream)
        {
            try
            {
                _stream.Flush();
                _stream.Close();
            }
            catch { }
            return FOpen(_filename, _mode);
        }

        public static void FFlush(Stream _stream)
        {
            _stream.Flush();
        }

        public static int FError(Stream _stream)
        {
            return 0;
        }

        public static int FClose(Stream _stream)
        {
            _stream.Close();
            return 0;
        }

#if !XBOX
        public static Stream TmpFile()
        {
            return new FileStream(Path.GetTempFileName(), FileMode.Create, FileAccess.ReadWrite);
        }
#endif

        public static int FScanF(Stream _f, CharPtr _format, params object[] _arg_p)
        {
            string _str = Console.ReadLine();
            return ParseScanf(_str, _format, _arg_p);
        }

        public static int FSeek(Stream _f, long _offset, int _origin)
        {
            try
            {
                _f.Seek(_offset, (SeekOrigin)_origin);
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        public static int FTell(Stream _f)
        {
            return (int)_f.Position;
        }

        public static int ClearErr(Stream _f)
        {
            return 0;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int SetVBuf(Stream _stream, CharPtr _buffer, int _mode, uint _size)
        {
            Debug.Assert(false, "setvbuf not implemented yet - mjf");
            return 0;
        }

        public static void MemCpy<T>(T[] _dest, T[] _src, int _length)
        {
            for (int i = 0; i < _length; i++)
                _dest[i] = _src[i];
        }

        public static void MemCpy<T>(T[] _dest, int _offset, T[] _src, int _length)
        {
            for (int i = 0; i < _length; i++)
                _dest[_offset + i] = _src[i];
        }

        public static void MemCpy<T>(T[] dst, T[] src, int srcofs, int length)
        {
            for (int i = 0; i < length; i++)
                dst[i] = src[srcofs + i];
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static void MemCpy(CharPtr _ptr_1, CharPtr _ptr_2, uint _size) { MemCpy(_ptr_1, _ptr_2, (int)_size); }
        public static void MemCpy(CharPtr _ptr_1, CharPtr _ptr_2, int _size)
        {
            for (int i = 0; i < _size; i++)
                _ptr_1[i] = _ptr_2[i];
        }

        public static object VOID(object f) { return f; }

        public const double HUGE_VAL = System.Double.MaxValue;
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const uint SHRT_MAX = System.UInt16.MaxValue;

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const int _IONBF = 0;
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const int _IOFBF = 1;
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public const int _IOLBF = 2;

        public const int SEEK_SET = 0;
        public const int SEEK_CUR = 1;
        public const int SEEK_END = 2;
        public static int GetUnmanagedSize(Type t)
        {
            if (t == typeof(GlobalState))
                return 228;
            else if (t == typeof(LG))
                return 376;
            else if (t == typeof(CallInfo))
                return 24;
            else if (t == typeof(LuaTypeValue))
                return 16;
            else if (t == typeof(Table))
                return 32;
            else if (t == typeof(Node))
                return 32;
            else if (t == typeof(GCObject))
                return 120;
            else if (t == typeof(GCObjectRef))
                return 4;
            else if (t == typeof(ArrayRef))
                return 4;
            else if (t == typeof(Closure))
                return 0;
            else if (t == typeof(Proto))
                return 76;
            else if (t == typeof(LuaLReg))
                return 8;
            else if (t == typeof(LuaLBuffer))
                return 524;
            else if (t == typeof(LuaState))
                return 120;
            else if (t == typeof(LuaDebug))
                return 100;
            else if (t == typeof(CallS))
                return 8;
            else if (t == typeof(LoadF))
                return 520;
            else if (t == typeof(LoadS))
                return 8;
            else if (t == typeof(LuaLongJmp))
                return 72;
            else if (t == typeof(SParser))
                return 20;
            else if (t == typeof(Token))
                return 16;
            else if (t == typeof(LexState))
                return 52;
            else if (t == typeof(FuncState))
                return 572;
            else if (t == typeof(GCheader))
                return 8;
            else if (t == typeof(LuaTypeValue))
                return 16;
            else if (t == typeof(TString))
                return 16;
            else if (t == typeof(LocVar))
                return 12;
            else if (t == typeof(UpVal))
                return 32;
            else if (t == typeof(CClosure))
                return 40;
            else if (t == typeof(LClosure))
                return 24;
            else if (t == typeof(TKey))
                return 16;
            else if (t == typeof(ConsControl))
                return 40;
            else if (t == typeof(LHS_assign))
                return 32;
            else if (t == typeof(expdesc))
                return 24;
            else if (t == typeof(upvaldesc))
                return 2;
            else if (t == typeof(BlockCnt))
                return 12;
            else if (t == typeof(Zio))
                return 20;
            else if (t == typeof(Mbuffer))
                return 12;
            else if (t == typeof(LoadState))
                return 16;
            else if (t == typeof(MatchState))
                return 272;
            else if (t == typeof(stringtable))
                return 12;
            else if (t == typeof(FilePtr))
                return 4;
            else if (t == typeof(Udata))
                return 24;
            else if (t == typeof(Char))
                return 1;
            else if (t == typeof(UInt16))
                return 2;
            else if (t == typeof(Int16))
                return 2;
            else if (t == typeof(UInt32))
                return 4;
            else if (t == typeof(Int32))
                return 4;
            else if (t == typeof(Single))
                return 4;
            Debug.Assert(false, "Trying to get unknown sized of unmanaged type " + t.ToString());
            return 0;
        }
    }
}
