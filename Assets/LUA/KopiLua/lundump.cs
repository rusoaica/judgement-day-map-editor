﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace KopiLua
{
    using Instruction = System.UInt32;
    using lu_byte = System.Byte;
    using lua_Number = System.Double;
    using StkId = Lua.LuaTypeValue;
    using TValue = Lua.LuaTypeValue;
    using ZIO = Lua.Zio;

    public partial class Lua
    {
        public const int LUAC_VERSION = 0x51;
        public const int LUAC_FORMAT = 0;
        public const int LUAC_HEADERSIZE = 12;

        public class LoadState
        {
            public LuaState l;
            public ZIO Z;
            public Mbuffer b;
            public CharPtr name;
        };

        public static void IF(int _c, string _s) { }
        public static void IF(bool _c, string _s) { }

        static void Error(LoadState _s, CharPtr _why)
        {
            LuaOPushFString(_s.l, "%s: %s in precompiled chunk", _s.name, _why);
            LuaDThrow(_s.l, LUA_ERRSYNTAX);
        }

        public static object LoadMem(LoadState _s, Type _t)
        {
#if SILVERLIGHT
			int _size = 0;
			if (_t.Equals(typeof(UInt32)))
				_size = 4;
			else if (_t.Equals(typeof(Int32)))
				_size = 4;
			else if (_t.Equals(typeof(Char)))
				_size = 1;
			else if (_t.Equals(typeof(Byte)))
				_size = 1;
			else if (_t.Equals(typeof(Double)))
				_size = 8;
#else
            int _size = Marshal.SizeOf(_t);
#endif
            CharPtr _str = new char[_size];
            LoadBlock(_s, _str, _size);
            byte[] _bytes = new byte[_str.chars.Length];
            for (int i = 0; i < _str.chars.Length; i++)
                _bytes[i] = (byte)_str.chars[i];
#if SILVERLIGHT
			object _b = null;
			if (_t.Equals(typeof(UInt32)))
				_b = (UInt32)BitConverter.ToUInt32(bytes, 0);
			else if (_t.Equals(typeof(Int32)))
				_b = (Int32)BitConverter.ToInt32(bytes, 0);
			else if (_t.Equals(typeof(Char)))
				_b = (Char)bytes[0];
			else if (_t.Equals(typeof(Byte)))
				_b = (Byte)bytes[0];
			else if (_t.Equals(typeof(Double)))
				_b = (Double)BitConverter.ToDouble(bytes, 0);
#else
            GCHandle _pinned_packet = GCHandle.Alloc(_bytes, GCHandleType.Pinned);
            object _b = Marshal.PtrToStructure(_pinned_packet.AddrOfPinnedObject(), _t);
            _pinned_packet.Free();
#endif
            return _b;
        }

        public static object LoadMem(LoadState _s, Type _t, int _n)
        {
#if SILVERLIGHT
			Array _array = Array.CreateInstance(_t, _n);
			for (int i = 0; i < _n; i++)
				_array.SetValue(LoadMem(_s, _t), i);
			return _array;
#else
            ArrayList _array = new ArrayList();
            for (int i = 0; i < _n; i++)
                _array.Add(LoadMem(_s, _t));
            return _array.ToArray(_t);
#endif
        }

        public static lu_byte LoadByte(LoadState _s) { return (lu_byte)LoadChar(_s); }

        public static object LoadVar(LoadState _s, Type _t) { return LoadMem(_s, _t); }

        public static object LoadVector(LoadState _s, Type _t, int _n) { return LoadMem(_s, _t, _n); }

        private static void LoadBlock(LoadState _s, CharPtr _b, int _size)
        {
            uint _r = LuaZRead(_s.Z, _b, (uint)_size);
            IF(_r != 0, "unexpected end");
        }

        private static int LoadChar(LoadState _s)
        {
            return (char)LoadVar(_s, typeof(char));
        }

        private static int LoadInt(LoadState _s)
        {
            int _x = (int)LoadVar(_s, typeof(int));
            IF(_x < 0, "bad integer");
            return _x;
        }

        private static lua_Number LoadNumber(LoadState _s)
        {
            return (lua_Number)LoadVar(_s, typeof(lua_Number));
        }

        private static TString LoadString(LoadState _s)
        {
            uint _size = (uint)LoadVar(_s, typeof(uint));
            if (_size == 0)
                return null;
            else
            {
                CharPtr s = LuaZOpenSpace(_s.l, _s.b, _size);
                LoadBlock(_s, s, (int)_size);
                return LuaSNewLStr(_s.l, s, _size - 1);
            }
        }

        private static void LoadCode(LoadState _s, Proto _f)
        {
            int _n = LoadInt(_s);
            _f.code = LuaMNewVector<Instruction>(_s.l, _n);
            _f.sizeCode = _n;
            _f.code = (Instruction[])LoadVector(_s, typeof(Instruction), _n);
        }

        private static void LoadConstants(LoadState _s, Proto _f)
        {
            int _i, _n;
            _n = LoadInt(_s);
            _f.k = LuaMNewVector<TValue>(_s.l, _n);
            _f.sizeK = _n;
            for (_i = 0; _i < _n; _i++) SetNilValue(_f.k[_i]);
            for (_i = 0; _i < _n; _i++)
            {
                TValue _o = _f.k[_i];
                int _t = LoadChar(_s);
                switch (_t)
                {
                    case LUA_TNIL:
                        SetNilValue(_o);
                        break;
                    case LUA_TBOOLEAN:
                        SetBValue(_o, LoadChar(_s));
                        break;
                    case LUA_TNUMBER:
                        SetNValue(_o, LoadNumber(_s));
                        break;
                    case LUA_TSTRING:
                        SetSValue2N(_s.l, _o, LoadString(_s));
                        break;
                    default:
                        Error(_s, "bad constant");
                        break;
                }
            }
            _n = LoadInt(_s);
            _f.p = LuaMNewVector<Proto>(_s.l, _n);
            _f.sizeP = _n;
            for (_i = 0; _i < _n; _i++) _f.p[_i] = null;
            for (_i = 0; _i < _n; _i++) _f.p[_i] = LoadFunction(_s, _f.source);
        }

        private static void LoadDebug(LoadState _s, Proto _f)
        {
            int _i, _n;
            _n = LoadInt(_s);
            _f.lineInfo = LuaMNewVector<int>(_s.l, _n);
            _f.sizeLineInfo = _n;
            _f.lineInfo = (int[])LoadVector(_s, typeof(int), _n);
            _n = LoadInt(_s);
            _f.locVars = LuaMNewVector<LocVar>(_s.l, _n);
            _f.sizeLocVars = _n;
            for (_i = 0; _i < _n; _i++) _f.locVars[_i].varname = null;
            for (_i = 0; _i < _n; _i++)
            {
                _f.locVars[_i].varname = LoadString(_s);
                _f.locVars[_i].startpc = LoadInt(_s);
                _f.locVars[_i].endpc = LoadInt(_s);
            }
            _n = LoadInt(_s);
            _f.upValues = LuaMNewVector<TString>(_s.l, _n);
            _f.sizeUpValues = _n;
            for (_i = 0; _i < _n; _i++) _f.upValues[_i] = null;
            for (_i = 0; _i < _n; _i++) _f.upValues[_i] = LoadString(_s);
        }

        private static Proto LoadFunction(LoadState _s, TString _p)
        {
            Proto _f;
            if (++_s.l.nCcalls > LUAI_MAXCCALLS) Error(_s, "code too deep");
            _f = LuaFNewProto(_s.l);
            SetPTValue2S(_s.l, _s.l.top, _f); IncrTop(_s.l);
            _f.source = LoadString(_s); if (_f.source == null) _f.source = _p;
            _f.lineDefined = LoadInt(_s);
            _f.lastLineDefined = LoadInt(_s);
            _f.nups = LoadByte(_s);
            _f.numParams = LoadByte(_s);
            _f.isVarArg = LoadByte(_s);
            _f.maxStackSize = LoadByte(_s);
            LoadCode(_s, _f);
            LoadConstants(_s, _f);
            LoadDebug(_s, _f);
            IF(LuaGCheckCode(_f) == 0 ? 1 : 0, "bad code");
            StkId.Dec(ref _s.l.top);
            _s.l.nCcalls--;
            return _f;
        }

        private static void LoadHeader(LoadState _s)
        {
            CharPtr _h = new char[LUAC_HEADERSIZE];
            CharPtr __s = new char[LUAC_HEADERSIZE];
            LuaUHeader(_h);
            LoadBlock(_s, __s, LUAC_HEADERSIZE);
            IF(MemCmp(_h, __s, LUAC_HEADERSIZE) != 0, "bad header");
        }

        public static Proto LuaUUndump(LuaState _l, ZIO _z, Mbuffer _buff, CharPtr _name)
        {
            LoadState _s = new LoadState();
            if (_name[0] == '@' || _name[0] == '=')
                _s.name = _name + 1;
            else if (_name[0] == LUA_SIGNATURE[0])
                _s.name = "binary string";
            else
                _s.name = _name;
            _s.l = _l;
            _s.Z = _z;
            _s.b = _buff;
            LoadHeader(_s);
            return LoadFunction(_s, LuaSNewLiteral(_l, "=?"));
        }

        public static void LuaUHeader(CharPtr _h)
        {
            _h = new CharPtr(_h);
            int _x = 1;
            MemCpy(_h, LUA_SIGNATURE, LUA_SIGNATURE.Length);
            _h = _h.add(LUA_SIGNATURE.Length);
            _h[0] = (char)LUAC_VERSION;
            _h.inc();
            _h[0] = (char)LUAC_FORMAT;
            _h.inc();
            _h[0] = (char)_x;
            _h.inc();
            _h[0] = (char)sizeof(int);
            _h.inc();
            _h[0] = (char)sizeof(uint);
            _h.inc();
            _h[0] = (char)sizeof(Instruction);
            _h.inc();
            _h[0] = (char)sizeof(lua_Number);
            _h.inc();
            _h[0] = (char)0;
        }
    }
}