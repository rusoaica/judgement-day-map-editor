﻿using System;
using System.Diagnostics;

namespace KopiLua
{
    using Instruction = System.UInt32;
    using lu_byte = System.Byte;
    using lua_Number = System.Double;
    using ptrdiff_t = System.Int32;
    using StkId = Lua.LuaTypeValue;
    using TValue = Lua.LuaTypeValue;

    public partial class Lua
    {
        static CharPtr mybuff = null;
        public const int MAXTAGLOOP = 100;

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static int ToString(LuaState _l, StkId _o)
        {
            return ((TType(_o) == LUA_TSTRING) || (LuaVToString(_l, _o) != 0)) ? 1 : 0;
        }

        public static int ToNumber(ref StkId _o, TValue _n)
        {
            return ((TType(_o) == LUA_TNUMBER || (((_o) = LuaVToNumber(_o, _n)) != null))) ? 1 : 0;
        }

        public static int EqualObj(LuaState _l, TValue _o_1, TValue _o_2)
        {
            return ((TType(_o_1) == TType(_o_2)) && (LuaVEqualVal(_l, _o_1, _o_2) != 0)) ? 1 : 0;
        }

        public static TValue LuaVToNumber(TValue _obj, TValue _n)
        {
            lua_Number _num;
            if (TTIsNumber(_obj)) return _obj;
            if (TTIsString(_obj) && (LuaOStr2d(SValue(_obj), out _num) != 0))
            {
                SetNValue(_n, _num);
                return _n;
            }
            else
                return null;
        }

        public static int LuaVToString(LuaState _l, StkId _obj)
        {
            if (!TTIsNumber(_obj))
                return 0;
            else
            {
                lua_Number _n = NValue(_obj);
                CharPtr _s = LuaNumber2Str(_n);
                SetSValue2S(_l, _obj, LuaSNew(_l, _s));
                return 1;
            }
        }

        private static void TraceExec(LuaState _l, InstructionPtr _pc)
        {
            lu_byte _mask = _l.hookMask;
            InstructionPtr _old_pc = InstructionPtr.Assign(_l.saveDpc);
            _l.saveDpc = InstructionPtr.Assign(_pc);
            if (((_mask & LUA_MASKCOUNT) != 0) && (_l.hookCount == 0))
            {
                ResetHookCount(_l);
                LuaDCallHook(_l, LUA_HOOKCOUNT, -1);
            }
            if ((_mask & LUA_MASKLINE) != 0)
            {
                Proto _p = CIFunc(_l.ci).l.p;
                int _npc = PCRel(_pc, _p);
                int _new_line = GetLine(_p, _npc);
                if (_npc == 0 || _pc <= _old_pc || _new_line != GetLine(_p, PCRel(_old_pc, _p)))
                    LuaDCallHook(_l, LUA_HOOKLINE, _new_line);
            }
        }

        private static void CallTMRes(LuaState _l, StkId _res, TValue _f, TValue _p_1, TValue _p_2)
        {
            ptrdiff_t _result = SaveStack(_l, _res);
            SetObj2S(_l, _l.top, _f);
            SetObj2S(_l, _l.top + 1, _p_1);
            SetObj2S(_l, _l.top + 2, _p_2);
            LuaDCheckStack(_l, 3);
            _l.top += 3;
            LuaDCall(_l, _l.top - 3, 1);
            _res = RestoreStack(_l, _result);
            StkId.Dec(ref _l.top);
            SetObj2S(_l, _res, _l.top);
        }

        private static void CallTM(LuaState _l, TValue _f, TValue _p_1, TValue _p_2, TValue _p_3)
        {
            SetObj2S(_l, _l.top, _f);
            SetObj2S(_l, _l.top + 1, _p_1);
            SetObj2S(_l, _l.top + 2, _p_2);
            SetObj2S(_l, _l.top + 3, _p_3);
            LuaDCheckStack(_l, 4);
            _l.top += 4;
            LuaDCall(_l, _l.top - 4, 0);
        }

        public static void LuaVGetTable(LuaState _l, TValue _t, TValue _key, StkId _val)
        {
            int _loop;
            for (_loop = 0; _loop < MAXTAGLOOP; _loop++)
            {
                TValue _tm;
                if (TTIsTable(_t))
                {
                    Table _h = HValue(_t);
                    TValue _res = LuaHGet(_h, _key);
                    if (!TTIsNil(_res) || (_tm = FastTm(_l, _h.metatable, TMS.TM_INDEX)) == null)
                    {
                        SetObj2S(_l, _val, _res);
                        return;
                    }
                }
                else if (TTIsNil(_tm = LuaTGetTmByObj(_l, _t, TMS.TM_INDEX)))
                    LuaGTypeError(_l, _t, "index");
                if (TTIsFunction(_tm))
                {
                    CallTMRes(_l, _val, _tm, _t, _key);
                    return;
                }
                _t = _tm;
            }
            LuaGRunError(_l, "loop in gettable");
        }

        public static void LuaVSetTable(LuaState _l, TValue _t, TValue _key, StkId _val)
        {
            int _loop;
            TValue _temp = new LuaTypeValue();
            for (_loop = 0; _loop < MAXTAGLOOP; _loop++)
            {
                TValue _tm;
                if (TTIsTable(_t))
                {
                    Table _h = HValue(_t);
                    TValue _old_val = LuaHSet(_l, _h, _key);
                    if (!TTIsNil(_old_val) ||
                        (_tm = FastTm(_l, _h.metatable, TMS.TM_NEWINDEX)) == null)
                    {
                        SetObj2T(_l, _old_val, _val);
                        _h.flags = 0;
                        LuaCBarrierT(_l, _h, _val);
                        return;
                    }
                }
                else if (TTIsNil(_tm = LuaTGetTmByObj(_l, _t, TMS.TM_NEWINDEX)))
                    LuaGTypeError(_l, _t, "index");
                if (TTIsFunction(_tm))
                {
                    CallTM(_l, _tm, _t, _key, _val);
                    return;
                }
                SetObj(_l, _temp, _tm);
                _t = _temp;
            }
            LuaGRunError(_l, "loop in settable");
        }

        private static int CallBinTM(LuaState _l, TValue _p_1, TValue _p_2, StkId _res, TMS _event)
        {
            TValue _tm = LuaTGetTmByObj(_l, _p_1, _event);
            if (TTIsNil(_tm))
                _tm = LuaTGetTmByObj(_l, _p_2, _event);
            if (TTIsNil(_tm)) return 0;
            CallTMRes(_l, _res, _tm, _p_1, _p_2);
            return 1;
        }

        private static TValue GetCompTM(LuaState _l, Table _mt_1, Table _mt_2, TMS _event)
        {
            TValue _tm_1 = FastTm(_l, _mt_1, _event);
            TValue _tm_2;
            if (_tm_1 == null) return null;
            if (_mt_1 == _mt_2) return _tm_1;
            _tm_2 = FastTm(_l, _mt_2, _event);
            if (_tm_2 == null) return null;
            if (LuaORawEqualObj(_tm_1, _tm_2) != 0)
                return _tm_1;
            return null;
        }

        private static int CallOrderTM(LuaState _l, TValue _p_1, TValue _p_2, TMS _event)
        {
            TValue _tm_1 = LuaTGetTmByObj(_l, _p_1, _event);
            TValue _tm_2;
            if (TTIsNil(_tm_1)) return -1;
            _tm_2 = LuaTGetTmByObj(_l, _p_2, _event);
            if (LuaORawEqualObj(_tm_1, _tm_2) == 0)
                return -1;
            CallTMRes(_l, _l.top, _tm_1, _p_1, _p_2);
            return LIsFalse(_l.top) == 0 ? 1 : 0;
        }

        private static int LStrCmp(TString _ls, TString _rs)
        {
            CharPtr _l = GetStr(_ls);
            uint _ll = _ls.tsv.len;
            CharPtr _r = GetStr(_rs);
            uint _lr = _rs.tsv.len;
            for (;;)
            {
                int _temp = String.Compare(_l.ToString(), _r.ToString());
                if (_temp != 0) return _temp;
                else
                {
                    uint len = (uint)_l.ToString().Length;
                    if (len == _lr)
                        return (len == _ll) ? 0 : 1;
                    else if (len == _ll)
                        return -1;
                    len++;
                    _l += len; _ll -= len; _r += len; _lr -= len;
                }
            }
        }

        public static int LuaVLessThan(LuaState _l, TValue __l, TValue _r)
        {
            int _res;
            if (TType(__l) != TType(_r))
                return LuaGOrderError(_l, __l, _r);
            else if (TTIsNumber(__l))
                return LuaINumLt(NValue(__l), NValue(_r)) ? 1 : 0;
            else if (TTIsString(__l))
                return (LStrCmp(RawTSValue(__l), RawTSValue(_r)) < 0) ? 1 : 0;
            else if ((_res = CallOrderTM(_l, __l, _r, TMS.TM_LT)) != -1)
                return _res;
            return LuaGOrderError(_l, __l, _r);
        }

        private static int LessEqual(LuaState _l, TValue __l, TValue _r)
        {
            int _res;
            if (TType(__l) != TType(_r))
                return LuaGOrderError(_l, __l, _r);
            else if (TTIsNumber(__l))
                return LuaINumLe(NValue(__l), NValue(_r)) ? 1 : 0;
            else if (TTIsString(__l))
                return (LStrCmp(RawTSValue(__l), RawTSValue(_r)) <= 0) ? 1 : 0;
            else if ((_res = CallOrderTM(_l, __l, _r, TMS.TM_LE)) != -1)
                return _res;
            else if ((_res = CallOrderTM(_l, _r, __l, TMS.TM_LT)) != -1)
                return (_res == 0) ? 1 : 0;
            return LuaGOrderError(_l, __l, _r);
        }

        public static int LuaVEqualVal(LuaState _l, TValue _t_1, TValue _t_2)
        {
            TValue _tm = null;
            LuaAssert(TType(_t_1) == TType(_t_2));
            switch (TType(_t_1))
            {
                case LUA_TNIL: return 1;
                case LUA_TNUMBER: return LuaINumEq(NValue(_t_1), NValue(_t_2)) ? 1 : 0;
                case LUA_TBOOLEAN: return (BValue(_t_1) == BValue(_t_2)) ? 1 : 0;
                case LUA_TLIGHTUSERDATA: return (PValue(_t_1) == PValue(_t_2)) ? 1 : 0;
                case LUA_TUSERDATA:
                    {
                        if (UValue(_t_1) == UValue(_t_2)) return 1;
                        _tm = GetCompTM(_l, UValue(_t_1).metatable, UValue(_t_2).metatable, TMS.TM_EQ);
                        break;
                    }
                case LUA_TTABLE:
                    {
                        if (HValue(_t_1) == HValue(_t_2)) return 1;
                        _tm = GetCompTM(_l, HValue(_t_1).metatable, HValue(_t_2).metatable, TMS.TM_EQ);
                        break;
                    }
                default: return (GCValue(_t_1) == GCValue(_t_2)) ? 1 : 0;
            }
            if (_tm == null) return 0;
            CallTMRes(_l, _l.top, _tm, _t_1, _t_2);
            return LIsFalse(_l.top) == 0 ? 1 : 0;
        }

        public static void LuaVConcat(LuaState _l, int _total, int _last)
        {
            do
            {
                StkId _top = _l.base_ + _last + 1;
                int _n = 2;
                if (!(TTIsString(_top - 2) || TTIsNumber(_top - 2)) || (ToString(_l, _top - 1) == 0))
                {
                    if (CallBinTM(_l, _top - 2, _top - 1, _top - 2, TMS.TM_CONCAT) == 0)
                        LuaGConcatError(_l, _top - 2, _top - 1);
                }
                else if (TSValue(_top - 1).len == 0)
                    ToString(_l, _top - 2);
                else
                {
                    uint _tl = TSValue(_top - 1).len;
                    CharPtr _buffer;
                    int _i;
                    for (_n = 1; _n < _total && (ToString(_l, _top - _n - 1) != 0); _n++)
                    {
                        uint __l = TSValue(_top - _n - 1).len;
                        if (__l >= MAXSIZET - _tl) LuaGRunError(_l, "string length overflow");
                        _tl += __l;
                    }
                    _buffer = LuaZOpenSpace(_l, G(_l).buff, _tl);
                    if (mybuff == null)
                        mybuff = _buffer;
                    _tl = 0;
                    for (_i = _n; _i > 0; _i--)
                    {
                        uint __l = TSValue(_top - _i).len;
                        MemCpy(_buffer.chars, (int)_tl, SValue(_top - _i).chars, (int)__l);
                        _tl += __l;
                    }
                    SetSValue2S(_l, _top - _n, LuaSNewLStr(_l, _buffer, _tl));
                }
                _total -= _n - 1;
                _last -= _n - 1;
            } while (_total > 1);
        }


        public static void Arith(LuaState _l, StkId _ra, TValue _rb, TValue _rc, TMS _op)
        {
            TValue _temp_b = new LuaTypeValue(), _temp_c = new LuaTypeValue();
            TValue _b, _c;
            if ((_b = LuaVToNumber(_rb, _temp_b)) != null && (_c = LuaVToNumber(_rc, _temp_c)) != null)
            {
                lua_Number _nb = NValue(_b), nc = NValue(_c);
                switch (_op)
                {
                    case TMS.TM_ADD: SetNValue(_ra, LuaINumAdd(_nb, nc)); break;
                    case TMS.TM_SUB: SetNValue(_ra, LuaINumSub(_nb, nc)); break;
                    case TMS.TM_MUL: SetNValue(_ra, LuaINumMul(_nb, nc)); break;
                    case TMS.TM_DIV: SetNValue(_ra, LuaINumDiv(_nb, nc)); break;
                    case TMS.TM_MOD: SetNValue(_ra, LuaINumMod(_nb, nc)); break;
                    case TMS.TM_POW: SetNValue(_ra, LuaINumPow(_nb, nc)); break;
                    case TMS.TM_UNM: SetNValue(_ra, LuaINumUnm(_nb)); break;
                    default: LuaAssert(false); break;
                }
            }
            else if (CallBinTM(_l, _rb, _rc, _ra, _op) == 0)
                LuaGArithError(_l, _rb, _rc);
        }

        public static void RuntimeCheck(LuaState _l, bool _c) { Debug.Assert(_c); }

        internal static TValue RA(LuaState _l, StkId _base, Instruction _i) { return _base + GETARG_A(_i); }

        internal static TValue RB(LuaState _l, StkId _base, Instruction _i) { return _base + GETARG_B(_i); }

        internal static TValue RC(LuaState _l, StkId _base, Instruction _i) { return _base + GETARG_C(_i); }

        internal static TValue RKB(LuaState _l, StkId _base, Instruction _i, TValue[] _k) { return ISK(GETARG_B(_i)) != 0 ? _k[INDEXK(GETARG_B(_i))] : _base + GETARG_B(_i); }

        internal static TValue RKC(LuaState _l, StkId _base, Instruction _i, TValue[] _k) { return ISK(GETARG_C(_i)) != 0 ? _k[INDEXK(GETARG_C(_i))] : _base + GETARG_C(_i); }

        internal static TValue KBx(LuaState _l, Instruction _i, TValue[] _k) { return _k[GETARG_Bx(_i)]; }

        public static void dojump(LuaState L, InstructionPtr pc, int i) { pc.pc += i; LuaIThreadYield(L); }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static void ArithOp(LuaState _l, OpDelegate _op, TMS _tm, StkId _base, Instruction _i, TValue[] _k, StkId _ra, InstructionPtr _pc)
        {
            TValue _rb = RKB(_l, _base, _i, _k);
            TValue _rc = RKC(_l, _base, _i, _k);
            if (TTIsNumber(_rb) && TTIsNumber(_rc))
            {
                lua_Number _nb = NValue(_rb), nc = NValue(_rc);
                SetNValue(_ra, _op(_nb, nc));
            }
            else
            {
                _l.saveDpc = InstructionPtr.Assign(_pc);
                Arith(_l, _ra, _rb, _rc, _tm);
                _base = _l.base_;
            }
        }

        internal static void Dump(int _pc, Instruction _i)
        {
            int _a = GETARG_A(_i);
            int _b = GETARG_B(_i);
            int _c = GETARG_C(_i);
            int _bx = GETARG_Bx(_i);
            int _sbx = GETARG_sBx(_i);
            if ((_sbx & 0x100) != 0)
                _sbx = -(_sbx & 0xff);
            Console.Write("{0,5} ({1,10}): ", _pc, _i);
            Console.Write("{0,-10}\t", _luaPOpNames[(int)GET_OPCODE(_i)]);
            switch (GET_OPCODE(_i))
            {
                case OpCode.OP_CLOSE:
                    Console.Write("{0}", _a);
                    break;
                case OpCode.OP_MOVE:
                case OpCode.OP_LOADNIL:
                case OpCode.OP_GETUPVAL:
                case OpCode.OP_SETUPVAL:
                case OpCode.OP_UNM:
                case OpCode.OP_NOT:
                case OpCode.OP_RETURN:
                    Console.Write("{0}, {1}", _a, _b);
                    break;
                case OpCode.OP_LOADBOOL:
                case OpCode.OP_GETTABLE:
                case OpCode.OP_SETTABLE:
                case OpCode.OP_NEWTABLE:
                case OpCode.OP_SELF:
                case OpCode.OP_ADD:
                case OpCode.OP_SUB:
                case OpCode.OP_MUL:
                case OpCode.OP_DIV:
                case OpCode.OP_POW:
                case OpCode.OP_CONCAT:
                case OpCode.OP_EQ:
                case OpCode.OP_LT:
                case OpCode.OP_LE:
                case OpCode.OP_TEST:
                case OpCode.OP_CALL:
                case OpCode.OP_TAILCALL:
                    Console.Write("{0}, {1}, {2}", _a, _b, _c);
                    break;

                case OpCode.OP_LOADK:
                    Console.Write("{0}, {1}", _a, _bx);
                    break;

                case OpCode.OP_GETGLOBAL:
                case OpCode.OP_SETGLOBAL:
                case OpCode.OP_SETLIST:
                case OpCode.OP_CLOSURE:
                    Console.Write("{0}, {1}", _a, _bx);
                    break;

                case OpCode.OP_TFORLOOP:
                    Console.Write("{0}, {1}", _a, _c);
                    break;

                case OpCode.OP_JMP:
                case OpCode.OP_FORLOOP:
                case OpCode.OP_FORPREP:
                    Console.Write("{0}, {1}", _a, _sbx);
                    break;
            }
            Console.WriteLine();
        }

        public static void LuaVExecute(LuaState _l, int _n_exec_calls)
        {
            LClosure _cl;
            StkId _base;
            TValue[] _k;
            InstructionPtr _pc;
            reentry:
            LuaAssert(IsLua(_l.ci));
            _pc = InstructionPtr.Assign(_l.saveDpc);
            _cl = CLValue(_l.ci.func).l;
            _base = _l.base_;
            _k = _cl.p.k;
            for (;;)
            {
                Instruction _i = InstructionPtr.inc(ref _pc)[0];
                StkId _ra;
                if (((_l.hookMask & (LUA_MASKLINE | LUA_MASKCOUNT)) != 0) && (((--_l.hookCount) == 0) || ((_l.hookMask & LUA_MASKLINE) != 0)))
                {
                    TraceExec(_l, _pc);
                    if (_l.status == LUA_YIELD)
                    {
                        _l.saveDpc = new InstructionPtr(_pc.codes, _pc.pc - 1);
                        return;
                    }
                    _base = _l.base_;
                }
                _ra = RA(_l, _base, _i);
                LuaAssert(_base == _l.base_ && _l.base_ == _l.ci.base_);
                LuaAssert(_base <= _l.top && ((_l.top - _l.stack) <= _l.stackSize));
                LuaAssert(_l.top == _l.ci.top || (LuaGCheckOpenOp(_i) != 0));
                switch (GET_OPCODE(_i))
                {
                    case OpCode.OP_MOVE:
                        {
                            SetObj2S(_l, _ra, RB(_l, _base, _i));
                            continue;
                        }
                    case OpCode.OP_LOADK:
                        {
                            SetObj2S(_l, _ra, KBx(_l, _i, _k));
                            continue;
                        }
                    case OpCode.OP_LOADBOOL:
                        {
                            SetBValue(_ra, GETARG_B(_i));
                            if (GETARG_C(_i) != 0) InstructionPtr.inc(ref _pc);
                            continue;
                        }
                    case OpCode.OP_LOADNIL:
                        {
                            TValue rb = RB(_l, _base, _i);
                            do
                            {
                                SetNilValue(StkId.Dec(ref rb));
                            } while (rb >= _ra);
                            continue;
                        }
                    case OpCode.OP_GETUPVAL:
                        {
                            int _b = GETARG_B(_i);
                            SetObj2S(_l, _ra, _cl.upvals[_b].v);
                            continue;
                        }
                    case OpCode.OP_GETGLOBAL:
                        {
                            TValue _g = new LuaTypeValue();
                            TValue _rb = KBx(_l, _i, _k);
                            SetHValue(_l, _g, _cl.env);
                            LuaAssert(TTIsString(_rb));
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaVGetTable(_l, _g, _rb, _ra);
                            _base = _l.base_;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            continue;
                        }
                    case OpCode.OP_GETTABLE:
                        {
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaVGetTable(_l, RB(_l, _base, _i), RKC(_l, _base, _i, _k), _ra);
                            _base = _l.base_;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            continue;
                        }
                    case OpCode.OP_SETGLOBAL:
                        {
                            TValue _g = new LuaTypeValue();
                            SetHValue(_l, _g, _cl.env);
                            LuaAssert(TTIsString(KBx(_l, _i, _k)));
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaVSetTable(_l, _g, KBx(_l, _i, _k), _ra);
                            _base = _l.base_;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            continue;
                        }
                    case OpCode.OP_SETUPVAL:
                        {
                            UpVal _uv = _cl.upvals[GETARG_B(_i)];
                            SetObj(_l, _uv.v, _ra);
                            LuaCBarrier(_l, _uv, _ra);
                            continue;
                        }
                    case OpCode.OP_SETTABLE:
                        {
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaVSetTable(_l, _ra, RKB(_l, _base, _i, _k), RKC(_l, _base, _i, _k));
                            _base = _l.base_;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            continue;
                        }
                    case OpCode.OP_NEWTABLE:
                        {
                            int _b = GETARG_B(_i);
                            int _c = GETARG_C(_i);
                            SetHValue(_l, _ra, LuaHNew(_l, LuaOFBInt(_b), LuaOFBInt(_c)));
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaCCheckGC(_l);
                            _base = _l.base_;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            continue;
                        }
                    case OpCode.OP_SELF:
                        {
                            StkId _rb = RB(_l, _base, _i);
                            SetObj2S(_l, _ra + 1, _rb);
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaVGetTable(_l, _rb, RKC(_l, _base, _i, _k), _ra);
                            _base = _l.base_;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            continue;
                        }
                    case OpCode.OP_ADD:
                        {
                            ArithOp(_l, LuaINumAdd, TMS.TM_ADD, _base, _i, _k, _ra, _pc);
                            continue;
                        }
                    case OpCode.OP_SUB:
                        {
                            ArithOp(_l, LuaINumSub, TMS.TM_SUB, _base, _i, _k, _ra, _pc);
                            continue;
                        }
                    case OpCode.OP_MUL:
                        {
                            ArithOp(_l, LuaINumMul, TMS.TM_MUL, _base, _i, _k, _ra, _pc);
                            continue;
                        }
                    case OpCode.OP_DIV:
                        {
                            ArithOp(_l, LuaINumDiv, TMS.TM_DIV, _base, _i, _k, _ra, _pc);
                            continue;
                        }
                    case OpCode.OP_MOD:
                        {
                            ArithOp(_l, LuaINumMod, TMS.TM_MOD, _base, _i, _k, _ra, _pc);
                            continue;
                        }
                    case OpCode.OP_POW:
                        {
                            ArithOp(_l, LuaINumPow, TMS.TM_POW, _base, _i, _k, _ra, _pc);
                            continue;
                        }
                    case OpCode.OP_UNM:
                        {
                            TValue _rb = RB(_l, _base, _i);
                            if (TTIsNumber(_rb))
                            {
                                lua_Number _nb = NValue(_rb);
                                SetNValue(_ra, LuaINumUnm(_nb));
                            }
                            else
                            {
                                _l.saveDpc = InstructionPtr.Assign(_pc);
                                Arith(_l, _ra, _rb, _rb, TMS.TM_UNM);
                                _base = _l.base_;
                                _l.saveDpc = InstructionPtr.Assign(_pc);
                            }
                            continue;
                        }
                    case OpCode.OP_NOT:
                        {
                            int _res = LIsFalse(RB(_l, _base, _i)) == 0 ? 0 : 1;
                            SetBValue(_ra, _res);
                            continue;
                        }
                    case OpCode.OP_LEN:
                        {
                            TValue _rb = RB(_l, _base, _i);
                            switch (TType(_rb))
                            {
                                case LUA_TTABLE:
                                    {
                                        SetNValue(_ra, (lua_Number)LuaHGetN(HValue(_rb)));
                                        break;
                                    }
                                case LUA_TSTRING:
                                    {
                                        SetNValue(_ra, (lua_Number)TSValue(_rb).len);
                                        break;
                                    }
                                default:
                                    {
                                        _l.saveDpc = InstructionPtr.Assign(_pc);
                                        if (CallBinTM(_l, _rb, LuaONilObject, _ra, TMS.TM_LEN) == 0)
                                            LuaGTypeError(_l, _rb, "get length of");
                                        _base = _l.base_;
                                        break;
                                    }
                            }
                            continue;
                        }
                    case OpCode.OP_CONCAT:
                        {
                            int _b = GETARG_B(_i);
                            int _c = GETARG_C(_i);
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaVConcat(_l, _c - _b + 1, _c); LuaCCheckGC(_l);
                            _base = _l.base_;
                            SetObj2S(_l, RA(_l, _base, _i), _base + _b);
                            continue;
                        }
                    case OpCode.OP_JMP:
                        {
                            dojump(_l, _pc, GETARG_sBx(_i));
                            continue;
                        }
                    case OpCode.OP_EQ:
                        {
                            TValue _rb = RKB(_l, _base, _i, _k);
                            TValue _rc = RKC(_l, _base, _i, _k);
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            if (EqualObj(_l, _rb, _rc) == GETARG_A(_i))
                                dojump(_l, _pc, GETARG_sBx(_pc[0]));
                            _base = _l.base_;
                            InstructionPtr.inc(ref _pc);
                            continue;
                        }
                    case OpCode.OP_LT:
                        {
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            if (LuaVLessThan(_l, RKB(_l, _base, _i, _k), RKC(_l, _base, _i, _k)) == GETARG_A(_i))
                                dojump(_l, _pc, GETARG_sBx(_pc[0]));
                            _base = _l.base_;
                            InstructionPtr.inc(ref _pc);
                            continue;
                        }
                    case OpCode.OP_LE:
                        {
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            if (LessEqual(_l, RKB(_l, _base, _i, _k), RKC(_l, _base, _i, _k)) == GETARG_A(_i))
                                dojump(_l, _pc, GETARG_sBx(_pc[0]));
                            _base = _l.base_;
                            InstructionPtr.inc(ref _pc);
                            continue;
                        }
                    case OpCode.OP_TEST:
                        {
                            if (LIsFalse(_ra) != GETARG_C(_i))
                                dojump(_l, _pc, GETARG_sBx(_pc[0]));
                            InstructionPtr.inc(ref _pc);
                            continue;
                        }
                    case OpCode.OP_TESTSET:
                        {
                            TValue _rb = RB(_l, _base, _i);
                            if (LIsFalse(_rb) != GETARG_C(_i))
                            {
                                SetObj2S(_l, _ra, _rb);
                                dojump(_l, _pc, GETARG_sBx(_pc[0]));
                            }
                            InstructionPtr.inc(ref _pc);
                            continue;
                        }
                    case OpCode.OP_CALL:
                        {
                            int _b = GETARG_B(_i);
                            int _n_results = GETARG_C(_i) - 1;
                            if (_b != 0) _l.top = _ra + _b;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            switch (LuaDPreCall(_l, _ra, _n_results))
                            {
                                case PCRLUA:
                                    {
                                        _n_exec_calls++;
                                        goto reentry;
                                    }
                                case PCRC:
                                    {
                                        if (_n_results >= 0) _l.top = _l.ci.top;
                                        _base = _l.base_;
                                        continue;
                                    }
                                default:
                                    {
                                        return;
                                    }
                            }
                        }
                    case OpCode.OP_TAILCALL:
                        {
                            int _b = GETARG_B(_i);
                            if (_b != 0) _l.top = _ra + _b;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaAssert(GETARG_C(_i) - 1 == LUA_MULTRET);
                            switch (LuaDPreCall(_l, _ra, LUA_MULTRET))
                            {
                                case PCRLUA:
                                    {
                                        CallInfo _ci = _l.ci - 1;
                                        int _aux;
                                        StkId _func = _ci.func;
                                        StkId _p_func = (_ci + 1).func;
                                        if (_l.openUpVal != null) LuaFClose(_l, _ci.base_);
                                        _l.base_ = _ci.base_ = _ci.func + (_ci[1].base_ - _p_func);
                                        for (_aux = 0; _p_func + _aux < _l.top; _aux++)
                                            SetObj2S(_l, _func + _aux, _p_func + _aux);
                                        _ci.top = _l.top = _func + _aux;
                                        LuaAssert(_l.top == _l.base_ + CLValue(_func).l.p.maxStackSize);
                                        _ci.saveDpc = InstructionPtr.Assign(_l.saveDpc);
                                        _ci.tailCalls++;
                                        CallInfo.Dec(ref _l.ci);
                                        goto reentry;
                                    }
                                case PCRC:
                                    {
                                        _base = _l.base_;
                                        continue;
                                    }
                                default:
                                    {
                                        return;
                                    }
                            }
                        }
                    case OpCode.OP_RETURN:
                        {
                            int _b = GETARG_B(_i);
                            if (_b != 0) _l.top = _ra + _b - 1;
                            if (_l.openUpVal != null) LuaFClose(_l, _base);
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            _b = LuaDPosCall(_l, _ra);
                            if (--_n_exec_calls == 0)
                                return;
                            else
                            {
                                if (_b != 0) _l.top = _l.ci.top;
                                LuaAssert(IsLua(_l.ci));
                                LuaAssert(GET_OPCODE(_l.ci.saveDpc[-1]) == OpCode.OP_CALL);
                                goto reentry;
                            }
                        }
                    case OpCode.OP_FORLOOP:
                        {
                            lua_Number _step = NValue(_ra + 2);
                            lua_Number _idx = LuaINumAdd(NValue(_ra), _step);
                            lua_Number _limit = NValue(_ra + 1);
                            if (LuaINumLt(0, _step) ? LuaINumLe(_idx, _limit) : LuaINumLe(_limit, _idx))
                            {
                                dojump(_l, _pc, GETARG_sBx(_i));
                                SetNValue(_ra, _idx);
                                SetNValue(_ra + 3, _idx);
                            }
                            continue;
                        }
                    case OpCode.OP_FORPREP:
                        {
                            TValue _init = _ra;
                            TValue _p_limit = _ra + 1;
                            TValue _p_step = _ra + 2; 
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            if (ToNumber(ref _init, _ra) == 0)
                                LuaGRunError(_l, LUA_QL("for") + " initial value must be a number");
                            else if (ToNumber(ref _p_limit, _ra + 1) == 0)
                                LuaGRunError(_l, LUA_QL("for") + " limit must be a number");
                            else if (ToNumber(ref _p_step, _ra + 2) == 0)
                                LuaGRunError(_l, LUA_QL("for") + " step must be a number");
                            SetNValue(_ra, LuaINumSub(NValue(_ra), NValue(_p_step)));
                            dojump(_l, _pc, GETARG_sBx(_i));
                            continue;
                        }
                    case OpCode.OP_TFORLOOP:
                        {
                            StkId _cb = _ra + 3;
                            SetObj2S(_l, _cb + 2, _ra + 2);
                            SetObj2S(_l, _cb + 1, _ra + 1);
                            SetObj2S(_l, _cb, _ra);
                            _l.top = _cb + 3;
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaDCall(_l, _cb, GETARG_C(_i));
                            _base = _l.base_;
                            _l.top = _l.ci.top;
                            _cb = RA(_l, _base, _i) + 3;
                            if (!TTIsNil(_cb))
                            {
                                SetObj2S(_l, _cb - 1, _cb);
                                dojump(_l, _pc, GETARG_sBx(_pc[0]));
                            }
                            InstructionPtr.inc(ref _pc);
                            continue;
                        }
                    case OpCode.OP_SETLIST:
                        {
                            int _n = GETARG_B(_i);
                            int _c = GETARG_C(_i);
                            int _last;
                            Table _h;
                            if (_n == 0)
                            {
                                _n = CastInt(_l.top - _ra) - 1;
                                _l.top = _l.ci.top;
                            }
                            if (_c == 0)
                            {
                                _c = CastInt(_pc[0]);
                                InstructionPtr.inc(ref _pc);
                            }
                            RuntimeCheck(_l, TTIsTable(_ra));
                            _h = HValue(_ra);
                            _last = ((_c - 1) * LFIELDS_PER_FLUSH) + _n;
                            if (_last > _h.sizeArray)
                                LuaHResizeArray(_l, _h, _last);
                            for (; _n > 0; _n--)
                            {
                                TValue _val = _ra + _n;
                                SetObj2T(_l, LuaHSetNum(_l, _h, _last--), _val);
                                LuaCBarrierT(_l, _h, _val);
                            }
                            continue;
                        }
                    case OpCode.OP_CLOSE:
                        {
                            LuaFClose(_l, _ra);
                            continue;
                        }
                    case OpCode.OP_CLOSURE:
                        {
                            Proto _p;
                            Closure _ncl;
                            int _nup, _j;
                            _p = _cl.p.p[GETARG_Bx(_i)];
                            _nup = _p.nups;
                            _ncl = LuaFNewLClosure(_l, _nup, _cl.env);
                            _ncl.l.p = _p;
                            for (_j = 0; _j < _nup; _j++, InstructionPtr.inc(ref _pc))
                            {
                                if (GET_OPCODE(_pc[0]) == OpCode.OP_GETUPVAL)
                                    _ncl.l.upvals[_j] = _cl.upvals[GETARG_B(_pc[0])];
                                else
                                {
                                    LuaAssert(GET_OPCODE(_pc[0]) == OpCode.OP_MOVE);
                                    _ncl.l.upvals[_j] = LuaFindUpVal(_l, _base + GETARG_B(_pc[0]));
                                }
                            }
                            SetCLValue(_l, _ra, _ncl);
                            _l.saveDpc = InstructionPtr.Assign(_pc);
                            LuaCCheckGC(_l);
                            _base = _l.base_;
                            continue;
                        }
                    case OpCode.OP_VARARG:
                        {
                            int _b = GETARG_B(_i) - 1;
                            int _j;
                            CallInfo _ci = _l.ci;
                            int _n = CastInt(_ci.base_ - _ci.func) - _cl.p.numParams - 1;
                            if (_b == LUA_MULTRET)
                            {
                                _l.saveDpc = InstructionPtr.Assign(_pc);
                                LuaDCheckStack(_l, _n);
                                _base = _l.base_;
                                _ra = RA(_l, _base, _i);
                                _b = _n;
                                _l.top = _ra + _n;
                            }
                            for (_j = 0; _j < _b; _j++)
                            {
                                if (_j < _n)
                                {
                                    SetObj2S(_l, _ra + _j, _ci.base_ - _n + _j);
                                }
                                else
                                {
                                    SetNilValue(_ra + _j);
                                }
                            }
                            continue;
                        }
                }
            }
        }
    }
}