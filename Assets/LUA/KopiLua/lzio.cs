﻿using System;

namespace KopiLua
{
    using ZIO = Lua.Zio;

    public partial class Lua
    {
        public const int EOZ = -1; 

        public static int char2int(char _c) { return (int)_c; }

        public static int zgetc(ZIO _z)
        {
            if (_z.n-- > 0)
            {
                int _ch = char2int(_z.p[0]);
                _z.p.inc();
                return _ch;
            }
            else
                return LuaZFill(_z);
        }

        public class Mbuffer
        {
            public CharPtr buffer = new CharPtr();
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public uint n;
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public uint buffSize;
        };

        public static void LuaZInitBuffer(LuaState _l, Mbuffer _buff)
        {
            _buff.buffer = null;
        }

        public static CharPtr LuaZBuffer(Mbuffer _buff) { return _buff.buffer; }
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static uint LuaZSizeBuffer(Mbuffer _buff) { return _buff.buffSize; }
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static uint LuaZBuffLen(Mbuffer _buff) { return _buff.n; }

        public static void LuaZResetBuffer(Mbuffer _buff) { _buff.n = 0; }

        public static void LuaZResizeBuffer(LuaState _l, Mbuffer _buff, int _size)
        {
            if (_buff.buffer == null)
                _buff.buffer = new CharPtr();
            LuaMReallocVector(_l, ref _buff.buffer.chars, (int)_buff.buffSize, _size);
            _buff.buffSize = (uint)_buff.buffer.chars.Length;
        }

        public static void LuaZFreeBuffer(LuaState _l, Mbuffer _buff) { LuaZResizeBuffer(_l, _buff, 0); }

        public class Zio
        {
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public uint n;          
            public CharPtr p; 
#if !UNITY_3D
            [CLSCompliantAttribute(false)]
#endif
            public LuaReader reader;
            public object data;        
            public LuaState l;        
            public int eoz;
        };

        public static int LuaZFill(ZIO _z)
        {
            uint _size;
            LuaState _l = _z.l;
            CharPtr _buff;
            if (_z.eoz != 0) return EOZ;
            LuaUnlock(_l);
            _buff = _z.reader(_l, _z.data, out _size);
            LuaLock(_l);
            if (_buff == null || _size == 0)
            {
                _z.eoz = 1; 
                return EOZ;
            }
            _z.n = _size - 1;
            _z.p = new CharPtr(_buff);
            int result = char2int(_z.p[0]);
            _z.p.inc();
            return result;
        }

        public static int LuaZLookAhead(ZIO _z)
        {
            if (_z.n == 0)
            {
                if (LuaZFill(_z) == EOZ)
                    return EOZ;
                else
                {
                    _z.n++;  
                    _z.p.dec();
                }
            }
            return char2int(_z.p[0]);
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static void LuaZInit(LuaState _l, ZIO _z, LuaReader _reader, object _data)
        {
            _z.l = _l;
            _z.reader = _reader;
            _z.data = _data;
            _z.n = 0;
            _z.p = null;
            _z.eoz = 0;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static uint LuaZRead(ZIO _z, CharPtr _b, uint _n)
        {
            _b = new CharPtr(_b);
            while (_n != 0)
            {
                uint _m;
                if (LuaZLookAhead(_z) == EOZ)
                    return _n;  
                _m = (_n <= _z.n) ? _n : _z.n;
                MemCpy(_b, _z.p, _m);
                _z.n -= _m;
                _z.p += _m;
                _b = _b + _m;
                _n -= _m;
            }
            return 0;
        }

#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        public static CharPtr LuaZOpenSpace(LuaState _l, Mbuffer _buff, uint _n)
        {
            if (_n > _buff.buffSize)
            {
                if (_n < LUAMINBUFFER) _n = LUAMINBUFFER;
                LuaZResizeBuffer(_l, _buff, (int)_n);
            }
            return _buff.buffer;
        }
    }
}