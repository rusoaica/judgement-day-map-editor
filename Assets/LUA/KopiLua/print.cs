﻿namespace KopiLua
{
    using Instruction = System.UInt32;
    using TValue = Lua.LuaTypeValue;

    public partial class Lua
    {

        public static void LuaUPrint(Proto _f, int _full) { PrintFunction(_f, _full); }

        public static void PrintString(TString _ts)
        {
            CharPtr _s = GetStr(_ts);
            uint _i, _n = _ts.tsv.len;
            PutChar('"');
            for (_i = 0; _i < _n; _i++)
            {
                int _c = _s[_i];
                switch (_c)
                {
                    case '"': PrintF("\\\""); break;
                    case '\\': PrintF("\\\\"); break;
                    case '\a': PrintF("\\a"); break;
                    case '\b': PrintF("\\b"); break;
                    case '\f': PrintF("\\f"); break;
                    case '\n': PrintF("\\n"); break;
                    case '\r': PrintF("\\r"); break;
                    case '\t': PrintF("\\t"); break;
                    case '\v': PrintF("\\v"); break;
                    default:
                        if (IsPrint((byte)_c))
                            PutChar(_c);
                        else
                            PrintF("\\%03u", (byte)_c);
                        break;
                }
            }
            PutChar('"');
        }

        private static void PrintConstant(Proto _f, int _i)
        {
            TValue _o = _f.k[_i];
            switch (TType(_o))
            {
                case LUA_TNIL:
                    PrintF("nil");
                    break;
                case LUA_TBOOLEAN:
                    PrintF(BValue(_o) != 0 ? "true" : "false");
                    break;
                case LUA_TNUMBER:
                    PrintF(LUA_NUMBER_FMT, NValue(_o));
                    break;
                case LUA_TSTRING:
                    PrintString(RawTSValue(_o));
                    break;
                default:                
                    PrintF("? type=%d", TType(_o));
                    break;
            }
        }

        private static void PrintCode(Proto _f)
        {
            Instruction[] _code = _f.code;
            int _pc, _n = _f.sizeCode;
            for (_pc = 0; _pc < _n; _pc++)
            {
                Instruction _i = _f.code[_pc];
                OpCode _o = GET_OPCODE(_i);
                int _a = GETARG_A(_i);
                int _b = GETARG_B(_i);
                int _c = GETARG_C(_i);
                int _bx = GETARG_Bx(_i);
                int _sbx = GETARG_sBx(_i);
                int _line = GetLine(_f, _pc);
                PrintF("\t%d\t", _pc + 1);
                if (_line > 0) PrintF("[%d]\t", _line); else PrintF("[-]\t");
                PrintF("%-9s\t", _luaPOpNames[(int)_o]);
                switch (getOpMode(_o))
                {
                    case OpMode.iABC:
                        PrintF("%d", _a);
                        if (getBMode(_o) != OpArgMask.OpArgN) PrintF(" %d", (ISK(_b) != 0) ? (-1 - INDEXK(_b)) : _b);
                        if (getCMode(_o) != OpArgMask.OpArgN) PrintF(" %d", (ISK(_c) != 0) ? (-1 - INDEXK(_c)) : _c);
                        break;
                    case OpMode.iABx:
                        if (getBMode(_o) == OpArgMask.OpArgK) PrintF("%d %d", _a, -1 - _bx); else PrintF("%d %d", _a, _bx);
                        break;
                    case OpMode.iAsBx:
                        if (_o == OpCode.OP_JMP) PrintF("%d", _sbx); else PrintF("%d %d", _a, _sbx);
                        break;
                }
                switch (_o)
                {
                    case OpCode.OP_LOADK:
                        PrintF("\t; "); PrintConstant(_f, _bx);
                        break;
                    case OpCode.OP_GETUPVAL:
                    case OpCode.OP_SETUPVAL:
                        PrintF("\t; %s", (_f.sizeUpValues > 0) ? GetStr(_f.upValues[_b]) : "-");
                        break;
                    case OpCode.OP_GETGLOBAL:
                    case OpCode.OP_SETGLOBAL:
                        PrintF("\t; %s", SValue(_f.k[_bx]));
                        break;
                    case OpCode.OP_GETTABLE:
                    case OpCode.OP_SELF:
                        if (ISK(_c) != 0) { PrintF("\t; "); PrintConstant(_f, INDEXK(_c)); }
                        break;
                    case OpCode.OP_SETTABLE:
                    case OpCode.OP_ADD:
                    case OpCode.OP_SUB:
                    case OpCode.OP_MUL:
                    case OpCode.OP_DIV:
                    case OpCode.OP_POW:
                    case OpCode.OP_EQ:
                    case OpCode.OP_LT:
                    case OpCode.OP_LE:
                        if (ISK(_b) != 0 || ISK(_c) != 0)
                        {
                            PrintF("\t; ");
                            if (ISK(_b) != 0) PrintConstant(_f, INDEXK(_b)); else PrintF("-");
                            PrintF(" ");
                            if (ISK(_c) != 0) PrintConstant(_f, INDEXK(_c)); else PrintF("-");
                        }
                        break;
                    case OpCode.OP_JMP:
                    case OpCode.OP_FORLOOP:
                    case OpCode.OP_FORPREP:
                        PrintF("\t; to %d", _sbx + _pc + 2);
                        break;
                    case OpCode.OP_CLOSURE:
                        PrintF("\t; %p", VOID(_f.p[_bx]));
                        break;
                    case OpCode.OP_SETLIST:
                        if (_c == 0) PrintF("\t; %d", (int)_code[++_pc]);
                        else PrintF("\t; %d", _c);
                        break;
                    default:
                        break;
                }
                PrintF("\n");
            }
        }

        public static string SS(int _x) { return (_x == 1) ? "" : "s"; }

        private static void PrintHeader(Proto _f)
        {
            CharPtr _s = GetStr(_f.source);
            if (_s[0] == '@' || _s[0] == '=')
                _s = _s.next();
            else if (_s[0] == LUA_SIGNATURE[0])
                _s = "(bstring)";
            else
                _s = "(string)";
            PrintF("\n%s <%s:%d,%d> (%d Instruction%s, %d bytes at %p)\n",
                (_f.lineDefined == 0) ? "main" : "function", _s,
               _f.lineDefined, _f.lastLineDefined,
               _f.sizeCode, SS(_f.sizeCode), _f.sizeCode * GetUnmanagedSize(typeof(Instruction)), VOID(_f));
            PrintF("%d%s param%s, %d slot%s, %d upvalue%s, ",
               _f.numParams, (_f.isVarArg != 0) ? "+" : "", SS(_f.numParams),
               _f.maxStackSize, SS(_f.maxStackSize), _f.nups, SS(_f.nups));
            PrintF("%d local%s, %d constant%s, %d function%s\n",
               _f.sizeLocVars, SS(_f.sizeLocVars), _f.sizeK, SS(_f.sizeK), _f.sizeP, SS(_f.sizeP));
        }

        private static void PrintConstants(Proto _f)
        {
            int _i, _n = _f.sizeK;
            PrintF("constants (%d) for %p:\n", _n, VOID(_f));
            for (_i = 0; _i < _n; _i++)
            {
                PrintF("\t%d\t", _i + 1);
                PrintConstant(_f, _i);
                PrintF("\n");
            }
        }

        private static void PrintLocals(Proto _f)
        {
            int _i, _n = _f.sizeLocVars;
            PrintF("locals (%d) for %p:\n", _n, VOID(_f));
            for (_i = 0; _i < _n; _i++)
                PrintF("\t%d\t%s\t%d\t%d\n", _i, GetStr(_f.locVars[_i].varname), _f.locVars[_i].startpc + 1, _f.locVars[_i].endpc + 1);
        }

        private static void PrintUpvalues(Proto _f)
        {
            int _i, _n = _f.sizeUpValues;
            PrintF("upvalues (%d) for %p:\n", _n, VOID(_f));
            if (_f.upValues == null) return;
            for (_i = 0; _i < _n; _i++)
            {
                PrintF("\t%d\t%s\n", _i, GetStr(_f.upValues[_i]));
            }
        }

        public static void PrintFunction(Proto _f, int _full)
        {
            int _i, _n = _f.sizeP;
            PrintHeader(_f);
            PrintCode(_f);
            if (_full != 0)
            {
                PrintConstants(_f);
                PrintLocals(_f);
                PrintUpvalues(_f);
            }
            for (_i = 0; _i < _n; _i++) PrintFunction(_f.p[_i], _full);
        }
    }
}