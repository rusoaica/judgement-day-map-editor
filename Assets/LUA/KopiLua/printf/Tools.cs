﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace AT.MIN
{
    public static class Tools
    {
        internal static Regex r = new Regex(@"\%(\d*\$)?([\'\#\-\+ ]*)(\d*)(?:\.(\d+))?([hl])?([dioxXucsfeEgGpn%])");

        #region Public Methods
        #region IsNumericType
        /// <summary>
        /// Determines whether the specified value is of numeric type.
        /// </summary>
        /// <param name="_o">The object to check.</param>
        /// <returns>
        /// 	<c>true</c> if o is a numeric type; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNumericType(object _o)
        {
            return (_o is byte || _o is sbyte || _o is short || _o is ushort || _o is int || _o is uint || _o is long || _o is ulong || _o is float || _o is double || _o is decimal);
        }
        #endregion

        #region IsPositive
        /// <summary>
        /// Determines whether the specified value is positive.
        /// </summary>
        /// <param name="_value">The value.</param>
        /// <param name="_zero_is_positive">if set to <c>true</c> treats 0 as positive.</param>
        /// <returns>
        /// 	<c>true</c> if the specified value is positive; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsPositive(object _value, bool _zero_is_positive)
        {
            switch (Type.GetTypeCode(_value.GetType()))
            {
                case TypeCode.SByte:
                    return (_zero_is_positive ? (sbyte)_value >= 0 : (sbyte)_value > 0);
                case TypeCode.Int16:
                    return (_zero_is_positive ? (short)_value >= 0 : (short)_value > 0);
                case TypeCode.Int32:
                    return (_zero_is_positive ? (int)_value >= 0 : (int)_value > 0);
                case TypeCode.Int64:
                    return (_zero_is_positive ? (long)_value >= 0 : (long)_value > 0);
                case TypeCode.Single:
                    return (_zero_is_positive ? (float)_value >= 0 : (float)_value > 0);
                case TypeCode.Double:
                    return (_zero_is_positive ? (double)_value >= 0 : (double)_value > 0);
                case TypeCode.Decimal:
                    return (_zero_is_positive ? (decimal)_value >= 0 : (decimal)_value > 0);
                case TypeCode.Byte:
                    return (_zero_is_positive ? true : (byte)_value > 0);
                case TypeCode.UInt16:
                    return (_zero_is_positive ? true : (ushort)_value > 0);
                case TypeCode.UInt32:
                    return (_zero_is_positive ? true : (uint)_value > 0);
                case TypeCode.UInt64:
                    return (_zero_is_positive ? true : (ulong)_value > 0);
                case TypeCode.Char:
                    return (_zero_is_positive ? true : (char)_value != '\0');
                default:
                    return false;
            }
        }
        #endregion

        #region ToUnsigned
        /// <summary>
        /// Converts the specified values boxed type to its correpsonding unsigned
        /// type.
        /// </summary>
        /// <param name="_value">The value.</param>
        /// <returns>A boxed numeric object whos type is unsigned.</returns>
        public static object ToUnsigned(object _value)
        {
            switch (Type.GetTypeCode(_value.GetType()))
            {
                case TypeCode.SByte:
                    return (byte)((sbyte)_value);
                case TypeCode.Int16:
                    return (ushort)((short)_value);
                case TypeCode.Int32:
                    return (uint)((int)_value);
                case TypeCode.Int64:
                    return (ulong)((long)_value);
                case TypeCode.Byte:
                    return _value;
                case TypeCode.UInt16:
                    return _value;
                case TypeCode.UInt32:
                    return _value;
                case TypeCode.UInt64:
                    return _value;
                case TypeCode.Single:
                    return (UInt32)((float)_value);
                case TypeCode.Double:
                    return (ulong)((double)_value);
                case TypeCode.Decimal:
                    return (ulong)((decimal)_value);
                default:
                    return null;
            }
        }
        #endregion

        #region ToInteger
        /// <summary>
        /// Converts the specified values boxed type to its correpsonding integer
        /// type.
        /// </summary>
        /// <param name="_value">The value.</param>
        /// <returns>A boxed numeric object whos type is an integer type.</returns>
        public static object ToInteger(object _value, bool _round)
        {
            switch (Type.GetTypeCode(_value.GetType()))
            {
                case TypeCode.SByte:
                    return _value;
                case TypeCode.Int16:
                    return _value;
                case TypeCode.Int32:
                    return _value;
                case TypeCode.Int64:
                    return _value;
                case TypeCode.Byte:
                    return _value;
                case TypeCode.UInt16:
                    return _value;
                case TypeCode.UInt32:
                    return _value;
                case TypeCode.UInt64:
                    return _value;
                case TypeCode.Single:
                    return (_round ? (int)Math.Round((float)_value) : (int)((float)_value));
                case TypeCode.Double:
                    return (_round ? (long)Math.Round((double)_value) : (long)((double)_value));
                case TypeCode.Decimal:
                    return (_round ? Math.Round((decimal)_value) : (decimal)_value);
                default:
                    return null;
            }
        }
        #endregion

        #region UnboxToLong
        public static long UnboxToLong(object _value, bool _round)
        {
            switch (Type.GetTypeCode(_value.GetType()))
            {
                case TypeCode.SByte:
                    return (long)((sbyte)_value);
                case TypeCode.Int16:
                    return (long)((short)_value);
                case TypeCode.Int32:
                    return (long)((int)_value);
                case TypeCode.Int64:
                    return (long)_value;
                case TypeCode.Byte:
                    return (long)((byte)_value);
                case TypeCode.UInt16:
                    return (long)((ushort)_value);
                case TypeCode.UInt32:
                    return (long)((uint)_value);
                case TypeCode.UInt64:
                    return (long)((ulong)_value);
                case TypeCode.Single:
                    return (_round ? (long)Math.Round((float)_value) : (long)((float)_value));
                case TypeCode.Double:
                    return (_round ? (long)Math.Round((double)_value) : (long)((double)_value));
                case TypeCode.Decimal:
                    return (_round ? (long)Math.Round((decimal)_value) : (long)((decimal)_value));
                default:
                    return 0;
            }
        }
        #endregion

        #region ReplaceMetaChars
        /// <summary>
        /// Replaces the string representations of meta chars with their corresponding
        /// character values.
        /// </summary>
        /// <param name="_input">The input.</param>
        /// <returns>A string with all string meta chars are replaced</returns>
        public static string ReplaceMetaChars(string _input)
        {
            return Regex.Replace(_input, @"(\\)(\d{3}|[^\d])?", new MatchEvaluator(ReplaceMetaCharsMatch));
        }

        private static string ReplaceMetaCharsMatch(Match _m)
        {
            if (_m.Groups[2].Length == 3)
                return Convert.ToChar(Convert.ToByte(_m.Groups[2].Value, 8)).ToString();
            else
            {
                switch (_m.Groups[2].Value)
                {
                    case "0":           
                        return "\0";
                    case "a":          
                        return "\a";
                    case "b":         
                        return "\b";
                    case "f":          
                        return "\f";
                    case "v":           
                        return "\v";
                    case "r":          
                        return "\r";
                    case "n":          
                        return "\n";
                    case "t":           
                        return "\t";
                    default:
                        return _m.Groups[2].Value;
                }
            }
        }
        #endregion

        #region printf
        public static void PrintF(string _format, params object[] _parameters)
        {
            Console.Write(Tools.SPrintF(_format, _parameters));
        }
        #endregion

        #region fprintf
        public static void FPrintF(TextWriter _destination, string _format, params object[] _parameters)
        {
            _destination.Write(Tools.SPrintF(_format, _parameters));
        }
        #endregion

        #region sprintf
        public static string SPrintF(string _format, params object[] _parameters)
        {
            #region Variables
            StringBuilder _f = new StringBuilder();
            Match _m = null;
            string _w = String.Empty;
            int _default_param_ix = 0;
            int _param_ix;
            object _o = null;
            bool _flag_left2_right = false;
            bool _flag_alternate = false;
            bool _flag_positive_sign = false;
            bool _flag_positive_space = false;
            bool _flag_zero_padding = false;
            bool _flag_group_thousands = false;
            int _field_length = 0;
            int _field_precision = 0;
            char _short_long_indicator = '\0';
            char _format_specifier = '\0';
            char _padding_character = ' ';
            #endregion

            _f.Append(_format);
            _m = r.Match(_f.ToString());
            while (_m.Success)
            {
                #region parameter index
                _param_ix = _default_param_ix;
                if (_m.Groups[1] != null && _m.Groups[1].Value.Length > 0)
                {
                    string _val = _m.Groups[1].Value.Substring(0, _m.Groups[1].Value.Length - 1);
                    _param_ix = Convert.ToInt32(_val) - 1;
                };
                #endregion
                #region format flags
                _flag_alternate = false;
                _flag_left2_right = false;
                _flag_positive_sign = false;
                _flag_positive_space = false;
                _flag_zero_padding = false;
                _flag_group_thousands = false;
                if (_m.Groups[2] != null && _m.Groups[2].Value.Length > 0)
                {
                    string _flags = _m.Groups[2].Value;
                    _flag_alternate = (_flags.IndexOf('#') >= 0);
                    _flag_left2_right = (_flags.IndexOf('-') >= 0);
                    _flag_positive_sign = (_flags.IndexOf('+') >= 0);
                    _flag_positive_space = (_flags.IndexOf(' ') >= 0);
                    _flag_group_thousands = (_flags.IndexOf('\'') >= 0);
                    if (_flag_positive_sign && _flag_positive_space)
                        _flag_positive_space = false;
                }
                #endregion
                #region field length
                _padding_character = ' ';
                _field_length = int.MinValue;
                if (_m.Groups[3] != null && _m.Groups[3].Value.Length > 0)
                {
                    _field_length = Convert.ToInt32(_m.Groups[3].Value);
                    _flag_zero_padding = (_m.Groups[3].Value[0] == '0');
                }
                #endregion
                if (_flag_zero_padding)
                    _padding_character = '0';
                if (_flag_left2_right && _flag_zero_padding)
                {
                    _flag_zero_padding = false;
                    _padding_character = ' ';
                }
                #region field precision
                _field_precision = int.MinValue;
                if (_m.Groups[4] != null && _m.Groups[4].Value.Length > 0)
                    _field_precision = Convert.ToInt32(_m.Groups[4].Value);
                #endregion
                #region short / long indicator
                _short_long_indicator = Char.MinValue;
                if (_m.Groups[5] != null && _m.Groups[5].Value.Length > 0)
                    _short_long_indicator = _m.Groups[5].Value[0];
                #endregion
                #region format specifier
                _format_specifier = Char.MinValue;
                if (_m.Groups[6] != null && _m.Groups[6].Value.Length > 0)
                    _format_specifier = _m.Groups[6].Value[0];
                #endregion
                if (_field_precision == int.MinValue && _format_specifier != 's' && _format_specifier != 'c' && Char.ToUpper(_format_specifier) != 'X' && _format_specifier != 'o')
                    _field_precision = 6;
                #region get next value parameter
                if (_parameters == null || _param_ix >= _parameters.Length)
                    _o = null;
                else
                {
                    _o = _parameters[_param_ix];
                    if (_short_long_indicator == 'h')
                    {
                        if (_o is int)
                            _o = (short)((int)_o);
                        else if (_o is long)
                            _o = (short)((long)_o);
                        else if (_o is uint)
                            _o = (ushort)((uint)_o);
                        else if (_o is ulong)
                            _o = (ushort)((ulong)_o);
                    }
                    else if (_short_long_indicator == 'l')
                    {
                        if (_o is short)
                            _o = (long)((short)_o);
                        else if (_o is int)
                            _o = (long)((int)_o);
                        else if (_o is ushort)
                            _o = (ulong)((ushort)_o);
                        else if (_o is uint)
                            _o = (ulong)((uint)_o);
                    }
                }
                #endregion
                _w = String.Empty;
                switch (_format_specifier)
                {
                    #region % - character
                    case '%':  
                        _w = "%";
                        break;
                    #endregion
                    #region d - integer
                    case 'd':   
                        _w = FormatNumber((_flag_group_thousands ? "n" : "d"), _flag_alternate, _field_length, int.MinValue, _flag_left2_right, _flag_positive_sign, _flag_positive_space, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region i - integer
                    case 'i':   
                        goto case 'd';
                    #endregion
                    #region o - octal integer
                    case 'o':  
                        _w = FormatOct("o", _flag_alternate, _field_length, int.MinValue, _flag_left2_right, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region x - hex integer
                    case 'x':   
                        _w = FormatHex("x", _flag_alternate, _field_length, _field_precision, _flag_left2_right, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region X - hex integer
                    case 'X':   
                        _w = FormatHex("X", _flag_alternate, _field_length, _field_precision, _flag_left2_right, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region u - unsigned integer
                    case 'u':   
                        _w = FormatNumber((_flag_group_thousands ? "n" : "d"), _flag_alternate, _field_length, int.MinValue, _flag_left2_right, false, false, _padding_character, ToUnsigned(_o));
                        _default_param_ix++;
                        break;
                    #endregion
                    #region c - character
                    case 'c':   
                        if (IsNumericType(_o))
                            _w = Convert.ToChar(_o).ToString();
                        else if (_o is char)
                            _w = ((char)_o).ToString();
                        else if (_o is string && ((string)_o).Length > 0)
                            _w = ((string)_o)[0].ToString();
                        _default_param_ix++;
                        break;
                    #endregion
                    #region s - string
                    case 's':   
                        _w = _o.ToString();
                        if (_field_precision >= 0)
                            _w = _w.Substring(0, _field_precision);
                        if (_field_length != int.MinValue)
                            if (_flag_left2_right)
                                _w = _w.PadRight(_field_length, _padding_character);
                            else
                                _w = _w.PadLeft(_field_length, _padding_character);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region f - double number
                    case 'f':  
                        _w = FormatNumber((_flag_group_thousands ? "n" : "f"), _flag_alternate, _field_length, _field_precision, _flag_left2_right, _flag_positive_sign, _flag_positive_space, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region e - exponent number
                    case 'e':   
                        _w = FormatNumber("e", _flag_alternate, _field_length, _field_precision, _flag_left2_right, _flag_positive_sign, _flag_positive_space, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region E - exponent number
                    case 'E':  
                        _w = FormatNumber("E", _flag_alternate, _field_length, _field_precision, _flag_left2_right, _flag_positive_sign, _flag_positive_space, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region g - general number
                    case 'g':   
                        _w = FormatNumber("g", _flag_alternate, _field_length, _field_precision, _flag_left2_right, _flag_positive_sign, _flag_positive_space, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region G - general number
                    case 'G':  
                        _w = FormatNumber("G", _flag_alternate, _field_length, _field_precision, _flag_left2_right, _flag_positive_sign, _flag_positive_space, _padding_character, _o);
                        _default_param_ix++;
                        break;
                    #endregion
                    #region p - pointer
                    case 'p':   
                        if (_o is IntPtr)
#if XBOX || SILVERLIGHT
							_w = ( (IntPtr)o ).ToString();
#else
                            _w = "0x" + ((IntPtr)_o).ToString("x");
#endif
                        _default_param_ix++;
                        break;
                    #endregion
                    #region n - number of processed chars so far
                    case 'n':   
                        _w = FormatNumber("d", _flag_alternate, _field_length, int.MinValue, _flag_left2_right, _flag_positive_sign, _flag_positive_space, _padding_character, _m.Index);
                        break;
                    #endregion
                    default:
                        _w = String.Empty;
                        _default_param_ix++;
                        break;
                }
                _f.Remove(_m.Index, _m.Length);
                _f.Insert(_m.Index, _w);
                _m = r.Match(_f.ToString(), _m.Index + _w.Length);
            }
            return _f.ToString();
        }
        #endregion
        #endregion

        #region Private Methods
        #region FormatOCT
        private static string FormatOct(string _native_format, bool _alternate, int _field_length, int _field_precision, bool _left2_right, char _padding, object _value)
        {
            string _w = String.Empty;
            string _length_format = "{0" + (_field_length != int.MinValue ? "," + (_left2_right ? "-" : String.Empty) + _field_length.ToString() : String.Empty) + "}";
            if (IsNumericType(_value))
            {
                _w = Convert.ToString(UnboxToLong(_value, true), 8);
                if (_left2_right || _padding == ' ')
                {
                    if (_alternate && _w != "0")
                        _w = "0" + _w;
                    _w = String.Format(_length_format, _w);
                }
                else
                {
                    if (_field_length != int.MinValue)
                        _w = _w.PadLeft(_field_length - (_alternate && _w != "0" ? 1 : 0), _padding);
                    if (_alternate && _w != "0")
                        _w = "0" + _w;
                }
            }
            return _w;
        }
        #endregion

        #region FormatHEX
        private static string FormatHex(string _native_format, bool _alternate, int _field_length, int _field_precision, bool _left2_right, char _padding, object _value)
        {
            string _w = String.Empty;
            string _length_format = "{0" + (_field_length != int.MinValue ? "," + (_left2_right ? "-" : String.Empty) + _field_length.ToString() : String.Empty) + "}";
            string _number_format = "{0:" + _native_format + (_field_precision != int.MinValue ? _field_precision.ToString() : String.Empty) + "}";
            if (IsNumericType(_value))
            {
                _w = String.Format(_number_format, _value);
                if (_left2_right || _padding == ' ')
                {
                    if (_alternate)
                        _w = (_native_format == "x" ? "0x" : "0X") + _w;
                    _w = String.Format(_length_format, _w);
                }
                else
                {
                    if (_field_length != int.MinValue)
                        _w = _w.PadLeft(_field_length - (_alternate ? 2 : 0), _padding);
                    if (_alternate)
                        _w = (_native_format == "x" ? "0x" : "0X") + _w;
                }
            }
            return _w;
        }
        #endregion

        #region FormatNumber
        private static string FormatNumber(string _native_format, bool _alternate, int _field_length, int _field_precision, bool _left2_right, bool _positive_sign, bool _positive_space, char _padding, object _value)
        {
            string _w = String.Empty;
            string _length_format = "{0" + (_field_length != int.MinValue ? "," + (_left2_right ? "-" : String.Empty) + _field_length.ToString() : String.Empty) + "}";
            string _number_format = "{0:" + _native_format + (_field_precision != int.MinValue ? _field_precision.ToString() : "0") + "}";
            if (IsNumericType(_value))
            {
                _w = String.Format(CultureInfo.InvariantCulture, _number_format, _value);
                if (_left2_right || _padding == ' ')
                {
                    if (IsPositive(_value, true))
                        _w = (_positive_sign ? "+" : (_positive_space ? " " : String.Empty)) + _w;
                    _w = String.Format(_length_format, _w);
                }
                else
                {
                    if (_w.StartsWith("-"))
                        _w = _w.Substring(1);
                    if (_field_length != int.MinValue)
                        _w = _w.PadLeft(_field_length - 1, _padding);
                    if (IsPositive(_value, true))
                        _w = (_positive_sign ? "+" : (_positive_space ? " " : (_field_length != int.MinValue ? _padding.ToString() : String.Empty))) + _w;
                    else
                        _w = "-" + _w;
                }
            }
            return _w;
        }
        #endregion
        #endregion
    }
}