using NLua.Method;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
#else
    using LuaState = KeraLua.LuaState;
#endif

    sealed class CheckType
    {
#if SILVERLIGHT
		private Dictionary<Type, ExtractValue> extractValues = new Dictionary<Type, ExtractValue>();
#else
        private Dictionary<long, ExtractValue> extractValues = new Dictionary<long, ExtractValue>();
#endif
        private ExtractValue _extractNetObject;
        private ObjectTranslator _translator;

        public CheckType(ObjectTranslator _translator)
        {
            this._translator = _translator;
            extractValues.Add(GetExtractDictionaryKey(typeof(object)), new ExtractValue(GetAsObject));
            extractValues.Add(GetExtractDictionaryKey(typeof(sbyte)), new ExtractValue(GetAsSbyte));
            extractValues.Add(GetExtractDictionaryKey(typeof(byte)), new ExtractValue(GetAsByte));
            extractValues.Add(GetExtractDictionaryKey(typeof(short)), new ExtractValue(GetAsShort));
            extractValues.Add(GetExtractDictionaryKey(typeof(ushort)), new ExtractValue(GetAsUshort));
            extractValues.Add(GetExtractDictionaryKey(typeof(int)), new ExtractValue(GetAsInt));
            extractValues.Add(GetExtractDictionaryKey(typeof(uint)), new ExtractValue(GetAsUint));
            extractValues.Add(GetExtractDictionaryKey(typeof(long)), new ExtractValue(GetAsLong));
            extractValues.Add(GetExtractDictionaryKey(typeof(ulong)), new ExtractValue(GetAsUlong));
            extractValues.Add(GetExtractDictionaryKey(typeof(double)), new ExtractValue(GetAsDouble));
            extractValues.Add(GetExtractDictionaryKey(typeof(char)), new ExtractValue(GetAsChar));
            extractValues.Add(GetExtractDictionaryKey(typeof(float)), new ExtractValue(GetAsFloat));
            extractValues.Add(GetExtractDictionaryKey(typeof(decimal)), new ExtractValue(GetAsDecimal));
            extractValues.Add(GetExtractDictionaryKey(typeof(bool)), new ExtractValue(GetAsBoolean));
            extractValues.Add(GetExtractDictionaryKey(typeof(string)), new ExtractValue(GetAsString));
            extractValues.Add(GetExtractDictionaryKey(typeof(char[])), new ExtractValue(GetAsCharArray));
            extractValues.Add(GetExtractDictionaryKey(typeof(LuaFunction)), new ExtractValue(GetAsFunction));
            extractValues.Add(GetExtractDictionaryKey(typeof(LuaTable)), new ExtractValue(GetAsTable));
            extractValues.Add(GetExtractDictionaryKey(typeof(LuaUserData)), new ExtractValue(GetAsUserdata));
            _extractNetObject = new ExtractValue(GetAsNetObject);
        }

        /*
		 * Checks if the value at Lua stack index _stack_pos matches paramType, 
		 * returning a conversion function if it does and null otherwise.
		 */
        internal ExtractValue GetExtractor(IReflect _param_type)
        {
            return GetExtractor(_param_type.UnderlyingSystemType);
        }

        internal ExtractValue GetExtractor(Type _param_type)
        {
            if (_param_type.IsByRef)
                _param_type = _param_type.GetElementType();
            var _extract_key = GetExtractDictionaryKey(_param_type);
            return extractValues.ContainsKey(_extract_key) ? extractValues[_extract_key] : _extractNetObject;
        }

        internal ExtractValue CheckLuaType(LuaState _lua_state, int _stack_pos, Type _param_type)
        {
            var _lua_type = LuaLib.LuaType(_lua_state, _stack_pos);
            if (_param_type.IsByRef)
                _param_type = _param_type.GetElementType();
            var _underlying_type = Nullable.GetUnderlyingType(_param_type);
            if (_underlying_type != null)
                _param_type = _underlying_type; 
            var _extract_key = GetExtractDictionaryKey(_param_type);
            if (_param_type.Equals(typeof(object)))
                return extractValues[_extract_key];
            if (_param_type.IsGenericParameter)
            {
                if (_lua_type == LuaTypes.Boolean)
                    return extractValues[GetExtractDictionaryKey(typeof(bool))];
                else if (_lua_type == LuaTypes.String)
                    return extractValues[GetExtractDictionaryKey(typeof(string))];
                else if (_lua_type == LuaTypes.Table)
                    return extractValues[GetExtractDictionaryKey(typeof(LuaTable))];
                else if (_lua_type == LuaTypes.UserData)
                    return extractValues[GetExtractDictionaryKey(typeof(object))];
                else if (_lua_type == LuaTypes.Function)
                    return extractValues[GetExtractDictionaryKey(typeof(LuaFunction))];
                else if (_lua_type == LuaTypes.Number)
                    return extractValues[GetExtractDictionaryKey(typeof(double))];
            }
            bool _net_param_is_string = _param_type == typeof(string) || _param_type == typeof(char[]);
            bool _net_param_is_numeric = _param_type == typeof(int) || _param_type == typeof(uint) || _param_type == typeof(long) || _param_type == typeof(ulong) || _param_type == typeof(short) ||
                                     _param_type == typeof(float) || _param_type == typeof(double) ||  _param_type == typeof(decimal) || _param_type == typeof(byte);
            if (_net_param_is_numeric)
            {
                if (LuaLib.LuaIsNumber(_lua_state, _stack_pos) && !_net_param_is_string)
                    return extractValues[_extract_key];
            }
            else if (_param_type == typeof(bool))
            {
                if (LuaLib.LuaIsBoolean(_lua_state, _stack_pos))
                    return extractValues[_extract_key];
            }
            else if (_net_param_is_string)
            {
                if (LuaLib.LuaNetIsStringStrict(_lua_state, _stack_pos))
                    return extractValues[_extract_key];
                else if (_lua_type == LuaTypes.Nil)
                    return _extractNetObject; 
            }
            else if (_param_type == typeof(LuaTable))
            {
                if (_lua_type == LuaTypes.Table)
                    return extractValues[_extract_key];
            }
            else if (_param_type == typeof(LuaUserData))
            {
                if (_lua_type == LuaTypes.UserData)
                    return extractValues[_extract_key];
            }
            else if (_param_type == typeof(LuaFunction))
            {
                if (_lua_type == LuaTypes.Function)
                    return extractValues[_extract_key];
            }
            else if (typeof(Delegate).IsAssignableFrom(_param_type) && _lua_type == LuaTypes.Function)
                return new ExtractValue(new DelegateGenerator(_translator, _param_type).ExtractGenerated);
            else if (_param_type.IsInterface && _lua_type == LuaTypes.Table)
                return new ExtractValue(new ClassGenerator(_translator, _param_type).ExtractGenerated);
            else if ((_param_type.IsInterface || _param_type.IsClass) && _lua_type == LuaTypes.Nil)
                return _extractNetObject;
            else if (LuaLib.LuaType(_lua_state, _stack_pos) == LuaTypes.Table)
            {
                if (LuaLib.LuaLGetMetafield(_lua_state, _stack_pos, "__index"))
                {
                    object _obj = _translator.GetNetObject(_lua_state, -1);
                    LuaLib.LuaSetTop(_lua_state, -2);
                    if (_obj != null && _param_type.IsAssignableFrom(_obj.GetType()))
                        return _extractNetObject;
                }
                else
                    return null;
            }
            else
            {
                object _obj = _translator.GetNetObject(_lua_state, _stack_pos);
                if (_obj != null && _param_type.IsAssignableFrom(_obj.GetType()))
                    return _extractNetObject;
            }
            return null;
        }

#if SILVERLIGHT
		private Type GetExtractDictionaryKey(Type _target_type)
		{
			return _target_type;
		}
#else
        private long GetExtractDictionaryKey(Type _target_type)
        {
            return _target_type.TypeHandle.Value.ToInt64();
        }
#endif

        /*
		 * The following functions return the value in the Lua stack
		 * index _stack_pos as the desired type if it can, or null
		 * otherwise.
		 */
        private object GetAsSbyte(LuaState _lua_state, int _stack_pos)
        {
            sbyte _ret_val = (sbyte)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsByte(LuaState _lua_state, int _stack_pos)
        {
            byte _ret_val = (byte)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsShort(LuaState _lua_state, int _stack_pos)
        {
            short _ret_val = (short)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsUshort(LuaState _lua_state, int _stack_pos)
        {
            ushort _ret_val = (ushort)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsInt(LuaState _lua_state, int _stack_pos)
        {
            if (!LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            int _ret_val = (int)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            return _ret_val;
        }

        private object GetAsUint(LuaState _lua_state, int _stack_pos)
        {
            uint _ret_val = (uint)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsLong(LuaState _lua_state, int _stack_pos)
        {
            long _ret_val = (long)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsUlong(LuaState _lua_state, int _stack_pos)
        {
            ulong _ret_val = (ulong)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsDouble(LuaState _lua_state, int _stack_pos)
        {
            double _ret_val = LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsChar(LuaState _lua_state, int _stack_pos)
        {
            char _ret_val = (char)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsFloat(LuaState _lua_state, int _stack_pos)
        {
            float _ret_val = (float)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsDecimal(LuaState _lua_state, int _stack_pos)
        {
            decimal _ret_val = (decimal)LuaLib.LuaToNumber(_lua_state, _stack_pos);
            if (_ret_val == 0 && !LuaLib.LuaIsNumber(_lua_state, _stack_pos))
                return null;
            return _ret_val;
        }

        private object GetAsBoolean(LuaState _lua_state, int _stack_pos)
        {
            return LuaLib.LuaToBoolean(_lua_state, _stack_pos);
        }

        private object GetAsCharArray(LuaState _lua_state, int _stack_pos)
        {
            if (!LuaLib.LuaNetIsStringStrict(_lua_state, _stack_pos))
                return null;
            string _ret_val = LuaLib.LuaToString(_lua_state, _stack_pos).ToString();
            return _ret_val.ToCharArray();
        }

        private object GetAsString(LuaState _lua_state, int _stack_pos)
        {
            if (!LuaLib.LuaNetIsStringStrict(_lua_state, _stack_pos))
                return null;
            string _ret_val = LuaLib.LuaToString(_lua_state, _stack_pos).ToString();
            return _ret_val;
        }

        private object GetAsTable(LuaState _lua_state, int _stack_pos)
        {
            return _translator.GetTable(_lua_state, _stack_pos);
        }

        private object GetAsFunction(LuaState _lua_state, int _stack_pos)
        {
            return _translator.GetFunction(_lua_state, _stack_pos);
        }

        private object GetAsUserdata(LuaState _lua_state, int _stack_pos)
        {
            return _translator.GetUserData(_lua_state, _stack_pos);
        }

        public object GetAsObject(LuaState _lua_state, int _stack_pos)
        {
            if (LuaLib.LuaType(_lua_state, _stack_pos) == LuaTypes.Table)
            {
                if (LuaLib.LuaLGetMetafield(_lua_state, _stack_pos, "__index"))
                {
                    if (LuaLib.LuaLCheckMetatable(_lua_state, -1))
                    {
                        LuaLib.LuaInsert(_lua_state, _stack_pos);
                        LuaLib.LuaRemove(_lua_state, _stack_pos + 1);
                    }
                    else
                        LuaLib.LuaSetTop(_lua_state, -2);
                }
            }
            object _obj = _translator.GetObject(_lua_state, _stack_pos);
            return _obj;
        }

        public object GetAsNetObject(LuaState _lua_state, int _stack_pos)
        {
            object _obj = _translator.GetNetObject(_lua_state, _stack_pos);
            if (_obj == null && LuaLib.LuaType(_lua_state, _stack_pos) == LuaTypes.Table)
            {
                if (LuaLib.LuaLGetMetafield(_lua_state, _stack_pos, "__index"))
                {
                    if (LuaLib.LuaLCheckMetatable(_lua_state, -1))
                    {
                        LuaLib.LuaInsert(_lua_state, _stack_pos);
                        LuaLib.LuaRemove(_lua_state, _stack_pos + 1);
                        _obj = _translator.GetNetObject(_lua_state, _stack_pos);
                    }
                    else
                        LuaLib.LuaSetTop(_lua_state, -2);
                }
            }
            return _obj;
        }
    }
}