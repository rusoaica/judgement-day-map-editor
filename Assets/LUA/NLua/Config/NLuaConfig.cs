﻿using System;

namespace NLua.Config
{
    public static class Consts
    {
        public const string N_LUA_DESCRIPTION = "Bridge between the Lua runtime and the CLR";
#if DEBUG
        public const string N_LUA_CONFIGURATION = "Debug";
#else
		public const string N_LUA_CONFIGURATION = "Release";
#endif
        public const string N_LUA_COMPANY = "NLua.org";
        public const string N_LUA_PRODUCT = "NLua";
        public const string N_LUA_COPYRIGHT = "Copyright 2003-2014 Vinicius Jarina , Fabio Mascarenhas, Kevin Hesterm and Megax";
        public const string N_LUA_TRADEMARK = "MIT license";
        public const string N_LUA_VERSION = "1.3.1";
        public const string N_LUA_FILE_VERSION = "1.3.1";
    }
}