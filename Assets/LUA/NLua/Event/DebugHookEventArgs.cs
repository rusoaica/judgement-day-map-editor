using System;

namespace NLua.Event
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	using LuaDebug = KopiLua.LuaDebug;
#else
	using LuaCore  = KeraLua.Lua;
	using LuaState = KeraLua.LuaState;
	using LuaDebug = KeraLua.LuaDebug;
#endif

	/// <summary>
	/// Event args for hook callback event
	/// </summary>
	/// <author>Reinhard Ostermeier</author>
	public class DebugHookEventArgs : EventArgs
	{
		private readonly LuaDebug _luaDebug;

		public DebugHookEventArgs (LuaDebug _lua_debug)
		{
			this._luaDebug = _lua_debug;
		}

		public LuaDebug LuaDebug {
			get { return _luaDebug; }
		}
	}
}