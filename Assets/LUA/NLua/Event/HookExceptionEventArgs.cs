using System;

namespace NLua.Event
{
	public class HookExceptionEventArgs : EventArgs
	{
		private readonly Exception _mException;

		public Exception Exception {
			get { return _mException; }
		}

		public HookExceptionEventArgs (Exception _ex)
		{
			_mException = _ex;
		}
	}
}