using System;
#if !SILVERLIGHT
using System.Runtime.Serialization;
#endif

namespace NLua.Exceptions
{
	/// <summary>
	/// Exceptions thrown by the Lua runtime
	/// </summary>
#if !SILVERLIGHT
	[Serializable]
#endif
	public class LuaException : Exception
	{
		public LuaException () { }

		public LuaException (string _message) : base(_message) { }

		public LuaException (string _message, Exception _inner_exception) : base(_message, _inner_exception) { }

#if !SILVERLIGHT
		protected LuaException (SerializationInfo _info, StreamingContext _context) : base(_info, _context) { }
#endif
	}
}