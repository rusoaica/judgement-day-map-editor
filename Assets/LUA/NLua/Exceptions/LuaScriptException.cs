﻿using System;

namespace NLua.Exceptions
{
	/// <summary>
	/// Exceptions thrown by the Lua runtime because of errors in the script
	/// </summary>
	/// 
#if !SILVERLIGHT
	[Serializable]
#endif
	public class LuaScriptException : LuaException
	{
        private readonly string _source;

        /// <summary>
        /// Returns true if the exception has occured as the result of a .NET exception in user code
        /// </summary>
        public bool IsNetException { get; private set; }

		/// <summary>
		/// The position in the script where the exception was triggered.
		/// </summary>
#if SILVERLIGHT
		public string Source { get { return _source; } }
#else
		public override string Source { get { return _source; } }
#endif

		/// <summary>
		/// Creates a new Lua-only exception.
		/// </summary>
		/// <param name="_message">The message that describes the error.</param>
		/// <param name="_source">The position in the script where the exception was triggered.</param>
		public LuaScriptException (string _message, string _source) : base(_message)
		{
			this._source = _source;
		}

		/// <summary>
		/// Creates a new .NET wrapping exception.
		/// </summary>
		/// <param name="_inner_exception">The .NET exception triggered by user-code.</param>
		/// <param name="_source">The position in the script where the exception was triggered.</param>
		public LuaScriptException (Exception _inner_exception, string _source) : base("A .NET exception occured in user-code", _inner_exception)
		{
			this._source = _source;
			this.IsNetException = true;
		}

		public override string ToString ()
		{
			return GetType ().FullName + ": " + _source + Message;
		}
	}
}