using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace NLua.Extensions
{
    /// <summary>
    /// Some random extension stuff.
    /// </summary>
    static class CheckNull
    {
        /// <summary>
        /// Determines whether the specified obj is null.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns>
        /// 	<c>true</c> if the specified obj is null; otherwise, <c>false</c>.
        /// </returns>
        /// 

#if USE_KOPILUA
		public static bool IsNull (object _obj)
		{
			return (_obj == null);
		}
#else

        public static bool IsNull(IntPtr _ptr)
        {
            return (_ptr.Equals(IntPtr.Zero));
        }
#endif
    }

    static class TypeExtensions
    {
        public static bool HasMethod(this Type _t, string _name)
        {
            var _op = _t.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
            return _op.Count(m => m.Name == _name) > 0;
        }

        public static bool HasAdditionOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            return _t.HasMethod("op_Addition");
        }

        public static bool HasSubtractionOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            return _t.HasMethod("op_Subtraction");
        }

        public static bool HasMultiplyOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            return _t.HasMethod("op_Multiply");
        }

        public static bool HasDivisionOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            return _t.HasMethod("op_Division");
        }

        public static bool HasModulusOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            return _t.HasMethod("op_Modulus");
        }

        public static bool HasUnaryNegationOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            var _op = _t.GetMethod("op_UnaryNegation", BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
            return _op != null;
        }

        public static bool HasEqualityOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            return _t.HasMethod("op_Equality");
        }

        public static bool HasLessThanOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            return _t.HasMethod("op_LessThan");
        }

        public static bool HasLessThanOrEqualOpertator(this Type _t)
        {
            if (_t.IsPrimitive)
                return true;
            return _t.HasMethod("op_LessThanOrEqual");
        }

        public static IEnumerable<MethodInfo> GetMethods(this Type _t, string _name, BindingFlags _flags)
        {
            return _t.GetMethods(_flags).Where(m => m.Name == _name);
        }

        public static MethodInfo[] GetExtensionMethods(this Type _type, IEnumerable<Assembly> _assemblies = null)
        {
            List<Type> _types = new List<Type>();
            _types.AddRange(_type.Assembly.GetTypes().Where(t => t.IsPublic));
            if (_assemblies != null)
            {
                foreach (Assembly _item in _assemblies)
                {
                    if (_item == _type.Assembly)
                        continue;
                    _types.AddRange(_item.GetTypes().Where(t => t.IsPublic));
                }
            }
            var query = from _extension_type in _types
                        where _extension_type.IsSealed && !_extension_type.IsGenericType && !_extension_type.IsNested
                        from _method in _extension_type.GetMethods(BindingFlags.Static | BindingFlags.Public)
                        where _method.IsDefined(typeof(ExtensionAttribute), false)
                        where _method.GetParameters()[0].ParameterType == _type
                        select _method;
            return query.ToArray<MethodInfo>();
        }

        /// <summary>
        /// Extends the System.Type-type to search for a given extended MethodeName.
        /// </summary>
        /// <param name="MethodeName">Name of the Methode</param>
        /// <returns>the found Methode or null</returns>
        public static MethodInfo GetExtensionMethod(this Type _t, string _name, IEnumerable<Assembly> _assemblies = null)
        {
            var _mi = from _method in _t.GetExtensionMethods(_assemblies)
                     where _method.Name == _name
                     select _method;
            if (!_mi.Any<MethodInfo>())
                return null;
            else
                return _mi.First<MethodInfo>();
        }
    }

    static class StringExtensions
    {
        public static IEnumerable<string> SplitWithEscape(this string _input, char _separator, char _escape_character)
        {
            int _start = 0;
            int _index = 0;
            while (_index < _input.Length)
            {
                _index = _input.IndexOf(_separator, _index);
                if (_index == -1)
                    break;
                if (_input[_index - 1] == _escape_character)
                {
                    _input = _input.Remove(_index - 1, 1);
                    continue;
                }
                yield return _input.Substring(_start, _index - _start);
                _index++;
                _start = _index;
            }
            yield return _input.Substring(_start);
        }
    }
}