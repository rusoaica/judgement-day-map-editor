using System;

namespace NLua
{
	#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	#else
	using LuaCore  = KeraLua.Lua;
	using LuaState = KeraLua.LuaState;
	#endif
	class ClassGenerator
	{
		private ObjectTranslator _translator;
		private Type _class;

		public ClassGenerator (ObjectTranslator _obj_translator, Type _type_class)
		{
			_translator = _obj_translator;
			_class = _type_class;
		}

		public object ExtractGenerated (LuaState _lua_state, int _stack_pos)
		{
			return CodeGeneration.Instance.GetClassInstance (_class, _translator.GetTable (_lua_state, _stack_pos));
		}
	}
}