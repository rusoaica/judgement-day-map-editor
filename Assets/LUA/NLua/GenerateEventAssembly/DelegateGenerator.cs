using System;

namespace NLua
{
	#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	#else
	using LuaCore  = KeraLua.Lua;
	using LuaState = KeraLua.LuaState;
	#endif
	class DelegateGenerator
	{
		private ObjectTranslator _translator;
		private Type _delegateType;
		

		public DelegateGenerator (ObjectTranslator _object_translator, Type _type)
		{
			_translator = _object_translator;
			_delegateType = _type;
		}

		public object ExtractGenerated (LuaState _lua_state, int _stack_pos)
		{
			return CodeGeneration.Instance.GetDelegate (_delegateType, _translator.GetFunction (_lua_state, _stack_pos));
		}
	}
}