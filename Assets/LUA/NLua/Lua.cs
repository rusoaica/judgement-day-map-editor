using NLua.Event;
using NLua.Exceptions;
using NLua.Extensions;
using NLua.Method;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	using LuaHook  = KopiLua.LuaHook;
	using LuaDebug = KopiLua.LuaDebug;
	using LuaNativeFunction = KopiLua.LuaNativeFunction;
#else
    using LuaCore = KeraLua.Lua;
    using LuaDebug = KeraLua.LuaDebug;
    using LuaHook = KeraLua.LuaHook;
    using LuaNativeFunction = KeraLua.LuaNativeFunction;
    using LuaState = KeraLua.LuaState;
#endif

#if !UNITY_3D
    [CLSCompliant(true)]
#endif
    public class Lua : IDisposable
    {
        #region lua debug functions
        /// <summary>
        /// Event that is raised when an exception occures during a hook call.
        /// </summary>
        public event EventHandler<HookExceptionEventArgs> hookException;
        /// <summary>
        /// Event when lua hook callback is called
        /// </summary>
        /// <remarks>
        /// Is only raised if SetDebugHook is called before.
        /// </remarks>
        public event EventHandler<DebugHookEventArgs> debugHook;
        /// <summary>
        /// lua hook calback delegate
        /// </summary>
        private LuaHook _hookCallback = null;
        private LuaNativeFunction _panicCallback;
        private ObjectTranslator _translator;
        private bool _statePassed;
        private bool _executing;
        #endregion
        #region Globals auto-complete
        private readonly List<string> _globals = new List<string>();
        private bool _globalsSorted;
        #endregion
        private LuaState _luaState;
        /// <summary>
        /// True while a script is being executed
        /// </summary>
        public bool IsExecuting { get { return _executing; } }

        private static string initLuanet =
 @"local metatable = {}
        local rawget = rawget
        local import_type = luanet.import_type
        local load_assembly = luanet.load_assembly
        luanet.error, luanet.type = error, type
        -- Lookup a .NET identifier component.
        function metatable:__index(key) -- key is e.g. 'Form'
            -- Get the fully-qualified name, e.g. 'System.Windows.Forms.Form'
            local fqn = rawget(self,'.fqn')
            fqn = ((fqn and fqn .. '.') or '') .. key

            -- Try to find either a luanet function or a CLR type
            local obj = rawget(luanet,key) or import_type(fqn)

            -- If key is neither a luanet function or a CLR type, then it is simply
            -- an identifier component.
            if obj == nil then
                -- It might be an assembly, so we load it too.
                pcall(load_assembly,fqn)
                obj = { ['.fqn'] = fqn }
                setmetatable(obj, metatable)
            end

            -- Cache this lookup
            rawset(self, key, obj)
            return obj
        end

        -- A non-type has been called; e.g. foo = System.Foo()
        function metatable:__call(...)
            error('No such type: ' .. rawget(self,'.fqn'), 2)
        end

        -- This is the root of the .NET namespace
        luanet['.fqn'] = false
        setmetatable(luanet, metatable)

        -- Preload the mscorlib assembly
        luanet.load_assembly('mscorlib')";

        private static string _clrPackage = @"---
--- This lua module provides auto importing of .net classes into a named package.
--- Makes for super easy use of LuaInterface glue
---
--- example:
---   Threading = CLRPackage(""System"", ""System.Threading"")
---   Threading.Thread.Sleep(100)
---
--- Extensions:
--- import() is a version of CLRPackage() which puts the package into a list which is used by a global __index lookup,
--- and thus works rather like C#'s using statement. It also recognizes the case where one is importing a local
--- assembly, which must end with an explicit .dll extension.

--- Alternatively, luanet.namespace can be used for convenience without polluting the global namespace:
---   local sys,sysi = luanet.namespace {'System','System.IO'}
--    sys.Console.WriteLine(""we are at {0}"",sysi.Directory.GetCurrentDirectory())


-- LuaInterface hosted with stock Lua interpreter will need to explicitly require this...
if not luanet then require 'luanet' end

local import_type, load_assembly = luanet.import_type, luanet.load_assembly

local mt = {
	--- Lookup a previously unfound class and add it to our table
	__index = function(package, classname)
		local class = rawget(package, classname)
		if class == nil then
			class = import_type(package.packageName .. ""."" .. classname)
			if class == nil then class = import_type(classname) end
			package[classname] = class		-- keep what we found around, so it will be shared
		end
		return class
	end
}

function luanet.namespace(ns)
    if type(ns) == 'table' then
        local res = {}
        for i = 1,#ns do
            res[i] = luanet.namespace(ns[i])
        end
        return unpack(res)
    end
    -- FIXME - table.packageName could instead be a private index (see Lua 13.4.4)
    local t = { packageName = ns }
    setmetatable(t,mt)
    return t
end

local globalMT, packages

local function set_global_mt()
    packages = {}
    globalMT = {
        __index = function(T,classname)
                for i,package in ipairs(packages) do
                    local class = package[classname]
                    if class then
                        _G[classname] = class
                        return class
                    end
                end
        end
    }
    setmetatable(_G, globalMT)
end

--- Create a new Package class
function CLRPackage(assemblyName, packageName)
  -- a sensible default...
  packageName = packageName or assemblyName
  local ok = pcall(load_assembly,assemblyName)			-- Make sure our assembly is loaded
  return luanet.namespace(packageName)
end

function import (assemblyName, packageName)
    if not globalMT then
        set_global_mt()
    end
    if not packageName then
		local i = assemblyName:find('%.dll$')
		if i then packageName = assemblyName:sub(1,i-1)
		else packageName = assemblyName end
	end
    local t = CLRPackage(assemblyName,packageName)
	table.insert(packages,t)
	return t
end


function luanet.make_array (tp,tbl)
    local arr = tp[#tbl]
	for i,v in ipairs(tbl) do
	    arr:SetValue(v,i-1)
	end
	return arr
end

function luanet.each(o)
   local e = o:GetEnumerator()
   return function()
      if e:MoveNext() then
        return e.Current
     end
   end
end
";

        #region Globals auto-complete
        /// <summary>
        /// An alphabetically sorted list of all globals (objects, methods, etc.) externally added to this Lua instance
        /// </summary>
        /// <remarks>Members of globals are also listed. The formatting is optimized for text input auto-completion.</remarks>
        public IEnumerable<string> Globals
        {
            get
            {
                if (!_globalsSorted)
                {
                    _globals.Sort();
                    _globalsSorted = true;
                }
                return _globals;
            }
        }

        public static string InitLuanet
        {
            get
            {
                return initLuanet;
            }
            set
            {
                initLuanet = value;
            }
        }
        #endregion

        public Lua()
        {
            _luaState = LuaLib.LuaLNewState();
            LuaLib.LuaLOpenLibs(_luaState);
            Init();
            _panicCallback = new LuaNativeFunction(PanicCallback);
            LuaLib.LuaAtPanic(_luaState, _panicCallback);
        }

        /*
		* CAUTION: NLua.Lua instances can't share the same lua state! 
		*/
        public Lua(LuaState _l_state)
        {
            LuaLib.LuaPushString(_l_state, "LUAINTERFACE LOADED");
            LuaLib.LuaGetTable(_l_state, (int)LuaIndexes.Registry);
            if (LuaLib.LuaToBoolean(_l_state, -1))
            {
                LuaLib.LuaSetTop(_l_state, -2);
                throw new LuaException("There is already a NLua.Lua instance associated with this Lua state");
            }
            else
            {
                _luaState = _l_state;
                _statePassed = true;
                LuaLib.LuaSetTop(_luaState, -2);
                Init();
            }
        }

        void Init()
        {
            LuaLib.LuaPushString(_luaState, "LUAINTERFACE LOADED");
            LuaLib.LuaPushBoolean(_luaState, true);
            LuaLib.LuaSetTable(_luaState, (int)LuaIndexes.Registry);
            if (_statePassed == false)
            {
                LuaLib.LuaNewTable(_luaState);
                LuaLib.LuaSetGlobal(_luaState, "luanet");
            }
            LuaLib.LuaNetPushGlobalTable(_luaState);
            LuaLib.LuaGetGlobal(_luaState, "luanet");
            LuaLib.LuaPushString(_luaState, "getmetatable");
            LuaLib.LuaGetGlobal(_luaState, "getmetatable");
            LuaLib.LuaSetTable(_luaState, -3);
            LuaLib.LuaNetPopGlobalTable(_luaState);
            _translator = new ObjectTranslator(this, _luaState);
            ObjectTranslatorPool.Instance.Add(_luaState, _translator);
            LuaLib.LuaNetPopGlobalTable(_luaState);
            LuaLib.LuaLDoString(_luaState, Lua.InitLuanet);
        }

        public void Close()
        {
            if (_statePassed)
                return;
            if (!CheckNull.IsNull(_luaState))
            {
                LuaCore.LuaClose(_luaState);
                ObjectTranslatorPool.Instance.Remove(_luaState);
            }
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback(typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int PanicCallback(LuaState __lua_state)
        {
            string reason = string.Format("unprotected error in call to Lua API ({0})", LuaLib.LuaToString(__lua_state, -1));
            throw new LuaException(reason);
        }

        /// <summary>
        /// Assuming we have a Lua error string sitting on the stack, throw a C# exception out to the user's app
        /// </summary>
        /// <exception cref = "LuaScriptException">Thrown if the script caused an exception</exception>
        private void ThrowExceptionFromError(int _old_top)
        {
            object _err = _translator.GetObject(_luaState, -1);
            LuaLib.LuaSetTop(_luaState, _old_top);
            var _lua_ex = _err as LuaScriptException;
            if (_lua_ex != null)
                throw _lua_ex;
            if (_err == null)
                _err = "Unknown Lua Error";
            throw new LuaScriptException(_err.ToString(), string.Empty);
        }

        /// <summary>
        /// Convert C# exceptions into Lua errors
        /// </summary>
        /// <returns>num of things on stack</returns>
        /// <param name = "_e">null for no pending exception</param>
        internal int SetPendingException(Exception _e)
        {
            var _caught_except = _e;
            if (_caught_except != null)
            {
                _translator.ThrowError(_luaState, _caught_except);
                LuaLib.LuaPushNil(_luaState);
                return 1;
            }
            else
                return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name = "_chunk"></param>
        /// <param name = "_name"></param>
        /// <returns></returns>
        public LuaFunction LoadString(string _chunk, string _name)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            _executing = true;
            try
            {
                if (LuaLib.LuaLLoadBuffer(_luaState, _chunk, _name) != 0)
                    ThrowExceptionFromError(_old_top);
            }
            finally
            {
                _executing = false;
            }

            var result = _translator.GetFunction(_luaState, -1);
            _translator.PopValues(_luaState, _old_top);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name = "_chunk"></param>
        /// <param name = "_name"></param>
        /// <returns></returns>
        public LuaFunction LoadString(byte[] _chunk, string _name)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            _executing = true;
            try
            {
                if (LuaLib.LuaLLoadBuffer(_luaState, _chunk, _name) != 0)
                    ThrowExceptionFromError(_old_top);
            }
            finally
            {
                _executing = false;
            }
            var _result = _translator.GetFunction(_luaState, -1);
            _translator.PopValues(_luaState, _old_top);
            return _result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name = "_fileName"></param>
        /// <returns></returns>
        public LuaFunction LoadFile(string _fileName)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            if (LuaLib.LuaLLoadFile(_luaState, _fileName) != 0)
                ThrowExceptionFromError(_old_top);
            var _result = _translator.GetFunction(_luaState, -1);
            _translator.PopValues(_luaState, _old_top);
            return _result;
        }

        /// <summary>
        /// Executes a Lua chunk and returns all the chunk's return values in an array.
        /// </summary>
        /// <param name = "_chunk">Chunk to execute</param>
        /// <param name = "chunkName">Name to associate with the chunk. Defaults to "chunk".</param>
        /// <returns></returns>
        public object[] DoString(byte[] _chunk, string _chunk_name = "chunk")
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            _executing = true;
            if (LuaLib.LuaLLoadBuffer(_luaState, _chunk, _chunk_name) == 0)
            {
                try
                {
                    if (LuaLib.LuaPCall(_luaState, 0, -1, 0) == 0)
                        return _translator.PopValues(_luaState, _old_top);
                    else
                        ThrowExceptionFromError(_old_top);
                }
                finally
                {
                    _executing = false;
                }
            }
            else
                ThrowExceptionFromError(_old_top);
            return null;            
        }

        /// <summary>
        /// Executes a Lua chunk and returns all the chunk's return values in an array.
        /// </summary>
        /// <param name = "_chunk">Chunk to execute</param>
        /// <param name = "_chunk_name">Name to associate with the chunk. Defaults to "chunk".</param>
        /// <returns></returns>
        public object[] DoString(string _chunk, string _chunk_name = "chunk")
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            _executing = true;
            if (LuaLib.LuaLLoadBuffer(_luaState, _chunk, _chunk_name) == 0)
            {
                try
                {
                    if (LuaLib.LuaPCall(_luaState, 0, -1, 0) == 0)
                        return _translator.PopValues(_luaState, _old_top);
                    else
                        ThrowExceptionFromError(_old_top);
                }
                finally
                {
                    _executing = false;
                }
            }
            else
                ThrowExceptionFromError(_old_top);
            return null;  
        }

        /*
		* Excutes a Lua file and returns all the chunk's return
		* values in an array
		*/
        public object[] DoFile(string _fileName)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            if (LuaLib.LuaLLoadFile(_luaState, _fileName) == 0)
            {
                _executing = true;
                try
                {
                    if (LuaLib.LuaPCall(_luaState, 0, -1, 0) == 0)
                        return _translator.PopValues(_luaState, _old_top);
                    else
                        ThrowExceptionFromError(_old_top);
                }
                finally
                {
                    _executing = false;
                }
            }
            else
                ThrowExceptionFromError(_old_top);
            return null;            
        }

        /*
		* Indexer for global variables from the LuaInterpreter
		* Supports navigation of tables by using . operator
		*/
        public object this[string _fullPath]
        {
            get
            {
                object _return_value = null;
                int _old_top = LuaLib.LuaGetTop(_luaState);
                string[] _path = FullPathToArray(_fullPath);
                LuaLib.LuaGetGlobal(_luaState, _path[0]);
                _return_value = _translator.GetObject(_luaState, -1);
                LuaBase _dispose = null;
                if (_path.Length > 1)
                {
                    _dispose = _return_value as LuaBase;
                    string[] _remaining_path = new string[_path.Length - 1];
                    Array.Copy(_path, 1, _remaining_path, 0, _path.Length - 1);
                    _return_value = GetObject(_remaining_path);
                    if (_dispose != null)
                        _dispose.Dispose();
                }
                LuaLib.LuaSetTop(_luaState, _old_top);
                return _return_value;
            }
            set
            {
                int _old_top = LuaLib.LuaGetTop(_luaState);
                string[] _path = FullPathToArray(_fullPath);
                if (_path.Length == 1)
                {
                    _translator.Push(_luaState, value);
                    LuaLib.LuaSetGlobal(_luaState, _fullPath);
                }
                else
                {
                    LuaLib.LuaGetGlobal(_luaState, _path[0]);
                    string[] _remaining_path = new string[_path.Length - 1];
                    Array.Copy(_path, 1, _remaining_path, 0, _path.Length - 1);
                    SetObject(_remaining_path, value);
                }
                LuaLib.LuaSetTop(_luaState, _old_top);
                if (value == null)
                    _globals.Remove(_fullPath);
                else
                {
                    if (!_globals.Contains(_fullPath))
                        RegisterGlobal(_fullPath, value.GetType(), 0);
                }
            }
        }

        /// <summary>
        /// Adds an entry to <see cref = "_globals"/> (recursivley handles 2 levels of members)
        /// </summary>
        /// <param name = "_path">The index accessor path ot the entry</param>
        /// <param name = "_type">The type of the entry</param>
        /// <param name = "_recursion_counter">How deep have we gone with recursion?</param>
        private void RegisterGlobal(string _path, Type _type, int _recursion_counter)
        {
            if (_type == typeof(LuaNativeFunction))
                _globals.Add(_path + "(");
            else if ((_type.IsClass || _type.IsInterface) && _type != typeof(string) && _recursion_counter < 2)
            {
                foreach (var method in _type.GetMethods(BindingFlags.Public | BindingFlags.Instance))
                {
                    string name = method.Name;
                    if ((method.GetCustomAttributes(typeof(LuaHideAttribute), false).Length == 0) && (method.GetCustomAttributes(typeof(LuaGlobalAttribute), false).Length == 0) && name != "GetType" && name != "GetHashCode" && 
                        name != "Equals" && name != "ToString" && name != "Clone" && name != "Dispose" && name != "GetEnumerator" && name != "CopyTo" && !name.StartsWith("get_", StringComparison.Ordinal) &&
                        !name.StartsWith("set_", StringComparison.Ordinal) && !name.StartsWith("add_", StringComparison.Ordinal) && !name.StartsWith("remove_", StringComparison.Ordinal))
                    {
                        string _command = _path + ":" + name + "(";
                        if (method.GetParameters().Length == 0)
                            _command += ")";
                        _globals.Add(_command);
                    }
                }
                foreach (var _field in _type.GetFields(BindingFlags.Public | BindingFlags.Instance))
                    if ((_field.GetCustomAttributes(typeof(LuaHideAttribute), false).Length == 0) && (_field.GetCustomAttributes(typeof(LuaGlobalAttribute), false).Length == 0))
                        RegisterGlobal(_path + "." + _field.Name, _field.FieldType, _recursion_counter + 1);
                foreach (var _property in _type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                    if ((_property.GetCustomAttributes(typeof(LuaHideAttribute), false).Length == 0) && (_property.GetCustomAttributes(typeof(LuaGlobalAttribute), false).Length == 0) && _property.Name != "Item")
                        RegisterGlobal(_path + "." + _property.Name, _property.PropertyType, _recursion_counter + 1);
            }
            else
                _globals.Add(_path);
            _globalsSorted = false;
        }

        /*
		* Navigates a table in the top of the stack, returning
		* the value of the specified field
		*/
        internal object GetObject(string[] _remaining_path)
        {
            object _return_value = null;

            for (int i = 0; i < _remaining_path.Length; i++)
            {
                LuaLib.LuaPushString(_luaState, _remaining_path[i]);
                LuaLib.LuaGetTable(_luaState, -2);
                _return_value = _translator.GetObject(_luaState, -1);
                if (_return_value == null)
                    break;
            }
            return _return_value;
        }

        /*
		* Gets a numeric global variable
		*/
        public double GetNumber(string _full_path)
        {
            return (double)this[_full_path];
        }

        /*
		* Gets a string global variable
		*/
        public string GetString(string _full_path)
        {
            return this[_full_path].ToString();
        }

        /*
		* Gets a table global variable
		*/
        public LuaTable GetTable(string _full_path)
        {
            return (LuaTable)this[_full_path];
        }

        /*
		* Gets a table global variable as an object implementing
		* the interfaceType interface
		*/
        public object GetTable(Type _interface_type, string _full_path)
        {
            return CodeGeneration.Instance.GetClassInstance(_interface_type, GetTable(_full_path));
        }

        /*
		* Gets a function global variable
		*/
        public LuaFunction GetFunction(string _full_path)
        {
            object _obj = this[_full_path];
            return (_obj is LuaNativeFunction ? new LuaFunction((LuaNativeFunction)_obj, this) : (LuaFunction)_obj);
        }

        /*
		* Register a delegate type to be used to convert Lua functions to C# delegates (useful for iOS where there is no dynamic code generation)
		* type delegateType
		*/
        public void RegisterLuaDelegateType(Type _delegate_type, Type _lua_delegate_type)
        {
            CodeGeneration.Instance.RegisterLuaDelegateType(_delegate_type, _lua_delegate_type);
        }

        public void RegisterLuaClassType(Type _class, Type _lua_class)
        {
            CodeGeneration.Instance.RegisterLuaClassType(_class, _lua_class);
        }

        public void LoadCLRPackage()
        {
            LuaLib.LuaLDoString(_luaState, Lua._clrPackage);
        }

        /*
		* Gets a function global variable as a delegate of
		* type delegateType
		*/
        public Delegate GetFunction(Type _delegate_type, string _full_path)
        {
            return CodeGeneration.Instance.GetDelegate(_delegate_type, GetFunction(_full_path));
        }

        /*
		* Calls the object as a function with the provided arguments, 
		* returning the function's returned values inside an array
		*/
        internal object[] CallFunction(object _function, object[] _args)
        {
            return CallFunction(_function, _args, null);
        }

        /*
		* Calls the object as a function with the provided arguments and
		* casting returned values to the types in returnTypes before returning
		* them in an array
		*/
        internal object[] CallFunction(object _function, object[] _args, Type[] _return_types)
        {
            int _n_args = 0;
            int _old_top = LuaLib.LuaGetTop(_luaState);
            if (!LuaLib.LuaCheckStack(_luaState, _args.Length + 6))
                throw new LuaException("Lua stack overflow");
            _translator.Push(_luaState, _function);
            if (_args != null)
            {
                _n_args = _args.Length;
                for (int i = 0; i < _args.Length; i++)
                    _translator.Push(_luaState, _args[i]);
            }
            _executing = true;
            try
            {
                int _error = LuaLib.LuaPCall(_luaState, _n_args, -1, 0);
                if (_error != 0)
                    ThrowExceptionFromError(_old_top);
            }
            finally
            {
                _executing = false;
            }
            return _return_types != null ? _translator.PopValues(_luaState, _old_top, _return_types) : _translator.PopValues(_luaState, _old_top);
        }

        /*
		* Navigates a table to set the value of one of its fields
		*/
        internal void SetObject(string[] _remaining_path, object _val)
        {
            for (int i = 0; i < _remaining_path.Length - 1; i++)
            {
                LuaLib.LuaPushString(_luaState, _remaining_path[i]);
                LuaLib.LuaGetTable(_luaState, -2);
            }
            LuaLib.LuaPushString(_luaState, _remaining_path[_remaining_path.Length - 1]);
            _translator.Push(_luaState, _val);
            LuaLib.LuaSetTable(_luaState, -3);
        }

        string[] FullPathToArray(string _full_path)
        {
            return _full_path.SplitWithEscape('.', '\\').ToArray();
        }

        /*
		* Creates a new table as a global variable or as a field
		* inside an existing table
		*/
        public void NewTable(string _full_path)
        {
            string[] _path = FullPathToArray(_full_path);
            int _old_top = LuaLib.LuaGetTop(_luaState);
            if (_path.Length == 1)
            {
                LuaLib.LuaNewTable(_luaState);
                LuaLib.LuaSetGlobal(_luaState, _full_path);
            }
            else
            {
                LuaLib.LuaGetGlobal(_luaState, _path[0]);
                for (int i = 1; i < _path.Length - 1; i++)
                {
                    LuaLib.LuaPushString(_luaState, _path[i]);
                    LuaLib.LuaGetTable(_luaState, -2);
                }
                LuaLib.LuaPushString(_luaState, _path[_path.Length - 1]);
                LuaLib.LuaNewTable(_luaState);
                LuaLib.LuaSetTable(_luaState, -3);
            }
            LuaLib.LuaSetTop(_luaState, _old_top);
        }

        public Dictionary<object, object> GetTableDict(LuaTable _table)
        {
            var _dict = new Dictionary<object, object>();
            int _old_top = LuaLib.LuaGetTop(_luaState);
            _translator.Push(_luaState, _table);
            LuaLib.LuaPushNil(_luaState);
            while (LuaLib.LuaNext(_luaState, -2) != 0)
            {
                _dict[_translator.GetObject(_luaState, -2)] = _translator.GetObject(_luaState, -1);
                LuaLib.LuaSetTop(_luaState, -2);
            }
            LuaLib.LuaSetTop(_luaState, _old_top);
            return _dict;
        }

        #region lua debug functions
        /// <summary>
        /// Activates the debug hook
        /// </summary>
        /// <param name = "_mask">Mask</param>
        /// <param name = "_count">Count</param>
        /// <returns>see lua docs. -1 if hook is already set</returns>
        public int SetDebugHook(EventMasks _mask, int _count)
        {
            if (_hookCallback == null)
            {
                _hookCallback = new LuaHook(Lua.DebugHookCallback);
                return LuaCore.LuaSetHook(_luaState, _hookCallback, (int)_mask, _count);
            }
            return -1;
        }

        /// <summary>
        /// Removes the debug hook
        /// </summary>
        /// <returns>see lua docs</returns>
        public int RemoveDebugHook()
        {
            _hookCallback = null;
            return LuaCore.LuaSetHook(_luaState, null, 0, 0);
        }

        /// <summary>
        /// Gets the hook mask.
        /// </summary>
        /// <returns>hook mask</returns>
        public EventMasks GetHookMask()
        {
            return (EventMasks)LuaCore.LuaGetHookMask(_luaState);
        }

        /// <summary>
        /// Gets the hook count
        /// </summary>
        /// <returns>see lua docs</returns>
        public int GetHookCount()
        {
            return LuaCore.LuaGetHookCount(_luaState);
        }


        /// <summary>
        /// Gets local (see lua docs)
        /// </summary>
        /// <param name = "_lua_debug">lua debug structure</param>
        /// <param name = "_n">see lua docs</param>
        /// <returns>see lua docs</returns>
        public string GetLocal(LuaDebug _lua_debug, int _n)
        {
            return LuaCore.LuaGetLocal(_luaState, _lua_debug, _n).ToString();
        }

        /// <summary>
        /// Sets local (see lua docs)
        /// </summary>
        /// <param name = "_lua_debug">lua debug structure</param>
        /// <param name = "_n">see lua docs</param>
        /// <returns>see lua docs</returns>
        public string SetLocal(LuaDebug _lua_debug, int _n)
        {
            return LuaCore.LuaSetLocal(_luaState, _lua_debug, _n).ToString();
        }

        public int GetStack(int _level, ref LuaDebug _ar)
        {
            return LuaCore.LuaGetStack(_luaState, _level, ref _ar);
        }

        public int GetInfo(string what, ref LuaDebug ar)
        {
            return LuaCore.LuaGetInfo(_luaState, what, ref ar);
        }

        /// <summary>
        /// Gets up value (see lua docs)
        /// </summary>
        /// <param name = "_func_index">see lua docs</param>
        /// <param name = "_n">see lua docs</param>
        /// <returns>see lua docs</returns>
        public string GetUpValue(int _func_index, int _n)
        {
            return LuaCore.LuaGetUpValue(_luaState, _func_index, _n).ToString();
        }

        /// <summary>
        /// Sets up value (see lua docs)
        /// </summary>
        /// <param name = "_func_index">see lua docs</param>
        /// <param name = "_n">see lua docs</param>
        /// <returns>see lua docs</returns>
        public string SetUpValue(int _func_index, int _n)
        {
            return LuaCore.LuaSetUpValue(_luaState, _func_index, _n).ToString();
        }

        /// <summary>
        /// Delegate that is called on lua hook callback
        /// </summary>
        /// <param name = "_lua_state">lua state</param>
        /// <param name = "_lua_debug">Pointer to LuaDebug (lua_debug) structure</param>
        /// 
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaHook))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
#if USE_KOPILUA
		static void DebugHookCallback (LuaState _lua_state, LuaDebug _debug)
		{
#else
        static void DebugHookCallback(LuaState _lua_state, IntPtr _lua_debug)
        {
            LuaDebug _debug = (LuaDebug)System.Runtime.InteropServices.Marshal.PtrToStructure(_lua_debug, typeof(LuaDebug));
#endif
            ObjectTranslator _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            Lua _lua = _translator.Interpreter;
            _lua.DebugHookCallbackInternal(_lua_state, _debug);
        }

        private void DebugHookCallbackInternal(LuaState _lua_state, LuaDebug _lua_debug)
        {
            try
            {
                var _temp = debugHook;
                if (_temp != null)
                    _temp(this, new DebugHookEventArgs(_lua_debug));
            }
            catch (Exception ex)
            {
                OnHookException(new HookExceptionEventArgs(ex));
            }
        }

        private void OnHookException(HookExceptionEventArgs e)
        {
            var _temp = hookException;
            if (_temp != null)
                _temp(this, e);
        }

        /// <summary>
        /// Pops a value from the lua stack.
        /// </summary>
        /// <returns>Returns the top value from the lua stack.</returns>
        public object Pop()
        {
            int _top = LuaLib.LuaGetTop(_luaState);
            return _translator.PopValues(_luaState, _top - 1)[0];
        }

        /// <summary>
        /// Pushes a value onto the lua stack.
        /// </summary>
        /// <param name = "_value">Value to push.</param>
        public void Push(object _value)
        {
            _translator.Push(_luaState, _value);
        }
        #endregion

        internal void DisposeInternal(int _reference)
        {
            if (!CheckNull.IsNull(_luaState)) 
                LuaLib.LuaUnref(_luaState, _reference);
        }

        /*
		 * Gets a field of the table corresponding to the provided reference
		 * using rawget (do not use metatables)
		 */
        internal object RawGetObject(int _reference, string _field)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            LuaLib.LuaGetRef(_luaState, _reference);
            LuaLib.LuaPushString(_luaState, _field);
            LuaLib.LuaRawGet(_luaState, -2);
            object _obj = _translator.GetObject(_luaState, -1);
            LuaLib.LuaSetTop(_luaState, _old_top);
            return _obj;
        }

        /*
		 * Gets a field of the table or userdata corresponding to the provided reference
		 */
        internal object GetObject(int _reference, string _field)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            LuaLib.LuaGetRef(_luaState, _reference);
            object returnValue = GetObject(FullPathToArray(_field));
            LuaLib.LuaSetTop(_luaState, _old_top);
            return returnValue;
        }

        /*
		 * Gets a numeric field of the table or userdata corresponding the the provided reference
		 */
        internal object GetObject(int _reference, object _field)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            LuaLib.LuaGetRef(_luaState, _reference);
            _translator.Push(_luaState, _field);
            LuaLib.LuaGetTable(_luaState, -2);
            object _return_value = _translator.GetObject(_luaState, -1);
            LuaLib.LuaSetTop(_luaState, _old_top);
            return _return_value;
        }

        /*
		 * Sets a field of the table or userdata corresponding the the provided reference
		 * to the provided value
		 */
        internal void SetObject(int _reference, string _field, object _val)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            LuaLib.LuaGetRef(_luaState, _reference);
            SetObject(FullPathToArray(_field), _val);
            LuaLib.LuaSetTop(_luaState, _old_top);
        }

        /*
		 * Sets a numeric field of the table or userdata corresponding the the provided reference
		 * to the provided value
		 */
        internal void SetObject(int _reference, object _field, object _val)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            LuaLib.LuaGetRef(_luaState, _reference);
            _translator.Push(_luaState, _field);
            _translator.Push(_luaState, _val);
            LuaLib.LuaSetTable(_luaState, -3);
            LuaLib.LuaSetTop(_luaState, _old_top);
        }

        public LuaFunction RegisterFunction(string _path, MethodBase _function)
        {
            return RegisterFunction(_path, null, _function);
        }

        /*
		 * Registers an object's method as a Lua function (global or table field)
		 * The method may have any signature
		 */
        public LuaFunction RegisterFunction(string _path, object _target, MethodBase _function)
        {
            int _old_top = LuaLib.LuaGetTop(_luaState);
            var _wrapper = new LuaMethodWrapper(_translator, _target, _function.DeclaringType, _function);
            _translator.Push(_luaState, new LuaNativeFunction(_wrapper.invokeFunction));
            this[_path] = _translator.GetObject(_luaState, -1);
            var _f = GetFunction(_path);
            LuaLib.LuaSetTop(_luaState, _old_top);
            return _f;
        }

        /*
		 * Compares the two values referenced by ref1 and ref2 for equality
		 */
        internal bool CompareRef(int _ref_1, int _ref_2)
        {
            int _top = LuaLib.LuaGetTop(_luaState);
            LuaLib.LuaGetRef(_luaState, _ref_1);
            LuaLib.LuaGetRef(_luaState, _ref_2);
            int _equal = LuaLib.LuaEqual(_luaState, -1, -2);
            LuaLib.LuaSetTop(_luaState, _top);
            return (_equal != 0);
        }

        internal void PushCSFunction(LuaNativeFunction _function)
        {
            _translator.PushFunction(_luaState, _function);
        }

        #region IDisposable Members
        public virtual void Dispose()
        {
            if (_translator != null)
            {
                _translator.pendingEvents.Dispose();
                _translator = null;
            }

            Close();
            GC.WaitForPendingFinalizers();
        }
        #endregion
    }
}