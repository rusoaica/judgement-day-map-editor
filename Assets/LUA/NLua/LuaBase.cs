﻿using System;

namespace NLua
{
    public abstract class LuaBase : IDisposable
    {
        private bool _disposed;
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        protected int _Reference;
#if !UNITY_3D
        [CLSCompliantAttribute(false)]
#endif
        protected Lua _Interpreter;

        ~LuaBase()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool _dispose_managed_resources)
        {
            if (!_disposed)
            {
                if (_dispose_managed_resources)
                    if (_Reference != 0)
                        _Interpreter.DisposeInternal(_Reference);
                _Interpreter = null;
                _disposed = true;
            }
        }

        public override bool Equals(object _o)
        {
            if (_o is LuaBase)
            {
                var _l = (LuaBase)_o;
                return _Interpreter.CompareRef(_l._Reference, _Reference);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return _Reference;
        }
    }
}