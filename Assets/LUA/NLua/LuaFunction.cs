﻿using System;

namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	using LuaNativeFunction = KopiLua.LuaNativeFunction;
#else
    using LuaNativeFunction = KeraLua.LuaNativeFunction;
    using LuaState = KeraLua.LuaState;
#endif

    public class LuaFunction : LuaBase
    {
        internal LuaNativeFunction function;

        public LuaFunction(int _reference, Lua _interpreter)
        {
            _Reference = _reference;
            this.function = null;
            _Interpreter = _interpreter;
        }

        public LuaFunction(LuaNativeFunction _function, Lua _interpreter)
        {
            _Reference = 0;
            this.function = _function;
            _Interpreter = _interpreter;
        }

        /*
		 * Calls the function casting return values to the types
		 * in returnTypes
		 */
        internal object[] Call(object[] _args, Type[] _return_types)
        {
            return _Interpreter.CallFunction(this, _args, _return_types);
        }

        /*
		 * Calls the function and returns its return values inside
		 * an array
		 */
        public object[] Call(params object[] _args)
        {
            return _Interpreter.CallFunction(this, _args);
        }

        /*
		 * Pushes the function into the Lua stack
		 */
        internal void Push(LuaState _lua_state)
        {
            if (_Reference != 0)
                LuaLib.LuaGetRef(_lua_state, _Reference);
            else
                _Interpreter.PushCSFunction(function);
        }

        public override string ToString()
        {
            return "function";
        }

        public override bool Equals(object _o)
        {
            if (_o is LuaFunction)
            {
                var _l = (LuaFunction)_o;
                if (this._Reference != 0 && _l._Reference != 0)
                    return _Interpreter.CompareRef(_l._Reference, this._Reference);
                else
                    return this.function == _l.function;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return _Reference != 0 ? _Reference : function.GetHashCode();
        }
    }
}