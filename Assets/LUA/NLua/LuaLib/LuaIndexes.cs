namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
#else
    using LuaCore = KeraLua.Lua;
    using LuaState = KeraLua.LuaState;
#endif

    public class LuaIndexes
    {
        private static int _registryIndex = 0;

        public static int Registry
        {
            get
            {
                if (_registryIndex != 0)
                    return _registryIndex;
                _registryIndex = LuaCore.LuaNetRegistryIndex();
                return _registryIndex;
            }
        }
    }
}