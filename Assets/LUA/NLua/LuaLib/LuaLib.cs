namespace NLua
{

#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	using LuaTag = KopiLua.LuaTag;
	using LuaNativeFunction = KopiLua.LuaNativeFunction;
#else
    using LuaCore = KeraLua.Lua;
    using LuaState = KeraLua.LuaState;
    using LuaTag = KeraLua.LuaTag;
    using LuaNativeFunction = KeraLua.LuaNativeFunction;
#endif



    public class LuaLib
    {
        public static int LuaGC(LuaState _lua_state, GCOptions _what, int _data)
        {
            return LuaCore.LuaGC(_lua_state, (int)_what, _data);
        }

        public static string LuaTypeName(LuaState _lua_state, LuaTypes _type)
        {
            return LuaCore.LuaTypeName(_lua_state, (int)_type).ToString();
        }

        public static string LuaLTypeName(LuaState _lua_state, int _stack_pos)
        {
            return LuaTypeName(_lua_state, LuaType(_lua_state, _stack_pos));
        }

        public static void LuaLError(LuaState _lua_state, string _message)
        {
            LuaCore.LuaLError(_lua_state, _message);
        }

        public static void LuaLWhere(LuaState _lua_state, int _level)
        {
            LuaCore.LuaLWhere(_lua_state, _level);
        }

        public static LuaState LuaLNewState()
        {
            return LuaCore.LuaLNewState();
        }

        public static void LuaLOpenLibs(LuaState _lua_state)
        {
            LuaCore.LuaLOpenLibs(_lua_state);
        }

        public static int LuaLLoadString(LuaState _lua_state, string _chunk)
        {
            return LuaCore.LuaLLoadString(_lua_state, _chunk);
        }

        public static int LuaLLoadString(LuaState _lua_state, byte[] _chunk)
        {
            return LuaCore.LuaLLoadString(_lua_state, _chunk);
        }

        public static int LuaLDoString(LuaState _lua_state, string _chunk)
        {
            int _result = LuaLLoadString(_lua_state, _chunk);
            if (_result != 0)
                return _result;

            return LuaPCall(_lua_state, 0, -1, 0);
        }

        public static int LuaLDoString(LuaState _lua_state, byte[] _chunk)
        {
            int _result = LuaLLoadString(_lua_state, _chunk);
            if (_result != 0)
                return _result;

            return LuaPCall(_lua_state, 0, -1, 0);
        }

        public static void LuaCreateTable(LuaState _lua_state, int _n_arr, int _n_rec)
        {
            LuaCore.LuaCreateTable(_lua_state, _n_arr, _n_rec);
        }

        public static void LuaNewTable(LuaState _lua_state)
        {
            LuaCreateTable(_lua_state, 0, 0);
        }

        public static int LuaLDoFile(LuaState _lua_state, string _filename)
        {
            int _result = LuaCore.LuaNetLoadFile(_lua_state, _filename);
            if (_result != 0)
                return _result;
            return LuaCore.LuaNetPCall(_lua_state, 0, -1, 0);
        }

        public static void LuaGetGlobal(LuaState _lua_state, string _name)
        {
            LuaCore.LuaNetGetGlobal(_lua_state, _name);
        }

        public static void LuaSetGlobal(LuaState _lua_state, string _name)
        {
            LuaCore.LuaNetSetGlobal(_lua_state, _name);
        }

        public static void LuaSetTop(LuaState _lua_state, int _new_top)
        {
            LuaCore.LuaSetTop(_lua_state, _new_top);
        }

        public static void LuaPop(LuaState _lua_state, int _amount)
        {
            LuaSetTop(_lua_state, -(_amount) - 1);
        }

        public static void LuaInsert(LuaState _lua_state, int _new_top)
        {
            LuaCore.LuaInsert(_lua_state, _new_top);
        }

        public static void LuaRemove(LuaState _lua_state, int _index)
        {
            LuaCore.LuaRemove(_lua_state, _index);
        }

        public static void LuaGetTable(LuaState _lua_state, int _index)
        {
            LuaCore.LuaGetTable(_lua_state, _index);
        }

        public static void LuaRawGet(LuaState _lua_state, int _index)
        {
            LuaCore.LuaRawGet(_lua_state, _index);
        }

        public static void LuaSetTable(LuaState _lua_state, int _index)
        {
            LuaCore.LuaSetTable(_lua_state, _index);
        }

        public static void LuaRawSet(LuaState _lua_state, int _index)
        {
            LuaCore.LuaRawSet(_lua_state, _index);
        }

        public static void LuaSetMetatable(LuaState _lua_state, int _obj_index)
        {
            LuaCore.LuaSetMetatable(_lua_state, _obj_index);
        }

        public static int LuaGetMetatable(LuaState _lua_state, int _obj_index)
        {
            return LuaCore.LuaGetMetatable(_lua_state, _obj_index);
        }

        public static int LuaEqual(LuaState _lua_state, int _index_1, int _index_2)
        {
            return LuaCore.LuaNetEqual(_lua_state, _index_1, _index_2);
        }

        public static void LuaPushValue(LuaState _lua_state, int _index)
        {
            LuaCore.LuaPushValue(_lua_state, _index);
        }

        public static void LuaReplace(LuaState _lua_state, int _index)
        {
            LuaCore.LuaReplace(_lua_state, _index);
        }

        public static int LuaGetTop(LuaState _lua_state)
        {
            return LuaCore.LuaGetTop(_lua_state);
        }

        public static LuaTypes LuaType(LuaState _lua_state, int _index)
        {
            return (LuaTypes)LuaCore.LuaType(_lua_state, _index);
        }

        public static bool LuaIsNil(LuaState _lua_state, int _index)
        {
            return LuaType(_lua_state, _index) == LuaTypes.Nil;
        }

        public static bool LuaIsNumber(LuaState _lua_state, int _index)
        {
            return LuaType(_lua_state, _index) == LuaTypes.Number;
        }

        public static bool LuaIsBoolean(LuaState _lua_state, int _index)
        {
            return LuaType(_lua_state, _index) == LuaTypes.Boolean;
        }

        public static int LuaLRef(LuaState _lua_state, int _registry_index)
        {
            return LuaCore.LuaLRef(_lua_state, _registry_index);
        }

        public static int LuaRef(LuaState _lua_state, int _lock_ref)
        {
            return _lock_ref != 0 ? LuaLRef(_lua_state, (int)LuaIndexes.Registry) : 0;
        }

        public static void LuaRawGetI(LuaState _lua_state, int _table_index, int _index)
        {
            LuaCore.LuaRawGetI(_lua_state, _table_index, _index);
        }

        public static void LuaRawSetI(LuaState _lua_state, int _table_index, int _index)
        {
            LuaCore.LuaRawSetI(_lua_state, _table_index, _index);
        }

        public static object LuaNewUserData(LuaState _lua_state, int _size)
        {
            return LuaCore.LuaNewUserData(_lua_state, (uint)_size);
        }

        public static object LuaToUserData(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaToUserData(_lua_state, _index);
        }

        public static void LuaGetRef(LuaState _lua_state, int _reference)
        {
            LuaRawGetI(_lua_state, (int)LuaIndexes.Registry, _reference);
        }

        public static void LuaUnref(LuaState _lua_state, int _reference)
        {
            LuaCore.LuaLUnref(_lua_state, (int)LuaIndexes.Registry, _reference);
        }

        public static bool LuaIsString(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaIsString(_lua_state, _index) != 0;
        }

        public static bool LuaNetIsStringStrict(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaNetIsStringStrict(_lua_state, _index) != 0;
        }

        public static bool LuaIsCFunction(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaIsCFunction(_lua_state, _index);
        }

        public static void LuaPushNil(LuaState _lua_state)
        {
            LuaCore.LuaPushNil(_lua_state);
        }

        public static void LuaPushStdCallCFunction(LuaState _lua_state, LuaNativeFunction _function)
        {
            LuaCore.LuaPushStdCallCFunction(_lua_state, _function);
        }

        public static int LuaPCall(LuaState _lua_state, int _n_args, int _n_results, int _err_func)
        {
            return LuaCore.LuaNetPCall(_lua_state, _n_args, _n_results, _err_func);
        }

        public static LuaNativeFunction LuaToCFunction(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaToCFunction(_lua_state, _index);
        }

        public static double LuaToNumber(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaNetToNumber(_lua_state, _index);
        }

        public static bool LuaToBoolean(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaToBoolean(_lua_state, _index) != 0;
        }

        public static string LuaToString(LuaState _lua_state, int _index)
        {
            var _t = LuaType(_lua_state, _index);
            if (_t == LuaTypes.Number)
                return string.Format("{0}", LuaToNumber(_lua_state, _index));
            else if (_t == LuaTypes.String)
            {
                uint _str_len;
                return LuaCore.LuaToLString(_lua_state, _index, out _str_len).ToString((int)_str_len);
            }
            else if (_t == LuaTypes.Nil)
                return null;         
            else
                return "0"; 
        }

        public static void LuaAtPanic(LuaState _lua_state, LuaNativeFunction _panic_f)
        {
            LuaCore.LuaAtPanic(_lua_state, (LuaNativeFunction)_panic_f);
        }

        public static void LuaPushNumber(LuaState _lua_state, double _number)
        {
            LuaCore.LuaPushNumber(_lua_state, _number);
        }

        public static void LuaPushBoolean(LuaState _lua_state, bool _value)
        {
            LuaCore.LuaPushBoolean(_lua_state, _value ? 1 : 0);
        }

        public static void LuaPushString(LuaState _lua_state, string _str)
        {
            LuaCore.LuaPushString(_lua_state, _str);
        }

        public static int LuaLNewMetatable(LuaState _lua_state, string _meta)
        {
            return LuaCore.LuaLNewMetatable(_lua_state, _meta);
        }

        public static void LuaGetField(LuaState _lua_state, int _stack_pos, string _meta)
        {
            LuaCore.LuaGetField(_lua_state, _stack_pos, _meta);
        }

        public static void LuaLGetMetatable(LuaState _lua_state, string _meta)
        {
            LuaGetField(_lua_state, (int)LuaIndexes.Registry, _meta);
        }

        public static object LuaLCheckUData(LuaState _lua_state, int _stack_pos, string _meta)
        {
            return LuaCore.LuaLCheckUData(_lua_state, _stack_pos, _meta);
        }

        public static bool LuaLGetMetafield(LuaState _lua_state, int _stack_pos, string _field)
        {
            return LuaCore.LuaLGetMetafield(_lua_state, _stack_pos, _field) != 0;
        }

        public static int LuaLLoadBuffer(LuaState _lua_state, string _buff, string _name)
        {
            return LuaCore.LuaNetLoadBuffer(_lua_state, _buff, (uint)0, _name);
        }

        public static int LuaLLoadBuffer(LuaState _lua_state, byte[] _buff, string _name)
        {
            return LuaCore.LuaNetLoadBuffer(_lua_state, _buff, (uint)_buff.Length, _name);
        }

        public static int LuaLLoadFile(LuaState _lua_state, string _filename)
        {
            return LuaCore.LuaNetLoadFile(_lua_state, _filename);
        }

        public static bool LuaLCheckMetatable(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaLCheckMetatable(_lua_state, _index);
        }

        public static int LuaNetRegistryIndex()
        {
            return LuaCore.LuaNetRegistryIndex();
        }

        public static int LuaNetToNetObject(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaNetToNetObject(_lua_state, _index);
        }

        public static void LuaNetNewUData(LuaState _lua_state, int _val)
        {
            LuaCore.LuaNetNewUData(_lua_state, _val);
        }

        public static int LuaNetRawNetObj(LuaState _lua_state, int _obj)
        {
            return LuaCore.LuaNetRawNetObj(_lua_state, _obj);
        }

        public static int LuaNetCheckUData(LuaState _lua_state, int _ud, string _t_name)
        {
            return LuaCore.LuaNetCheckUData(_lua_state, _ud, _t_name);
        }

        public static void LuaError(LuaState _lua_state)
        {
            LuaCore.LuaError(_lua_state);
        }

        public static bool LuaCheckStack(LuaState _lua_state, int _extra)
        {
            return LuaCore.LuaCheckStack(_lua_state, _extra) != 0;
        }

        public static int LuaNext(LuaState _lua_state, int _index)
        {
            return LuaCore.LuaNext(_lua_state, _index);
        }

        public static void LuaPushLightUserData(LuaState _lua_state, LuaTag _u_data)
        {
            LuaCore.LuaPushLightUserData(_lua_state, _u_data.Tag);
        }

        public static LuaTag LuaNetGetTag()
        {
            return LuaCore.LuaNetGetTag();
        }

        public static void LuaNetPushGlobalTable(LuaState _lua_state)
        {
            LuaCore.LuaNetPushGlobalTable(_lua_state);
        }

        public static void LuaNetPopGlobalTable(LuaState _lua_state)
        {
            LuaCore.LuaNetPopGlobalTable(_lua_state);
        }
    }
}