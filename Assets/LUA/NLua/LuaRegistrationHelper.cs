﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
#if SILVERLIGHT
using System.Linq;
#endif

namespace NLua
{
    public static class LuaRegistrationHelper
    {
        #region Tagged instance methods
        /// <summary>
        /// Registers all public instance methods in an object tagged with <see cref="LuaGlobalAttribute"/> as Lua global functions
        /// </summary>
        /// <param name="_lua">The Lua VM to add the methods to</param>
        /// <param name="_o">The object to get the methods from</param>
        public static void TaggedInstanceMethods(Lua _lua, object _o)
        {
            #region Sanity checks
            if (_lua == null)
                throw new ArgumentNullException("lua");
            if (_o == null)
                throw new ArgumentNullException("o");
            #endregion
            foreach (var _method in _o.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public))
            {
                foreach (LuaGlobalAttribute attribute in _method.GetCustomAttributes(typeof(LuaGlobalAttribute), true))
                {
                    if (string.IsNullOrEmpty(attribute.Name))
                        _lua.RegisterFunction(_method.Name, _o, _method); 
                    else
                        _lua.RegisterFunction(attribute.Name, _o, _method); 
                }
            }
        }
        #endregion

        #region Tagged static methods
        /// <summary>
        /// Registers all public static methods in a class tagged with <see cref="LuaGlobalAttribute"/> as Lua global functions
        /// </summary>
        /// <param name="_lua">The Lua VM to add the methods to</param>
        /// <param name="_type">The class type to get the methods from</param>
        public static void TaggedStaticMethods(Lua _lua, Type _type)
        {
            #region Sanity checks
            if (_lua == null)
                throw new ArgumentNullException("lua");
            if (_type == null)
                throw new ArgumentNullException("type");
            if (!_type.IsClass)
                throw new ArgumentException("The type must be a class!", "type");
            #endregion
            foreach (var _method in _type.GetMethods(BindingFlags.Static | BindingFlags.Public))
            {
                foreach (LuaGlobalAttribute attribute in _method.GetCustomAttributes(typeof(LuaGlobalAttribute), false))
                {
                    if (string.IsNullOrEmpty(attribute.Name))
                        _lua.RegisterFunction(_method.Name, null, _method); // CLR name
                    else
                        _lua.RegisterFunction(attribute.Name, null, _method); // Custom name
                }
            }
        }
        #endregion

        #region Enumeration
        /// <summary>
        /// Registers an enumeration's values for usage as a Lua variable table
        /// </summary>
        /// <typeparam name="T">The enum type to register</typeparam>
        /// <param name="_lua">The Lua VM to add the enum to</param>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "The type parameter is used to select an enum type")]
        public static void Enumeration<T>(Lua _lua)
        {
            #region Sanity checks
            if (_lua == null)
                throw new ArgumentNullException("lua");
            #endregion
            var _type = typeof(T);
            if (!_type.IsEnum)
                throw new ArgumentException("The type must be an enumeration!");

#if SILVERLIGHT
			string[] _names = type.GetFields().Where(x => x.IsLiteral).Select(field => field.Name).ToArray();
			var _values = type.GetFields().Where(x => x.IsLiteral).Select(field => (T)field.GetValue(null)).ToArray();
#else
            string[] _names = Enum.GetNames(_type);
            var _values = (T[])Enum.GetValues(_type);
#endif
            _lua.NewTable(_type.Name);
            for (int i = 0; i < _names.Length; i++)
            {
                string path = _type.Name + "." + _names[i];
                _lua[path] = _values[i];
            }
        }
        #endregion
    }
}