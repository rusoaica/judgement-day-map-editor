﻿using System.Collections;

namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	using LuaNativeFunction = KopiLua.LuaNativeFunction;
#else
    using LuaState = KeraLua.LuaState;
#endif

    public class LuaTable : LuaBase
    {
        public LuaTable(int _reference, Lua _interpreter)
        {
            _Reference = _reference;
            _Interpreter = _interpreter;
        }

        /*
		 * Indexer for string fields of the table
		 */
        public object this[string _field]
        {
            get
            {
                return _Interpreter.GetObject(_Reference, _field);
            }
            set
            {
                _Interpreter.SetObject(_Reference, _field, value);
            }
        }

        /*
		 * Indexer for numeric fields of the table
		 */
        public object this[object _field]
        {
            get
            {
                return _Interpreter.GetObject(_Reference, _field);
            }
            set
            {
                _Interpreter.SetObject(_Reference, _field, value);
            }
        }

        public System.Collections.IDictionaryEnumerator GetEnumerator()
        {
            return _Interpreter.GetTableDict(this).GetEnumerator();
        }

        public ICollection Keys
        {
            get { return _Interpreter.GetTableDict(this).Keys; }
        }

        public ICollection Values
        {
            get { return _Interpreter.GetTableDict(this).Values; }
        }

        /*
		 * Gets an string fields of a table ignoring its metatable,
		 * if it exists
		 */
        internal object RawGet(string _field)
        {
            return _Interpreter.RawGetObject(_Reference, _field);
        }

        /*
		 * Pushes this table into the Lua stack
		 */
        internal void Push(LuaState _lua_state)
        {
            LuaLib.LuaGetRef(_lua_state, _Reference);
        }

        public override string ToString()
        {
            return "table";
        }
    }
}