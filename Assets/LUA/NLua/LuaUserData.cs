﻿namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
#else
    using LuaCore = KeraLua.Lua;
    using LuaState = KeraLua.LuaState;
#endif

    public class LuaUserData : LuaBase
    {
        public LuaUserData(int _reference, Lua _interpreter)
        {
            _Reference = _reference;
            _Interpreter = _interpreter;
        }

        /*
		 * Indexer for string fields of the userdata
		 */
        public object this[string _field]
        {
            get
            {
                return _Interpreter.GetObject(_Reference, _field);
            }
            set
            {
                _Interpreter.SetObject(_Reference, _field, value);
            }
        }

        /*
		 * Indexer for numeric fields of the userdata
		 */
        public object this[object _field]
        {
            get
            {
                return _Interpreter.GetObject(_Reference, _field);
            }
            set
            {
                _Interpreter.SetObject(_Reference, _field, value);
            }
        }

        /*
		 * Calls the userdata and returns its return values inside
		 * an array
		 */
        public object[] Call(params object[] _args)
        {
            return _Interpreter.CallFunction(this, _args);
        }

        public override string ToString()
        {
            return "userdata";
        }
    }
}