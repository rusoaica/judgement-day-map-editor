using NLua.Extensions;
using NLua.Method;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	using LuaNativeFunction = KopiLua.LuaNativeFunction;
#else
    using LuaNativeFunction = KeraLua.LuaNativeFunction;
    using LuaState = KeraLua.LuaState;
#endif

    public class MetaFunctions
    {
        public LuaNativeFunction GcFunction { get; private set; }
        public LuaNativeFunction IndexFunction { get; private set; }
        public LuaNativeFunction NewIndexFunction { get; private set; }
        public LuaNativeFunction BaseIndexFunction { get; private set; }
        public LuaNativeFunction ClassIndexFunction { get; private set; }
        public LuaNativeFunction ClassNewindexFunction { get; private set; }
        public LuaNativeFunction ExecuteDelegateFunction { get; private set; }
        public LuaNativeFunction CallConstructorFunction { get; private set; }
        public LuaNativeFunction ToStringFunction { get; private set; }
        public LuaNativeFunction AddFunction { get; private set; }
        public LuaNativeFunction SubtractFunction { get; private set; }
        public LuaNativeFunction MultiplyFunction { get; private set; }
        public LuaNativeFunction DivisionFunction { get; private set; }
        public LuaNativeFunction ModulosFunction { get; private set; }
        public LuaNativeFunction UnaryNegationFunction { get; private set; }
        public LuaNativeFunction EqualFunction { get; private set; }
        public LuaNativeFunction LessThanFunction { get; private set; }
        public LuaNativeFunction LessThanOrEqualFunction { get; private set; }
        private Dictionary<object, object> _memberCache = new Dictionary<object, object>();
        private ObjectTranslator _translator;

        /*
		 * __index metafunction for CLR objects. Implemented in Lua.
		 */
        static string luaIndexFunction =
            @"local function index(obj,name)
			    local meta = getmetatable(obj)
			    local cached = meta.cache[name]
			    if cached ~= nil then
			       return cached
			    else
			       local value,isFunc = get_object_member(obj,name)
			       
			       if isFunc then
					meta.cache[name]=value
			       end
			       return value
			     end
		    end
		    return index";

        public static string LuaIndexFunction
        {
            get { return luaIndexFunction; }
        }

        public MetaFunctions(ObjectTranslator _translator)
        {
            this._translator = _translator;
            GcFunction = new LuaNativeFunction(MetaFunctions.CollectObject);
            ToStringFunction = new LuaNativeFunction(MetaFunctions.ToStringLua);
            IndexFunction = new LuaNativeFunction(MetaFunctions.GetMethod);
            NewIndexFunction = new LuaNativeFunction(MetaFunctions.SetFieldOrProperty);
            BaseIndexFunction = new LuaNativeFunction(MetaFunctions.GetBaseMethod);
            CallConstructorFunction = new LuaNativeFunction(MetaFunctions.CallConstructor);
            ClassIndexFunction = new LuaNativeFunction(MetaFunctions.GetClassMethod);
            ClassNewindexFunction = new LuaNativeFunction(MetaFunctions.SetClassFieldOrProperty);
            ExecuteDelegateFunction = new LuaNativeFunction(MetaFunctions.RunFunctionDelegate);
            AddFunction = new LuaNativeFunction(MetaFunctions.AddLua);
            SubtractFunction = new LuaNativeFunction(MetaFunctions.SubtractLua);
            MultiplyFunction = new LuaNativeFunction(MetaFunctions.MultiplyLua);
            DivisionFunction = new LuaNativeFunction(MetaFunctions.DivideLua);
            ModulosFunction = new LuaNativeFunction(MetaFunctions.ModLua);
            UnaryNegationFunction = new LuaNativeFunction(MetaFunctions.UnaryNegationLua);
            EqualFunction = new LuaNativeFunction(MetaFunctions.EqualLua);
            LessThanFunction = new LuaNativeFunction(MetaFunctions.LessThanLua);
            LessThanOrEqualFunction = new LuaNativeFunction(MetaFunctions.LessThanOrEqualLua);
        }

        /*
		 * __call metafunction of CLR delegates, retrieves and calls the delegate.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int RunFunctionDelegate(LuaState _lua_state)
        {
            var translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return RunFunctionDelegate(_lua_state, translator);
        }

        private static int RunFunctionDelegate(LuaState _lua_state, ObjectTranslator _translator)
        {
            LuaNativeFunction _func = (LuaNativeFunction)_translator.GetRawNetObject(_lua_state, 1);
            LuaLib.LuaRemove(_lua_state, 1);
            return _func(_lua_state);
        }

        /*
		 * __gc metafunction of CLR objects.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int CollectObject(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return CollectObject(_lua_state, _translator);
        }

        private static int CollectObject(LuaState _lua_state, ObjectTranslator _translator)
        {
            int _u_data = LuaLib.LuaNetRawNetObj(_lua_state, 1);
            if (_u_data != -1)
                _translator.CollectObject(_u_data);
            return 0;
        }

        /*
		 * __tostring metafunction of CLR objects.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int ToStringLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return ToStringLua(_lua_state, _translator);
        }

        private static int ToStringLua(LuaState _lua_state, ObjectTranslator _translator)
        {
            object obj = _translator.GetRawNetObject(_lua_state, 1);

            if (obj != null)
                _translator.Push(_lua_state, obj.ToString() + ": " + obj.GetHashCode().ToString());
            else
                LuaLib.LuaPushNil(_lua_state);
            return 1;
        }

        /*
         * __add metafunction of CLR objects.
         */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int AddLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return MatchOperator(_lua_state, "op_Addition", _translator);
        }

        /*
		* __sub metafunction of CLR objects.
		*/
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int SubtractLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return MatchOperator(_lua_state, "op_Subtraction", _translator);
        }

        /*
		* __mul metafunction of CLR objects.
		*/
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int MultiplyLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return MatchOperator(_lua_state, "op_Multiply", _translator);
        }

        /*
		* __div metafunction of CLR objects.
		*/
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int DivideLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return MatchOperator(_lua_state, "op_Division", _translator);
        }

        /*
		* __mod metafunction of CLR objects.
		*/
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int ModLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return MatchOperator(_lua_state, "op_Modulus", _translator);
        }

        /*
		* __unm metafunction of CLR objects.
		*/
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int UnaryNegationLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return UnaryNegationLua(_lua_state, _translator);
        }

        static int UnaryNegationLua(LuaState _lua_state, ObjectTranslator _translator)
        {
            object _obj_1 = _translator.GetRawNetObject(_lua_state, 1);
            if (_obj_1 == null)
            {
                _translator.ThrowError(_lua_state, "Cannot negate a nil object");
                LuaLib.LuaPushNil(_lua_state);
                return 1;
            }
            Type _type = _obj_1.GetType();
            MethodInfo _op_unary_negation = _type.GetMethod("op_UnaryNegation");
            if (_op_unary_negation == null)
            {
                _translator.ThrowError(_lua_state, "Cannot negate object (" + _type.Name + " does not overload the operator -)");
                LuaLib.LuaPushNil(_lua_state);
                return 1;
            }
            _obj_1 = _op_unary_negation.Invoke(_obj_1, new object[] { _obj_1 });
            _translator.Push(_lua_state, _obj_1);
            return 1;
        }

        /*
		* __eq metafunction of CLR objects.
		*/
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int EqualLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return MatchOperator(_lua_state, "op_Equality", _translator);
        }

        /*
		* __lt metafunction of CLR objects.
		*/
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int LessThanLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return MatchOperator(_lua_state, "op_LessThan", _translator);
        }

        /*
		 * __le metafunction of CLR objects.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        static int LessThanOrEqualLua(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return MatchOperator(_lua_state, "op_LessThanOrEqual", _translator);
        }

        /// <summary>
        /// Debug tool to dump the lua stack
        /// </summary>
        /// FIXME, move somewhere else
        public static void DumpStack(ObjectTranslator _translator, LuaState _lua_state)
        {
            int _depth = LuaLib.LuaGetTop(_lua_state);

#if WINDOWS_PHONE
			Debug.WriteLine("lua stack depth: {0}", depth);
#elif UNITY_3D
			UnityEngine.Debug.Log(string.Format("lua stack depth: {0}", _depth));
#elif !SILVERLIGHT
            Debug.Print("lua stack depth: {0}", _depth);
#endif
            for (int i = 1; i <= _depth; i++)
            {
                var _type = LuaLib.LuaType(_lua_state, i);
                string _type_str = (_type == LuaTypes.Table) ? "table" : LuaLib.LuaTypeName(_lua_state, _type);
                string _str_rep = LuaLib.LuaToString(_lua_state, i).ToString();
                if (_type == LuaTypes.UserData)
                {
                    object _obj = _translator.GetRawNetObject(_lua_state, i);
                    _str_rep = _obj.ToString();
                }
#if WINDOWS_PHONE
                Debug.WriteLine("{0}: ({1}) {2}", i, _type_str, _str_rep);
#elif UNITY_3D
				UnityEngine.Debug.Log(string.Format("{0}: ({1}) {2}", i, _type_str, _str_rep));
#elif !SILVERLIGHT
                Debug.Print("{0}: ({1}) {2}", i, _type_str, _str_rep);
#endif
            }
        }

        /*
		 * Called by the __index metafunction of CLR objects in case the
		 * method is not cached or it is a field/property/event.
		 * Receives the object and the member name as arguments and returns
		 * either the value of the member or a delegate to call it.
		 * If the member does not exist returns nil.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int GetMethod(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            var _instance = _translator.MetaFunctionsInstance;
            return _instance.GetMethodInternal(_lua_state);
        }

        private int GetMethodInternal(LuaState _lua_state)
        {
            object _obj = _translator.GetRawNetObject(_lua_state, 1);
            if (_obj == null)
            {
                _translator.ThrowError(_lua_state, "trying to index an invalid object reference");
                LuaLib.LuaPushNil(_lua_state);
                return 1;
            }
            object _index = _translator.GetObject(_lua_state, 2);
            string _method_name = _index as string;   
            var _obj_type = _obj.GetType();
            try
            {
                if (!string.IsNullOrEmpty(_method_name) && IsMemberPresent(_obj_type, _method_name))
                    return GetMember(_lua_state, _obj_type, _obj, _method_name, BindingFlags.Instance);
            }
            catch { }
            if (_obj_type.IsArray && _index is double)
            {
                int _int_index = (int)((double)_index);
                if (_obj_type.UnderlyingSystemType == typeof(float[]))
                {
                    float[] _arr = ((float[])_obj);
                    _translator.Push(_lua_state, _arr[_int_index]);
                }
                else if (_obj_type.UnderlyingSystemType == typeof(double[]))
                {
                    double[] _arr = ((double[])_obj);
                    _translator.Push(_lua_state, _arr[_int_index]);
                }
                else if (_obj_type.UnderlyingSystemType == typeof(int[]))
                {
                    int[] _arr = ((int[])_obj);
                    _translator.Push(_lua_state, _arr[_int_index]);
                }
                else
                {
                    object[] _arr = (object[])_obj;
                    _translator.Push(_lua_state, _arr[_int_index]);
                }
            }
            else
            {

                if (!string.IsNullOrEmpty(_method_name) && IsExtensionMethodPresent(_obj_type, _method_name))
                    return GetExtensionMethod(_lua_state, _obj_type, _obj, _method_name);
                var _methods = _obj_type.GetMethods();
                foreach (var _m_info in _methods)
                {
                    if (_m_info.Name == "get_Item")
                    {
                        if (_m_info.GetParameters().Length == 1)
                        {
                            var _getter = _m_info;
                            var _actual_parms = (_getter != null) ? _getter.GetParameters() : null;
                            if (_actual_parms == null || _actual_parms.Length != 1)
                            {
                                _translator.ThrowError(_lua_state, "method not found (or no indexer): " + _index);
                                LuaLib.LuaPushNil(_lua_state);
                            }
                            else
                            {
                                _index = _translator.GetAsType(_lua_state, 2, _actual_parms[0].ParameterType);
                                object[] _args = new object[1];
                                _args[0] = _index;
                                try
                                {
                                    object _result = _getter.Invoke(_obj, _args);
                                    _translator.Push(_lua_state, _result);
                                }
                                catch (TargetInvocationException e)
                                {
                                    if (e.InnerException is KeyNotFoundException)
                                        _translator.ThrowError(_lua_state, "key '" + _index + "' not found ");
                                    else
                                        _translator.ThrowError(_lua_state, "exception indexing '" + _index + "' " + e.Message);
                                    LuaLib.LuaPushNil(_lua_state);
                                }
                            }
                        }
                    }
                }
            }
            LuaLib.LuaPushBoolean(_lua_state, false);
            return 2;
        }

        /*
		 * __index metafunction of base classes (the base field of Lua tables).
		 * Adds a prefix to the method name to call the base version of the method.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int GetBaseMethod(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            var _instance = _translator.MetaFunctionsInstance;
            return _instance.GetBaseMethodInternal(_lua_state);
        }

        private int GetBaseMethodInternal(LuaState _lua_state)
        {
            object _obj = _translator.GetRawNetObject(_lua_state, 1);
            if (_obj == null)
            {
                _translator.ThrowError(_lua_state, "trying to index an invalid object reference");
                LuaLib.LuaPushNil(_lua_state);
                LuaLib.LuaPushBoolean(_lua_state, false);
                return 2;
            }
            string _method_name = LuaLib.LuaToString(_lua_state, 2).ToString();
            if (string.IsNullOrEmpty(_method_name))
            {
                LuaLib.LuaPushNil(_lua_state);
                LuaLib.LuaPushBoolean(_lua_state, false);
                return 2;
            }
            GetMember(_lua_state, _obj.GetType(), _obj, "__luaInterface_base_" + _method_name, BindingFlags.Instance);
            LuaLib.LuaSetTop(_lua_state, -2);
            if (LuaLib.LuaType(_lua_state, -1) == LuaTypes.Nil)
            {
                LuaLib.LuaSetTop(_lua_state, -2);
                return GetMember(_lua_state, _obj.GetType(), _obj, _method_name, BindingFlags.Instance);
            }
            LuaLib.LuaPushBoolean(_lua_state, false);
            return 2;
        }

        /// <summary>
        /// Does this method exist as either an instance or static?
        /// </summary>
        /// <param name="_obj_type"></param>
        /// <param name="_method_name"></param>
        /// <returns></returns>
        bool IsMemberPresent(IReflect _obj_type, string _method_name)
        {
            object _cached_member = CheckMemberCache(_memberCache, _obj_type, _method_name);
            if (_cached_member != null)
                return true;
            var _members = _obj_type.GetMember(_method_name, BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public);
            return (_members.Length > 0);
        }

        bool IsExtensionMethodPresent(Type _type, string _name)
        {
            object _cached_member = CheckMemberCache(_memberCache, _type, _name);
            if (_cached_member != null)
                return true;
            return _translator.IsExtensionMethodPresent(_type, _name);
        }

        int GetExtensionMethod(LuaState _lua_state, Type _type, object _obj, string _name)
        {
            object _cached_member = CheckMemberCache(_memberCache, _type, _name);
            if (_cached_member != null && _cached_member is LuaNativeFunction)
            {
                _translator.PushFunction(_lua_state, (LuaNativeFunction)_cached_member);
                _translator.Push(_lua_state, true);
                return 2;
            }
            MethodInfo _method_info = _translator.GetExtensionMethod(_type, _name);
            var _wrapper = new LuaNativeFunction((new LuaMethodWrapper(_translator, _obj, _type, _method_info)).invokeFunction);
            SetMemberCache(_memberCache, _type, _name, _wrapper);
            _translator.PushFunction(_lua_state, _wrapper);
            _translator.Push(_lua_state, true);
            return 2;
        }

        /*
		 * Pushes the value of a member or a delegate to call it, depending on the type of
		 * the member. Works with static or instance members.
		 * Uses reflection to find members, and stores the reflected MemberInfo object in
		 * a cache (indexed by the type of the object and the name of the member).
		 */
        int GetMember(LuaState _lua_state, IReflect _obj_type, object _obj, string _method_name, BindingFlags _binding_type)
        {
            bool _implicit_static = false;
            MemberInfo _member = null;
            object _cached_member = CheckMemberCache(_memberCache, _obj_type, _method_name);
            if (_cached_member is LuaNativeFunction)
            {
                _translator.PushFunction(_lua_state, (LuaNativeFunction)_cached_member);
                _translator.Push(_lua_state, true);
                return 2;
            }
            else if (_cached_member != null)
                _member = (MemberInfo)_cached_member;
            else
            {
                var _members = _obj_type.GetMember(_method_name, _binding_type | BindingFlags.Public);
                if (_members.Length > 0)
                    _member = _members[0];
                else
                {
                    _members = _obj_type.GetMember(_method_name, _binding_type | BindingFlags.Static | BindingFlags.Public);
                    if (_members.Length > 0)
                    {
                        _member = _members[0];
                        _implicit_static = true;
                    }
                }
            }
            if (_member != null)
            {
                if (_member.MemberType == MemberTypes.Field)
                {
                    var _field = (FieldInfo)_member;
                    if (_cached_member == null)
                        SetMemberCache(_memberCache, _obj_type, _method_name, _member);
                    try
                    {
                        _translator.Push(_lua_state, _field.GetValue(_obj));
                    }
                    catch
                    {
                        LuaLib.LuaPushNil(_lua_state);
                    }
                }
                else if (_member.MemberType == MemberTypes.Property)
                {
                    var _property = (PropertyInfo)_member;
                    if (_cached_member == null)
                        SetMemberCache(_memberCache, _obj_type, _method_name, _member);
                    try
                    {
                        object _val = _property.GetValue(_obj, null);
                        _translator.Push(_lua_state, _val);
                    }
                    catch (ArgumentException)
                    {
                        if (_obj_type is Type && !(((Type)_obj_type) == typeof(object)))
                            return GetMember(_lua_state, ((Type)_obj_type).BaseType, _obj, _method_name, _binding_type);
                        else
                            LuaLib.LuaPushNil(_lua_state);
                    }
                    catch (TargetInvocationException e)
                    {  
                        ThrowError(_lua_state, e);
                        LuaLib.LuaPushNil(_lua_state);
                    }
                }
                else if (_member.MemberType == MemberTypes.Event)
                {
                    var _event_info = (EventInfo)_member;
                    if (_cached_member == null)
                        SetMemberCache(_memberCache, _obj_type, _method_name, _member);
                    _translator.Push(_lua_state, new RegisterEventHandler(_translator.pendingEvents, _obj, _event_info));
                }
                else if (!_implicit_static)
                {
                    if (_member.MemberType == MemberTypes.NestedType)
                    {
                        if (_cached_member == null)
                            SetMemberCache(_memberCache, _obj_type, _method_name, _member);
                        string _name = _member.Name;
                        var _dec_type = _member.DeclaringType;
                        string _long_name = _dec_type.FullName + "+" + _name;
                        var _nested_type = _translator.FindType(_long_name);
                        _translator.PushType(_lua_state, _nested_type);
                    }
                    else
                    {
                        var _wrapper = new LuaNativeFunction((new LuaMethodWrapper(_translator, _obj_type, _method_name, _binding_type)).invokeFunction);
                        if (_cached_member == null)
                            SetMemberCache(_memberCache, _obj_type, _method_name, _wrapper);
                        _translator.PushFunction(_lua_state, _wrapper);
                        _translator.Push(_lua_state, true);
                        return 2;
                    }
                }
                else
                {
                    _translator.ThrowError(_lua_state, "can't pass instance to static method " + _method_name);
                    LuaLib.LuaPushNil(_lua_state);
                }
            }
            else
            {
                _translator.ThrowError(_lua_state, "unknown member name " + _method_name);
                LuaLib.LuaPushNil(_lua_state);
            }
            _translator.Push(_lua_state, false);
            return 2;
        }

        private object CheckMemberCache(Dictionary<object, object> _member_cache, IReflect _obj_type, string _member_name)
        {
            object _members = null;
            if (_member_cache.TryGetValue(_obj_type, out _members))
            {
                var _members_dict = _members as Dictionary<object, object>;
                object _member_value = null;
                if (_members != null && _members_dict.TryGetValue(_member_name, out _member_value))
                    return _member_value;
            }
            return null;
        }

        /*
		 * Stores a MemberInfo object in the member cache.
		 */
        private void SetMemberCache(Dictionary<object, object> _member_cache, IReflect _obj_type, string _member_name, object _member) 
        {
            Dictionary<object, object> _members = null;
            object _member_cache_value = null;
            if (_member_cache.TryGetValue(_obj_type, out _member_cache_value))
                _members = (Dictionary<object, object>)_member_cache_value;
            else
            {
                _members = new Dictionary<object, object>();
                _member_cache[_obj_type] = _members;
            }
            _members[_member_name] = _member;
        }

        /*
		 * __newindex metafunction of CLR objects. Receives the object,
		 * the member name and the value to be stored as arguments. Throws
		 * and error if the assignment is invalid.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback(typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int SetFieldOrProperty(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            var _instance = _translator.MetaFunctionsInstance;
            return _instance.SetFieldOrPropertyInternal(_lua_state);
        }

        private int SetFieldOrPropertyInternal(LuaState _lua_state)
        {
            object _target = _translator.GetRawNetObject(_lua_state, 1);
            if (_target == null)
            {
                _translator.ThrowError(_lua_state, "trying to index and invalid object reference");
                return 0;
            }
            var _type = _target.GetType();
            string _detail_message;
            bool _did_member = TrySetMember(_lua_state, _type, _target, BindingFlags.Instance, out _detail_message);
            if (_did_member)
                return 0; 
            try
            {
                if (_type.IsArray && LuaLib.LuaIsNumber(_lua_state, 2))
                {
                    int _index = (int)LuaLib.LuaToNumber(_lua_state, 2);
                    var _arr = (Array)_target;
                    object _val = _translator.GetAsType(_lua_state, 3, _arr.GetType().GetElementType());
                    _arr.SetValue(_val, _index);
                }
                else
                {
                    var _setter = _type.GetMethod("set_Item");
                    if (_setter != null)
                    {
                        var _args = _setter.GetParameters();
                        var _value_type = _args[1].ParameterType;
                        object _val = _translator.GetAsType(_lua_state, 3, _value_type);
                        var _index_type = _args[0].ParameterType;
                        object _index = _translator.GetAsType(_lua_state, 2, _index_type);
                        object[] _method_args = new object[2];
                        _method_args[0] = _index;
                        _method_args[1] = _val;
                        _setter.Invoke(_target, _method_args);
                    }
                    else
                        _translator.ThrowError(_lua_state, _detail_message); 
                }
#if !SILVERLIGHT
            }
            catch (SEHException)
            {
                throw;
#endif
            }
            catch (Exception e)
            {
                ThrowError(_lua_state, e);
            }
            return 0;
        }

        /// <summary>
        /// Tries to set a named property or field
        /// </summary>
        /// <param name="_lua_state"></param>
        /// <param name="_target_type"></param>
        /// <param name="_target"></param>
        /// <param name="_binding_type"></param>
        /// <returns>false if unable to find the named member, true for success</returns>
        private bool TrySetMember(LuaState _lua_state, IReflect _target_type, object _target, BindingFlags _binding_type, out string _detail_message)
        {
            _detail_message = null; 
            if (LuaLib.LuaType(_lua_state, 2) != LuaTypes.String)
            {
                _detail_message = "property names must be strings";
                return false;
            }
            string _field_name = LuaLib.LuaToString(_lua_state, 2).ToString();
            if (_field_name == null || _field_name.Length < 1 || !(char.IsLetter(_field_name[0]) || _field_name[0] == '_'))
            {
                _detail_message = "invalid property name";
                return false;
            }
            var _member = (MemberInfo)CheckMemberCache(_memberCache, _target_type, _field_name);
            if (_member == null)
            {
                var _members = _target_type.GetMember(_field_name, _binding_type | BindingFlags.Public);
                if (_members.Length > 0)
                {
                    _member = _members[0];
                    SetMemberCache(_memberCache, _target_type, _field_name, _member);
                }
                else
                {
                    _detail_message = "field or property '" + _field_name + "' does not exist";
                    return false;
                }
            }
            if (_member.MemberType == MemberTypes.Field)
            {
                var _field = (FieldInfo)_member;
                object _val = _translator.GetAsType(_lua_state, 3, _field.FieldType);
                try
                {
                    _field.SetValue(_target, _val);
                }
                catch (Exception e)
                {
                    ThrowError(_lua_state, e);
                }
                return true;
            }
            else if (_member.MemberType == MemberTypes.Property)
            {
                var _property = (PropertyInfo)_member;
                object _val = _translator.GetAsType(_lua_state, 3, _property.PropertyType);
                try
                {
                    _property.SetValue(_target, _val, null);
                }
                catch (Exception e)
                {
                    ThrowError(_lua_state, e);
                }
                return true;
            }
            _detail_message = "'" + _field_name + "' is not a .net field or property";
            return false;
        }

        /*
		 * Writes to fields or properties, either static or instance. Throws an error
		 * if the operation is invalid.
		 */
        private int SetMember(LuaState _lua_state, IReflect _target_type, object _target, BindingFlags _binding_type)
        {
            string _detail;
            bool _success = TrySetMember(_lua_state, _target_type, _target, _binding_type, out _detail);
            if (!_success)
                _translator.ThrowError(_lua_state, _detail);
            return 0;
        }

        /// <summary>
        /// Convert a C# exception into a Lua error
        /// </summary>
        /// <param name="_e"></param>
        /// We try to look into the exception to give the most meaningful description
        void ThrowError(LuaState _lua_state, Exception _e)
        {
            var _te = _e as TargetInvocationException;
            if (_te != null)
                _e = _te.InnerException;
            _translator.ThrowError(_lua_state, _e);
        }

        /*
		 * __index metafunction of type references, works on static members.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int GetClassMethod(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            var _instance = _translator.MetaFunctionsInstance;
            return _instance.GetClassMethodInternal(_lua_state);
        }

        private int GetClassMethodInternal(LuaState _lua_state)
        {
            IReflect _class;
            object _obj = _translator.GetRawNetObject(_lua_state, 1);
            if (_obj == null || !(_obj is IReflect))
            {
                _translator.ThrowError(_lua_state, "trying to index an invalid type reference");
                LuaLib.LuaPushNil(_lua_state);
                return 1;
            }
            else
                _class = (IReflect)_obj;
            if (LuaLib.LuaIsNumber(_lua_state, 2))
            {
                int _size = (int)LuaLib.LuaToNumber(_lua_state, 2);
                _translator.Push(_lua_state, Array.CreateInstance(_class.UnderlyingSystemType, _size));
                return 1;
            }
            else
            {
                string _method_name = LuaLib.LuaToString(_lua_state, 2).ToString();
                if (string.IsNullOrEmpty(_method_name))
                {
                    LuaLib.LuaPushNil(_lua_state);
                    return 1;
                }
                else
                    return GetMember(_lua_state, _class, null, _method_name, BindingFlags.FlattenHierarchy | BindingFlags.Static);
            }
        }

        /*
		 * __newindex function of type references, works on static members.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int SetClassFieldOrProperty(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            var _instance = _translator.MetaFunctionsInstance;
            return _instance.SetClassFieldOrPropertyInternal(_lua_state);
        }

        private int SetClassFieldOrPropertyInternal(LuaState _lua_state)
        {
            IReflect _target;
            object _obj = _translator.GetRawNetObject(_lua_state, 1);
            if (_obj == null || !(_obj is IReflect))
            {
                _translator.ThrowError(_lua_state, "trying to index an invalid type reference");
                return 0;
            }
            else
                _target = (IReflect)_obj;
            return SetMember(_lua_state, _target, null, BindingFlags.FlattenHierarchy | BindingFlags.Static);
        }

        /*
		 * __call metafunction of type references. Searches for and calls
		 * a constructor for the type. Returns nil if the constructor is not
		 * found or if the arguments are invalid. Throws an error if the constructor
		 * generates an exception.
		 */
#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int CallConstructor(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            var _instance = _translator.MetaFunctionsInstance;
            return _instance.CallConstructorInternal(_lua_state);
        }

        private int CallConstructorInternal(LuaState _lua_state)
        {
            var _valid_constructor = new MethodCache();
            IReflect _class;
            object _obj = _translator.GetRawNetObject(_lua_state, 1);
            if (_obj == null || !(_obj is IReflect))
            {
                _translator.ThrowError(_lua_state, "trying to call constructor on an invalid type reference");
                LuaLib.LuaPushNil(_lua_state);
                return 1;
            }
            else
                _class = (IReflect)_obj;
            LuaLib.LuaRemove(_lua_state, 1);
            var _constructors = _class.UnderlyingSystemType.GetConstructors();
            foreach (var _constructor in _constructors)
            {
                bool _is_constructor = MatchParameters(_lua_state, _constructor, ref _valid_constructor);
                if (_is_constructor)
                {
                    try
                    {
                        _translator.Push(_lua_state, _constructor.Invoke(_valid_constructor.args));
                    }
                    catch (TargetInvocationException e)
                    {
                        ThrowError(_lua_state, e);
                        LuaLib.LuaPushNil(_lua_state);
                    }
                    catch
                    {
                        LuaLib.LuaPushNil(_lua_state);
                    }
                    return 1;
                }
            }
            string _constructor_name = (_constructors.Length == 0) ? "unknown" : _constructors[0].Name;
            _translator.ThrowError(_lua_state, String.Format("{0} does not contain constructor({1}) argument match",
                _class.UnderlyingSystemType, _constructor_name));
            LuaLib.LuaPushNil(_lua_state);
            return 1;
        }

        static bool IsInteger(double _x)
        {
            return Math.Ceiling(_x) == _x;
        }

        static object GetTargetObject(LuaState _lua_state, string _operation, ObjectTranslator _translator)
        {
            Type _t;
            object _target = _translator.GetRawNetObject(_lua_state, 1);
            if (_target != null)
            {
                _t = _target.GetType();
                if (_t.HasMethod(_operation))
                    return _target;
            }
            _target = _translator.GetRawNetObject(_lua_state, 2);
            if (_target != null)
            {
                _t = _target.GetType();
                if (_t.HasMethod(_operation))
                    return _target;
            }
            return null;
        }

        static int MatchOperator(LuaState _lua_state, string _operation, ObjectTranslator _translator)
        {
            var _valid_operator = new MethodCache();
            object _target = GetTargetObject(_lua_state, _operation, _translator);
            if (_target == null)
            {
                _translator.ThrowError(_lua_state, "Cannot call " + _operation + " on a nil object");
                LuaLib.LuaPushNil(_lua_state);
                return 1;
            }
            Type _type = _target.GetType();
            var _operators = _type.GetMethods(_operation, BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
            foreach (var _op in _operators)
            {
                bool _is_ok = _translator.MatchParameters(_lua_state, _op, ref _valid_operator);
                if (!_is_ok)
                    continue;
                object _result;
                if (_op.IsStatic)
                    _result = _op.Invoke(null, _valid_operator.args);
                else
                    _result = _op.Invoke(_target, _valid_operator.args);
                _translator.Push(_lua_state, _result);
                return 1;
            }
            _translator.ThrowError(_lua_state, "Cannot call (" + _operation + ") on object type " + _type.Name);
            LuaLib.LuaPushNil(_lua_state);
            return 1;
        }

        internal Array TableToArray(Func<int, object> _lua_param_value_extractor, Type _param_array_type, int _start_index, int _count)
        {
            Array _param_array;

            if (_count == 0)
                return Array.CreateInstance(_param_array_type, 0);
            var _lua_param_value = _lua_param_value_extractor(_start_index);
            if (_lua_param_value is LuaTable)
            {
                LuaTable _table = (LuaTable)_lua_param_value;
                IDictionaryEnumerator _table_enumerator = _table.GetEnumerator();
                _table_enumerator.Reset();
                _param_array = Array.CreateInstance(_param_array_type, _table.Values.Count);
                int _param_array_index = 0;
                while (_table_enumerator.MoveNext())
                {
                    object _value = _table_enumerator.Value;
                    if (_param_array_type == typeof(object))
                        if (_value != null && _value.GetType() == typeof(double) && IsInteger((double)_value))
                            _value = Convert.ToInt32((double)_value);
#if SILVERLIGHT
					_param_array.SetValue (Convert.ChangeType (_value, paramArrayType, System.Globalization.CultureInfo.InvariantCulture), _param_array_index);
#else
                    _param_array.SetValue(Convert.ChangeType(_value, _param_array_type), _param_array_index);
#endif
                    _param_array_index++;
                }
            }
            else
            {
                _param_array = Array.CreateInstance(_param_array_type, _count);
                _param_array.SetValue(_lua_param_value, 0);
                for (int i = 1; i < _count; i++)
                {
                    _start_index++;
                    var _value = _lua_param_value_extractor(_start_index);
                    _param_array.SetValue(_value, i);
                }
            }
            return _param_array;
        }

        /*
		 * Matches a method against its arguments in the Lua stack. Returns
		 * if the match was successful. It it was also returns the information
		 * necessary to invoke the method.
		 */
        internal bool MatchParameters(LuaState _lua_state, MethodBase _method, ref MethodCache _method_cache)
        {
            ExtractValue _extract_value;
            bool _is_method = true;
            var _param_info = _method.GetParameters();
            int _current_lua_param = 1;
            int _n_lua_params = LuaLib.LuaGetTop(_lua_state);
            var _param_list = new List<object>();
            var _out_list = new List<int>();
            var _arg_types = new List<MethodArgs>();
            foreach (var _current_net_param in _param_info)
            {
#if !SILVERLIGHT
                if (!_current_net_param.IsIn && _current_net_param.IsOut)  
#else
				if (_current_net_param.IsOut)  
#endif
                {
                    _param_list.Add(null);
                    _out_list.Add(_param_list.LastIndexOf(null));
                }
                else if (IsTypeCorrect(_lua_state, _current_lua_param, _current_net_param, out _extract_value))
                {  
                    var _value = _extract_value(_lua_state, _current_lua_param);
                    _param_list.Add(_value);
                    int _index = _param_list.LastIndexOf(_value);
                    var _method_arg = new MethodArgs();
                    _method_arg.index = _index;
                    _method_arg.extractValue = _extract_value;
                    _arg_types.Add(_method_arg);
                    if (_current_net_param.ParameterType.IsByRef)
                        _out_list.Add(_index);
                    _current_lua_param++;
                }  
                else if (IsParamsArray(_lua_state, _current_lua_param, _current_net_param, out _extract_value))
                {

                    var _param_array_type = _current_net_param.ParameterType.GetElementType();
                    Func<int, object> _extract_delegate = (_current_param) =>
                    {
                        _current_lua_param++;
                        return _extract_value(_lua_state, _current_param);
                    };
                    int _count = (_n_lua_params - _current_lua_param) + 1;
                    Array _param_array = TableToArray(_extract_delegate, _param_array_type, _current_lua_param, _count);
                    _param_list.Add(_param_array);
                    int _index = _param_list.LastIndexOf(_param_array);
                    var _method_arg = new MethodArgs();
                    _method_arg.index = _index;
                    _method_arg.extractValue = _extract_value;
                    _method_arg.isParamsArray = true;
                    _method_arg.paramsArrayType = _param_array_type;
                    _arg_types.Add(_method_arg);
                }
                else if (_current_lua_param > _n_lua_params)
                { 
                    if (_current_net_param.IsOptional)
                        _param_list.Add(_current_net_param.DefaultValue);
                    else
                    {
                        _is_method = false;
                        break;
                    }
                }
                else if (_current_net_param.IsOptional)
                    _param_list.Add(_current_net_param.DefaultValue);
                else
                {  
                    _is_method = false;
                    break;
                }
            }
            if (_current_lua_param != _n_lua_params + 1) 
                _is_method = false;
            if (_is_method)
            {
                _method_cache.args = _param_list.ToArray();
                _method_cache.cachedMethod = _method;
                _method_cache.outList = _out_list.ToArray();
                _method_cache.argTypes = _arg_types.ToArray();
            }
            return _is_method;
        }

        /// <summary>
        /// CP: Fix for operator overloading failure
        /// Returns true if the type is set and assigns the extract value
        /// </summary>
        /// <param name="_lua_state"></param>
        /// <param name="_current_lua_param"></param>
        /// <param name="_current_net_param"></param>
        /// <param name="_extract_value"></param>
        /// <returns></returns>
        private bool IsTypeCorrect(LuaState _lua_state, int _current_lua_param, ParameterInfo _current_net_param, out ExtractValue _extract_value)
        {
            try
            {
                return (_extract_value = _translator.typeChecker.CheckLuaType(_lua_state, _current_lua_param, _current_net_param.ParameterType)) != null;
            }
            catch
            {
                _extract_value = null;
                Debug.WriteLine("Type wasn't correct");
                return false;
            }
        }

        private bool IsParamsArray(LuaState _lua_state, int _current_lua_param, ParameterInfo _current_net_param, out ExtractValue _extract_value)
        {
            _extract_value = null;
            if (_current_net_param.GetCustomAttributes(typeof(ParamArrayAttribute), false).Length > 0)
            {
                LuaTypes _lua_type;
                try
                {
                    _lua_type = LuaLib.LuaType(_lua_state, _current_lua_param);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Could not retrieve lua type while attempting to determine params Array Status.");
                    Debug.WriteLine(ex.Message);
                    _extract_value = null;
                    return false;
                }
                if (_lua_type == LuaTypes.Table)
                {
                    try
                    {
                        _extract_value = _translator.typeChecker.GetExtractor(typeof(LuaTable));
                    }
                    catch (Exception)
                    {
                        Debug.WriteLine("An error occurred during an attempt to retrieve a LuaTable extractor while checking for params array status.");
                    }
                    if (_extract_value != null)
                        return true;
                }
                else
                {
                    var _param_element_type = _current_net_param.ParameterType.GetElementType();
                    try
                    {
                        _extract_value = _translator.typeChecker.CheckLuaType(_lua_state, _current_lua_param, _param_element_type);
                    }
                    catch (Exception)
                    {
                        Debug.WriteLine(string.Format("An error occurred during an attempt to retrieve an extractor ({0}) while checking for params array status.", _param_element_type.FullName));
                    }
                    if (_extract_value != null)
                        return true;
                }
            }
            Debug.WriteLine("Type wasn't Params object.");
            return false;
        }
    }
}