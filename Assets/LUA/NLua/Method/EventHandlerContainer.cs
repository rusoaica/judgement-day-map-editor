using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace NLua.Method
{
	/// <summary>
	/// We keep track of what delegates we have auto attached to an event - to allow us to cleanly exit a NLua session
	/// </summary>
	class EventHandlerContainer : IDisposable
	{
		private Dictionary<Delegate, RegisterEventHandler> _dict = new Dictionary<Delegate, RegisterEventHandler> ();

		public void Add(Delegate _handler, RegisterEventHandler _event_info)
		{
			_dict.Add (_handler, _event_info);
		}

		public void Remove (Delegate _handler)
		{
			bool _found = _dict.Remove (_handler);
			Debug.Assert (_found);
		}

		/// <summary>
		/// Remove any still registered handlers
		/// </summary>
		public void Dispose ()
		{
			foreach (KeyValuePair<Delegate, RegisterEventHandler> _pair in _dict)
				_pair.Value.RemovePending (_pair.Key);
			_dict.Clear ();
		}
	}
}