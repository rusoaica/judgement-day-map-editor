using System;

namespace NLua.Method
{
    public class LuaClassHelper
    {
        /*
		 *  Gets the function called name from the provided table,
		 * returning null if it does not exist
		 */
        public static LuaFunction GetTableFunction(LuaTable _lua_table, string _name)
        {
            if (_lua_table == null)
                return null;
            object _func_obj = _lua_table.RawGet(_name);
            if (_func_obj is LuaFunction)
                return (LuaFunction)_func_obj;
            else
                return null;
        }

        /*
		 * Calls the provided function with the provided parameters
		 */
        public static object CallFunction(LuaFunction _function, object[] _args, Type[] _return_types, object[] _in_args, int[] _out_args)
        {
            object _return_value;
            int _i_ref_args;
            object[] _return_values = _function.Call(_in_args, _return_types);
            if (_return_types[0] == typeof(void))
            {
                _return_value = null;
                _i_ref_args = 0;
            }
            else
            {
                _return_value = _return_values[0];
                _i_ref_args = 1;
            }
            for (int i = 0; i < _out_args.Length; i++)
            {
                _args[_out_args[i]] = _return_values[_i_ref_args];
                _i_ref_args++;
            }
            return _return_value;
        }
    }
}