using System;

namespace NLua.Method
{
    public class LuaDelegate
    {
        public LuaFunction function;
        public Type[] returnTypes;

        public LuaDelegate()
        {
            function = null;
            returnTypes = null;
        }

        public object CallFunction(object[] _args, object[] _in_args, int[] _out_args)
        {
            object _return_value;
            int _i_ref_args;
            object[] _return_values = function.Call(_in_args, returnTypes);
            if (returnTypes[0] == typeof(void))
            {
                _return_value = null;
                _i_ref_args = 0;
            }
            else
            {
                _return_value = _return_values[0];
                _i_ref_args = 1;
            }
            for (int i = 0; i < _out_args.Length; i++)
            {
                _args[_out_args[i]] = _return_values[_i_ref_args];
                _i_ref_args++;
            }
            return _return_value;
        }
    }
}