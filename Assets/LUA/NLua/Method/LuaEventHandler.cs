namespace NLua.Method
{
	public class LuaEventHandler
	{
		public LuaFunction handler = null;

		public void HandleEvent(object[] _args)
		{
			handler.Call (_args);
		}
	}
}