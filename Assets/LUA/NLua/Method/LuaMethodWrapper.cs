using NLua.Exceptions;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace NLua.Method
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	using LuaNativeFunction = KopiLua.LuaNativeFunction;
#else
    using LuaNativeFunction = KeraLua.LuaNativeFunction;
    using LuaState = KeraLua.LuaState;
#endif

    /*
	 * Argument extraction with type-conversion function
	 */
    delegate object ExtractValue(LuaState _lua_state, int _stack_pos);

    class LuaMethodWrapper
    {
        internal LuaNativeFunction invokeFunction;
        private ObjectTranslator _translator;
        private MethodBase _method;
        private MethodCache _lastCalledMethod = new MethodCache();
        private string _methodName;
        private MemberInfo[] _members;
        private ExtractValue _extractTarget;
        private object _target;
        private BindingFlags _bindingType;

        /*
		 * Constructs the wrapper for a known MethodBase instance
		 */
        public LuaMethodWrapper(ObjectTranslator _translator, object _target, IReflect _target_type, MethodBase _method)
        {
            invokeFunction = new LuaNativeFunction(this.Call);
            this._translator = _translator;
            this._target = _target;
            if (_target_type != null)
                _extractTarget = _translator.typeChecker.GetExtractor(_target_type);
            this._method = _method;
            _methodName = _method.Name;
            if (_method.IsStatic)
                _bindingType = BindingFlags.Static;
            else
                _bindingType = BindingFlags.Instance;
        }

        /*
		 * Constructs the wrapper for a known method name
		 */
        public LuaMethodWrapper(ObjectTranslator _translator, IReflect _target_type, string _method_name, BindingFlags _binding_type)
        {
            invokeFunction = new LuaNativeFunction(this.Call);
            this._translator = _translator;
            _methodName = _method_name;
            if (_target_type != null)
                _extractTarget = _translator.typeChecker.GetExtractor(_target_type);
            _bindingType = _binding_type;
            _members = _target_type.UnderlyingSystemType.GetMember(_method_name, MemberTypes.Method, _binding_type | BindingFlags.Public);
        }

        /// <summary>
        /// Convert C# exceptions into Lua errors
        /// </summary>
        /// <returns>num of things on stack</returns>
        /// <param name="_e">null for no pending exception</param>
        int SetPendingException(Exception _e)
        {
            return _translator.interpreter.SetPendingException(_e);
        }

        /*
		 * Calls the method. Receives the arguments from the Lua stack
		 * and returns values in it.
		 */
        int Call(LuaState _lua_state)
        {
            var _method_to_call = _method;
            object _target_object = _target;
            bool _failed_call = true;
            int _n_return_values = 0;
            if (!LuaLib.LuaCheckStack(_lua_state, 5))
                throw new LuaException("Lua stack overflow");
            bool _is_static = (_bindingType & BindingFlags.Static) == BindingFlags.Static;
            SetPendingException(null);
            if (_method_to_call == null)
            { 
                if (_is_static)
                    _target_object = null;
                else
                    _target_object = _extractTarget(_lua_state, 1);
                if (_lastCalledMethod.cachedMethod != null)
                { 
                    int _num_stack_to_skip = _is_static ? 0 : 1;
                    int _num_args_passed = LuaLib.LuaGetTop(_lua_state) - _num_stack_to_skip;
                    MethodBase __method = _lastCalledMethod.cachedMethod;
                    if (_num_args_passed == _lastCalledMethod.argTypes.Length)
                    { 
                        if (!LuaLib.LuaCheckStack(_lua_state, _lastCalledMethod.outList.Length + 6))
                            throw new LuaException("Lua stack overflow");
                        object[] _args = _lastCalledMethod.args;
                        try
                        {
                            for (int i = 0; i < _lastCalledMethod.argTypes.Length; i++)
                            {
                                MethodArgs _type = _lastCalledMethod.argTypes[i];
                                int _index = i + 1 + _num_stack_to_skip;
                                Func<int, object> _value_extractor = (_current_param) =>
                                {
                                    return _type.extractValue(_lua_state, _current_param);
                                };
                                if (_lastCalledMethod.argTypes[i].isParamsArray)
                                {
                                    int _count = _index - _lastCalledMethod.argTypes.Length;
                                    Array _param_array = _translator.TableToArray(_value_extractor, _type.paramsArrayType, _index, _count);
                                    _args[_lastCalledMethod.argTypes[i].index] = _param_array;
                                }
                                else
                                    _args[_type.index] = _value_extractor(_index);
                                if (_lastCalledMethod.args[_lastCalledMethod.argTypes[i].index] == null && !LuaLib.LuaIsNil(_lua_state, i + 1 + _num_stack_to_skip))
                                    throw new LuaException(string.Format("argument number {0} is invalid", (i + 1)));
                            }
                            if ((_bindingType & BindingFlags.Static) == BindingFlags.Static)
                                _translator.Push(_lua_state, __method.Invoke(null, _lastCalledMethod.args));
                            else
                            {
                                if (__method.IsConstructor)
                                    _translator.Push(_lua_state, ((ConstructorInfo)__method).Invoke(_lastCalledMethod.args));
                                else
                                    _translator.Push(_lua_state, __method.Invoke(_target_object, _lastCalledMethod.args));
                            }
                            _failed_call = false;
                        }
                        catch (TargetInvocationException _e)
                        {
                            return SetPendingException(_e.GetBaseException());
                        }
                        catch (Exception e)
                        {
                            if (_members.Length == 1) 
                                return SetPendingException(e);
                        }
                    }
                }
                if (_failed_call)
                {
                    if (!_is_static)
                    {
                        if (_target_object == null)
                        {
                            _translator.ThrowError(_lua_state, String.Format("instance method '{0}' requires a non null target object", _methodName));
                            LuaLib.LuaPushNil(_lua_state);
                            return 1;
                        }
                        LuaLib.LuaRemove(_lua_state, 1); 
                    }
                    bool _has_match = false;
                    string _candidate_name = null;
                    foreach (var _member in _members)
                    {
                        _candidate_name = _member.ReflectedType.Name + "." + _member.Name;
                        var _m = (MethodInfo)_member;
                        bool _is_method = _translator.MatchParameters(_lua_state, _m, ref _lastCalledMethod);
                        if (_is_method)
                        {
                            _has_match = true;
                            break;
                        }
                    }
                    if (!_has_match)
                    {
                        string _msg = (_candidate_name == null) ? "invalid arguments to method call" : ("invalid arguments to method: " + _candidate_name);
                        _translator.ThrowError(_lua_state, _msg);
                        LuaLib.LuaPushNil(_lua_state);
                        return 1;
                    }
                }
            }
            else
            { 
                if (_method_to_call.ContainsGenericParameters)
                {
                    _translator.MatchParameters(_lua_state, _method_to_call, ref _lastCalledMethod);
                    if (_method_to_call.IsGenericMethodDefinition)
                    {
                        var _type_args = new List<Type>();
                        foreach (object _arg in _lastCalledMethod.args)
                            _type_args.Add(_arg.GetType());
                        var _concrete_method = (_method_to_call as MethodInfo).MakeGenericMethod(_type_args.ToArray());
                        _translator.Push(_lua_state, _concrete_method.Invoke(_target_object, _lastCalledMethod.args));
                        _failed_call = false;
                    }
                    else if (_method_to_call.ContainsGenericParameters)
                    {
                        _translator.ThrowError(_lua_state, "unable to invoke method on generic class as the current method is an open generic method");
                        LuaLib.LuaPushNil(_lua_state);
                        return 1;
                    }
                }
                else
                {
                    if (!_method_to_call.IsStatic && !_method_to_call.IsConstructor && _target_object == null)
                    {
                        _target_object = _extractTarget(_lua_state, 1);
                        LuaLib.LuaRemove(_lua_state, 1); 
                    }
                    if (!_translator.MatchParameters(_lua_state, _method_to_call, ref _lastCalledMethod))
                    {
                        _translator.ThrowError(_lua_state, "invalid arguments to method call");
                        LuaLib.LuaPushNil(_lua_state);
                        return 1;
                    }
                }
            }
            if (_failed_call)
            {
                if (!LuaLib.LuaCheckStack(_lua_state, _lastCalledMethod.outList.Length + 6))
                    throw new LuaException("Lua stack overflow");
                try
                {
                    if (_is_static)
                        _translator.Push(_lua_state, _lastCalledMethod.cachedMethod.Invoke(null, _lastCalledMethod.args));
                    else
                    {
                        if (_lastCalledMethod.cachedMethod.IsConstructor)
                            _translator.Push(_lua_state, ((ConstructorInfo)_lastCalledMethod.cachedMethod).Invoke(_lastCalledMethod.args));
                        else
                            _translator.Push(_lua_state, _lastCalledMethod.cachedMethod.Invoke(_target_object, _lastCalledMethod.args));
                    }
                }
                catch (TargetInvocationException e)
                {
                    return SetPendingException(e.GetBaseException());
                }
                catch (Exception e)
                {
                    return SetPendingException(e);
                }
            }
            for (int _index = 0; _index < _lastCalledMethod.outList.Length; _index++)
            {
                _n_return_values++;
                _translator.Push(_lua_state, _lastCalledMethod.args[_lastCalledMethod.outList[_index]]);
            }
            if (!_lastCalledMethod.isReturnVoid && _n_return_values > 0)
                _n_return_values++;
            return _n_return_values < 1 ? 1 : _n_return_values;
        }
    }
}