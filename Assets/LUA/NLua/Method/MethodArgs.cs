using System;

namespace NLua.Method
{
	struct MethodArgs
	{
		public int index;
		public ExtractValue extractValue;
		public bool isParamsArray;
		public Type paramsArrayType;
	}
}