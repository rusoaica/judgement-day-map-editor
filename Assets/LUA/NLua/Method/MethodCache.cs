using System.Reflection;

namespace NLua.Method
{
    struct MethodCache
    {
        public bool isReturnVoid;
        public object[] args;
        public int[] outList;
        public MethodArgs[] argTypes;
        private MethodBase _cachedMethod;

        public MethodBase cachedMethod
        {
            get
            {
                return _cachedMethod;
            }
            set
            {
                _cachedMethod = value;
                var _mi = value as MethodInfo;
                if (_mi != null)
                    isReturnVoid = _mi.ReturnType == typeof(void);
            }
        }
    }
}