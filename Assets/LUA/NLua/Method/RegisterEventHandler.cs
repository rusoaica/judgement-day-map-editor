using System;
using System.Reflection;

namespace NLua.Method
{
    class RegisterEventHandler
    {
        private EventHandlerContainer _pendingEvents;
        private EventInfo _eventInfo;
        private object _target;

        public RegisterEventHandler(EventHandlerContainer _pending_events, object _target, EventInfo _event_info)
        {
            this._target = _target;
            this._eventInfo = _event_info;
            this._pendingEvents = _pending_events;
        }

        /*
		 * Adds a new event handler
		 */
        public Delegate Add(LuaFunction _function)
        {
            Delegate _handler_delegate = CodeGeneration.Instance.GetDelegate(_eventInfo.EventHandlerType, _function);
            _eventInfo.AddEventHandler(_target, _handler_delegate);
            _pendingEvents.Add(_handler_delegate, this);
            return _handler_delegate;
        }

        /*
		 * Removes an existing event handler
		 */
        public void Remove(Delegate _handler_delegate)
        {
            RemovePending(_handler_delegate);
            _pendingEvents.Remove(_handler_delegate);
        }

        /*
		 * Removes an existing event handler (without updating the pending handlers list)
		 */
        internal void RemovePending(Delegate _handler_delegate)
        {
            _eventInfo.RemoveEventHandler(_target, _handler_delegate);
        }
    }
}