using NLua.Exceptions;
using NLua.Extensions;
using NLua.Method;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;


namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
	using LuaNativeFunction = KopiLua.LuaNativeFunction;
#else
    using LuaNativeFunction = KeraLua.LuaNativeFunction;
    using LuaState = KeraLua.LuaState;
#endif

    public class ObjectTranslator
    {
        private LuaNativeFunction _registerTableFunction, _unregisterTableFunction, _getMethodSigFunction, _getConstructorSigFunction, _importTypeFunction, _loadAssemblyFunction, _ctypeFunction, _enumFromIntFunction;
        private readonly Dictionary<object, int> _objectsBackMap = new Dictionary<object, int>();
        private readonly Dictionary<int, object> _objects = new Dictionary<int, object>();
        internal EventHandlerContainer pendingEvents = new EventHandlerContainer();
        private MetaFunctions _metaFunctions;
        private List<Assembly> _assemblies;
        internal CheckType typeChecker;
        internal Lua interpreter;
        private int _nextObj = 0;

        public MetaFunctions MetaFunctionsInstance
        {
            get
            {
                return _metaFunctions;
            }
        }

        public Lua Interpreter
        {
            get
            {
                return interpreter;
            }
        }

        public ObjectTranslator(Lua _interpreter, LuaState _lua_state)
        {
            this.interpreter = _interpreter;
            typeChecker = new CheckType(this);
            _metaFunctions = new MetaFunctions(this);
            _assemblies = new List<Assembly>();
            _importTypeFunction = new LuaNativeFunction(ObjectTranslator.ImportType);
            _loadAssemblyFunction = new LuaNativeFunction(ObjectTranslator.LoadAssembly);
            _registerTableFunction = new LuaNativeFunction(ObjectTranslator.RegisterTable);
            _unregisterTableFunction = new LuaNativeFunction(ObjectTranslator.UnregisterTable);
            _getMethodSigFunction = new LuaNativeFunction(ObjectTranslator.GetMethodSignature);
            _getConstructorSigFunction = new LuaNativeFunction(ObjectTranslator.GetConstructorSignature);
            _ctypeFunction = new LuaNativeFunction(ObjectTranslator.CType);
            _enumFromIntFunction = new LuaNativeFunction(ObjectTranslator.EnumFromInt);
            CreateLuaObjectList(_lua_state);
            CreateIndexingMetaFunction(_lua_state);
            CreateBaseClassMetatable(_lua_state);
            CreateClassMetatable(_lua_state);
            CreateFunctionMetatable(_lua_state);
            SetGlobalFunctions(_lua_state);
        }

        private void CreateLuaObjectList(LuaState _lua_state)
        {
            LuaLib.LuaPushString(_lua_state, "luaNet_objects");
            LuaLib.LuaNewTable(_lua_state);
            LuaLib.LuaNewTable(_lua_state);
            LuaLib.LuaPushString(_lua_state, "__mode");
            LuaLib.LuaPushString(_lua_state, "v");
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaSetMetatable(_lua_state, -2);
            LuaLib.LuaSetTable(_lua_state, (int)LuaIndexes.Registry);
        }

        private void CreateIndexingMetaFunction(LuaState _lua_state)
        {
            LuaLib.LuaPushString(_lua_state, "luaNet_indexfunction");
            LuaLib.LuaLDoString(_lua_state, MetaFunctions.LuaIndexFunction);
            LuaLib.LuaRawSet(_lua_state, (int)LuaIndexes.Registry);
        }

        private void CreateBaseClassMetatable(LuaState _lua_state)
        {
            LuaLib.LuaLNewMetatable(_lua_state, "luaNet_searchbase");
            LuaLib.LuaPushString(_lua_state, "__gc");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.GcFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaPushString(_lua_state, "__tostring");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.ToStringFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaPushString(_lua_state, "__index");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.BaseIndexFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaPushString(_lua_state, "__newindex");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.NewIndexFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaSetTop(_lua_state, -2);
        }

        private void CreateClassMetatable(LuaState _lua_state)
        {
            LuaLib.LuaLNewMetatable(_lua_state, "luaNet_class");
            LuaLib.LuaPushString(_lua_state, "__gc");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.GcFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaPushString(_lua_state, "__tostring");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.ToStringFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaPushString(_lua_state, "__index");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.ClassIndexFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaPushString(_lua_state, "__newindex");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.ClassNewindexFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaPushString(_lua_state, "__call");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.CallConstructorFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaSetTop(_lua_state, -2);
        }

        private void SetGlobalFunctions(LuaState _lua_state)
        {
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.IndexFunction);
            LuaLib.LuaSetGlobal(_lua_state, "get_object_member");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _importTypeFunction);
            LuaLib.LuaSetGlobal(_lua_state, "import_type");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _loadAssemblyFunction);
            LuaLib.LuaSetGlobal(_lua_state, "load_assembly");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _registerTableFunction);
            LuaLib.LuaSetGlobal(_lua_state, "make_object");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _unregisterTableFunction);
            LuaLib.LuaSetGlobal(_lua_state, "free_object");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _getMethodSigFunction);
            LuaLib.LuaSetGlobal(_lua_state, "get_method_bysig");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _getConstructorSigFunction);
            LuaLib.LuaSetGlobal(_lua_state, "get_constructor_bysig");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _ctypeFunction);
            LuaLib.LuaSetGlobal(_lua_state, "ctype");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _enumFromIntFunction);
            LuaLib.LuaSetGlobal(_lua_state, "enum");
        }

        private void CreateFunctionMetatable(LuaState _lua_state)
        {
            LuaLib.LuaLNewMetatable(_lua_state, "luaNet_function");
            LuaLib.LuaPushString(_lua_state, "__gc");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.GcFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaPushString(_lua_state, "__call");
            LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.ExecuteDelegateFunction);
            LuaLib.LuaSetTable(_lua_state, -3);
            LuaLib.LuaSetTop(_lua_state, -2);
        }

        internal void ThrowError(LuaState _lua_state, object _e)
        {
            int _old_top = LuaLib.LuaGetTop(_lua_state);
            LuaLib.LuaLWhere(_lua_state, 1);
            var _cur_lev = PopValues(_lua_state, _old_top);
            string _err_location = string.Empty;
            if (_cur_lev.Length > 0)
                _err_location = _cur_lev[0].ToString();
            string _message = _e as string;
            if (_message != null)
                _e = new LuaScriptException(_message, _err_location);
            else
            {
                var _ex = _e as Exception;
                if (_ex != null)
                    _e = new LuaScriptException(_ex, _err_location);
            }
            Push(_lua_state, _e);
            LuaLib.LuaError(_lua_state);
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int LoadAssembly(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return _translator.LoadAssemblyInternal(_lua_state);
        }

        private int LoadAssemblyInternal(LuaState _lua_state)
        {
            try
            {
                string _assembly_name = LuaLib.LuaToString(_lua_state, 1).ToString();
                Assembly _assembly = null;
                Exception _exception = null;
                try
                {
                    _assembly = Assembly.Load(_assembly_name);
                }
                catch (BadImageFormatException) { }
                catch (FileNotFoundException e)
                {
                    _exception = e;
                }
#if !SILVERLIGHT
                if (_assembly == null)
                {
                    try
                    {
                        _assembly = Assembly.Load(AssemblyName.GetAssemblyName(_assembly_name));
                    }
                    catch (FileNotFoundException e)
                    {
                        _exception = e;
                    }
                    if (_assembly == null)
                    {
                        AssemblyName _mscor = _assemblies[0].GetName();
                        AssemblyName _name = new AssemblyName();
                        _name.Name = _assembly_name;
                        _name.CultureInfo = _mscor.CultureInfo;
                        _name.Version = _mscor.Version;
                        _name.SetPublicKeyToken(_mscor.GetPublicKeyToken());
                        _name.SetPublicKey(_mscor.GetPublicKey());
                        _assembly = Assembly.Load(_name);
                        if (_assembly != null)
                            _exception = null;
                    }
                    if (_exception != null)
                        ThrowError(_lua_state, _exception);
                }
#endif
                if (_assembly != null && !_assemblies.Contains(_assembly))
                    _assemblies.Add(_assembly);
            }
            catch (Exception e)
            {
                ThrowError(_lua_state, e);
            }
            return 0;
        }

        internal Type FindType(string _class_name)
        {
            foreach (var _assembly in _assemblies)
            {
                var _class = _assembly.GetType(_class_name);
                if (_class != null)
                    return _class;
            }
            return null;
        }

        public bool IsExtensionMethodPresent(Type _type, string _name)
        {
            return GetExtensionMethod(_type, _name) != null;
        }

        public MethodInfo GetExtensionMethod(Type _type, string _name)
        {
            return _type.GetExtensionMethod(_name, _assemblies);
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int ImportType(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return _translator.ImportTypeInternal(_lua_state);
        }

        private int ImportTypeInternal(LuaState _lua_state)
        {
            string _class_name = LuaLib.LuaToString(_lua_state, 1).ToString();
            var _class = FindType(_class_name);
            if (_class != null)
                PushType(_lua_state, _class);
            else
                LuaLib.LuaPushNil(_lua_state);
            return 1;
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int RegisterTable(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return _translator.RegisterTableInternal(_lua_state);
        }

        private int RegisterTableInternal(LuaState _lua_state)
        {
            if (LuaLib.LuaType(_lua_state, 1) == LuaTypes.Table)
            {
                var _lua_table = GetTable(_lua_state, 1);
                string _super_class_name = LuaLib.LuaToString(_lua_state, 2).ToString();
                if (_super_class_name != null)
                {
                    var _class = FindType(_super_class_name);
                    if (_class != null)
                    {
                        object _obj = CodeGeneration.Instance.GetClassInstance(_class, _lua_table);
                        PushObject(_lua_state, _obj, "luaNet_metatable");
                        LuaLib.LuaNewTable(_lua_state);
                        LuaLib.LuaPushString(_lua_state, "__index");
                        LuaLib.LuaPushValue(_lua_state, -3);
                        LuaLib.LuaSetTable(_lua_state, -3);
                        LuaLib.LuaPushString(_lua_state, "__newindex");
                        LuaLib.LuaPushValue(_lua_state, -3);
                        LuaLib.LuaSetTable(_lua_state, -3);
                        LuaLib.LuaSetMetatable(_lua_state, 1);
                        LuaLib.LuaPushString(_lua_state, "base");
                        int _index = AddObject(_obj);
                        PushNewObject(_lua_state, _obj, _index, "luaNet_searchbase");
                        LuaLib.LuaRawSet(_lua_state, 1);
                    }
                    else
                        ThrowError(_lua_state, "register_table: can not find superclass '" + _super_class_name + "'");
                }
                else
                    ThrowError(_lua_state, "register_table: superclass name can not be null");
            }
            else
                ThrowError(_lua_state, "register_table: first arg is not a table");
            return 0;
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int UnregisterTable(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return _translator.UnregisterTableInternal(_lua_state);
        }

        private int UnregisterTableInternal(LuaState _lua_state)
        {
            try
            {
                if (LuaLib.LuaGetMetatable(_lua_state, 1) != 0)
                {
                    LuaLib.LuaPushString(_lua_state, "__index");
                    LuaLib.LuaGetTable(_lua_state, -2);
                    object _obj = GetRawNetObject(_lua_state, -1);
                    if (_obj == null)
                        ThrowError(_lua_state, "unregister_table: arg is not valid table");
                    var _lua_table_field = _obj.GetType().GetField("__luaInterface_luaTable");
                    if (_lua_table_field == null)
                        ThrowError(_lua_state, "unregister_table: arg is not valid table");
                    _lua_table_field.SetValue(_obj, null);
                    LuaLib.LuaPushNil(_lua_state);
                    LuaLib.LuaSetMetatable(_lua_state, 1);
                    LuaLib.LuaPushString(_lua_state, "base");
                    LuaLib.LuaPushNil(_lua_state);
                    LuaLib.LuaSetTable(_lua_state, 1);
                }
                else
                    ThrowError(_lua_state, "unregister_table: arg is not valid table");
            }
            catch (Exception e)
            {
                ThrowError(_lua_state, e.Message);
            }
            return 0;
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int GetMethodSignature(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return _translator.GetMethodSignatureInternal(_lua_state);
        }

        private int GetMethodSignatureInternal(LuaState _lua_state)
        {
            IReflect _class;
            object _target;
            int _u_data = LuaLib.LuaNetCheckUData(_lua_state, 1, "luaNet_class");
            if (_u_data != -1)
            {
                _class = (IReflect)_objects[_u_data];
                _target = null;
            }
            else
            {
                _target = GetRawNetObject(_lua_state, 1);
                if (_target == null)
                {
                    ThrowError(_lua_state, "get_method_bysig: first arg is not type or object reference");
                    LuaLib.LuaPushNil(_lua_state);
                    return 1;
                }
                _class = _target.GetType();
            }
            string _method_name = LuaLib.LuaToString(_lua_state, 2).ToString();
            var _signature = new Type[LuaLib.LuaGetTop(_lua_state) - 2];
            for (int i = 0; i < _signature.Length; i++)
                _signature[i] = FindType(LuaLib.LuaToString(_lua_state, i + 3).ToString());
            try
            {
                var _method = _class.GetMethod(_method_name, BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.FlattenHierarchy, null, _signature, null);
                PushFunction(_lua_state, new LuaNativeFunction((new LuaMethodWrapper(this, _target, _class, _method)).invokeFunction));
            }
            catch (Exception e)
            {
                ThrowError(_lua_state, e);
                LuaLib.LuaPushNil(_lua_state);
            }
            return 1;
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int GetConstructorSignature(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return _translator.GetConstructorSignatureInternal(_lua_state);
        }

        private int GetConstructorSignatureInternal(LuaState _lua_state)
        {
            IReflect _class = null;
            int _u_data = LuaLib.LuaNetCheckUData(_lua_state, 1, "luaNet_class");
            if (_u_data != -1)
                _class = (IReflect)_objects[_u_data];
            if (_class == null)
                ThrowError(_lua_state, "get_constructor_bysig: first arg is invalid type reference");
            var _signature = new Type[LuaLib.LuaGetTop(_lua_state) - 1];
            for (int i = 0; i < _signature.Length; i++)
                _signature[i] = FindType(LuaLib.LuaToString(_lua_state, i + 2).ToString());
            try
            {
                ConstructorInfo _constructor = _class.UnderlyingSystemType.GetConstructor(_signature);
                PushFunction(_lua_state, new LuaNativeFunction((new LuaMethodWrapper(this, null, _class, _constructor)).invokeFunction));
            }
            catch (Exception e)
            {
                ThrowError(_lua_state, e);
                LuaLib.LuaPushNil(_lua_state);
            }
            return 1;
        }

        internal void PushType(LuaState _lua_state, Type _t)
        {
            PushObject(_lua_state, new ProxyType(_t), "luaNet_class");
        }

        internal void PushFunction(LuaState _lua_state, LuaNativeFunction _func)
        {
            PushObject(_lua_state, _func, "luaNet_function");
        }

        internal void PushObject(LuaState _lua_state, object _o, string _meta_table)
        {
            int _index = -1;
            if (_o == null)
            {
                LuaLib.LuaPushNil(_lua_state);
                return;
            }
            bool _found = (!_o.GetType().IsValueType) && _objectsBackMap.TryGetValue(_o, out _index);
            if (_found)
            {
                LuaLib.LuaLGetMetatable(_lua_state, "luaNet_objects");
                LuaLib.LuaRawGetI(_lua_state, -1, _index);
                var _type = LuaLib.LuaType(_lua_state, -1);
                if (_type != LuaTypes.Nil)
                {
                    LuaLib.LuaRemove(_lua_state, -2);
                    return;
                }
                LuaLib.LuaRemove(_lua_state, -1);
                LuaLib.LuaRemove(_lua_state, -1);
                CollectObject(_o, _index);
            }
            _index = AddObject(_o);
            PushNewObject(_lua_state, _o, _index, _meta_table);
        }

        private void PushNewObject(LuaState _lua_state, object _o, int _index, string _meta_table)
        {
            if (_meta_table == "luaNet_metatable")
            {
                LuaLib.LuaLGetMetatable(_lua_state, _o.GetType().AssemblyQualifiedName);
                if (LuaLib.LuaIsNil(_lua_state, -1))
                {
                    LuaLib.LuaSetTop(_lua_state, -2);
                    LuaLib.LuaLNewMetatable(_lua_state, _o.GetType().AssemblyQualifiedName);
                    LuaLib.LuaPushString(_lua_state, "cache");
                    LuaLib.LuaNewTable(_lua_state);
                    LuaLib.LuaRawSet(_lua_state, -3);
                    LuaLib.LuaPushLightUserData(_lua_state, LuaLib.LuaNetGetTag());
                    LuaLib.LuaPushNumber(_lua_state, 1);
                    LuaLib.LuaRawSet(_lua_state, -3);
                    LuaLib.LuaPushString(_lua_state, "__index");
                    LuaLib.LuaPushString(_lua_state, "luaNet_indexfunction");
                    LuaLib.LuaRawGet(_lua_state, (int)LuaIndexes.Registry);
                    LuaLib.LuaRawSet(_lua_state, -3);
                    LuaLib.LuaPushString(_lua_state, "__gc");
                    LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.GcFunction);
                    LuaLib.LuaRawSet(_lua_state, -3);
                    LuaLib.LuaPushString(_lua_state, "__tostring");
                    LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.ToStringFunction);
                    LuaLib.LuaRawSet(_lua_state, -3);
                    LuaLib.LuaPushString(_lua_state, "__newindex");
                    LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.NewIndexFunction);
                    LuaLib.LuaRawSet(_lua_state, -3);
                    RegisterOperatorsFunctions(_lua_state, _o.GetType());
                }
            }
            else
                LuaLib.LuaLGetMetatable(_lua_state, _meta_table);
            LuaLib.LuaLGetMetatable(_lua_state, "luaNet_objects");
            LuaLib.LuaNetNewUData(_lua_state, _index);
            LuaLib.LuaPushValue(_lua_state, -3);
            LuaLib.LuaRemove(_lua_state, -4);
            LuaLib.LuaSetMetatable(_lua_state, -2);
            LuaLib.LuaPushValue(_lua_state, -1);
            LuaLib.LuaRawSetI(_lua_state, -3, _index);
            LuaLib.LuaRemove(_lua_state, -2);
        }

        void RegisterOperatorsFunctions(LuaState _lua_state, Type type)
        {
            if (type.HasAdditionOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__add");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.AddFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
            if (type.HasSubtractionOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__sub");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.SubtractFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
            if (type.HasMultiplyOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__mul");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.MultiplyFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
            if (type.HasDivisionOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__div");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.DivisionFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
            if (type.HasModulusOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__mod");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.ModulosFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
            if (type.HasUnaryNegationOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__unm");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.UnaryNegationFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
            if (type.HasEqualityOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__eq");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.EqualFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
            if (type.HasLessThanOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__lt");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.LessThanFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
            if (type.HasLessThanOrEqualOpertator())
            {
                LuaLib.LuaPushString(_lua_state, "__le");
                LuaLib.LuaPushStdCallCFunction(_lua_state, _metaFunctions.LessThanOrEqualFunction);
                LuaLib.LuaRawSet(_lua_state, -3);
            }
        }

        internal object GetAsType(LuaState _lua_state, int _stack_pos, Type _param_type)
        {
            var _extractor = typeChecker.CheckLuaType(_lua_state, _stack_pos, _param_type);
            return _extractor != null ? _extractor(_lua_state, _stack_pos) : null;
        }

        internal void CollectObject(int _u_data)
        {
            object _o;
            bool _found = _objects.TryGetValue(_u_data, out _o);
            if (_found)
                CollectObject(_o, _u_data);
        }

        private void CollectObject(object _o, int _u_data)
        {
            _objects.Remove(_u_data);
            if (!_o.GetType().IsValueType)
                _objectsBackMap.Remove(_o);
        }

        private int AddObject(object _obj)
        {
            int _index = _nextObj++;
            _objects[_index] = _obj;
            if (!_obj.GetType().IsValueType)
                _objectsBackMap[_obj] = _index;
            return _index;
        }

        internal object GetObject(LuaState _lua_state, int _index)
        {
            var _type = LuaLib.LuaType(_lua_state, _index);
            switch (_type)
            {
                case LuaTypes.Number:
                    {
                        return LuaLib.LuaToNumber(_lua_state, _index);
                    }
                case LuaTypes.String:
                    {
                        return LuaLib.LuaToString(_lua_state, _index);
                    }
                case LuaTypes.Boolean:
                    {
                        return LuaLib.LuaToBoolean(_lua_state, _index);
                    }
                case LuaTypes.Table:
                    {
                        return GetTable(_lua_state, _index);
                    }
                case LuaTypes.Function:
                    {
                        return GetFunction(_lua_state, _index);
                    }
                case LuaTypes.UserData:
                    {
                        int _u_data = LuaLib.LuaNetToNetObject(_lua_state, _index);
                        return _u_data != -1 ? _objects[_u_data] : GetUserData(_lua_state, _index);
                    }
                default:
                    return null;
            }
        }

        internal LuaTable GetTable(LuaState _lua_state, int _index)
        {
            LuaLib.LuaPushValue(_lua_state, _index);
            return new LuaTable(LuaLib.LuaRef(_lua_state, 1), interpreter);
        }

        internal LuaUserData GetUserData(LuaState _lua_state, int _index)
        {
            LuaLib.LuaPushValue(_lua_state, _index);
            return new LuaUserData(LuaLib.LuaRef(_lua_state, 1), interpreter);
        }

        internal LuaFunction GetFunction(LuaState _lua_state, int _index)
        {
            LuaLib.LuaPushValue(_lua_state, _index);
            return new LuaFunction(LuaLib.LuaRef(_lua_state, 1), interpreter);
        }

        internal object GetNetObject(LuaState _lua_state, int _index)
        {
            int _idx = LuaLib.LuaNetToNetObject(_lua_state, _index);
            return _idx != -1 ? _objects[_idx] : null;
        }

        internal object GetRawNetObject(LuaState _lua_state, int _index)
        {
            int _u_data = LuaLib.LuaNetRawNetObj(_lua_state, _index);
            return _u_data != -1 ? _objects[_u_data] : null;
        }

        internal object[] PopValues(LuaState _lua_state, int _old_top)
        {
            int _new_top = LuaLib.LuaGetTop(_lua_state);
            if (_old_top == _new_top)
                return null;
            else
            {
                var _return_values = new List<object>();
                for (int i = _old_top + 1; i <= _new_top; i++)
                    _return_values.Add(GetObject(_lua_state, i));
                LuaLib.LuaSetTop(_lua_state, _old_top);
                return _return_values.ToArray();
            }
        }

        internal object[] PopValues(LuaState _lua_state, int _old_top, Type[] _pop_types)
        {
            int _new_top = LuaLib.LuaGetTop(_lua_state);
            if (_old_top == _new_top)
                return null;
            else
            {
                int _i_types;
                var _return_values = new List<object>();
                if (_pop_types[0] == typeof(void))
                    _i_types = 1;
                else
                    _i_types = 0;
                for (int i = _old_top + 1; i <= _new_top; i++)
                {
                    _return_values.Add(GetAsType(_lua_state, i, _pop_types[_i_types]));
                    _i_types++;
                }
                LuaLib.LuaSetTop(_lua_state, _old_top);
                return _return_values.ToArray();
            }
        }

        private static bool IsILua(object _o)
        {
            if (_o is ILuaGeneratedType)
            {
                var _type = _o.GetType();
                return _type.GetInterface("ILuaGeneratedType", true) != null;
            }
            return false;
        }

        internal void Push(LuaState _lua_state, object _o)
        {
            if (_o == null)
                LuaLib.LuaPushNil(_lua_state);
            else if (_o is sbyte || _o is byte || _o is short || _o is ushort || _o is int || _o is uint || _o is long || _o is float || _o is ulong || _o is decimal || _o is double)
            {
                double _d = Convert.ToDouble(_o);
                LuaLib.LuaPushNumber(_lua_state, _d);
            }
            else if (_o is char)
            {
                double _d = (char)_o;
                LuaLib.LuaPushNumber(_lua_state, _d);
            }
            else if (_o is string)
            {
                string _str = (string)_o;
                LuaLib.LuaPushString(_lua_state, _str);
            }
            else if (_o is bool)
            {
                bool _b = (bool)_o;
                LuaLib.LuaPushBoolean(_lua_state, _b);
            }
            else if (IsILua(_o))
                (((ILuaGeneratedType)_o).LuaInterfaceGetLuaTable()).Push(_lua_state);
            else if (_o is LuaTable)
                ((LuaTable)_o).Push(_lua_state);
            else if (_o is LuaNativeFunction)
                PushFunction(_lua_state, (LuaNativeFunction)_o);
            else if (_o is LuaFunction)
                ((LuaFunction)_o).Push(_lua_state);
            else
                PushObject(_lua_state, _o, "luaNet_metatable");
        }

        internal bool MatchParameters(LuaState _lua_state, MethodBase _method, ref MethodCache _method_cache)
        {
            return _metaFunctions.MatchParameters(_lua_state, _method, ref _method_cache);
        }

        internal Array TableToArray(Func<int, object> _lua_param_value, Type _param_array_type, int _start_index, int _count)
        {
            return _metaFunctions.TableToArray(_lua_param_value, _param_array_type, _start_index, _count);
        }

        private Type TypeOf(LuaState _lua_state, int _idx)
        {
            int _u_data = LuaLib.LuaNetCheckUData(_lua_state, 1, "luaNet_class");
            if (_u_data == -1)
                return null;
            ProxyType _pt = (ProxyType)_objects[_u_data];
            return _pt.UnderlyingSystemType;
        }

        static int PushError(LuaState _lua_state, string _msg)
        {
            LuaLib.LuaPushNil(_lua_state);
            LuaLib.LuaPushString(_lua_state, _msg);
            return 2;
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int CType(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return _translator.CTypeInternal(_lua_state);
        }

        private int CTypeInternal(LuaState _lua_state)
        {
            Type _t = TypeOf(_lua_state, 1);
            if (_t == null)
                return PushError(_lua_state, "Not a CLR Class");
            PushObject(_lua_state, _t, "luaNet_metatable");
            return 1;
        }

#if MONOTOUCH
		[MonoTouch.MonoPInvokeCallback (typeof (LuaNativeFunction))]
#endif
        [System.Runtime.InteropServices.AllowReversePInvokeCalls]
        private static int EnumFromInt(LuaState _lua_state)
        {
            var _translator = ObjectTranslatorPool.Instance.Find(_lua_state);
            return _translator.EnumFromIntInternal(_lua_state);
        }

        private int EnumFromIntInternal(LuaState _lua_state)
        {
            Type _t = TypeOf(_lua_state, 1);
            if (_t == null || !_t.IsEnum)
                return PushError(_lua_state, "Not an Enum.");
            object _res = null;
            LuaTypes _lt = LuaLib.LuaType(_lua_state, 2);
            if (_lt == LuaTypes.Number)
            {
                int _i_val = (int)LuaLib.LuaToNumber(_lua_state, 2);
                _res = Enum.ToObject(_t, _i_val);
            }
            else if (_lt == LuaTypes.String)
            {
                string _s_flags = LuaLib.LuaToString(_lua_state, 2);
                string _err = null;
                try
                {
                    _res = Enum.Parse(_t, _s_flags, true);
                }
                catch (ArgumentException e)
                {
                    _err = e.Message;
                }
                if (_err != null)
                    return PushError(_lua_state, _err);
            }
            else
            {
                return PushError(_lua_state, "Second argument must be a integer or a string.");
            }
            PushObject(_lua_state, _res, "luaNet_metatable");
            return 1;
        }
    }
}