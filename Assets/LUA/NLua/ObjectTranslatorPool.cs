using System.Collections.Generic;

namespace NLua
{
#if USE_KOPILUA
	using LuaCore  = KopiLua.Lua;
	using LuaState = KopiLua.LuaState;
#else
    using LuaCore = KeraLua.Lua;
    using LuaState = KeraLua.LuaState;
#endif

    internal class ObjectTranslatorPool
    {
        private static volatile ObjectTranslatorPool _instance = new ObjectTranslatorPool();
        private Dictionary<LuaState, ObjectTranslator> _translators = new Dictionary<LuaState, ObjectTranslator>();

        public static ObjectTranslatorPool Instance
        {
            get
            {
                return _instance;
            }
        }

        public ObjectTranslatorPool()
        {
        }

        public void Add(LuaState _lua_state, ObjectTranslator translator)
        {
            _translators.Add(_lua_state, translator);
        }

        public ObjectTranslator Find(LuaState _lua_state)
        {
            if (_translators.ContainsKey(_lua_state))
                return _translators[_lua_state];
            LuaState _main = LuaCore.LuaNetGetMainState(_lua_state);
            if (_translators.ContainsKey(_main))
                return _translators[_main];
            return null;
        }

        public void Remove(LuaState _lua_state)
        {
            if (!_translators.ContainsKey(_lua_state))
                return;
            _translators.Remove(_lua_state);
        }
    }
}