using System;
using System.Globalization;
using System.Reflection;

namespace NLua
{
    /// <summary>
    /// Summary description for ProxyType.
    /// </summary>
    public class ProxyType : IReflect
    {
        private Type _proxy;

        public ProxyType(Type _proxy)
        {
            this._proxy = _proxy;
        }

        /// <summary>
        /// Provide human readable short hand for this proxy object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "ProxyType(" + UnderlyingSystemType + ")";
        }

        public Type UnderlyingSystemType
        {
            get { return _proxy; }
        }

        public FieldInfo GetField(string _name, BindingFlags _binding_attr)
        {
            return _proxy.GetField(_name, _binding_attr);
        }

        public FieldInfo[] GetFields(BindingFlags _binding_attr)
        {
            return _proxy.GetFields(_binding_attr);
        }

        public MemberInfo[] GetMember(string _name, BindingFlags _binding_attr)
        {
            return _proxy.GetMember(_name, _binding_attr);
        }

        public MemberInfo[] GetMembers(BindingFlags _binding_attr)
        {
            return _proxy.GetMembers(_binding_attr);
        }

        public MethodInfo GetMethod(string _name, BindingFlags _binding_attr)
        {
            return _proxy.GetMethod(_name, _binding_attr);
        }

        public MethodInfo GetMethod(string _name, BindingFlags _binding_attr, Binder _binder, Type[] _types, ParameterModifier[] _modifiers)
        {
            return _proxy.GetMethod(_name, _binding_attr, _binder, _types, _modifiers);
        }

        public MethodInfo[] GetMethods(BindingFlags _binding_attr)
        {
            return _proxy.GetMethods(_binding_attr);
        }

        public PropertyInfo GetProperty(string _name, BindingFlags _binding_attr)
        {
            return _proxy.GetProperty(_name, _binding_attr);
        }

        public PropertyInfo GetProperty(string _name, BindingFlags _binding_attr, Binder _binder, Type _return_type, Type[] _types, ParameterModifier[] _modifiers)
        {
            return _proxy.GetProperty(_name, _binding_attr, _binder, _return_type, _types, _modifiers);
        }

        public PropertyInfo[] GetProperties(BindingFlags _binding_attr)
        {
            return _proxy.GetProperties(_binding_attr);
        }

        public object InvokeMember(string _name, BindingFlags _invoke_attr, Binder _binder, object _target, object[] _args, ParameterModifier[] _modifiers, CultureInfo _culture, string[] _named_parameters)
        {
            return _proxy.InvokeMember(_name, _invoke_attr, _binder, _target, _args, _modifiers, _culture, _named_parameters);
        }
    }
}