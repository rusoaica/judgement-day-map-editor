﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public partial class CameraControler : MonoBehaviour
{

    public static CameraControler Current;

    public Camera Cam;
    public Camera[] OtherCams;
    public GameObject LoadingPopup;
    public bool DragStartedGameplay = false;
    public bool DragStartedOverMenu = false;

    public Transform Pivot;
    public float MapSize;
    public float zoomIn = 1;
    public Vector3 Pos = Vector3.zero;
    public Vector3 Rot = Vector3.zero;
    Vector3 DeltaRot;
    Vector3 LastRot;
    Vector2 prevMausePos = Vector2.zero;

    public LayerMask Mask;
    public LayerMask MaskCursor;

    public Transform ReflectionCamera;
    public Text CursorInfo;
    const float CameraMinOffset = 0.5f;
    int LastWidth = 0;
    int LastHeight;

    void Awake()
    {
        Current = this;
    }

    public void RestartCam()
    {
        if (!Terrain.activeTerrain) return;
        Pos = Vector3.zero + Vector3.right * MapSize / 20.0f - Vector3.forward * MapSize / 20.0f;
        Pos.y = Terrain.activeTerrain.SampleHeight(Pos);
        Rot = Vector3.zero;

        zoomIn = 1;
        transform.localPosition = new Vector3(transform.localPosition.x, ZoomCamPos() * MapSize / 7 + CameraMinOffset, transform.localPosition.z);
        Pivot.localRotation = Quaternion.Euler(Rot);
        Pivot.localPosition = Pos;

        UpdateRect();
    }

    public void UpdateRect()
    {
        float RemoveCamPropHeight = 30f / (float)Screen.height;
        float RemoveCamPropWidth = 309f / (float)Screen.width;
        Cam.rect = new Rect(RemoveCamPropWidth, 0, 1 - RemoveCamPropWidth, 1 - RemoveCamPropHeight);

        LastWidth = Screen.width;
        LastHeight = Screen.height;

        //for (int i = 0; i < OtherCams.Length; i++)
        //    OtherCams[i].rect = Cam.rect;
    }

    void CheckScreenChange()
    {
        if (Screen.width != LastWidth || Screen.height != LastHeight)
            UpdateRect();
    }

    public static void FocusCamera(Transform Pivot, float Zoom = 30, float rot = 10)
    {
        float ZoomValue = Zoom - CameraMinOffset;
        ZoomValue /= Current.MapSize / 7f;
        ZoomValue = Mathf.Pow(ZoomValue, 1f / 3f);
        Current.zoomIn = ZoomValue;
        Current.transform.localPosition = new Vector3(Current.transform.localPosition.x, Zoom, Current.transform.localPosition.z);

        Current.Rot = Vector3.right * rot;
        Current.Pivot.localRotation = Quaternion.Euler(Current.Rot);

        Current.Pos = Pivot.position;
        Current.Pivot.localPosition = Current.Pos;
    }

    public static float GetCurrentZoom()
    {
        return Current.transform.localPosition.y * 10;
    }

    public void RenderCamera(int resWidth, int resHeight, string path)
    {
        // Set Camera
        Cam.orthographic = true;
        Cam.orthographicSize = MapSize * 0.05f;
        Pivot.localPosition = new Vector3(MapSize * 0.05f, 0, -MapSize * 0.05f);
        Pivot.rotation = Quaternion.identity;

        // Take Screenshoot
        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        Cam.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        Cam.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        Cam.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);
        byte[] bytes;
        if (path.Contains(".png")) bytes = screenShot.EncodeToPNG();
        else bytes = screenShot.EncodeToJPG();
        System.IO.File.WriteAllBytes(path, bytes);

        // Restart Camera
        Cam.orthographic = false;
        RestartCam();
    }

    void Update()
    {

        CheckScreenChange();

        if (LoadingPopup.activeSelf)
        {
            if (DragStartedOverMenu) DragStartedOverMenu = false;
            return;
        }
        else if (Input.GetMouseButtonUp(0))
            DragStartedOverMenu = false;
        DragStartedGameplay = !DragStartedOverMenu;
    }

    float ZoomCamPos()
    {
        return Mathf.Pow(zoomIn, 3);
    }

    public static void FocusOnObject(GameObject Obj)
    {
        Bounds FocusBound = new Bounds(Obj.transform.localPosition, Vector3.one);

        Current.Pos = FocusBound.center;
        float size = Mathf.Max(FocusBound.size.x, FocusBound.size.z) * 2;
        Current.zoomIn = size / (Current.MapSize * 0.075f) + 0.21f;
    }
}
