﻿using UnityEngine;

[RequireComponent(typeof(CanvasRenderer))]
[ExecuteInEditMode]
public class UI3DMesh : MonoBehaviour
{
    public Mesh theMesh;
    public Material material;

    void ResetData()
    {
        var cr = GetComponent<CanvasRenderer>();
        cr.SetMesh(theMesh);
        cr.materialCount = 1;
        cr.SetMaterial(material, 0);
    }

    void OnEnable()
    {
        ResetData();
    }

    void OnValidate()
    {
        ResetData();
    }
}