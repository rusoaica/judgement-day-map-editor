﻿using System;
using System.IO;
using UnityEngine;

#pragma warning disable 0162

public class GetGamedataFile : MonoBehaviour
{
    public static HeaderClass loadDDsHeader;
    public static float mipmapBias = 0.5f;
    public static int anisoLevel = 10;
    public static bool debugTextureLoad = false;
    public static bool isDxt3 = false;

    public static Texture2D LoadTexture2DFromBinaryArray(byte[] _dds_bytes, bool _normal_map = false)
    {
        Texture2D _texture = null;
        bool _mipmaps = false;
        try
        {
            TextureFormat _format = GetFormatOfDdsBytes(_dds_bytes);
            _mipmaps = loadDDsHeader.mipmapCount > 0;
            _texture = new Texture2D((int)loadDDsHeader.width, (int)loadDDsHeader.height, _format, _mipmaps, true);
            int DDS_HEADER_SIZE = 128;
            byte[] dxtBytes = new byte[_dds_bytes.Length - DDS_HEADER_SIZE];
            Buffer.BlockCopy(_dds_bytes, DDS_HEADER_SIZE, dxtBytes, 0, _dds_bytes.Length - DDS_HEADER_SIZE);
            if (isDxt3)
            {
                _texture = DDS.DDSReader.LoadDDSTexture(new MemoryStream(_dds_bytes), false).ToTexture2D();
                _texture.Apply(false);
            }
            else
            {
                _texture.LoadRawTextureData(dxtBytes);
                _texture.Apply(false);
            }
        }
        catch (Exception ex)
        {
            return null;
        }
        if (_normal_map)
        {
            _texture.Compress(true);
            Texture2D normalTexture = new Texture2D((int)loadDDsHeader.width, (int)loadDDsHeader.height, TextureFormat.RGBA32, _mipmaps, true);
            Color _theColour = new Color();
            Color[] _pixels;
            for (int m = 0; m < loadDDsHeader.mipmapCount + 1; m++)
            {
                int _texWidth = _texture.width;
                int _texHeight = _texture.height;
                if (m > 0)
                {
                    _texWidth /= (int)Mathf.Pow(2, m);
                    _texHeight /= (int)Mathf.Pow(2, m);
                }
                _pixels = _texture.GetPixels(0, 0, _texWidth, _texHeight, m);
                for (int i = 0; i < _pixels.Length; i++)
                {
                    _theColour.r = _pixels[i].r;
                    _theColour.g = _pixels[i].g;
                    _theColour.b = 1;
                    _theColour.a = _pixels[i].g;
                    _pixels[i] = _theColour;
                }
                normalTexture.SetPixels(0, 0, _texWidth, _texHeight, _pixels, m);
            }
            normalTexture.Apply(false);
            normalTexture.mipMapBias = mipmapBias;
            normalTexture.filterMode = FilterMode.Bilinear;
            normalTexture.anisoLevel = anisoLevel;
            return normalTexture;
        }
        else
        {
            _texture.mipMapBias = mipmapBias;
            _texture.filterMode = FilterMode.Bilinear;
            _texture.anisoLevel = anisoLevel;
        }
        return _texture;
    }

    public static TextureFormat GetFormatOfDdsBytes(byte[] _bytes)
    {

        Stream _ms = new MemoryStream(_bytes);
        BinaryReader _stream = new BinaryReader(_ms);
        loadDDsHeader = BinaryStreamDdsHeader(_stream);
        return ReadFourcc(loadDDsHeader.pixelFormatFourcc);
    }

    [System.Serializable]
    public class HeaderClass
    {
        public TextureFormat format;
        public uint magic;
        public uint size;
        public uint flags;
        public uint height;
        public uint width;
        public uint sizeOrPitch;
        public uint depth;
        public uint mipmapCount;
        public uint alphaBitDepth;
        public uint[] reserved;
        public uint pixelFormatSize;
        public uint pixelFormatFlags;
        public uint pixelFormatFourcc;
        public uint pixelFormatRgbBitCount;
        public uint pixelFormatRbitMask;
        public uint pixelFormatGbitMask;
        public uint pixelFormatBbitMask;
        public uint pixelFormatAbitMask;
        public uint caps1;
        public uint caps2;
        public uint caps3;
        public uint caps4;
    }

    public static HeaderClass BinaryStreamDdsHeader(BinaryReader _stream)
    {
        HeaderClass _ddsHeader = new HeaderClass();
        _ddsHeader.magic = _stream.ReadUInt32();
        _ddsHeader.size = _stream.ReadUInt32();
        _ddsHeader.flags = _stream.ReadUInt32();
        _ddsHeader.height = _stream.ReadUInt32();
        _ddsHeader.width = _stream.ReadUInt32();
        _ddsHeader.sizeOrPitch = _stream.ReadUInt32();
        _ddsHeader.depth = _stream.ReadUInt32();
        _ddsHeader.mipmapCount = _stream.ReadUInt32();
        _ddsHeader.alphaBitDepth = _stream.ReadUInt32();
        _ddsHeader.reserved = new uint[10];
        for (int i = 0; i < 10; i++)
            _ddsHeader.reserved[i] = _stream.ReadUInt32();
        _ddsHeader.pixelFormatSize = _stream.ReadUInt32();
        _ddsHeader.pixelFormatFlags = _stream.ReadUInt32();
        _ddsHeader.pixelFormatFourcc = _stream.ReadUInt32();
        _ddsHeader.pixelFormatRgbBitCount = _stream.ReadUInt32();
        _ddsHeader.pixelFormatRbitMask = _stream.ReadUInt32();
        _ddsHeader.pixelFormatGbitMask = _stream.ReadUInt32();
        _ddsHeader.pixelFormatBbitMask = _stream.ReadUInt32();
        _ddsHeader.pixelFormatAbitMask = _stream.ReadUInt32();
        _ddsHeader.caps1 = _stream.ReadUInt32();
        _ddsHeader.caps2 = _stream.ReadUInt32();
        _ddsHeader.caps3 = _stream.ReadUInt32();
        _ddsHeader.caps4 = _stream.ReadUInt32();
        return _ddsHeader;
    }

    public static TextureFormat ReadFourcc(uint _fourcc)
    {
        isDxt3 = false;
        if (debugTextureLoad) Debug.Log(
             "Size: " + loadDDsHeader.size +
             " flags: " + loadDDsHeader.flags +
             " height: " + loadDDsHeader.height +
             " width: " + loadDDsHeader.width +
             " sizeorpitch: " + loadDDsHeader.sizeOrPitch +
             " depth: " + loadDDsHeader.depth +
             " mipmapcount: " + loadDDsHeader.mipmapCount +
             " alphabitdepth: " + loadDDsHeader.alphaBitDepth +
             " pixelformatSize: " + loadDDsHeader.pixelFormatSize +
             " pixelformatflags: " + loadDDsHeader.pixelFormatFlags +
             " pixelformatFourcc: " + loadDDsHeader.pixelFormatFourcc +
             " pixelformatRgbBitCount: " + loadDDsHeader.pixelFormatRgbBitCount +
             " pixelformatRbitMask: " + loadDDsHeader.pixelFormatRbitMask +
             " pixelformatGbitMask: " + loadDDsHeader.pixelFormatGbitMask +
             " pixelformatBbitMask: " + loadDDsHeader.pixelFormatBbitMask +
             " pixelformatAbitMask: " + loadDDsHeader.pixelFormatAbitMask
         );
        switch (loadDDsHeader.pixelFormatFourcc)
        {
            case 861165636:
                isDxt3 = true;
                return TextureFormat.DXT5;
            case 827611204:
                return TextureFormat.DXT1;
            case 894720068:
                return TextureFormat.DXT5;
            case 64:
                return TextureFormat.RGB24;
            case 0:
                if (loadDDsHeader.pixelFormatFlags == 528391)
                    return TextureFormat.BGRA32;
                else if (loadDDsHeader.pixelFormatRgbBitCount == 24)
                    return TextureFormat.RGB24;
                else
                    return TextureFormat.BGRA32;
        }
        return TextureFormat.DXT5;
    }
}


