﻿using UnityEngine;

public partial class MapEditor : MonoBehaviour
{
    public static MapEditor current;
    [Header("Connections")]
    public Light Sun;
    public Camera Cam;
    public Terrain Teren;
    public Material TerrainMaterial;
    public Material WaterMaterial;
    public Transform WaterLevel;
    public TerrainData Data;
    public ResourceBrowser resBrowser;

    void Awake()
    {
        current = this;
        resBrowser.Instantiate();
    }
}
