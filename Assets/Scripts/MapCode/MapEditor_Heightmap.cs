﻿using UnityEngine;

public partial class MapEditor : MonoBehaviour
{
    public static float[,] returnValues;
    private static int _lastGetWidth = 0;
    private static int _lastGetHeight = 0;
    private static float[,] _heights = new float[1, 1];

    public static void ApplyHeightmap()
    {
        current.Data.SetHeightsDelayLOD(0, 0, _heights);
    }

    public static void SetHeights(int _x, int _y, float[,] _values)
    {
        int _width = _values.GetLength(1);
        int _height = _values.GetLength(0);
        for (int x = 0; x < _width; x++)
            for (int y = 0; y < _height; y++)
                _heights[_x + x, _y + y] = _values[x, y];
        current.Data.SetHeightsDelayLOD(_x, _y, _values);
    }

    public static void ApplyChanges(int _x, int _y)
    {
        int __x = 0;
        int __y = 0;
        for (__x = 0; __x < _lastGetWidth; __x++)
            for (__y = 0; __y < _lastGetHeight; __y++)
                _heights[_y + __x, _x + __y] = returnValues[__x, __y];
        current.Data.SetHeightsDelayLOD(_x, _y, returnValues);
    }

    public static void SetHeight(int _x, int _y, float _value)
    {
        _heights[_x, _y] = _value;
    }

    public static float GetHeight(int _x, int _y)
    {
        return _heights[_x, _y];
    }

    public static float[,] GetValues(int _x, int _y, int _width, int _height)
    {
        if (_width != _lastGetWidth || _height != _lastGetHeight)
        {
            returnValues = new float[_width, _height];
            _lastGetWidth = _width;
            _lastGetHeight = _height;
        }
        int __x = 0;
        int __y = 0;
        for (__x = 0; __x < _lastGetWidth; __x++)
            for (__y = 0; __y < _lastGetHeight; __y++)
                returnValues[__x, __y] = _heights[_y + __x, _x + __y];
        return returnValues;
    }
}
