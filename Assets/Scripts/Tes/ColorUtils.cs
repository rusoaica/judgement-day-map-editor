﻿using UnityEngine;

public static class ColorUtils
{
    public static Color B8G8R8ToColor(uint _b8g8r8)
    {
        return B8G8R8ToColor32(_b8g8r8);
    }

    public static Color32 B8G8R8ToColor32(uint _b8g8r8)
    {
        var _b8 = (byte)((_b8g8r8 >> 16) & 0xFF);
        var _g8 = (byte)((_b8g8r8 >> 8) & 0xFF);
        var _r8 = (byte)(_b8g8r8 & 0xFF);
        return new Color32(_r8, _g8, _b8, 255);
    }

    public static Color R5G6B5ToColor(ushort _r5g6b5)
    {
        var _r5 = ((_r5g6b5 >> 11) & 31);
        var _g6 = ((_r5g6b5 >> 5) & 63);
        var _b5 = (_r5g6b5 & 31);
        return new Color((float)_r5 / 31, (float)_g6 / 63, (float)_b5 / 31, 1);
    }

    public static Color32 R5G6B5ToColor32(ushort _r5g6b5)
    {
        return R5G6B5ToColor(_r5g6b5);
    }
}