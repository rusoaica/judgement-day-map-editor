﻿using System;
using System.IO;
using UnityEngine;

namespace DDS
{
    public enum DDSFlags
    {
        Caps = 0x1,
        Height = 0x2,
        Width = 0x4,
        Pitch = 0x8,
        PixelFormat = 0x1000,
        MipmapCount = 0x20000,
        LinearSize = 0x80000,
        Depth = 0x800000
    }

    public enum DDSPixelFormatFlags
    {
        AlphaPixels = 0x1,
        Alpha = 0x2,
        FourCC = 0x4,
        RGB = 0x40,
        YUV = 0x200,
        Luminance = 0x20000
    }

    public enum DDSCaps
    {
        Complex = 0x8,
        Mipmap = 0x400000,
        Texture = 0x1000
    }

    public enum DDSCaps2
    {
        Cubemap = 0x200,
        CubemapPositiveX = 0x400,
        CubemapNegativeX = 0x800,
        CubemapPositiveY = 0x1000,
        CubemapNegativeY = 0x2000,
        CubemapPositiveZ = 0x4000,
        CubemapNegativeZ = 0x8000,
        Volume = 0x200000
    }

    public struct DDSHeader
    {
        public uint dwSize;
        public uint dwFlags;
        public uint dwHeight;
        public uint dwWidth;
        public uint dwPitchOrLinearSize;
        public uint dwDepth;
        public uint dwMipMapCount;
        public uint[] dwReserved1;
        public DDSPixelFormat pixelFormat;
        public uint dwCaps;
        public uint dwCaps2;
        public uint dwCaps3;
        public uint dwCaps4;
        public uint dwReserved2;

        public void Deserialize(UnityBinaryReader _reader)
        {
            dwSize = _reader.ReadLEUInt32();
            if (dwSize != 124)
                throw new FileFormatException("Invalid DDS file header size: " + dwSize.ToString() + '.');
            dwFlags = _reader.ReadLEUInt32();
            if (!Utils.ContainsBitFlags(dwFlags, (uint)DDSFlags.Height, (uint)DDSFlags.Width))
                throw new FileFormatException("Invalid DDS file flags: " + dwFlags.ToString() + '.');
            dwHeight = _reader.ReadLEUInt32();
            dwWidth = _reader.ReadLEUInt32();
            dwPitchOrLinearSize = _reader.ReadLEUInt32();
            dwDepth = _reader.ReadLEUInt32();
            dwMipMapCount = _reader.ReadLEUInt32();
            dwReserved1 = new uint[11];
            for (int i = 0; i < dwReserved1.Length; i++)
                dwReserved1[i] = _reader.ReadLEUInt32();
            pixelFormat = new DDSPixelFormat();
            pixelFormat.Deserialize(_reader);
            dwCaps = _reader.ReadLEUInt32();
            if (!Utils.ContainsBitFlags(dwCaps, (uint)DDSCaps.Texture))
                throw new FileFormatException("Invalid DDS file caps: " + dwCaps.ToString() + '.');
            dwCaps2 = _reader.ReadLEUInt32();
            dwCaps3 = _reader.ReadLEUInt32();
            dwCaps4 = _reader.ReadLEUInt32();
            dwReserved2 = _reader.ReadLEUInt32();
        }
    }

    public struct DDSPixelFormat
    {
        public uint size;
        public uint flags;
        public byte[] fourCC;
        public uint RGBBitCount;
        public uint RBitMask;
        public uint GBitMask;
        public uint BBitMask;
        public uint ABitMask;

        public void Deserialize(UnityBinaryReader reader)
        {
            size = reader.ReadLEUInt32();
            if (size != 32)
                throw new FileFormatException("Invalid DDS file pixel format size: " + size.ToString() + '.');
            flags = reader.ReadLEUInt32();
            fourCC = reader.ReadBytes(4);
            RGBBitCount = reader.ReadLEUInt32();
            RBitMask = reader.ReadLEUInt32();
            GBitMask = reader.ReadLEUInt32();
            BBitMask = reader.ReadLEUInt32();
            ABitMask = reader.ReadLEUInt32();
        }
    }

    public static class DDSReader
    {
        static Color32[] Dxt1Colors = new Color32[16];
        static Color[] Dxt3_colorTable = new Color[4];
        static uint[] Dxt1colorIndices = new uint[16];
        static byte[] Dxt1colorIndexBytes = new byte[4];
        static byte[] Dxt3_Alphas = new byte[16];
        static byte[] Dxt3_compressedAlphas = new byte[16];

        /// <summary>
        /// Loads a DDS texture from a file.
        /// </summary>
        public static Texture2DInfo LoadDDSTexture(string _file_path, bool _flip_vertically = false)
        {
            return LoadDDSTexture(File.Open(_file_path, FileMode.Open, FileAccess.Read), _flip_vertically);
        }

        /// <summary>
        /// Loads a DDS texture from an input stream.
        /// </summary>
        public static Texture2DInfo LoadDDSTexture(Stream _input_stream, bool _flip_vertically = false)
        {
            using (var _reader = new UnityBinaryReader(_input_stream))
            {
                var _magic_string = _reader.ReadBytes(4); // Check the magic string.
                if (!StringUtils.Equals(_magic_string, "DDS "))
                    throw new FileFormatException("Invalid DDS file magic string: \"" + System.Text.Encoding.ASCII.GetString(_magic_string) + "\".");
                var _header = new DDSHeader(); // Deserialize the DDS file header.
                _header.Deserialize(_reader);
                bool _has_mipmaps; // Figure out the texture format and load the texture data.
                uint _dds_mipmap_level_count;
                TextureFormat _texture_format;
                int _bytes_per_pixel;
                byte[] _texture_data;
                ExtractDDSTextureFormatAndData(_header, _reader, out _has_mipmaps, out _dds_mipmap_level_count, out _texture_format, out _bytes_per_pixel, out _texture_data);
                // Post-process the texture to generate missing mipmaps and possibly flip it vertically.
                PostProcessDDSTexture((int)_header.dwWidth, (int)_header.dwHeight, _bytes_per_pixel, _has_mipmaps, (int)_dds_mipmap_level_count, _texture_data, _flip_vertically);
                return new Texture2DInfo((int)_header.dwWidth, (int)_header.dwHeight, _texture_format, _has_mipmaps, _texture_data);
            }
        }

        /// <summary>
        /// Decodes a DXT1-compressed 4x4 block of texels using a prebuilt 4-color color table.
        /// </summary>
        /// <remarks>See https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx#BC1 </remarks>
        private static Color32[] DecodeDXT1TexelBlock(UnityBinaryReader _reader, Color[] _color_table)
        {
            Debug.Assert(_color_table.Length == 4);
            _reader.Read(Dxt1colorIndexBytes, 0, Dxt1colorIndexBytes.Length);
            const uint _bits_per_color_index = 2;
            uint _row_index = 0;
            uint _column_index = 0;
            uint _row_base_color_index_index = 0;
            uint _row_base_bit_offset = 0;
            uint _bit_offset = 0;
            for (_row_index = 0; _row_index < 4; _row_index++)
            {
                _row_base_color_index_index = 4 * _row_index;
                _row_base_bit_offset = 8 * _row_index;
                for (_column_index = 0; _column_index < 4; _column_index++)
                {
                    // Color indices are arranged from right to left.
                    _bit_offset = _row_base_bit_offset + (_bits_per_color_index * (3 - _column_index));
                    Dxt1colorIndices[_row_base_color_index_index + _column_index] = (uint)Utils.GetBits(_bit_offset, _bits_per_color_index, Dxt1colorIndexBytes);
                }
            }
            // Calculate pixel colors
            for (_row_index = 0; _row_index < 16; _row_index++)
                Dxt1Colors[_row_index] = _color_table[Dxt1colorIndices[_row_index]];
            return Dxt1Colors;
        }

        /// <summary>
        /// Builds a 4-color color table for a DXT1-compressed 4x4 block of texels and then decodes the texels.
        /// </summary>
        /// <remarks>See https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx#BC1 </remarks>
        private static Color32[] DecodeDXT1TexelBlock(UnityBinaryReader _reader, bool _contains_alpha)
        {
            // Create the color table.
            var _color_table = new Color[4];
            _color_table[0] = ColorUtils.R5G6B5ToColor(_reader.ReadLEUInt16());
            _color_table[1] = ColorUtils.R5G6B5ToColor(_reader.ReadLEUInt16());
            if (!_contains_alpha)
            {
                _color_table[2] = Color.Lerp(_color_table[0], _color_table[1], 1.0f / 3);
                _color_table[3] = Color.Lerp(_color_table[0], _color_table[1], 2.0f / 3);
            }
            else
            {
                _color_table[2] = Color.Lerp(_color_table[0], _color_table[1], 1.0f / 2);
                _color_table[3] = new Color(0, 0, 0, 0);
            }
            // Calculate pixel colors.
            return DecodeDXT1TexelBlock(_reader, _color_table);
        }

        /// <summary>
        /// Decodes a DXT3-compressed 4x4 block of texels.
        /// </summary>
        /// <remarks>See https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx#BC2 </remarks>
        private static Color32[] DecodeDXT3TexelBlock(UnityBinaryReader _reader)
        {
            // Read compressed pixel alphas.
            int _row_index = 0;
            int _column_index = 0;
            ushort _compressed_alpha_row = 0;
            for (_row_index = 0; _row_index < 4; _row_index++)
            {
                _compressed_alpha_row = _reader.ReadLEUInt16();
                for (_column_index = 0; _column_index < 4; _column_index++) // Each compressed alpha is 4 bits.                   
                    Dxt3_compressedAlphas[(4 * _row_index) + _column_index] = (byte)((_compressed_alpha_row >> (_column_index * 4)) & 0xF);
            }
            // Calculate pixel alphas.
            int i = 0;
            for (i = 0; i < 16; i++)
                Dxt3_Alphas[i] = (byte)Mathf.RoundToInt(((float)Dxt3_compressedAlphas[i] / 15) * 255);
            // Create the color table.
            Dxt3_colorTable[0] = ColorUtils.R5G6B5ToColor(_reader.ReadLEUInt16());
            Dxt3_colorTable[1] = ColorUtils.R5G6B5ToColor(_reader.ReadLEUInt16());
            Dxt3_colorTable[2] = Color.Lerp(Dxt3_colorTable[0], Dxt3_colorTable[1], 1.0f / 3);
            Dxt3_colorTable[3] = Color.Lerp(Dxt3_colorTable[0], Dxt3_colorTable[1], 2.0f / 3);
            // Calculate pixel colors.
            Color32[] _colors = DecodeDXT1TexelBlock(_reader, Dxt3_colorTable);
            for (i = 0; i < 16; i++)
                _colors[i].a = Dxt3_Alphas[i];
            return _colors;
        }

        /// <summary>
        /// Decodes a DXT5-compressed 4x4 block of texels.
        /// </summary>
        /// <remarks>See https://msdn.microsoft.com/en-us/library/windows/desktop/bb694531(v=vs.85).aspx#BC3 </remarks>
        private static Color32[] DecodeDXT5TexelBlock(UnityBinaryReader _reader)
        {
            // Create the alpha table.
            var _alpha_table = new float[8];
            _alpha_table[0] = _reader.ReadByte();
            _alpha_table[1] = _reader.ReadByte();

            if (_alpha_table[0] > _alpha_table[1])
                for (int i = 0; i < 6; i++)
                    _alpha_table[2 + i] = Mathf.Lerp(_alpha_table[0], _alpha_table[1], (float)(1 + i) / 7);
            else
            {
                for (int i = 0; i < 4; i++)
                    _alpha_table[2 + i] = Mathf.Lerp(_alpha_table[0], _alpha_table[1], (float)(1 + i) / 5);
                _alpha_table[6] = 0;
                _alpha_table[7] = 255;
            }
            // Read pixel alpha indices.
            var _alpha_indices = new uint[16];
            var _alpha_index_bytes_row_0 = new byte[3];
            _reader.Read(_alpha_index_bytes_row_0, 0, _alpha_index_bytes_row_0.Length);
            Array.Reverse(_alpha_index_bytes_row_0); // beware little-endianness!
            var _alpha_index_bytes_row_1 = new byte[3];
            _reader.Read(_alpha_index_bytes_row_1, 0, _alpha_index_bytes_row_1.Length);
            Array.Reverse(_alpha_index_bytes_row_1); // take care of little-endianness.
            const uint _bits_per_alpha_index = 3;
            _alpha_indices[0] = (uint)Utils.GetBits(21, _bits_per_alpha_index, _alpha_index_bytes_row_0);
            _alpha_indices[1] = (uint)Utils.GetBits(18, _bits_per_alpha_index, _alpha_index_bytes_row_0);
            _alpha_indices[2] = (uint)Utils.GetBits(15, _bits_per_alpha_index, _alpha_index_bytes_row_0);
            _alpha_indices[3] = (uint)Utils.GetBits(12, _bits_per_alpha_index, _alpha_index_bytes_row_0);
            _alpha_indices[4] = (uint)Utils.GetBits(9, _bits_per_alpha_index, _alpha_index_bytes_row_0);
            _alpha_indices[5] = (uint)Utils.GetBits(6, _bits_per_alpha_index, _alpha_index_bytes_row_0);
            _alpha_indices[6] = (uint)Utils.GetBits(3, _bits_per_alpha_index, _alpha_index_bytes_row_0);
            _alpha_indices[7] = (uint)Utils.GetBits(0, _bits_per_alpha_index, _alpha_index_bytes_row_0);
            _alpha_indices[8] = (uint)Utils.GetBits(21, _bits_per_alpha_index, _alpha_index_bytes_row_1);
            _alpha_indices[9] = (uint)Utils.GetBits(18, _bits_per_alpha_index, _alpha_index_bytes_row_1);
            _alpha_indices[10] = (uint)Utils.GetBits(15, _bits_per_alpha_index, _alpha_index_bytes_row_1);
            _alpha_indices[11] = (uint)Utils.GetBits(12, _bits_per_alpha_index, _alpha_index_bytes_row_1);
            _alpha_indices[12] = (uint)Utils.GetBits(9, _bits_per_alpha_index, _alpha_index_bytes_row_1);
            _alpha_indices[13] = (uint)Utils.GetBits(6, _bits_per_alpha_index, _alpha_index_bytes_row_1);
            _alpha_indices[14] = (uint)Utils.GetBits(3, _bits_per_alpha_index, _alpha_index_bytes_row_1);
            _alpha_indices[15] = (uint)Utils.GetBits(0, _bits_per_alpha_index, _alpha_index_bytes_row_1);           
            var _color_table = new Color[4]; // Create the color table.
            _color_table[0] = ColorUtils.R5G6B5ToColor(_reader.ReadLEUInt16());
            _color_table[1] = ColorUtils.R5G6B5ToColor(_reader.ReadLEUInt16());
            _color_table[2] = Color.Lerp(_color_table[0], _color_table[1], 1.0f / 3);
            _color_table[3] = Color.Lerp(_color_table[0], _color_table[1], 2.0f / 3);
            // Calculate pixel colors.
            var _colors = DecodeDXT1TexelBlock(_reader, _color_table);
            for (int i = 0; i < 16; i++)
                _colors[i].a = (byte)Mathf.Round(_alpha_table[_alpha_indices[i]]);
            return _colors;
        }

        /// <summary>
        /// Copies a decoded texel block to a texture's data buffer. Takes into account DDS mipmap padding.
        /// </summary>
        /// <param name="decodedTexels">The decoded DDS texels.</param>
        /// <param name="_argb">The texture's data buffer.</param>
        /// <param name="_base_argb_index">The desired offset into the texture's data buffer. Used for mipmaps.</param>
        /// <param name="_base_row_index">The base row index in the texture where decoded texels are copied.</param>
        /// <param name="_base_column_index">The base column index in the texture where decoded texels are copied.</param>
        /// <param name="_texture_width">The width of the texture.</param>
        /// <param name="_texture_height">The height of the texture.</param>
        private static void CopyDecodedTexelBlock(Color32[] _decoded_texels, byte[] _argb, int _base_argb_index, int _base_row_index, int _base_column_index, int _texture_width, int _texture_height)
        {
            for (int i = 0; i < 4; i++) // row
            {
                for (int j = 0; j < 4; j++) // column
                {
                    var _row_index = _base_row_index + i;
                    var _column_index = _base_column_index + j;
                    // Don't copy padding on mipmaps.
                    if ((_row_index < _texture_height) && (_column_index < _texture_width))
                    {
                        var _decoded_texel_index = (4 * i) + j;
                        var _color = _decoded_texels[_decoded_texel_index];

                        var _argb_pixel_offset = (_texture_width * _row_index) + _column_index;
                        var _base_pixel_argb_index = _base_argb_index + (4 * _argb_pixel_offset);
                        _argb[_base_pixel_argb_index] = _color.a;
                        _argb[_base_pixel_argb_index + 1] = _color.r;
                        _argb[_base_pixel_argb_index + 2] = _color.g;
                        _argb[_base_pixel_argb_index + 3] = _color.b;
                    }
                }
            }
        }

        /// <summary>
        /// Decodes DXT data to ARGB.
        /// </summary>
        private static byte[] DecodeDXTToARGB(int _dxt_version, byte[] _compressed_data, uint _width, uint _height, DDSPixelFormat _pixel_format, uint _mipmap_count)
        {
            bool _alpha_flag = Utils.ContainsBitFlags(_pixel_format.flags, (uint)DDSPixelFormatFlags.AlphaPixels);
            bool _contains_alpha = _alpha_flag || ((_pixel_format.RGBBitCount == 32) && (_pixel_format.ABitMask != 0));

            var _reader = new UnityBinaryReader(new MemoryStream(_compressed_data));
            var _argb = new byte[TextureUtils.CalculateMipMappedTextureDataSize((int)_width, (int)_height, 4)];

            int _mip_map_width = (int)_width;
            int _mip_map_height = (int)_height;
            int _base_argb_index = 0;
            for (int _mip_map_index = 0; _mip_map_index < _mipmap_count; _mip_map_index++)
            {
                for (int _row_index = 0; _row_index < _mip_map_height; _row_index += 4)
                {
                    for (int _column_index = 0; _column_index < _mip_map_width; _column_index += 4)
                    {
                        Color32[] _colors = null;                       
                        switch (_dxt_version) // Doing a switch instead of using a delegate, for speed.
                        {
                            case 1:
                                _colors = DecodeDXT1TexelBlock(_reader, _contains_alpha);
                                break;
                            case 3:
                                _colors = DecodeDXT3TexelBlock(_reader);
                                break;
                            case 5:
                                _colors = DecodeDXT5TexelBlock(_reader);
                                break;
                            default:
                                throw new NotImplementedException("Tried decoding a DDS file using an unsupported DXT format: DXT" + _dxt_version.ToString());
                        }
                        CopyDecodedTexelBlock(_colors, _argb, _base_argb_index, _row_index, _column_index, _mip_map_width, _mip_map_height);
                    }
                }
                _base_argb_index += (_mip_map_width * _mip_map_height * 4);
                _mip_map_width /= 2;
                _mip_map_height /= 2;
            }
            return _argb;
        }

        private static byte[] DecodeDXT1ToARGB(byte[] _compressed_data, uint _width, uint _height, DDSPixelFormat _pixel_format, uint _mipmap_count)
        {
            return DecodeDXTToARGB(1, _compressed_data, _width, _height, _pixel_format, _mipmap_count);
        }

        private static byte[] DecodeDXT3ToARGB(byte[] _compressed_data, uint _width, uint _height, DDSPixelFormat _pixel_format, uint _mipmap_count)
        {
            return DecodeDXTToARGB(3, _compressed_data, _width, _height, _pixel_format, _mipmap_count);
        }

        private static byte[] DecodeDXT5ToARGB(byte[] _compressed_data, uint _width, uint _height, DDSPixelFormat _pixel_format, uint _mipmap_count)
        {
            return DecodeDXTToARGB(5, _compressed_data, _width, _height, _pixel_format, _mipmap_count);
        }

        /// <summary>
        /// Extracts a DDS file's texture format and pixel data.
        /// </summary>
        private static void ExtractDDSTextureFormatAndData(DDSHeader _header, UnityBinaryReader _reader, out bool _has_mipmaps, out uint _dds_mipmap_level_count, out TextureFormat _texture_format, out int _bytes_per_pixel, out byte[] _texture_data)
        {
            _has_mipmaps = Utils.ContainsBitFlags(_header.dwCaps, (uint)DDSCaps.Mipmap);
            _has_mipmaps = _has_mipmaps || _header.dwMipMapCount > 1;
            // Non-mipmapped textures still have one mipmap level: the texture itself.
            _dds_mipmap_level_count = _has_mipmaps ? _header.dwMipMapCount : 1;
            // If the DDS file contains uncompressed data.
            if (Utils.ContainsBitFlags(_header.pixelFormat.flags, (uint)DDSPixelFormatFlags.RGB))
            {
                // some permutation of RGB
                if (!Utils.ContainsBitFlags(_header.pixelFormat.flags, (uint)DDSPixelFormatFlags.AlphaPixels))
                    throw new NotImplementedException("Unsupported DDS file pixel format.");
                // some permutation of RGBA
                else
                {
                    // There should be 32 bits per pixel.
                    if (_header.pixelFormat.RGBBitCount != 32)
                        throw new FileFormatException("Invalid DDS file pixel format.");
                    // BGRA32
                    if ((_header.pixelFormat.BBitMask == 0x000000FF) && (_header.pixelFormat.GBitMask == 0x0000FF00) && (_header.pixelFormat.RBitMask == 0x00FF0000) && (_header.pixelFormat.ABitMask == 0xFF000000))
                    {
                        _texture_format = TextureFormat.BGRA32;
                        _bytes_per_pixel = 4;
                    }
                    // ARGB32
                    else if ((_header.pixelFormat.ABitMask == 0x000000FF) && (_header.pixelFormat.RBitMask == 0x0000FF00) && (_header.pixelFormat.GBitMask == 0x00FF0000) && (_header.pixelFormat.BBitMask == 0xFF000000))
                    {
                        _texture_format = TextureFormat.ARGB32;
                        _bytes_per_pixel = 4;
                    }
                    else
                        throw new NotImplementedException("Unsupported DDS file pixel format.");
                    if (!_has_mipmaps)
                        _texture_data = new byte[_header.dwPitchOrLinearSize * _header.dwHeight];
                    else // Create a data buffer to hold all mipmap levels down to 1x1.                       
                        _texture_data = new byte[TextureUtils.CalculateMipMappedTextureDataSize((int)_header.dwWidth, (int)_header.dwHeight, _bytes_per_pixel)];
                    _reader.ReadRestOfBytes(_texture_data, 0);
                }
            }
            else if (StringUtils.Equals(_header.pixelFormat.fourCC, "DXT1"))
            {
                _texture_format = TextureFormat.ARGB32;
                _bytes_per_pixel = 4;
                var _compressed_texture_data = _reader.ReadRestOfBytes();
                _texture_data = DecodeDXT1ToARGB(_compressed_texture_data, _header.dwWidth, _header.dwHeight, _header.pixelFormat, _dds_mipmap_level_count);
            }
            else if (StringUtils.Equals(_header.pixelFormat.fourCC, "DXT3"))
            {
                _texture_format = TextureFormat.ARGB32;
                _bytes_per_pixel = 4;
                var _compressed_texture_data = _reader.ReadRestOfBytes();
                _texture_data = DecodeDXT3ToARGB(_compressed_texture_data, _header.dwWidth, _header.dwHeight, _header.pixelFormat, _dds_mipmap_level_count);
            }
            else if (StringUtils.Equals(_header.pixelFormat.fourCC, "DXT5"))
            {
                _texture_format = TextureFormat.ARGB32;
                _bytes_per_pixel = 4;
                var _compressed_texture_data = _reader.ReadRestOfBytes();
                _texture_data = DecodeDXT5ToARGB(_compressed_texture_data, _header.dwWidth, _header.dwHeight, _header.pixelFormat, _dds_mipmap_level_count);
            }
            else
                throw new NotImplementedException("Unsupported DDS file pixel format.");
        }

        /// <summary>
        /// Generates missing mipmap levels for a DDS texture and optionally flips it.
        /// </summary>
        /// <param name="_width">The width of the texture.</param>
        /// <param name="_height">The height of the texture.</param>
        /// <param name="_bytes_per_pixel">The number of bytes per pixel.</param>
        /// <param name="_has_mipmaps">Does the DDS texture have mipmaps?</param>
        /// <param name="_dds_mipmap_level_count">The number of mipmap levels in the DDS file. 1 if the DDS file doesn't have mipmaps.</param>
        /// <param name="_data">The texture's data.</param>
        /// <param name="_flip_vertically">Should the texture be flipped vertically?</param>
        private static void PostProcessDDSTexture(int _width, int _height, int _bytes_per_pixel, bool _has_mipmaps, int _dds_mipmap_level_count, byte[] _data, bool _flip_vertically)
        {
            Debug.Assert((_width > 0) && (_height > 0) && (_bytes_per_pixel > 0) && (_dds_mipmap_level_count > 0) && (_data != null));
            // Flip mip-maps if necessary and generate missing mip-map levels.
            int _mipmap_level_width = _width;
            int _mipmap_level_height = _height;
            int _mipmap_level_index = 0;
            int _mipmap_level_data_offset = 0;
            // While we haven't processed all of the mipmap levels we should process.
            while ((_mipmap_level_width > 1) || (_mipmap_level_height > 1))
            {
                var _mipmap_data_size = (_mipmap_level_width * _mipmap_level_height * _bytes_per_pixel);
                // If the DDS file contains the current mipmap level, flip it vertically if necessary.
                if (_flip_vertically && (_mipmap_level_index < _dds_mipmap_level_count))
                    Utils.Flip2DSubArrayVertically(_data, _mipmap_level_data_offset, _mipmap_level_height, _mipmap_level_width * _bytes_per_pixel);
                // Break after optionally flipping the first mipmap level if the DDS texture doesn't have mipmaps.
                if (!_has_mipmaps)
                    break;
                // Generate the next mipmap level's data if the DDS file doesn't contain it.
                if ((_mipmap_level_index + 1) >= _dds_mipmap_level_count)
                    TextureUtils.Downscale4Component32BitPixelsX2(_data, _mipmap_level_data_offset, _mipmap_level_height, _mipmap_level_width, _data, _mipmap_level_data_offset + _mipmap_data_size);
                // Switch to the next mipmap level.
                _mipmap_level_index++;
                _mipmap_level_width = (_mipmap_level_width > 1) ? (_mipmap_level_width / 2) : _mipmap_level_width;
                _mipmap_level_height = (_mipmap_level_height > 1) ? (_mipmap_level_height / 2) : _mipmap_level_height;
                _mipmap_level_data_offset += _mipmap_data_size;
            }
        }
    }
}