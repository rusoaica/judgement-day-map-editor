﻿using UnityEngine;

/// <summary>
/// Stores information about a 2D texture.
/// Used when loading textures on background threads because Texture2D can't be used on any thread but the main thread.
/// </summary>
public class Texture2DInfo
{
    public int width, height;
    public TextureFormat format;
    public bool hasMipmaps;
    public byte[] rawData;

    public Texture2DInfo(int _width, int _height, TextureFormat _format, bool _has_mipmaps, byte[] _raw_data)
    {
        width = _width;
        height = _height;
        format = _format;
        hasMipmaps = _has_mipmaps;
        rawData = _raw_data;
    }

    /// <summary>
    /// Creates a Unity Texture2D from this Texture2DInfo. Can only be called from the main thread.
    /// </summary>
    public Texture2D ToTexture2D()
    {
        var _texture = new Texture2D(width, height, format, true, false);
        if (rawData != null)
        {
            _texture.LoadRawTextureData(rawData);
            _texture.Apply(true);
        }
        return _texture;
    }
}