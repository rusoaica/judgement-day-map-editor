﻿using UnityEngine;

public static class TextureUtils
{
    public static void FlipTexture2DVertically(Texture2D _texture_2d)
    {
        var _pixels = _texture_2d.GetPixels32();
        Utils.Flip2DArrayVertically(_pixels, _texture_2d.height, _texture_2d.width);
        _texture_2d.SetPixels32(_pixels);
        _texture_2d.Apply();
    }

    public static int CalculateMipMapCount(int _base_texture_width, int _base_texture_height)
    {
        Debug.Assert((_base_texture_width > 0) && (_base_texture_height > 0));
        int _longer_length = Mathf.Max(_base_texture_width, _base_texture_height);
        int _mipmap_count = 0;
        int _current_longer_length = _longer_length;
        while (_current_longer_length > 0)
        {
            _mipmap_count++;
            _current_longer_length /= 2;
        }
        return _mipmap_count;
    }

    public static int CalculateMipMappedTextureDataSize(int _base_texture_width, int _base_texture_height, int _bytes_per_pixel)
    {
        Debug.Assert((_base_texture_width > 0) && (_base_texture_height > 0) && (_bytes_per_pixel > 0));
        int _data_size = 0;
        int _current_width = _base_texture_width;
        int _current_height = _base_texture_height;
        while (true)
        {
            _data_size += (_current_width * _current_height * _bytes_per_pixel);
            if ((_current_width == 1) && (_current_height == 1))
                break;
            _current_width = (_current_width > 1) ? (_current_width / 2) : _current_width;
            _current_height = (_current_height > 1) ? (_current_height / 2) : _current_height;
        }
        return _data_size;
    }

    // TODO: Improve algorithm for images with odd dimensions.
    public static void Downscale4Component32BitPixelsX2(byte[] _source_bytes, int _source_start_index, int _source_row_count, int _source_column_count, byte[] _destination_bytes, int _destination_start_index)
    {
        int _bytes_per_pixel = 4;
        int _component_count = 4;
        Debug.Assert((_source_start_index >= 0) && (_source_row_count >= 0) && (_source_column_count >= 0) && ((_source_start_index + (_bytes_per_pixel * _source_row_count * _source_column_count)) <= _source_bytes.Length));
        var _destination_row_count = _source_row_count / 2;
        var _destination_column_count = _source_column_count / 2;
        Debug.Assert((_destination_start_index >= 0) && ((_destination_start_index + (_bytes_per_pixel * _destination_row_count * _destination_column_count)) <= _destination_bytes.Length));
        for (int _destination_row_index = 0; _destination_row_index < _destination_row_count; _destination_row_index++)
        {
            for (int _destination_column_index = 0; _destination_column_index < _destination_column_count; _destination_column_index++)
            {
                int _source_row_index_0 = 2 * _destination_row_index;
                int _source_column_index_0 = 2 * _destination_column_index;
                int _source_pixel_0_index = (_source_column_count * _source_row_index_0) + _source_column_index_0;
                var _source_pixel_start_indices = new int[4];
                _source_pixel_start_indices[0] = _source_start_index + (_bytes_per_pixel * _source_pixel_0_index); // top-left
                _source_pixel_start_indices[1] = _source_pixel_start_indices[0] + _bytes_per_pixel; // top-right
                _source_pixel_start_indices[2] = _source_pixel_start_indices[0] + (_bytes_per_pixel * _source_column_count); // bottom-left
                _source_pixel_start_indices[3] = _source_pixel_start_indices[2] + _bytes_per_pixel; // bottom-right
                int _destination_pixel_index = (_destination_column_count * _destination_row_index) + _destination_column_index;
                int _destination_pixel_start_index = _destination_start_index + (_bytes_per_pixel * _destination_pixel_index);
                for (int _component_index = 0; _component_index < _component_count; _component_index++)
                {
                    float _average_component = 0;
                    for (int _source_pixel_index = 0; _source_pixel_index < _source_pixel_start_indices.Length; _source_pixel_index++)
                        _average_component += _source_bytes[_source_pixel_start_indices[_source_pixel_index] + _component_index];
                    _average_component /= _source_pixel_start_indices.Length;
                    _destination_bytes[_destination_pixel_start_index + _component_index] = (byte)Mathf.RoundToInt(_average_component);
                }
            }
        }
    }
}