﻿using System;
using System.IO;
using UnityEngine;

#pragma warning disable 0168

public class UnityBinaryReader : IDisposable
{

    private BinaryReader _reader;
    private byte[] _readBuffer = new byte[16]; //buffer the size of a decimal, created to minimize allocations

    public Stream BaseStream
    {
        get { return _reader.BaseStream; }
    }

    public UnityBinaryReader(Stream _input)
    {
        _reader = new BinaryReader(_input);
    }

    public UnityBinaryReader(Stream _input, System.Text.Encoding _encoding)
    {
        _reader = new BinaryReader(_input);
    }

    void IDisposable.Dispose()
    {
        Close();
    }

    ~UnityBinaryReader()
    {
        Close();
    }

    public void Close()
    {
        if (_reader != null)
        {
            _reader.Close();
            _reader = null;
        }
    }

    public byte ReadByte()
    {
        return _reader.ReadByte();
    }

    public sbyte ReadSByte()
    {
        return _reader.ReadSByte();
    }

    public void Read(byte[] _buffer, int _index, int _count)
    {
        _reader.Read(_buffer, _index, _count);
    }

    public byte[] ReadBytes(int _count)
    {
        return _reader.ReadBytes(_count);
    }

    public byte[] ReadRestOfBytes()
    {
        var _remaining_byte_count = _reader.BaseStream.Length - _reader.BaseStream.Position;
        Debug.Assert(_remaining_byte_count <= int.MaxValue);
        return _reader.ReadBytes((int)_remaining_byte_count);
    }

    public void ReadRestOfBytes(byte[] _buffer, int _start_index)
    {
        var _remaining_byte_count = _reader.BaseStream.Length - _reader.BaseStream.Position;
        Debug.Assert((_start_index >= 0) && (_remaining_byte_count <= int.MaxValue) && ((_start_index + _remaining_byte_count) <= _buffer.Length));
        _reader.Read(_buffer, _start_index, (int)_remaining_byte_count);
    }

    public string ReadASCIIString(int _length)
    {
        Debug.Assert(_length >= 0);
        return System.Text.Encoding.ASCII.GetString(_reader.ReadBytes(_length));
    }

    public string ReadPossiblyNullTerminatedASCIIString(int _length_including_possible_null_terminator)
    {
        Debug.Assert(_length_including_possible_null_terminator > 0);
        var _bytes = _reader.ReadBytes(_length_including_possible_null_terminator);
        var _char_count = (Utils.Last(_bytes) != 0) ? _bytes.Length : (_bytes.Length - 1); // Ignore the null terminator.
        return System.Text.Encoding.Default.GetString(_bytes, 0, _char_count);
    }

    #region Little Endian
    public bool ReadLEBool32()
    {
        return ReadLEUInt32() != 0;
    }

    public ushort ReadLEUInt16()
    {
        _reader.Read(_readBuffer, 0, 2);
        return (ushort)((_readBuffer[1] << 8) | _readBuffer[0]);
    }

    public uint ReadLEUInt32()
    {
        _reader.Read(_readBuffer, 0, 4);
        return ((uint)_readBuffer[3] << 24) | ((uint)_readBuffer[2] << 16) | ((uint)_readBuffer[1] << 8) | _readBuffer[0];
    }

    public ulong ReadLEUInt64()
    {
        _reader.Read(_readBuffer, 0, 8);
        return ((ulong)_readBuffer[7] << 56) | ((ulong)_readBuffer[6] << 48) | ((ulong)_readBuffer[5] << 40) | ((ulong)_readBuffer[4] << 32) | ((ulong)_readBuffer[3] << 24) | ((ulong)_readBuffer[2] << 16) | ((ulong)_readBuffer[1] << 8) | _readBuffer[0];
    }

    public short ReadLEInt16()
    {
        var _short_as_ushort = ReadLEUInt16();
        unsafe
        {
            return *((short*)(&_short_as_ushort));
        }
    }

    public int ReadLEInt32()
    {
        var _int_as_uint = ReadLEUInt32();
        unsafe
        {
            return *((int*)(&_int_as_uint));
        }
    }

    public long ReadLEInt64()
    {
        var _long_as_ulong = ReadLEUInt64();
        unsafe
        {
            return *((long*)(&_long_as_ulong));
        }
    }

    public float ReadLESingle()
    {
        var _single_as_uint = ReadLEUInt32();
        unsafe
        {
            return *((float*)(&_single_as_uint));
        }
    }

    public double ReadLEDouble()
    {
        var _double_as_uint = ReadLEUInt64();
        unsafe
        {
            return *((double*)(&_double_as_uint));
        }
    }

    public byte[] ReadLELength32PrefixedBytes()
    {
        var _length = ReadLEUInt32();
        return _reader.ReadBytes((int)_length);
    }

    public string ReadLELength32PrefixedASCIIString()
    {
        return System.Text.Encoding.ASCII.GetString(ReadLELength32PrefixedBytes());
    }

    public Vector2 ReadLEVector2()
    {
        var _x = ReadLESingle();
        var _y = ReadLESingle();
        return new Vector2(_x, _y);
    }

    public Vector3 ReadLEVector3()
    {
        var _x = ReadLESingle();
        var _y = ReadLESingle();
        var _z = ReadLESingle();
        return new Vector3(_x, _y, _z);
    }

    public Vector4 ReadLEVector4()
    {
        var _x = ReadLESingle();
        var _y = ReadLESingle();
        var _z = ReadLESingle();
        var _w = ReadLESingle();
        return new Vector4(_x, _y, _z, _w);
    }

    /// <summary>
    /// Reads a column-major 3x3 matrix but returns a functionally equivalent 4x4 matrix.
    /// </summary>
    public Matrix4x4 ReadLEColumnMajorMatrix3x3()
    {
        var _matrix = new Matrix4x4();

        for (int _column_index = 0; _column_index < 4; _column_index++)
        {
            for (int _row_index = 0; _row_index < 4; _row_index++)
            {
                if ((_row_index <= 2) && (_column_index <= 2)) // If we're in the 3x3 part of the matrix, read values. Otherwise, use the identity matrix.
                    _matrix[_row_index, _column_index] = ReadLESingle();
                else
                    _matrix[_row_index, _column_index] = (_row_index == _column_index) ? 1 : 0;
            }
        }
        return _matrix;
    }

    /// <summary>
    /// Reads a row-major 3x3 matrix but returns a functionally equivalent 4x4 matrix.
    /// </summary>
    public Matrix4x4 ReadLERowMajorMatrix3x3()
    {
        var _matrix = new Matrix4x4();
        for (int _row_index = 0; _row_index < 4; _row_index++)
        {
            for (int _column_index = 0; _column_index < 4; _column_index++)
            {
                if ((_row_index <= 2) && (_column_index <= 2))
                    _matrix[_row_index, _column_index] = ReadLESingle();
                else
                    _matrix[_row_index, _column_index] = (_row_index == _column_index) ? 1 : 0;
            }
        }
        return _matrix;
    }

    public Matrix4x4 ReadLEColumnMajorMatrix4x4()
    {
        var _matrix = new Matrix4x4();
        for (int _column_index = 0; _column_index < 4; _column_index++)
            for (int _row_index = 0; _row_index < 4; _row_index++)
                _matrix[_row_index, _column_index] = ReadLESingle();
        return _matrix;
    }

    public Matrix4x4 ReadLERowMajorMatrix4x4()
    {
        var _matrix = new Matrix4x4();
        for (int _row_index = 0; _row_index < 4; _row_index++)
            for (int _column_index = 0; _column_index < 4; _column_index++)
                _matrix[_row_index, _column_index] = ReadLESingle();
        return _matrix;
    }

    public Quaternion ReadLEQuaternionWFirst()
    {
        float _w = ReadLESingle();
        float _x = ReadLESingle();
        float _y = ReadLESingle();
        float _z = ReadLESingle();
        return new Quaternion(_x, _y, _z, _w);
    }
    public Quaternion ReadLEQuaternionWLast()
    {
        float _x = ReadLESingle();
        float _y = ReadLESingle();
        float _z = ReadLESingle();
        float _w = ReadLESingle();
        return new Quaternion(_x, _y, _z, _w);
    }
    #endregion

    #region Big Endian
    public bool ReadBEBool32()
    {
        return ReadBEUInt32() != 0;
    }

    public ushort ReadBEUInt16()
    {
        _reader.Read(_readBuffer, 0, 2);
        return (ushort)((_readBuffer[0] << 8) | _readBuffer[1]);
    }

    public uint ReadBEUInt32()
    {
        _reader.Read(_readBuffer, 0, 4);
        return ((uint)_readBuffer[0] << 24) | ((uint)_readBuffer[1] << 16) | ((uint)_readBuffer[2] << 8) | _readBuffer[3];
    }

    public ulong ReadBEUInt64()
    {
        _reader.Read(_readBuffer, 0, 8);
        return ((ulong)_readBuffer[0] << 56) | ((ulong)_readBuffer[1] << 48) | ((ulong)_readBuffer[2] << 40) | ((ulong)_readBuffer[3] << 32) | ((ulong)_readBuffer[4] << 24) | ((ulong)_readBuffer[5] << 16) | ((ulong)_readBuffer[6] << 8) | _readBuffer[7];
    }

    public short ReadBEInt16()
    {
        var _short_as_ushort = ReadBEUInt16();
        unsafe
        {
            return *((short*)(&_short_as_ushort));
        }
    }

    public int ReadBEInt32()
    {
        var _int_as_uint = ReadBEUInt32();
        unsafe
        {
            return *((int*)(&_int_as_uint));
        }
    }

    public long ReadBEInt64()
    {
        var _long_as_ulong = ReadBEUInt64();
        unsafe
        {
            return *((long*)(&_long_as_ulong));
        }
    }

    public float ReadBESingle()
    {
        var _single_as_uint = ReadBEUInt32();
        unsafe
        {
            return *((float*)(&_single_as_uint));
        }
    }

    public double ReadBEDouble()
    {
        var _double_as_uint = ReadBEUInt64();
        unsafe
        {
            return *((double*)(&_double_as_uint));
        }
    }

    public byte[] ReadBELength32PrefixedBytes()
    {
        var _length = ReadBEUInt32();
        return _reader.ReadBytes((int)_length);
    }

    public string ReadBELength32PrefixedASCIIString()
    {
        return System.Text.Encoding.ASCII.GetString(ReadBELength32PrefixedBytes());
    }

    public Vector2 ReadBEVector2()
    {
        var _x = ReadBESingle();
        var _y = ReadBESingle();
        return new Vector2(_x, _y);
    }

    public Vector3 ReadBEVector3()
    {
        var _x = ReadBESingle();
        var _y = ReadBESingle();
        var _z = ReadBESingle();
        return new Vector3(_x, _y, _z);
    }

    public Vector4 ReadBEVector4()
    {
        var _x = ReadBESingle();
        var _y = ReadBESingle();
        var _z = ReadBESingle();
        var _w = ReadBESingle();
        return new Vector4(_x, _y, _z, _w);
    }

    /// <summary>
    /// Reads a column-major 3x3 matrix but returns a functionally equivalent 4x4 matrix.
    /// </summary>
    public Matrix4x4 ReadBEColumnMajorMatrix3x3()
    {
        var _matrix = new Matrix4x4();
        for (int _column_index = 0; _column_index < 4; _column_index++)
        {
            for (int _row_index = 0; _row_index < 4; _row_index++)
            {
                if ((_row_index <= 2) && (_column_index <= 2)) // If we're in the 3x3 part of the matrix, read values. Otherwise, use the identity matrix.
                    _matrix[_row_index, _column_index] = ReadBESingle();
                else
                    _matrix[_row_index, _column_index] = (_row_index == _column_index) ? 1 : 0;
            }
        }
        return _matrix;
    }

    public Matrix4x4 ReadBERowMajorMatrix3x3()
    {
        var _matrix = new Matrix4x4();

        for (int _row_index = 0; _row_index < 4; _row_index++)
        {
            for (int _column_index = 0; _column_index < 4; _column_index++)
            {
                if ((_row_index <= 2) && (_column_index <= 2))
                    _matrix[_row_index, _column_index] = ReadBESingle();
                else
                    _matrix[_row_index, _column_index] = (_row_index == _column_index) ? 1 : 0;
            }
        }
        return _matrix;
    }

    public Matrix4x4 ReadBEColumnMajorMatrix4x4()
    {
        var _matrix = new Matrix4x4();
        for (int _column_index = 0; _column_index < 4; _column_index++)
            for (int _row_index = 0; _row_index < 4; _row_index++)
                _matrix[_row_index, _column_index] = ReadBESingle();
        return _matrix;
    }

    public Matrix4x4 ReadBERowMajorMatrix4x4()
    {
        var _matrix = new Matrix4x4();
        for (int _row_index = 0; _row_index < 4; _row_index++)
            for (int _column_index = 0; _column_index < 4; _column_index++)
                _matrix[_row_index, _column_index] = ReadBESingle();
        return _matrix;
    }

    public Quaternion ReadBEQuaternionWFirst()
    {
        float _w = ReadBESingle();
        float _x = ReadBESingle();
        float _y = ReadBESingle();
        float _z = ReadBESingle();
        return new Quaternion(_x, _y, _z, _w);
    }

    public Quaternion ReadBEQuaternionWLast()
    {
        float _x = ReadBESingle();
        float _y = ReadBESingle();
        float _z = ReadBESingle();
        float _w = ReadBESingle();
        return new Quaternion(_x, _y, _z, _w);
    }
    #endregion
}