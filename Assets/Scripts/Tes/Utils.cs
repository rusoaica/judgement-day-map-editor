﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace System.IO
{
    /// <summary>
	/// A reimplementation of a standard exception type that was introduced in .NET 3.0.
	/// </summary>
	public class FileFormatException : FormatException
    {
        public FileFormatException() : base() { }
        public FileFormatException(string message) : base(message) { }
        public FileFormatException(string message, Exception innerException) : base(message, innerException) { }
    }
}

public static class StringUtils
{
    /// <summary>
	/// Quickly checks if an ASCII encoded string is equal to a C# string.
	/// </summary>
	public static bool Equals(byte[] _ascii_bytes, string _str)
    {
        if (_ascii_bytes.Length != _str.Length)
            return false;
        for (int i = 0; i < _ascii_bytes.Length; i++)
            if (_ascii_bytes[i] != _str[i])
                return false;
        return true;
    }
}

public static class Utils
{
    public static T Last<T>(T[] _array)
    {
        Debug.Assert(_array.Length > 0);
        return _array[_array.Length - 1];
    }

    public static T Last<T>(List<T> _list)
    {
        Debug.Assert(_list.Count > 0);
        return _list[_list.Count - 1];
    }

    public static void Swap<T>(ref T _a, ref T _b)
    {
        var tmp = _a;
        _a = _b;
        _b = tmp;
    }

    /// <summary>
	/// Checks if a bit string (an unsigned integer) contains a collection of bit flags.
	/// </summary>
	public static bool ContainsBitFlags(uint _bit_string, params uint[] _bit_flags)
    {
        uint _all_bit_flags = 0; // Construct a bit string containing all the bit flags.
        foreach (var bitFlag in _bit_flags)
            _all_bit_flags |= bitFlag;
        return (_bit_string & _all_bit_flags) == _all_bit_flags; // Check if the bit string contains all the bit flags.
    }

    /// <summary>
    /// Extracts a range of bits from a byte array.
    /// </summary>
    /// <param name="_bit_offset">An offset in bits from the most significant bit (byte 0, bit 0) of the byte array.</param>
    /// <param name="_bit_count">The number of bits to extract. Cannot exceed 64.</param>
    /// <param name="_bytes">A big-endian byte array.</param>
    /// <returns>A ulong containing the right-shifted extracted bits.</returns>
    public static ulong GetBits(uint _bit_offset, uint _bit_count, byte[] _bytes)
    {
        Debug.Assert((_bit_count <= 64) && ((_bit_offset + _bit_count) <= (8 * _bytes.Length)));
        ulong _bits = 0;
        uint _remaining_bit_count = _bit_count;
        uint _byte_index = _bit_offset / 8;
        uint _bit_index = _bit_offset - (8 * _byte_index);
        uint _num_bits_left_in_byte = 0;
        uint _num_bits_read_now = 0;
        uint _unmasked_bits = 0;
        uint _bit_mask = 0;
        uint _bits_read_now = 0;
        while (_remaining_bit_count > 0)
        {
            _num_bits_left_in_byte = 8 - _bit_index; // Read bits from the byte array.
            _num_bits_read_now = Math.Min(_remaining_bit_count, _num_bits_left_in_byte);
            _unmasked_bits = (uint)_bytes[_byte_index] >> (int)(8 - (_bit_index + _num_bits_read_now));
            _bit_mask = 0xFFu >> (int)(8 - _num_bits_read_now);
            _bits_read_now = _unmasked_bits & _bit_mask;
            _bits <<= (int)_num_bits_read_now; // Store the bits we read.
            _bits |= _bits_read_now;
            _bit_index += _num_bits_read_now; // Prepare for the next iteration.
            if (_bit_index == 8)
            {
                _byte_index++;
                _bit_index = 0;
            }
            _remaining_bit_count -= _num_bits_read_now;
        }
        return _bits;
    }

    /// <summary>
    /// Transforms _x from an element of [_min_0, _max_0] to an element of [_min_1, _max_1].
    /// </summary>
    public static float ChangeRange(float _x, float _min_0, float _max_0, float _min_1, float _max_1)
    {
        Debug.Assert((_min_0 <= _max_0) && (_min_1 <= _max_1) && (_x >= _min_0) && (_x <= _max_0));
        var _range_0 = _max_0 - _min_0;
        var _range_1 = _max_1 - _min_1;
        var _x_pct = (_x - _min_0) / _range_0;
        return _min_1 + (_x_pct * _range_1);
    }

    /// <summary>
    /// Calculates the minimum and maximum values of an array.
    /// </summary>
    public static void GetExtrema(float[] _array, out float _min, out float _max)
    {
        _min = float.MaxValue;
        _max = float.MinValue;
        foreach (var _element in _array)
        {
            _min = Math.Min(_min, _element);
            _max = Math.Max(_max, _element);
        }
    }

    /// <summary>
    /// Calculates the minimum and maximum values of a 2D array.
    /// </summary>
    public static void GetExtrema(float[,] _array, out float _min, out float _max)
    {
        _min = float.MaxValue;
        _max = float.MinValue;
        foreach (var _element in _array)
        {
            _min = Math.Min(_min, _element);
            _max = Math.Max(_max, _element);
        }
    }

    /// <summary>
    /// Calculates the minimum and maximum values of a 3D array.
    /// </summary>
    public static void GetExtrema(float[,,] _array, out float _min, out float _max)
    {
        _min = float.MaxValue;
        _max = float.MinValue;
        foreach (var _element in _array)
        {
            _min = Math.Min(_min, _element);
            _max = Math.Max(_max, _element);
        }
    }

    public static void Flip2DArrayVertically<T>(T[] _array, int _row_count, int _column_count)
    {
        Flip2DSubArrayVertically(_array, 0, _row_count, _column_count);
    }

    /// <summary>
    /// Flips a portion of a 2D array vertically.
    /// </summary>
    /// <param name="_array">A 2D array represented as a 1D row-major array.</param>
    /// <param name="_start_index">The 1D index of the top left element in the portion of the 2D array we want to flip.</param>
    /// <param name="_row_count">The number of rows in the sub-array.</param>
    /// <param name="_column_count">The number of columns in the sub-array.</param>
    public static void Flip2DSubArrayVertically<T>(T[] _array, int _start_index, int _row_count, int _column_count)
    {
        Debug.Assert((_start_index >= 0) && (_row_count >= 0) && (_column_count >= 0) && ((_start_index + (_row_count * _column_count)) <= _array.Length));
        var _temp_row = new T[_column_count];
        var _last_row_index = _row_count - 1;
        for (int _row_index = 0; _row_index < (_row_count / 2); _row_index++)
        {
            var _other_row_index = _last_row_index - _row_index;
            var _row_start_index = _start_index + (_row_index * _column_count);
            var _other_row_start_index = _start_index + (_other_row_index * _column_count);
            Array.Copy(_array, _other_row_start_index, _temp_row, 0, _column_count); // other -> temp
            Array.Copy(_array, _row_start_index, _array, _other_row_start_index, _column_count); // row -> other
            Array.Copy(_temp_row, 0, _array, _row_start_index, _column_count); // temp -> row
        }
    }
}