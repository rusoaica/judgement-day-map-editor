﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class AppMenu : MonoBehaviour
{
    public Button[] Buttons;
    public GameObject[] Popups;
    public Toggle gridToggle;
    public Toggle slopeToggle;
    public AudioClip menuClicked;
    private bool isMenuOpen = false;
    private bool buttonClicked;
    private bool isFileOpen = false;
    private bool isEditOpen = false;
    private bool isToolsOpen = false;
    private bool isSymmetryOpen = false;
    private bool isHelpOpen = false;

    private void Awake()
    {
        if (File.Exists(Application.persistentDataPath + "/GameSettings.dat"))
        {
            BinaryFormatter binaryformatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameSettings.dat", FileMode.Open);
            GameSettings.Data = (GameSettings)binaryformatter.Deserialize(file);
            SoundManager.Instance.musicAudio.volume = GameSettings.Data.musicVolumeValue;
            SoundManager.Instance.computerAudio.volume = GameSettings.Data.computerVolumeValue;
            SoundManager.Instance.effectAudio.volume = GameSettings.Data.fxVolumeValue;
            SoundManager.Instance.unitsAudio.volume = GameSettings.Data.unitsVolumeValue;
            SoundManager.Instance.buildingsAudio.volume = GameSettings.Data.buildingsVolumeValue;
            SoundManager.Instance.uiAudio.volume = GameSettings.Data.uiVolumeValue;
            file.Close();
        }
        CursorManager.main.ShowCursor();
    }

    void LateUpdate()
    {
        if (isMenuOpen)
        {
            if (buttonClicked)
            {
                buttonClicked = false;
                return;
            }
            if (Input.GetMouseButtonUp(0))
            {
                foreach (GameObject obj in Popups)
                {
                    obj.SetActive(false);
                    foreach (Transform _obj in obj.transform)
                        if (_obj.gameObject.GetComponent<MenuItemHoverEffect>() != null)
                            _obj.gameObject.GetComponent<MenuItemHoverEffect>().UnlitText();
                }
                foreach (Button but in Buttons)
                {
                    but.interactable = true;
                    but.transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                isFileOpen = false;
                isEditOpen = false;
                isToolsOpen = false;
                isSymmetryOpen = false;
                isHelpOpen = false;
                isMenuOpen = false;
            }
        }
    }

    public void MenuButton(string func)
    {
        if (!isMenuOpen) return;
        switch (func)
        {
            case "NewMap":
                break;
            case "Open":
                break;
            case "Recent":
                break;
            case "Save":
                break;
            case "SaveAs":
                break;
            case "Render":
                break;
            case "PlayMap":
                break;
            case "Exit":
                Application.Quit();
                break;
            case "Undo":
                break;
            case "Redo":
                break;
            case "Grid":
                //if (gridToggle.isOn)
                //    gridToggle.transform.GetChild(2).GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/GFX/UI/chkChecked.png", typeof(Sprite));
                //else
                //    gridToggle.transform.GetChild(2).GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/GFX/UI/chkUnchecked.png", typeof(Sprite));
                break;
            case "Slope":
                //if (slopeToggle.isOn)
                //    slopeToggle.transform.GetChild(2).GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/GFX/UI/chkChecked.png", typeof(Sprite));
                //else
                //    slopeToggle.transform.GetChild(2).GetComponent<Image>().sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/GFX/UI/chkUnchecked.png", typeof(Sprite));
                break;
            case "SymmetrySettings":
                break;
            case "Help":
                break;
            case "About":
                break;
            case "Donate":
                break;
            case "Shortcuts":
                Application.OpenURL("http://www.rusoaica.com/RTS/");
                break;
            case "Links":
                break;
        }
    }

    public void OpenMenu(string func)
    {
        if (!isMenuOpen)
        {
            foreach (GameObject obj in Popups)
                obj.SetActive(false);
            foreach (Button but in Buttons)
                but.transform.GetComponent<ButtonToggleEffect>().Unlit();
        }
        switch (func)
        {
            case "File":
                if (isFileOpen)
                {
                    isFileOpen = false;
                    isMenuOpen = false;
                    Popups[0].SetActive(false);
                    Buttons[0].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                else
                {
                    isFileOpen = true;
                    isEditOpen = false;
                    isToolsOpen = false;
                    isSymmetryOpen = false;
                    isHelpOpen = false;
                    isMenuOpen = true;
                    Popups[0].SetActive(true);
                    Popups[1].SetActive(false);
                    Popups[2].SetActive(false);
                    Popups[3].SetActive(false);
                    Popups[4].SetActive(false);
                    Buttons[0].transform.GetComponent<ButtonToggleEffect>().Lit();
                    Buttons[1].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[2].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[3].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[4].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                break;
            case "Edit":
                if (isEditOpen)
                {
                    isEditOpen = false;
                    isMenuOpen = false;
                    Popups[1].SetActive(false);
                    Buttons[1].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                else
                {
                    isFileOpen = false;
                    isEditOpen = true;
                    isToolsOpen = false;
                    isSymmetryOpen = false;
                    isHelpOpen = false;
                    isMenuOpen = true;
                    Popups[0].SetActive(false);
                    Popups[1].SetActive(true);
                    Popups[2].SetActive(false);
                    Popups[3].SetActive(false);
                    Popups[4].SetActive(false);
                    Buttons[0].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[1].transform.GetComponent<ButtonToggleEffect>().Lit();
                    Buttons[2].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[3].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[4].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                break;
            case "Tools":
                if (isToolsOpen)
                {
                    isToolsOpen = false;
                    isMenuOpen = false;
                    Popups[2].SetActive(false);
                    Buttons[2].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                else
                {
                    isFileOpen = false;
                    isEditOpen = false;
                    isToolsOpen = true;
                    isSymmetryOpen = false;
                    isHelpOpen = false;
                    isMenuOpen = true;
                    Popups[0].SetActive(false);
                    Popups[1].SetActive(false);
                    Popups[2].SetActive(true);
                    Popups[3].SetActive(false);
                    Popups[4].SetActive(false);
                    Buttons[0].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[1].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[2].transform.GetComponent<ButtonToggleEffect>().Lit();
                    Buttons[3].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[4].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                break;
            case "Symmetry":
                if (isSymmetryOpen)
                {
                    isSymmetryOpen = false;
                    isMenuOpen = false;
                    Popups[3].SetActive(false);
                    Buttons[3].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                else
                {
                    isFileOpen = false;
                    isEditOpen = false;
                    isToolsOpen = false;
                    isSymmetryOpen = true;
                    isHelpOpen = false;
                    isMenuOpen = true;
                    Popups[0].SetActive(false);
                    Popups[1].SetActive(false);
                    Popups[2].SetActive(false);
                    Popups[3].SetActive(true);
                    Popups[4].SetActive(false);
                    Buttons[0].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[1].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[2].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[3].transform.GetComponent<ButtonToggleEffect>().Lit();
                    Buttons[4].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                break;
            case "Help":
                if (isHelpOpen)
                {
                    isHelpOpen = false;
                    isMenuOpen = false;
                    Popups[4].SetActive(false);
                    Buttons[4].transform.GetComponent<ButtonToggleEffect>().Unlit();
                }
                else
                {
                    isFileOpen = false;
                    isEditOpen = false;
                    isToolsOpen = false;
                    isSymmetryOpen = false;
                    isHelpOpen = true;
                    isMenuOpen = true;
                    Popups[0].SetActive(false);
                    Popups[1].SetActive(false);
                    Popups[2].SetActive(false);
                    Popups[3].SetActive(false);
                    Popups[4].SetActive(true);
                    Buttons[0].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[1].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[2].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[3].transform.GetComponent<ButtonToggleEffect>().Unlit();
                    Buttons[4].transform.GetComponent<ButtonToggleEffect>().Lit();
                }
                break;
        }
        if (SoundManager.Instance != null)
            SoundManager.Instance.PlayUI(menuClicked);
        buttonClicked = true;
    }

    public bool IsMenuOpen()
    {
        return isMenuOpen;
    }
}
