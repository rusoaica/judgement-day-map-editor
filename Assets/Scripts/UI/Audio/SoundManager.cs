﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{
    public AudioSource effectAudio;
    public AudioSource musicAudio;
    public AudioSource computerAudio;
    public AudioSource unitsAudio;
    public AudioSource buildingsAudio;
    public AudioSource uiAudio;
    public static SoundManager Instance = null;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public void PlayEffect(AudioClip _clip)
    {
        effectAudio.clip = _clip;
        effectAudio.Play();
    }

    public void PlayUI(AudioClip _clip)
    {
        uiAudio.clip = _clip;
        uiAudio.Play();
    }

    public void PlayComputer(AudioClip _clip)
    {
        computerAudio.clip = _clip;
        computerAudio.Play();
    }

    public void PlayMusic(AudioClip _clip)
    {
        musicAudio.clip = _clip;
        musicAudio.Play();
    }

    public void PlayUnits(AudioClip _clip)
    {
        unitsAudio.clip = _clip;
        unitsAudio.Play();
    }

    public void PlayBuildings(AudioClip _clip)
    {
        buildingsAudio.clip = _clip;
        buildingsAudio.Play();
    }
}