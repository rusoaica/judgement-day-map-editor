﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggleEffect : MonoBehaviour
{
    public void Lit()
    {
        GetComponentsInChildren<Text>()[1].color = new Color32(0x32, 0xFF, 0x32, 0xFF);
        transform.GetChild(0).gameObject.SetActive(true);
    }

    public void Unlit()
    {
        GetComponentsInChildren<Text>()[1].color = new Color32(0x32, 0xFF, 0x32, 0xFF);
        transform.GetChild(0).gameObject.SetActive(false);
    }
}
