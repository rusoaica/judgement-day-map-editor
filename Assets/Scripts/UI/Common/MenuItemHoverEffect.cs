﻿using UnityEngine;
using UnityEngine.UI;

public class MenuItemHoverEffect : MonoBehaviour
{
    public void LitText()
    {
        gameObject.transform.GetChild(1).GetComponent<Text>().color = new RGBColor(0, 255, 0, 255).getRGBColor;
    }

    public void UnlitText()
    {
        gameObject.transform.GetChild(1).GetComponent<Text>().color = new RGBColor(107, 125, 97, 255).getRGBColor;
    }
}
