﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectVisibilityToggle : MonoBehaviour
{
    public GameObject objectToToggle;

    public void Show()
    {
        objectToToggle.SetActive(true);
    }

    public void Hide()
    {
        objectToToggle.SetActive(false);
    }
}
