﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrollbarToggleEffect : MonoBehaviour, IBeginDragHandler, IEndDragHandler
{
    public GameObject toggleObject;

    public void OnBeginDrag(PointerEventData eventData)
    {
        toggleObject.SetActive(true);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        toggleObject.SetActive(false);
    }
}
