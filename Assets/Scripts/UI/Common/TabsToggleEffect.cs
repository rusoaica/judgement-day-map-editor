﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabsToggleEffect : MonoBehaviour
{
    public GameObject[] Tabs;
    public GameObject[] TabLines;
    public GameObject[] Panels;
    public AudioClip TabClickedSound;

    public void Toggle(string _tab)
    {
        foreach (GameObject tab in Tabs)
            tab.transform.GetComponent<ButtonToggleEffect>().Unlit();
        foreach (GameObject _object in TabLines)
            _object.SetActive(false);

        switch (_tab)
        {
            case "Graphics":
                TabLines[0].SetActive(true);
                Tabs[0].transform.GetComponent<ButtonToggleEffect>().Lit();
                Panels[0].SetActive(true);
                Panels[1].SetActive(false);
                Panels[2].SetActive(false);
                Panels[3].SetActive(false);
                Panels[4].SetActive(false);
                break;
            case "Sound":
                TabLines[1].SetActive(true);
                Tabs[1].transform.GetComponent<ButtonToggleEffect>().Lit();
                Panels[0].SetActive(false);
                Panels[1].SetActive(true);
                Panels[2].SetActive(false);
                Panels[3].SetActive(false);
                Panels[4].SetActive(false);
                break;
            case "Game":
                TabLines[2].SetActive(true);
                Tabs[2].transform.GetComponent<ButtonToggleEffect>().Lit();
                Panels[0].SetActive(false);
                Panels[1].SetActive(false);
                Panels[2].SetActive(true);
                Panels[3].SetActive(false);
                Panels[4].SetActive(false);
                break;
            case "Speed":
                TabLines[3].SetActive(true);
                Tabs[3].transform.GetComponent<ButtonToggleEffect>().Lit();
                Panels[0].SetActive(false);
                Panels[1].SetActive(false);
                Panels[2].SetActive(false);
                Panels[3].SetActive(true);
                Panels[4].SetActive(false);
                break;
            case "Keyboard":
                TabLines[4].SetActive(true);
                Tabs[4].transform.GetComponent<ButtonToggleEffect>().Lit();
                Panels[0].SetActive(false);
                Panels[1].SetActive(false);
                Panels[2].SetActive(false);
                Panels[3].SetActive(false);
                Panels[4].SetActive(true);
                break;
        }
        if (SoundManager.Instance != null && TabClickedSound != null)
            SoundManager.Instance.PlayUI(TabClickedSound);
    }
}
