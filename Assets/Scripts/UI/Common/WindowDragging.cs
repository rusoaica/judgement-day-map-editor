﻿using UnityEngine;
using UnityEngine.EventSystems;

public class WindowDragging : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public bool isDragAreaChild;
    private Vector2 _pointerOffset;
    private RectTransform _canvasRectTransform;
    private RectTransform _panelRectTransform;

    private void Awake()
    {
        Canvas _canvas = GetComponentInParent<Canvas>();
        if (_canvas != null)
        {
            _canvasRectTransform = _canvas.transform as RectTransform;
            if (isDragAreaChild)
                _panelRectTransform = transform.parent as RectTransform;
            else
                _panelRectTransform = transform as RectTransform;
        }
    }

    public void OnPointerDown(PointerEventData _data)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_panelRectTransform, _data.position, _data.pressEventCamera, out _pointerOffset);
    }

    public void OnPointerUp(PointerEventData _data)
    {
        EditorSettings.SaveGameSettings();
    }

    public void OnDrag(PointerEventData _data)
    {
        if (_panelRectTransform == null)
            return;
        Vector2 _pointer_position = ClampToWindow(_data);
        Vector2 _local_pointer_position;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvasRectTransform, _pointer_position, _data.pressEventCamera, out _local_pointer_position))
        {
            _panelRectTransform.localPosition = _local_pointer_position - _pointerOffset;
            GameSettings.Data.preferencesWindowPosition = _panelRectTransform.position;
        }
    }

    private Vector2 ClampToWindow(PointerEventData _data)
    {
        Vector2 _raw_pointer_position = _data.position;
        Vector3[] _canvas_corners = new Vector3[4];
        _canvasRectTransform.GetWorldCorners(_canvas_corners);
        float _clamp_x = Mathf.Clamp(_raw_pointer_position.x, _canvas_corners[0].x, _canvas_corners[2].x);
        float _clamp_y = Mathf.Clamp(_raw_pointer_position.y, _canvas_corners[0].y, _canvas_corners[2].y);
        Vector2 _new_pointer_position = new Vector2(_clamp_x, _clamp_y);
        return _new_pointer_position;
    }
}
