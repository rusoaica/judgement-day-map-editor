﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WindowResizing : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Vector2 minSize;
    public Vector2 maxSize;
    public GameObject objectToResize;
    private RectTransform _rectTransform;
    private Vector2 _currentPointerPosition;
    private Vector2 _previousPointerPosition;
    private bool _isMouseButtonDown;

    private void Awake()
    {
        _rectTransform = objectToResize.GetComponent<RectTransform>();
    }

    public void OnPointerUp(PointerEventData _data)
    {
        EditorSettings.SaveGameSettings();
        _isMouseButtonDown = false;
        CursorManager.main.UpdateCursor(InteractionState.Nothing);
    }

    public void OnPointerDown(PointerEventData _data)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, _data.position, _data.pressEventCamera, out _previousPointerPosition);
        _isMouseButtonDown = true;
    }

    public void OnDrag(PointerEventData _data)
    {
        if (_rectTransform == null)
            return;
        Vector2 _size_delta = _rectTransform.sizeDelta;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, _data.position, _data.pressEventCamera, out _currentPointerPosition);
        Vector2 _resize_value = _currentPointerPosition - _previousPointerPosition;
        _size_delta += new Vector2(_resize_value.x, -_resize_value.y);
        _size_delta = new Vector2(Mathf.Clamp(_size_delta.x, minSize.x, maxSize.x), Mathf.Clamp(_size_delta.y, minSize.y, maxSize.y));
        _rectTransform.sizeDelta = _size_delta;
        _previousPointerPosition = _currentPointerPosition;
        GameSettings.Data.preferencesWindowSize = _size_delta;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        CursorManager.main.UpdateCursor(InteractionState.Resize);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!_isMouseButtonDown)
            CursorManager.main.UpdateCursor(InteractionState.Nothing);
    }
}
