﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cursor : MonoBehaviour
{

    public List<Texture2D> cursorPicture;
    public float animationTime = 0.1f;
    public int id;
    public bool centerTexture = true;
    private float _animationCounter = 0;
    private int _index = 0;
    private bool _isAnimated = false;

    public bool IsAnimated
    {
        get { return this._isAnimated; }
    }

    void Awake()
    {
        if (cursorPicture.Count == 0)
            Destroy(this);
        if (cursorPicture.Count > 1)
            _isAnimated = true;
    }

    public virtual void Animate(float _deltaTime)
    {
        _animationCounter += _deltaTime;
        if (_animationCounter >= animationTime)
        {
            _animationCounter = 0;
            _index++;
            if (_index >= cursorPicture.Count)
                _index = 0;
        }
    }

    public Texture2D GetCursorPicture()
    {
        return cursorPicture[_index];
    }
}
