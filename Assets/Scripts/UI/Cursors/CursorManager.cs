﻿using UnityEngine;

public class CursorManager : MonoBehaviour
{
    private Cursor[] _cursors;
    private Cursor _currentCursor;
    private float _cursorSize = 32.0f;
    private bool _showCursor = false;
    public static CursorManager main;

    void Awake()
    {
        main = this;
        UnityEngine.Cursor.visible = false;
    }

    void Start()
    {
        Cursor[] temp = GameObject.FindGameObjectWithTag("Cursors").GetComponents<Cursor>();
        _cursors = new Cursor[temp.Length];
        foreach (Cursor c in temp)
            _cursors[c.id] = c;
        _currentCursor = _cursors[0];
    }

    void Update()
    {
        if (_currentCursor.IsAnimated)
            _currentCursor.Animate(Time.deltaTime);
    }

    public void UpdateCursor(InteractionState _interactionState)
    {
        _currentCursor = _cursors[(int)_interactionState];
    }

    public void UpdateCursor(int _interactionState)
    {
        if (_currentCursor.id != _interactionState)
            _currentCursor = _cursors[_interactionState];
    }

    void OnGUI()
    {
        if (_showCursor)
        {
            GUI.depth = -2;
            float _offset;
            if (_currentCursor.centerTexture)
                _offset = _cursorSize;
            else
                _offset = 0;
            GUI.DrawTexture(new Rect(Input.mousePosition.x - (_offset / 2), Screen.height - Input.mousePosition.y - (_offset / 2), _cursorSize, _cursorSize), _currentCursor.GetCursorPicture());
        }
    }

    public void HideCursor()
    {
        _showCursor = false;
    }

    public void ShowCursor()
    {
        _showCursor = true;
    }
}

public enum InteractionState
{
    Nothing = 0,
    Resize = 1,
    Move = 2,
    Attack = 3,
    Select = 4,
    Deploy = 5,
    Interact = 6,
    Sell = 7,
    CantSell = 8,
    Fix = 9,
    CantFix = 10,
    Disable = 11,
    CantDisable = 12,
    Invalid = 13
}
