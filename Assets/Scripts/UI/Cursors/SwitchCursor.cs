﻿using UnityEngine;

public class SwitchCursor : MonoBehaviour
{
    public void UpdateCursor(int _interactionState)
    {
        CursorManager.main.UpdateCursor(_interactionState);
    }
}
