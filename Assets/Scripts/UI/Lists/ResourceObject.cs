﻿using UnityEngine;
using UnityEngine.UI;

public class ResourceObject : MonoBehaviour
{
    public int instanceId;
    public Text nameField;
    public Text nameFieldShadow;
    public GameObject selected;
    public RawImage[] rawImages;
    public Text[] customTexts;
    public UI3DMesh meshView;

    public void SetImages(Texture2D _tex)
    {
        foreach (RawImage _resource in rawImages)
            _resource.texture = _tex;
    }

    public void OnBeginDrag()
    {
        ResourceBrowser.dragedObject = this;
        UnityEngine.Cursor.SetCursor(ResourceBrowser.current.GetCursorImage(), Vector2.zero, CursorMode.Auto);
    }
}