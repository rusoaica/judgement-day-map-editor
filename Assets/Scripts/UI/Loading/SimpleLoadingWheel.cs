﻿using UnityEngine;

public class SimpleLoadingWheel : MonoBehaviour
{
    float lerp = 0;

    void OnEnable()
    {
        lerp = 0;
        transform.localRotation = Quaternion.identity;
    }

    void Update()
    {
        lerp += Time.unscaledDeltaTime * 0.9f;
        if (lerp >= 1)
            lerp--;
        transform.localRotation = Quaternion.Euler(Vector3.forward * -360 * lerp);
    }
}
