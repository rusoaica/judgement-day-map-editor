﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class ResourceBrowser : MonoBehaviour
{
    public static ResourceBrowser current;
    public static ResourceObject dragedObject;
    public static int selectedCategory = 0;

    [Header("UI")]
    public GameObject prefabTexture;
    public GameObject prefabDecal;
    public GameObject prefabProp;
    public GameObject loading;
    public Texture2D cursorImage;
    public Texture2D cursorImageProp;
    public Texture2D cursorImageDecal;
    public Dropdown envType;
    public Dropdown envCategory;
    public Material propMaterial;
    public Transform pivot;
    public ScrollRect scrollRect;
    public LayoutGroup layout;
    public ContentSizeFitter sizeFitter;

    [Header("Loaded assets")]
    public List<Texture2D> loadedTextures = new List<Texture2D>();
    public List<string> loadedPaths = new List<string>();

    private Coroutine _generatingList;
    private GameObject _lastSelection;
    private List<string> _loadedEnvPaths = new List<string>();  
    private string _selectedObject = "";
    private const string _currentMapPath = "current";
    private const string _currentMapFolderPath = "maps/";
    private const string _localPath = "env/";
    private const int _pauseEveryLoadedAsset = 1;
    private static string[] _categoryPaths = new string[] { "layers/", "splats/", "decals/", "Props/" };
    private bool _dontReload = false;
    private bool _customLoading = false;

    #region Helper Methods
    public Texture2D GetCursorImage()
    {
        if (envCategory.value == 1 || envCategory.value == 2)
            return cursorImageDecal;
        else if (envCategory.value == 3)
            return cursorImageProp;
        else
            return cursorImage;
    }

    public static bool IsDecal()
    {
        return current.envCategory.value == 1 || current.envCategory.value == 2;
    }

    public static bool IsProp()
    {
        return current.envCategory.value == 3;
    }
    #endregion

    #region UI
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
            if (dragedObject)
                UnityEngine.Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public void OnDropdownChanged()
    {
        if (!gameObject.activeSelf || _customLoading)
            return;
        _selectedObject = "";
        if (IsGenerating)
            StopCoroutine(_generatingList);
        _dontReload = false;
        pivot.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        _generatingList = StartCoroutine(GenerateList());
    }
    #endregion

    #region Init
    public void Instantiate()
    {
        current = this;
        ReadAllPacks();
    }

    public void Open()
    {
        gameObject.SetActive(true);
        if (IsGenerating)
            StopCoroutine(_generatingList);
        _dontReload = false;
        pivot.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        _generatingList = StartCoroutine(GenerateList());
    }

    void ReadAllPacks()
    {
        envType.ClearOptions();
        _loadedEnvPaths = new List<string>();
        List<Dropdown.OptionData> _new_options = new List<Dropdown.OptionData>();
        _loadedEnvPaths.Add("Common");
        _loadedEnvPaths.Add("Crystalline");
        _loadedEnvPaths.Add("Desert");
        _loadedEnvPaths.Add("Evergreen");
        _loadedEnvPaths.Add("Geothermal");
        _loadedEnvPaths.Add("Lava");
        _loadedEnvPaths.Add("Paradise");
        _loadedEnvPaths.Add("Red Barrens");
        _loadedEnvPaths.Add("Red Rocks");
        _loadedEnvPaths.Add("Swamp");
        _loadedEnvPaths.Add("Tropical");
        _loadedEnvPaths.Add("Tundra");
        _new_options.Add(new Dropdown.OptionData("Common"));
        _new_options.Add(new Dropdown.OptionData("Crystalline"));
        _new_options.Add(new Dropdown.OptionData("Desert"));
        _new_options.Add(new Dropdown.OptionData("Evergreen"));
        _new_options.Add(new Dropdown.OptionData("Geothermal"));
        _new_options.Add(new Dropdown.OptionData("Lava"));
        _new_options.Add(new Dropdown.OptionData("Paradise"));
        _new_options.Add(new Dropdown.OptionData("Red Barrens"));
        _new_options.Add(new Dropdown.OptionData("Red Rocks"));
        _new_options.Add(new Dropdown.OptionData("Swamp"));
        _new_options.Add(new Dropdown.OptionData("Tropical"));
        _new_options.Add(new Dropdown.OptionData("Tundra"));
        _loadedEnvPaths.Add(_currentMapFolderPath);
        _new_options.Add(new Dropdown.OptionData("Map folder"));
        _loadedEnvPaths.Add(_currentMapPath);
        _new_options.Add(new Dropdown.OptionData("On map"));
        envType.AddOptions(_new_options);
    }
    #endregion

    #region Generate List of Assets
    public void LoadStratumTexture(string _path)
    {
        int _last_category = envCategory.value;
        int _last_env_type = envType.value;
        string _begin_path = _path;
        _customLoading = true;
        StopCoroutine(_generatingList);
        for (int i = 0; i < envType.options.Count; i++)
        {
            if (envType.options[i].text.ToLower().Substring(0, 3) == _path.ToLower().Substring(0, 3))
            {
                envType.value = i;
                break;
            }
        }
        for (int i = 0; i < envCategory.options.Count; i++)
        {
            if (_categoryPaths[i].ToLower().Substring(0, 3) == _path.ToLower().Substring(3, 3))
            {
                envCategory.value = i;
                break;
            }
        }
        _selectedObject = _begin_path;
        gameObject.SetActive(true);
        _dontReload = _last_category == envCategory.value && _last_env_type == envType.value && !IsGenerating;
        if (!_dontReload)
            pivot.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        if (IsGenerating)
            StopCoroutine(_generatingList);
        _customLoading = false;
        _generatingList = StartCoroutine(GenerateList());
    }

    public void LoadPropBlueprint()
    {
        if (envCategory.value != 3)
        {
            envCategory.value = 3;
            gameObject.SetActive(true);
            if (IsGenerating)
                StopCoroutine(_generatingList);
            _customLoading = false;
            _generatingList = StartCoroutine(GenerateList());
        }
        else
            gameObject.SetActive(true);
    }

    private void Clean()
    {
        foreach (Transform child in pivot)
            Destroy(child.gameObject);
        loadedTextures = new List<Texture2D>();
    }

    private bool IsGenerating
    {
        get
        {
            return _generatingList != null;
        }
    }

    private IEnumerator GenerateList()
    {
        selectedCategory = envCategory.value;
        if (!_dontReload)
            Clean();
        else if (_lastSelection)
            _lastSelection.SetActive(false);
        loading.SetActive(true);
        int _counter = 0;
        int _id = 0;
        layout.enabled = true;
        sizeFitter.enabled = true;
        if (_loadedEnvPaths[envType.value] == _currentMapFolderPath) // "maps", user selects "map folder" from the dropdown list
        {            
            //to be implemented
        }
        else if (_loadedEnvPaths[envType.value] == _currentMapPath) // "current", user selects "on map" from drop down list
        {
            //to be implemented
        }
        else //generic resource from packs
        {
            yield return null;
            string _environment_type = envType.options[envType.value].text;
            if (_environment_type == "Red Barrens")
                _environment_type = "red";
            else if (_environment_type == "Red Rocks")
                _environment_type = "roc";
            string _local_path = _environment_type.ToLower().Substring(0, 3) + _categoryPaths[envCategory.value].ToLower().Substring(0, 3);
            List<TexturePackData> _list = new ReadWriteBinaryFile().ReadBinaryPack("/TEX/" + _local_path + ".dat");
            if (_list == null) //there are no textures for selected environment and category
            {
                loading.SetActive(false);
                yield break;
            }
            foreach (TexturePackData _file in _list)
            {
                if (_dontReload)
                {
                    if (_file.Filename.ToLower() == _selectedObject.ToLower())
                    {
                        _lastSelection = pivot.GetChild(_id).GetComponent<ResourceObject>().selected;
                        _lastSelection.SetActive(true);
                        pivot.GetComponent<RectTransform>().anchoredPosition = Vector2.up * 250 * Mathf.FloorToInt(_id / 5f);
                        break;
                    }
                }
                else
                {
                    string LocalName = _file.Filename;
                    LoadAtPath(_file);
                }
                switch (envCategory.value)
                {
                    case 0:
                        if (GenerateTextureButton(_file, prefabTexture))
                            yield return null;
                        else
                            yield return null;
                        break;
                    case 1:
                        if (GenerateTextureButton(_file, prefabDecal))
                            yield return null;
                        else
                            yield return null;
                        break;
                    case 2:
                        if (GenerateTextureButton(_file, prefabDecal))
                            yield return null;
                        else
                            yield return null;
                        break;
                }
                _counter++;
                if (_counter >= 6)
                {
                    _counter = 0;
                    yield return null;
                }
            }
            _id++;
            _counter++;
            if (_counter >= _pauseEveryLoadedAsset)
            {
                _counter = 0;
                yield return null;
            }
        }
        yield return null;
        layout.enabled = false;
        sizeFitter.enabled = false;
        loading.SetActive(false);
        _generatingList = null;
    }

    private void LoadAtPath(TexturePackData _data)
    {
        switch (envCategory.value)
        {
            case 0:
                if (GenerateTextureButton(_data, prefabTexture))
                { }
                break;
            case 1:
                if (GenerateTextureButton(_data, prefabDecal))
                { }
                break;
            case 2:
                if (GenerateTextureButton(_data, prefabDecal))
                { }
                break;
        }
    }
    #endregion

    #region Buttons
    private bool GenerateTextureButton(TexturePackData _texture_data, GameObject _prefab)
    {
        Texture2D _loaded_tex;
        try
        {
            _loaded_tex = GetGamedataFile.LoadTexture2DFromBinaryArray(_texture_data.Data, false);
        }
        catch (System.Exception e)
        {
            _loaded_tex = new Texture2D(128, 128);
            Debug.LogError("Unable to load DDS texture: " + e);
        }
        GameObject _new_button = Instantiate(_prefab) as GameObject;
        _new_button.transform.SetParent(pivot, false);
        _new_button.GetComponent<ResourceObject>().SetImages(_loaded_tex);
        _new_button.GetComponent<ResourceObject>().instanceId = loadedTextures.Count;
        _new_button.GetComponent<ResourceObject>().nameField.text = _texture_data.Filename.Replace(".dds", "");
        _new_button.GetComponent<ResourceObject>().nameFieldShadow.text = _texture_data.Filename.Replace(".dds", "");
        loadedTextures.Add(_loaded_tex);
        loadedPaths.Add(_texture_data.Filename);
        if (_texture_data.Filename.ToLower() == (_selectedObject.Contains("/") ? _selectedObject.ToLower().Substring(_selectedObject.LastIndexOf("/") + 1) : _selectedObject.ToLower()))
        {
            _new_button.GetComponent<ResourceObject>().selected.SetActive(true);
            pivot.GetComponent<RectTransform>().anchoredPosition = Vector2.up * 250 * Mathf.FloorToInt(loadedPaths.Count / 5f);
        }
        return false;
    }
    #endregion
}