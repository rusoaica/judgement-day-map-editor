﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UITextField : MonoBehaviour
{
    [Header("Config")]
    public FieldTypes fieldType;
    public float beginValue;

    [Header("UI")]
    public InputField inputFieldUi;
    public Slider sliderUi;

    [Header("Events")]
    public UnityEvent onBeginChange;
    public UnityEvent onValueChanged;
    public UnityEvent onEndEdit;

    private bool _hasValue = false;
    private bool _started = false;
    private bool _changingValue = false;
    private float _lastValue = 0;

    public enum FieldTypes
    {
        Text,
        Float,
        Int
    }

    private void Start()
    {
        if (_hasValue)
            return;
        if (fieldType == FieldTypes.Float)
            SetValue(beginValue);
        else if (fieldType == FieldTypes.Int)
            SetValue((int)beginValue);
    }

    void UpdateSliderValue(bool _clamp_text = false)
    {
        _changingValue = true;
        _hasValue = true;
        if (inputFieldUi.contentType == InputField.ContentType.IntegerNumber)
            _lastValue = int.Parse(inputFieldUi.text);
        else if (inputFieldUi.contentType == InputField.ContentType.DecimalNumber)
            _lastValue = float.Parse(inputFieldUi.text);
        if (sliderUi)
        {
            _lastValue = Mathf.Clamp(_lastValue, sliderUi.minValue, sliderUi.maxValue);
            if (_clamp_text)
                SetTextField();
            sliderUi.value = _lastValue;
        }
        _changingValue = false;
    }

    #region Events
    void InvokeStart()
    {
        if (_started)
            return;
        Debug.Log("Begin change");
        _started = true;
        onBeginChange.Invoke();
    }

    public void OnValueChangedInvoke()
    {
        if (_changingValue)
            return;
        InvokeStart();
        UpdateSliderValue();
        onValueChanged.Invoke();
    }

    public void OnEndEditInvoke()
    {
        if (_changingValue)
            return;
        UpdateSliderValue(true);
        onEndEdit.Invoke();
        SetTextField();
        _started = false;
    }

    public void OnSliderChanged()
    {
        if (_changingValue)
            return;
        InvokeStart();
        _changingValue = true;
        _hasValue = true;
        _lastValue = sliderUi.value;
        SetTextField();
        if (inputFieldUi.contentType == InputField.ContentType.IntegerNumber)
            sliderUi.value = int.Parse(inputFieldUi.text);
        if (inputFieldUi.contentType == InputField.ContentType.DecimalNumber)
            _lastValue = float.Parse(inputFieldUi.text);
        _changingValue = false;
        OnValueChangedInvoke();
    }

    public void OnSliderFinished()
    {
        _changingValue = true;
        _hasValue = true;
        _lastValue = sliderUi.value;
        SetTextField();
        if (inputFieldUi.contentType == InputField.ContentType.IntegerNumber)
            sliderUi.value = int.Parse(inputFieldUi.text);
        if (inputFieldUi.contentType == InputField.ContentType.DecimalNumber)
            _lastValue = float.Parse(inputFieldUi.text);
        _changingValue = false;
        OnEndEditInvoke();
    }
    #endregion

    #region Setters
    public void SetValue(string _value, bool _allow_invoke = false)
    {
        _hasValue = true;
        _changingValue = !_allow_invoke;
        inputFieldUi.text = _value;
        _changingValue = false;
    }

    public void SetValue(float _value, bool _allow_invoke = false)
    {
        _hasValue = true;
        _changingValue = !_allow_invoke;
        _lastValue = _value;
        if (sliderUi)
        {
            _lastValue = Mathf.Clamp(_lastValue, sliderUi.minValue, sliderUi.maxValue);
            sliderUi.value = _lastValue;
        }
        SetTextField();
        _changingValue = false;
    }

    public void SetValue(int _value, bool _allow_invoke = false)
    {
        _hasValue = true;
        _changingValue = !_allow_invoke;
        _lastValue = _value;
        if (sliderUi)
        {
            _lastValue = Mathf.Clamp(_lastValue, sliderUi.minValue, sliderUi.maxValue);
            sliderUi.value = _lastValue;
        }
        SetTextField();
        _changingValue = false;
    }

    void SetTextField()
    {
        if (inputFieldUi.contentType == InputField.ContentType.IntegerNumber)
            inputFieldUi.text = _lastValue.ToString();
        else if (inputFieldUi.contentType == InputField.ContentType.DecimalNumber)
            inputFieldUi.text = _lastValue.ToString("F2");
    }
    #endregion

    #region Getters
    public string Text
    {
        get { return inputFieldUi.text; }
    }

    public float Value
    {
        get
        {
            if (!_hasValue)
                _lastValue = beginValue;
            return _lastValue;
        }
    }

    public int IntValue
    {
        get
        {
            if (!_hasValue)
                _lastValue = (int)beginValue;
            return (int)_lastValue;
        }
    }
    #endregion
}

