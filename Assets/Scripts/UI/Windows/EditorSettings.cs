﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class EditorSettings : MonoBehaviour
{
    public GameObject modalBackground;
    public GameObject[] Buttons;
    public AudioClip ButtonClickAudio;
    public AudioClip StrategicLaunchDetectedVoice;
    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider computerVolumeSlider;
    public Slider soundFXVolumeSlider;
    public Slider unitsSpeechVolumeSlider;
    public Slider buildingsSpeechVolumeSlider;
    public Slider uiVolumeSlider;
    public Toggle playProductionMessages;

    public void Open()
    {
        if (SoundManager.Instance != null)
            SoundManager.Instance.PlayUI(ButtonClickAudio);
        foreach (GameObject obj in Buttons)
            obj.GetComponent<ButtonToggleEffect>().Unlit();
        LoadGameSettings();
        musicVolumeSlider.value = GameSettings.Data.musicVolumeValue;
        computerVolumeSlider.value = GameSettings.Data.computerVolumeValue;
        soundFXVolumeSlider.value = GameSettings.Data.fxVolumeValue;
        unitsSpeechVolumeSlider.value = GameSettings.Data.unitsVolumeValue; ;
        buildingsSpeechVolumeSlider.value = GameSettings.Data.buildingsVolumeValue;
        uiVolumeSlider.value = GameSettings.Data.uiVolumeValue;
        playProductionMessages.isOn = GameSettings.Data.playProductionMessage;
        if (GameSettings.Data.preferencesWindowSize.width != 0 && GameSettings.Data.preferencesWindowSize.height != 0)
            (transform as RectTransform).sizeDelta = GameSettings.Data.preferencesWindowSize;
        if (GameSettings.Data.preferencesWindowPosition.x != 0 && GameSettings.Data.preferencesWindowPosition.y != 0)
            transform.position = GameSettings.Data.preferencesWindowPosition;
        gameObject.SetActive(true);
        modalBackground.SetActive(true);
    }

    public void Close()
    {
        if (SoundManager.Instance != null)
            SoundManager.Instance.PlayUI(ButtonClickAudio);
        GameSettings.Data.masterVolumeValue = GameSettings.Data.masterVolumeSetValue;
        GameSettings.Data.musicVolumeValue = GameSettings.Data.musicVolumeSetValue;
        GameSettings.Data.fxVolumeValue = GameSettings.Data.fxVolumeSetValue;
        GameSettings.Data.computerVolumeValue = GameSettings.Data.computerVolumeSetValue;
        GameSettings.Data.unitsVolumeValue = GameSettings.Data.unitsVolumeSetValue;
        GameSettings.Data.buildingsVolumeValue = GameSettings.Data.buildingsVolumeSetValue;
        GameSettings.Data.playProductionMessage = GameSettings.Data.playProductionMessageSetValue;
        SoundManager.Instance.musicAudio.volume = GameSettings.Data.musicVolumeSetValue;
        SoundManager.Instance.computerAudio.volume = GameSettings.Data.computerVolumeSetValue;
        SoundManager.Instance.effectAudio.volume = GameSettings.Data.fxVolumeSetValue;
        SoundManager.Instance.unitsAudio.volume = GameSettings.Data.unitsVolumeSetValue;
        SoundManager.Instance.buildingsAudio.volume = GameSettings.Data.buildingsVolumeSetValue;
        SoundManager.Instance.uiAudio.volume = GameSettings.Data.uiVolumeSetValue;
        modalBackground.SetActive(false);
        gameObject.SetActive(false);
    }

    public void Save()
    {
        if (SoundManager.Instance != null)
            SoundManager.Instance.PlayUI(ButtonClickAudio);
        GameSettings.Data.masterVolumeValue = masterVolumeSlider.value;
        GameSettings.Data.musicVolumeValue = musicVolumeSlider.value;
        GameSettings.Data.computerVolumeValue = computerVolumeSlider.value;
        GameSettings.Data.fxVolumeValue = soundFXVolumeSlider.value;
        GameSettings.Data.unitsVolumeValue = unitsSpeechVolumeSlider.value;
        GameSettings.Data.buildingsVolumeValue = buildingsSpeechVolumeSlider.value;
        GameSettings.Data.uiVolumeValue = uiVolumeSlider.value;
        GameSettings.Data.playProductionMessage = playProductionMessages.isOn;
        SaveGameSettings();
        modalBackground.SetActive(false);
        gameObject.SetActive(false);
    }

    public void MasterVolumeChanged()
    {
        GameSettings.Data.masterVolumeInitialValue = masterVolumeSlider.value;
        if (GameSettings.Data.masterVolumeInitialValue != GameSettings.Data.masterVolumeValue)
        {
            GameSettings.Data.masterVolumeInitialValue = GameSettings.Data.masterVolumeValue;
            GameSettings.Data.musicVolumeValue = GameSettings.Data.masterVolumeValue;
            GameSettings.Data.computerVolumeValue = GameSettings.Data.masterVolumeValue;
            GameSettings.Data.fxVolumeValue = GameSettings.Data.masterVolumeValue;
            GameSettings.Data.unitsVolumeValue = GameSettings.Data.masterVolumeValue;
            GameSettings.Data.buildingsVolumeValue = GameSettings.Data.masterVolumeValue;
            musicVolumeSlider.value = masterVolumeSlider.value;
            computerVolumeSlider.value = masterVolumeSlider.value;
            soundFXVolumeSlider.value = masterVolumeSlider.value;
            unitsSpeechVolumeSlider.value = masterVolumeSlider.value;
            buildingsSpeechVolumeSlider.value = masterVolumeSlider.value;
            uiVolumeSlider.value = masterVolumeSlider.value;
        }
    }

    public void MusicVolumeChanged()
    {
        GameSettings.Data.musicVolumeValue = musicVolumeSlider.value;
        if (GameSettings.Data.musicVolumeValue != GameSettings.Data.musicVolumeInitialValue)
        {
            SoundManager.Instance.musicAudio.volume = GameSettings.Data.musicVolumeValue;
            GameSettings.Data.musicVolumeInitialValue = GameSettings.Data.musicVolumeValue;
        }
    }

    public void ComputerVolumeChanged()
    {
        GameSettings.Data.computerVolumeValue = computerVolumeSlider.value;
        if (GameSettings.Data.computerVolumeValue != GameSettings.Data.computerVolumeInitialValue)
        {
            SoundManager.Instance.computerAudio.volume = GameSettings.Data.computerVolumeValue;
            SoundManager.Instance.PlayComputer(StrategicLaunchDetectedVoice);
            GameSettings.Data.computerVolumeInitialValue = GameSettings.Data.computerVolumeValue;
        }
    }

    public void FXVolumeChanged()
    {
        GameSettings.Data.fxVolumeValue = soundFXVolumeSlider.value;
        if (GameSettings.Data.fxVolumeValue != GameSettings.Data.fxVolumeInitialValue)
        {
            SoundManager.Instance.effectAudio.volume = GameSettings.Data.fxVolumeValue;
            GameSettings.Data.fxVolumeInitialValue = GameSettings.Data.fxVolumeValue;
        }
    }

    public void UnitsSpeechVolumeChanged()
    {
        GameSettings.Data.unitsVolumeValue = unitsSpeechVolumeSlider.value;
        if (GameSettings.Data.unitsVolumeValue != GameSettings.Data.unitsVolumeInitialValue)
        {
            SoundManager.Instance.unitsAudio.volume = GameSettings.Data.unitsVolumeValue;
            GameSettings.Data.unitsVolumeInitialValue = GameSettings.Data.unitsVolumeValue;
        }
    }

    public void BuildingsSpeechVolumeChanged()
    {
        GameSettings.Data.buildingsVolumeValue = buildingsSpeechVolumeSlider.value;
        if (GameSettings.Data.buildingsVolumeValue != GameSettings.Data.buildingsVolumeInitialValue)
        {
            SoundManager.Instance.buildingsAudio.volume = GameSettings.Data.buildingsVolumeValue;
            GameSettings.Data.buildingsVolumeInitialValue = GameSettings.Data.buildingsVolumeValue;
        }
    }

    public void UIVolumeChanged()
    {
        GameSettings.Data.uiVolumeValue = uiVolumeSlider.value;
        if (GameSettings.Data.uiVolumeValue != GameSettings.Data.uiVolumeInitialValue)
        {
            SoundManager.Instance.uiAudio.volume = GameSettings.Data.uiVolumeValue;
            GameSettings.Data.uiVolumeInitialValue = GameSettings.Data.uiVolumeValue;
        }
    }

    public void PlayProductionCheckChanged()
    {
        GameSettings.Data.playProductionMessage = playProductionMessages.isOn;
    }

    public static void SaveGameSettings()
    {
        GameSettings.Data.masterVolumeSetValue = GameSettings.Data.masterVolumeValue;
        GameSettings.Data.musicVolumeSetValue = GameSettings.Data.musicVolumeValue;
        GameSettings.Data.fxVolumeSetValue = GameSettings.Data.fxVolumeValue;
        GameSettings.Data.computerVolumeSetValue = GameSettings.Data.computerVolumeValue;
        GameSettings.Data.unitsVolumeSetValue = GameSettings.Data.unitsVolumeValue;
        GameSettings.Data.buildingsVolumeSetValue = GameSettings.Data.buildingsVolumeValue;
        GameSettings.Data.uiVolumeSetValue = GameSettings.Data.uiVolumeValue;
        GameSettings.Data.playProductionMessageSetValue = GameSettings.Data.playProductionMessage;
        SoundManager.Instance.musicAudio.volume = GameSettings.Data.musicVolumeValue;
        SoundManager.Instance.computerAudio.volume = GameSettings.Data.computerVolumeValue;
        SoundManager.Instance.effectAudio.volume = GameSettings.Data.fxVolumeValue;
        SoundManager.Instance.unitsAudio.volume = GameSettings.Data.unitsVolumeValue;
        SoundManager.Instance.buildingsAudio.volume = GameSettings.Data.buildingsVolumeValue;
        SoundManager.Instance.uiAudio.volume = GameSettings.Data.uiVolumeValue;
        BinaryFormatter binaryformatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/GameSettings.dat");
        binaryformatter.Serialize(file, GameSettings.Data);
        file.Close();
    }

    public static void LoadGameSettings()
    {
        if (File.Exists(Application.persistentDataPath + "/GameSettings.dat"))
        {
            BinaryFormatter binaryformatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameSettings.dat", FileMode.Open);
            GameSettings.Data = (GameSettings)binaryformatter.Deserialize(file);
            SoundManager.Instance.musicAudio.volume = GameSettings.Data.musicVolumeValue;
            SoundManager.Instance.computerAudio.volume = GameSettings.Data.computerVolumeValue;
            SoundManager.Instance.effectAudio.volume = GameSettings.Data.fxVolumeValue;
            SoundManager.Instance.unitsAudio.volume = GameSettings.Data.unitsVolumeValue;
            SoundManager.Instance.buildingsAudio.volume = GameSettings.Data.buildingsVolumeValue;
            SoundManager.Instance.uiAudio.volume = GameSettings.Data.uiVolumeValue;
            file.Close();
        }
    }
}
