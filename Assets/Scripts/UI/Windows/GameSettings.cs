﻿using System;
using UnityEngine;

[Serializable]
public class GameSettings
{
    public static GameSettings Data = new GameSettings();
    public float masterVolumeValue = 0.05F;
    public float masterVolumeInitialValue = 0.05F;
    public float masterVolumeSetValue = 0.05F;
    public float musicVolumeValue = 0.05F;
    public float musicVolumeInitialValue = 0.05F;
    public float musicVolumeSetValue = 0.05F;
    public float fxVolumeValue = 0.5F;
    public float fxVolumeInitialValue = 0.5F;
    public float fxVolumeSetValue = 0.5F;
    public float computerVolumeValue = 0.05F;
    public float computerVolumeInitialValue = 0.05F;
    public float computerVolumeSetValue = 0.05F;
    public float unitsVolumeValue = 0.05F;
    public float unitsVolumeInitialValue = 0.05F;
    public float unitsVolumeSetValue = 0.05F;
    public float buildingsVolumeValue = 0.05F;
    public float buildingsVolumeInitialValue = 0.05F;
    public float buildingsVolumeSetValue = 0.05F;
    public float uiVolumeValue = 0.05F;
    public float uiVolumeInitialValue = 0.05F;
    public float uiVolumeSetValue = 0.05F;
    public bool playProductionMessage = false;
    public bool playProductionMessageSetValue = false;
    public UnityNonSerializable.__Vector3 preferencesWindowPosition;
    public UnityNonSerializable.__Vector2 preferencesWindowSize;
}

public class UnityNonSerializable
{
    [Serializable]
    public struct __Vector3
    {
        public float x;
        public float y;
        public float z;

        public __Vector3(float _x, float _y, float _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }

        public override string ToString()
        {
            return String.Format("[{0}, {1}, {2}]", x, y, z);
        }

        public static implicit operator Vector3(__Vector3 _value)
        {
            return new Vector3(_value.x, _value.y, _value.z);
        }

         public static implicit operator __Vector3(Vector3 _value)
        {
            return new __Vector3(_value.x, _value.y, _value.z);
        }
    }

    [Serializable]
    public struct __Vector2
    {
        public float width;
        public float height;

        public __Vector2(float _width, float _height)
        {
            width = _width;
            height = _height;
        }

        public override string ToString()
        {
            return String.Format("[{0}, {1}]", width, height);
        }

        public static implicit operator Vector2(__Vector2 _value)
        {
            return new Vector2(_value.width, _value.height);
        }

        public static implicit operator __Vector2(Vector2 _value)
        {
            return new __Vector2(_value.x, _value.y);
        }
    }
}
