﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GenericInfoPopup : MonoBehaviour
{
    public Text InfoText;
    public GameObject modalBackground;

    public void Show(bool on, string text = "")
    {
        gameObject.SetActive(on);
        modalBackground.SetActive(true);
        InfoText.text = text;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        modalBackground.SetActive(false);
    }

    public void InvokeHide()
    {
        Invoke("Hide", 4);
    }
}
