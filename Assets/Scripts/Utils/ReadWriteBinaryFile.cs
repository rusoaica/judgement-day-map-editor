﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ReadWriteBinaryFile : MonoBehaviour
{
    public Texture2D ReadTextureFromPack(string _path, string _texture_name)
    {
        Texture2D _tex = new Texture2D(0, 0);
        _path = Application.dataPath + _path;
        if (string.IsNullOrEmpty(_path))
            return null;
#if UNITY_EDITOR
        _path = _path.Contains("Assets") ? _path.Replace("Assets", "") : _path;
#endif
        if (!File.Exists(_path))
            return null;
        FileStream _fs = new FileStream(_path, FileMode.Open, FileAccess.Read);
        BinaryReader __stream = new BinaryReader(_fs);
        BinaryReader _stream = __stream;
        try
        {
            if (_stream.ReadInt32() == 22088)
            {
                int _file_version = _stream.ReadInt32();
                int _number_of_files = _stream.ReadInt32();
                for (int i = 0; i < _number_of_files; i++)
                {
                    var _filename = _stream.ReadString();
                    long _file_size = _stream.ReadInt64();
                    byte[] _file = new byte[_file_size];
                    _file = _stream.ReadBytes(_file.Length);
                    if (_filename.ToLower() == _texture_name.ToLower()) //these are the bytes of the texture we want
                    {
                        _tex = GetGamedataFile.LoadTexture2DFromBinaryArray(_file, false);
                        break;
                    }
                }
            }
            else
                return null; //not the correct file
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex);
            return null;
        }
        finally
        {
            _stream.Close();
            _fs.Close();
            _fs.Dispose();
        }
        return _tex;
    }

    public List<TexturePackData> ReadBinaryPack(string _path)
    {
        List<TexturePackData> _list = new List<TexturePackData>();
        _path = Application.dataPath + _path;
        if (string.IsNullOrEmpty(_path))
            return null;
#if UNITY_EDITOR
        _path = _path.Contains("Assets") ? _path.Replace("Assets", "") : _path;
#endif
        if (_path.Contains("//"))
            _path = _path.Replace("//", "/");
        if (!File.Exists(_path))
            return null;
        FileStream _fs = new FileStream(_path, FileMode.Open, FileAccess.Read);
        BinaryReader __stream = new BinaryReader(_fs);
        BinaryReader _stream = __stream;
        try
        {
            if (_stream.ReadInt32() == 22088)
            {
                int _file_version = _stream.ReadInt32();
                int _number_of_files = _stream.ReadInt32();
                for (int i = 0; i < _number_of_files; i++)
                {
                    var _filename = _stream.ReadString();
                    long _file_size = _stream.ReadInt64();
                    byte[] _file = new byte[_file_size];
                    _file = _stream.ReadBytes(_file.Length);
                    TexturePackData _texture_file = new TexturePackData() { Data = _file, Filename = _filename };
                    _list.Add(_texture_file);
                }
            }
            else
                return null; //not the correct file
        }
        catch (System.Exception)
        {
            return null;
        }
        finally
        {
            _stream.Close();
            _fs.Close();
            _fs.Dispose();
        }
        return _list;
    }

    public bool GenerateBinaryPack(string _source_path, string _destination_path, string _output_filename)
    {
        if (string.IsNullOrEmpty(_source_path) || string.IsNullOrEmpty(_destination_path) || string.IsNullOrEmpty(_output_filename))
            return false;
#if UNITY_EDITOR
        _source_path = _source_path.Contains("/Assets") ? _source_path.Replace("/Assets", "") : _source_path;
        _destination_path = _destination_path.Contains("/Assets") ? _destination_path.Replace("/Assets", "") : _destination_path;
#endif
        if (_source_path == _destination_path) //if saved to same directory where source files are, will try to pack itself!
            return false;
        if (!Directory.Exists(_source_path))
            return false;
        if (!Directory.Exists(_destination_path))
            return false;
        if (File.Exists(_destination_path + _output_filename))
            return false;        
        FileStream _fs = new FileStream(_destination_path + _output_filename, FileMode.Create, FileAccess.Write); //save dat file in parent directory
        BinaryWriter __stream = new BinaryWriter(_fs);
        var _stream = __stream;
        try
        {
            _stream.Write(22088); //number to check if file is the correct file
            _stream.Write(1); //file version
            _stream.Write(Directory.GetFiles(_source_path, "*", SearchOption.TopDirectoryOnly).Length);//number of files to be packed
            foreach (string file in Directory.GetFiles(_source_path))
            {
                FileInfo _info = new FileInfo(file); //get file info for every texture file 
                byte[] _file = File.ReadAllBytes(file); //get bytes of the current texture
                _stream.Write(_info.Name); //write the filename itself
                _stream.Write(_file.LongLength); //write file length, use it for knowing how many bytes to read at deserialization
                _stream.Write(_file); //write the texture file's bytes
            }
        }
        catch (System.Exception)
        {
            return false;
        }
        finally
        {
            _stream.Close();
            _fs.Close();
            _fs.Dispose();
        }
        return true;
    }
}

public class TexturePackData
{
    public byte[] Data { get; set; }
    public string Filename { get; set; }
}
